<p align="center">
<img src="https://gitlab.com/public-mint-community/publicmint-web3.js/-/raw/master/assets/pm-web3.png" width=200 />
</p>

# PublicMint library for javascript

Public Mint client library built around web3.js customized for blockchain interaction.

This package creates a new namespace pm `Public Mint` inside web3 with custom contracts instances, functions and networks configurations.

### Documentation

- [PublicMint-web3](https://public-mint-community.gitlab.io/publicmint-web3.js)

## Dev instructions

### How to install

```bash
npm install @publicmint/publicmint-web3
```

### How to use

- [Quick start tutorial](https://public-mint-community.gitlab.io/publicmint-web3.js/tutorial-QuickStart.html)

### Build doc

```bash
npm run build:doc
```

### Build dist

```bash
npm run dist
```

### Contributing

This package is just an wrapper around web3.js but for any reason if want contribute feel free to do this:

- Create a issue before any changes to discuss about them
- Submit your code changes against the `develop` branch
