/**
 * Utils for conversions module.
 * @module Conversions
 * @memberof pm.utils
 */


/**
 * Abstraction for conversion of tokens from USD to token value
 * @private
 * @param {*} toWei web3 
 * @param {*} value 
 */
function toToken({
    toWei
}, value) {
    return toWei(value, 'Ether')
}

/**
 * Abstraction for conversion of tokens from token value to USD
 * @private
 * @param {*} fromWei web3 
 * @param {*} value 
 */
function fromToken({
    fromWei
}, value) {
    return fromWei(value, 'Ether')
}

/**
 * Conversions Module
 * Just a abstraction of web3-utils module .fromWei, .toWei
 * @param { Object } utils module from web3
 * @return { {toToken: Function , fromToken: Function } } Utils functions in object 
 */
function Conversions(utils) {
    return {
        /**
         * Abstraction for conversion of tokens from USD to token value
         * @memberof pm.utils
         * @param { String | Number | BN } value Value to convert
         * @return { String | BN } If a number, or string is given it returns a number string, otherwise a BN.js instance.
         */
        toToken: (value) => toToken(utils, value),
        /**
         * Abstraction for conversion of tokens from token value to USD
         * @memberof pm.utils
         * @param { String | Number | BN } value Value to convert
         * @return { String | BN } If a number, or string is given it returns a number string, otherwise a BN.js instance.
         */
        fromToken: (value) => fromToken(utils, value)
    }
}

export default Conversions;
