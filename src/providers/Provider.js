/**
 * Provider class
 * @class
 */
class Provider {
    /**
     * @constructor
     * @param { String } chainId Chain identifier 
     * @param { String } name Name of chain 
     * @param { String } http Endpoint for http api 
     * @param { String } ws Endpoint for ws api 
     * @param { String } explorer Base URL for explorer 
     * @param { Object } [ opts =  {explorerRoutes: { transactions: '/tx/', address: '/address/', blocks: '/blocks/' } } ] - Base URL for explorer 
     */
    constructor(chainId, name, http, ws, explorer, opts = {}) {
        this.chainId = chainId;
        this.name = name;
        this.http = http;
        this.ws = ws;
        this.explorer = explorer;


        this.opts = {
            explorerRoutes: {
                transactions: '/tx/',
                address: '/address/',
                blocks: '/blocks/'
            },
            ...opts
        }
    }

    /**
     * Parse route to transactions give as response with receipt of calls to blockchain
     * @private
     * @param txRoute Transaction route e.g '/tx/'
     */
    explorerRoutesForTransaction(txRoute) {
        // TODO: parse receipt and provide a link to explorer
        this.explorerRoutes.transactions = txRoute;
    }

}

export default Provider;
