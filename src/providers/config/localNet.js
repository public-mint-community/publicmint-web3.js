export default {
    chainId: 9999,
    name: 'localNet',
    http: 'http://localhost:8545/',
    ws: 'ws://localhost:8546/',
    explorer: 'http://localhost/'
}
