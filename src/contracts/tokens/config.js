/**
 * ERC20 contracts configuration file 
 * @namespace ContractsConfiguration 
 * @property {string}  name          - Token symbol (must be unique).
 * @property {string}  deployType    - Type of deploy: 
 *  - `genesis` load one address
 *  - `manual`  load config by chain id.
 * @property {string}  abi    - ABI name
 * @property {string}  abiCommitHash - Commit hash of abi
 * @property {Array.<Object.<chainId, address>>}  deployInfo - Info with address and chainId
 */
export default [{
    "name": "USD",
    "deployType": "genesis",
    "abi": "ERC20",
    "abiCommitHash": "eabda32f811f6004da259e9da3cff5fac04ddbb1",
    "deployInfo": [{
        "address": "0x0000000000000000000000000000000000002070"
    }]
}]
