/**
 * MultiSignature configuration file
 * @namespace ContractsConfiguration 
 * @property {string}  name          - Contract name (must be unique).
 * @property {string}  deployType    - Type of deploy: 
 *  - `genesis` load one address
 *  - `manual`  load config by chain id.
 * @property {string}  abi    - ABI name
 * @property {string}  abiCommitHash - Commit hash of abi
 * @property {Array.<Object.<chainId, address>>}  deployInfo - Info with address and chainId
 * @property {Array.<Object.<chainId, address>>}  adminsTo - Info with address and chainId
 * 
 * @property {string}  target - MultiSig target
 * @property {Array.string}  proxyMethods - MultiSig target methods to call
 */
export default [{
    "name": "USD",
    "abi": "GNOSIS",
    "abiCommitHash": "1046ead915e6bdb9e4f2db7ba99a20d901b4ca06",
    "deployType": "genesis",
    "deployInfo": [{
        "address": "0x0000000000000000000000000000000000001010"
    }],
    "adminsTo": [{
        "target": "token.USD",
        "proxyMethods": ["deposit", "depositFromUSDC", "permitWithdrawalUSDC", "forbidWithdrawalUSDC", "setUSDCmaxWithdrawAllowed", "setUSDCmaxPoolPercentage", "addUsdcChain", "removeUsdcChain"]
    }]
}, {
    "name": "GasManager",
    "abi": "GNOSIS",
    "abiCommitHash": "34aa21f2f25e99ccfba94b2a55707d28857b48bb",
    "deployType": "genesis",
    "deployInfo": [{
        "address": "0x0000000000000000000000000000000000001020"
    }],
    "adminsTo": [{
        "target": "operations.GasManager",
        "proxyMethods": ["addContractWhitelist", "rmContractWhitelisted", "addSenderWhitelist", "rmSenderWhitelisted", "setGasPrice", "addERC", "rmERC", "addFee", "enableFee", "disableFee", "updateFeeAmount", "updatePercentageFee", "addSignatureWithFee", "rmSignatureWithFee", "transferTokens"]
    }]
}]
