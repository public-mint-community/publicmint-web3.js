/**
 * PublicMint Wallet accounts.
 * - Get all balance(s) of wallet
 * - Transfer tokens
 * @namespace Accounts
 */

import pickBy from 'lodash/pickBy';
import range from 'lodash/range';
import values from 'lodash/values';
import zipObject from 'lodash/zipObject';
import {
	mnemonicToSeedSync,
	generateMnemonic,
	validateMnemonic,
	setDefaultWordlist,
	getDefaultWordlist,
	wordlists
} from 'bip39';
import {
	fromMasterSeed
} from 'hdkey';

/**
 * Get Addresses from wallet
 * @memberof Accounts
 */
const getAccounts = wallet => {
	const n = wallet.length;
	if (n === 0) return null;

	const indexAccounts = range(n).map(index => index.toString());

	return values(
		pickBy(wallet, (_value, key) => {
			return indexAccounts.includes(key);
		})
	).map(accounts => accounts.address);
};

/**
 * Get all balances in wallet
 * @memberof wallet.accounts
 * @return Promise
 * @throws {error} In case of fail connection or empty wallet
 * @memberof Accounts
 */
const getBalances = async (eth, addresses) => {
	return new Promise((resolve, reject) => {
		if (addresses !== null && addresses.length !== 0) {
			Promise.all(addresses.map(address => eth.getBalance(address)))
				.then(balances => {
					resolve(zipObject(addresses, balances), (address, balance) => {
						return {
							address,
							balance
						};
					});
				})
				.catch(errors => {
					reject(errors);
				});
		} else {
			reject(new Error('Wallet is empty'));
		}
	});
};

/**
 * Transfer tokens from accounts in wallet to other account
 * @private
 * @param {string} to Recipient address
 * @param {string} value Amount of tokens to transfer
 * @param {*} options Overwrite defaults options [TransactionObject](https://web3js.readthedocs.io/en/v1.2.4/web3-eth.html#signtransaction)
 * @memberof Accounts
 */
const pTransfer = (eth, to, value, options) => {
	// TODO: change default gas
	const transactionObject = {
		to,
		value,
		gas: '25000',
		...options
	};

	return eth.sendTransaction(transactionObject);
};

/**
 * Transfer tokens from accounts in wallet to other account
 * By default send from first account in wallet
 * @param {string} to Recipient address
 * @param {string} value Amount of tokens to transfer
 * @param {*} options Overwrite defaults options [TransactionObject](https://web3js.readthedocs.io/en/v1.2.4/web3-eth.html#signtransaction)
 * @memberof Accounts
 */
function transfer(to, value, options = {}) {
	const newOptions = {
		from: 0,
		...options
	};
	return pTransfer(this, to, value, newOptions);
}

/**
 * Transfer tokens from accounts in wallet to other account
 * @param {number | string} from wallet address (index of account or address)
 * @param {string} to Recipient address
 * @param {string} value Amount of tokens to transfer
 * @param {*} options Overwrite defaults options [TransactionObject](https://web3js.readthedocs.io/en/v1.2.4/web3-eth.html#signtransaction)
 * @memberof Accounts
 */
function transferFrom(from, to, value, options = {}) {
	const newOptions = {
		from,
		...options
	};
	return pTransfer(this, to, value, newOptions);
}

const accountHandler = {
	get: (eth, key) => {
		const {
			wallet
		} = eth.accounts;
		let result = wallet;
		switch (key) {
			case 'getAccounts':
				result = getAccounts(wallet);
				break;
			case 'getBalances':
				result = getBalances(eth, getAccounts(wallet));
				break;
			case 'getBalance':
				/**
				 * @function getBalance
				 * @memberof Accounts
				 * @param { String } - Account address
				 * @return { Promise } - The current balance for the given address in wei.
				 */
				result = address => {
					return eth.getBalance(address);
				};
				break;
			case 'transfer':
				result = (...args) => {
					return transfer.call(eth, ...args);
				};
				break;
			case 'transferFrom':
				result = (...args) => {
					return transferFrom.call(eth, ...args);
				};
				break;
			default:
				break;
		}
		return result;
	}
};

function init(eth) {
	return new Proxy(eth, accountHandler);
}



/**
 * Create a HD wallet instance and import private keys for web3 wallet
 */
class HDwallet {
	seed = null;

	web3Context;
	wallet;
	constructor(_web3In, _wallet) {
		this.web3Context = _web3In;
		this.wallet = _wallet;
	}

	/**
	 * Create new seed and import accounts to wallet
	 * Under the hood this function delegates to bip39 module see more in (https://github.com/bitcoinjs/bip39)
	 * @public
	 * @memberof HDwallet
	 * @param {number} [start=0] - Start index
	 * @param {number} [total=1] - Total number of addresses to load
	 * @param strength?: number
	 * @param rng?: (size: number) => Buffer
	 * @param wordlist?: string[]
	 * @returns {string} seed of words
	 */
	create() {
		if (!this.seed) {
			this.seed = this.generateSeed.apply(this,
				Array.prototype.slice.call(arguments, 2)
			);
		}
		const start = arguments[0] || 0;
		const total = arguments[1] || 1;
		this.importFromSeed(this.seed, start, total);
		return this
	}

	/**
	 * Load seed for hd wallet and save seed
	 * Import from seed to wallet using bip39 algorithm with ethereum compatible derived path
	 * @public
	 * @memberof HDwallet
	 * @param {string} seed  - Mnemonic seed
	 * @param {number} [start=0] - Start index
	 * @param {number} [total=1] - Total number of addresses to load
	 */
	load() {
		const seed = arguments[0];
		if (!seed) throw new Error("Seed is missing");
		if (!this.validateSeed(seed)) throw new Error("Invalid seed as argument")
		
		const start = arguments[1] || 0;
		const total = arguments[2] || 1;

		this.seed = seed;

		return this.importFromSeed(seed, start, total);;
	}

	/**
	 * Generate seed for import in HD wallet
	 * @public
	 * @memberof HDwallet
	 * @param strength?: number
	 * @param rng?: (size: number) => Buffer
	 * @param wordlist?: string[]
	 * @returns {string} seed of words
	 */
	generateSeed() {
		return generateMnemonic.apply(this, arguments);
	}

	/**
	 * Validate seed
	 * @public
	 * @memberof HDwallet
	 * @param {string} seed 
	 * @returns {boolean} true if is valid seed, otherwise false.
	 */
	validateSeed(seed) {
		return validateMnemonic(seed);
	}

	/**
	 * Returns the default word list
	 * @public
	 * @memberof HDwallet
	 * @returns {string} default word list
	 */
	getDefaultSeedWordlist() {
		return getDefaultWordlist()
	}

	/**
	 * Set default word list
	 * @public
	 * @memberof HDwallet
	 * @returns {string} default word list
	 */
	setDefaultSeedWordlist(wordListName) {
		return setDefaultWordlist(wordListName)
	}

	/**
	 * Return available words
	 * @public
	 * @memberof HDwallet
	 * @returns { string[] }
	 */
	getAvailableSeedWordList() {
		return Object.keys(wordlists);
	}

	/**
	 * importFromSeed - Imports directly for accounts and not save the seed
	 * Import from seed to wallet using bip39 algorithm with ethereum compatible derived path
	 * @public
	 * @memberof HDwallet
	 * @param {string} seed  - Mnemonic seed
	 * @param {number} [start=0] - Start index
	 * @param {number} [total=1] - Total number of addresses to load
	 */
	importFromSeed(seed, start = 0, total = 1) {
		if (!validateMnemonic(seed)) {
			throw new Error('Invalid seed');
		}
		let seedBuffer = mnemonicToSeedSync(seed);
		let hdWallet = fromMasterSeed(seedBuffer);
		let privateKeys = [];

		for (let i = start; i < start + total; i++) {
			const privateKeyBuffer = hdWallet.derive("m/44'/60'/0'/0/" + i)._privateKey;
			const pkHex = '0x' + Buffer.from(privateKeyBuffer).toString('hex');
			privateKeys.push(pkHex);
			this.web3Context.eth.accounts.wallet.add(pkHex);
		}
		return privateKeys;
	}
}


const acc = function (web3In) {
	const {
		wallet
	} = web3In.eth.accounts;
	const hdwallet = new HDwallet(web3In, wallet);
	wallet.accounts = init(web3In.eth);
	wallet.hdwallet = hdwallet
	wallet.importFromSeed = hdwallet.importFromSeed.bind(hdwallet);
	return wallet;
};

export default acc;
