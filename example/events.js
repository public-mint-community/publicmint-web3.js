const PublicMint = require('../dist/index.js').default;

const web3 = new PublicMint(2019);

const {
    USD
} = web3.pm.contracts.token;

function log(...args) {
    console.info('\n', args);
}

USD.events.allEvents({
    fromBlock: 0
}, (error, event) => {
    if (!error) {
        log("\n : Logs", event)
    }
})
