// es6 import PublicMint from '../dist/index.js';
const PublicMint = require('../dist/index.js').default;

console.log('Modules info: ', PublicMint.versions());

const web3 = new PublicMint(2019);

const {
  wallet
} = web3.pm;

const {
  USD
} = web3.pm.contracts.token;

// Add first account
// 0x4389Af2E0515dDFe3453B1bD748aDfD5e2598cFd
const walletAccountPrivateKey1 =
  '0xba6cdfcc795484a9774eb98e756983da822ef3481b37d4ba649864e2d1ab4e5e';

wallet.add(walletAccountPrivateKey1);

const accounts = wallet.accounts.getAccounts;

const from = accounts[0];

function log(...args) {
  console.info('\n', args);
}

(async function init() {
  // WALLET INTERACTION WORK WITH NATIVE TOKEN DEFAULT USD ERC20
  log('\n Accounts', accounts);

  const balanceAccount1 = await wallet.accounts.getBalance(from);
  log('\n wallet.accounts.getBalance : ', balanceAccount1);

  // TRANSFER TOKENS
  const recipientAccount = '0xCa1BA9A1f19A3635af8A561bD75840Cef2Dc3dfC';
  const value = '2000000000000000000';

  const receiptTransfer = await wallet.accounts.transfer(recipientAccount, value);
  log('\n Receipt of transfer: ', receiptTransfer);

  // ERC INTERACTION, more slow and heavy prefer using  `wallet.accounts.getBalance`
  const myBalance = await USD.balanceOf('0x4389Af2E0515dDFe3453B1bD748aDfD5e2598cFd').call({
    from
  });
  log('\n USD.balanceOf: ', myBalance);

  process.exit(0);
})();
