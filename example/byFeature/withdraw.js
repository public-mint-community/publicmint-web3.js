// Load web3 instance with wallet created
const web3 = require ('./wallet');

const {wallet} = web3.pm;

const {USD} = web3.pm.contracts.token;

const {toToken} = web3.pm.utils;

function log (...args) {
  console.info ('\n', args);
}

const accounts = wallet.accounts.getAccounts;
log ('\n Accounts', accounts);

const from = accounts[0];

(async function () {
  // GET ALL NATIVE BALANCES
  const balances = await wallet.accounts.getBalances;
  log ('\n All balances from wallet: ', balances);

  // *** WARNING - LOST OF TOKENS *** DON'T GENERATE REF FROM API BY YOUR SELF, GET CORRECT REFERENCE FROM PUBLIC MINT API
  const refFromApi = '98cb5a14-ae4a-4456-8428-70a4c6ee68fe'

  // Withdrawals available:
  // withdrawWireUS
  // withdrawWireInt
  // withdrawAchUS
  // withdrawToUSDC

  // See all cost associated by each method:
  const estimateCosts = await USD.withdrawWireUS (
      toToken ('1'),
      refFromApi
  ).estimateCosts ({
      from
  });
  
  log('EstimateCosts output:', estimateCosts)
 
  /**
     Return Object 
    {
    Optional<message>:(Return on failure) UTF8 decoded message
    Optional<data>: (Return on failure) hex encoded string or null if not exist
    maybeReverts: boolean true if transaction may fail otherwise is false
    Optional<gas>: estimateGas for current method or null if fails
    gasPrice: current blockchain gasPrice
    Optional<minerFee>: (estimateGas * gasPrice <miner profit> ) or null if fails
    gasManagerFee: additionalFee - minerFee 
    additionalFee: additional cost is the result from a contract query - amount value
    totalCost: AdditionalFee is the total amount
    }
  */

  // output in case of revert
  /*
    EstimateCosts output: {
      gas: null,
      gasPrice: '2000000000000',
      additionalFee: '50000000000000000000',
      totalCost: '50000000000000000000',
      data: null,
      maybeReverts: true,
      message: 'Returned error: Upfront cost exceeds account balance',
      minerFee: null
    }
  */

  /*  output in case of transaction succeeds
    {
      gas: '61410',
      gasPrice: '2000000000000',
      additionalFee: '50000000000000000000',
      totalCost: '50000000000000000000',
      gasManagerFee: '49877180000000000000',
      maybeReverts: false,
      minerFee: '122820000000000000'
    }
  */


  // if the balance is 100 USD and totalCost of withdrawWireUS is 50 USD you only can withdrawal the difference
  const receipt = await USD.withdrawWireUS (toToken ('1'), refFromApi).send ({
    from,
  });

  log ('\n Receipt', receipt);

  const balances2 = await wallet.accounts.getBalances;
  log ('\n All balances from wallet: ', balances2);

  process.exit (0);
}) ();
