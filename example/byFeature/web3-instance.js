const PublicMint = require('../../dist/index.js').default;

console.log('Modules info: ', PublicMint.versions());

// default provider is 'ws'
module.exports = new PublicMint(2018, "http");
// module.exports = new PublicMint(2019, "http");
// Provider 2019 is testNet
// Provider 2020 is MainNet
// const customProvider = PublicMint.provider (
//     1941,
//     'CustomNetwork',
//     'http://127.0.0.1:8545/',
//     'ws://127.0.0.1:8546/'
//   );
  
// const publicMint = new PublicMint(customProvider, 'http');
  
// module.exports = publicMint;


