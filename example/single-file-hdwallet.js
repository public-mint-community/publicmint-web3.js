// es6 import PublicMint from '../dist/index.js';
const PublicMint = require('../dist/index.js').default;

console.log('Modules info: ', PublicMint.versions());

const web3 = new PublicMint(2019);
const MNEMONIC = "identify crime vicious banana exist horror number mean fix calm dolphin bomb"
const {
  wallet
} = web3.pm;

const {
  USD
} = web3.pm.contracts.token;

// Load and import and save the seed
wallet.hdwallet.load(MNEMONIC, 0, 10);

// Load and import pk to wallet without saving the see
// wallet.importFromSeed(MNEMONIC, 0, 10);

// create a seed and import first account
// wallet.hdwallet.create();

// create a seed and import 5 accounts starting from index 0
// wallet.hdwallet.create(0, 5);

const accounts = wallet.accounts.getAccounts;

const from = accounts[0];

function log(...args) {
  console.info('\n', args);
}
