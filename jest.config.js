module.exports = {
    name: "publicmint-web3",
    verbose: true,
    testTimeout: 50000,
    transform: {
        "\\.[jt]sx?$": "babel-jest"
    },
}
