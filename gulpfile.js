// var gulpDocumentation = require('gulp-documentation');
var jeditor = require('gulp-json-editor');
const { task, src, dest } = require('gulp');

task('docs', function() {
  var package = require('./package.json');

  return src('./docs/versions.json')
    .pipe(
      jeditor(function(_versions) {
        const versions = _versions || [];
        if (versions.some(({ version }) => version === package.version)) {
          return versions;
        } else {
          return versions.concat([
            {
              version: package.version
            }
          ]);
        }
      })
    )
    .pipe(dest('./docs'));
});
