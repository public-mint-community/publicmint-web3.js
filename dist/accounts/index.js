"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _pickBy = _interopRequireDefault(require("lodash/pickBy"));

var _range = _interopRequireDefault(require("lodash/range"));

var _values = _interopRequireDefault(require("lodash/values"));

var _zipObject = _interopRequireDefault(require("lodash/zipObject"));

var _bip = require("bip39");

var _hdkey = require("hdkey");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

/**
 * Get Addresses from wallet
 * @memberof Accounts
 */
var getAccounts = wallet => {
  var n = wallet.length;
  if (n === 0) return null;
  var indexAccounts = (0, _range.default)(n).map(index => index.toString());
  return (0, _values.default)((0, _pickBy.default)(wallet, (_value, key) => {
    return indexAccounts.includes(key);
  })).map(accounts => accounts.address);
};
/**
 * Get all balances in wallet
 * @memberof wallet.accounts
 * @return Promise
 * @throws {error} In case of fail connection or empty wallet
 * @memberof Accounts
 */


var getBalances = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator(function* (eth, addresses) {
    return new Promise((resolve, reject) => {
      if (addresses !== null && addresses.length !== 0) {
        Promise.all(addresses.map(address => eth.getBalance(address))).then(balances => {
          resolve((0, _zipObject.default)(addresses, balances), (address, balance) => {
            return {
              address,
              balance
            };
          });
        }).catch(errors => {
          reject(errors);
        });
      } else {
        reject(new Error('Wallet is empty'));
      }
    });
  });

  return function getBalances(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();
/**
 * Transfer tokens from accounts in wallet to other account
 * @private
 * @param {string} to Recipient address
 * @param {string} value Amount of tokens to transfer
 * @param {*} options Overwrite defaults options [TransactionObject](https://web3js.readthedocs.io/en/v1.2.4/web3-eth.html#signtransaction)
 * @memberof Accounts
 */


var pTransfer = (eth, to, value, options) => {
  // TODO: change default gas
  var transactionObject = _objectSpread({
    to,
    value,
    gas: '25000'
  }, options);

  return eth.sendTransaction(transactionObject);
};
/**
 * Transfer tokens from accounts in wallet to other account
 * By default send from first account in wallet
 * @param {string} to Recipient address
 * @param {string} value Amount of tokens to transfer
 * @param {*} options Overwrite defaults options [TransactionObject](https://web3js.readthedocs.io/en/v1.2.4/web3-eth.html#signtransaction)
 * @memberof Accounts
 */


function transfer(to, value) {
  var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

  var newOptions = _objectSpread({
    from: 0
  }, options);

  return pTransfer(this, to, value, newOptions);
}
/**
 * Transfer tokens from accounts in wallet to other account
 * @param {number | string} from wallet address (index of account or address)
 * @param {string} to Recipient address
 * @param {string} value Amount of tokens to transfer
 * @param {*} options Overwrite defaults options [TransactionObject](https://web3js.readthedocs.io/en/v1.2.4/web3-eth.html#signtransaction)
 * @memberof Accounts
 */


function transferFrom(from, to, value) {
  var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};

  var newOptions = _objectSpread({
    from
  }, options);

  return pTransfer(this, to, value, newOptions);
}

var accountHandler = {
  get: (eth, key) => {
    var {
      wallet
    } = eth.accounts;
    var result = wallet;

    switch (key) {
      case 'getAccounts':
        result = getAccounts(wallet);
        break;

      case 'getBalances':
        result = getBalances(eth, getAccounts(wallet));
        break;

      case 'getBalance':
        /**
         * @function getBalance
         * @memberof Accounts
         * @param { String } - Account address
         * @return { Promise } - The current balance for the given address in wei.
         */
        result = address => {
          return eth.getBalance(address);
        };

        break;

      case 'transfer':
        result = function result() {
          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          return transfer.call(eth, ...args);
        };

        break;

      case 'transferFrom':
        result = function result() {
          for (var _len2 = arguments.length, args = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
            args[_key2] = arguments[_key2];
          }

          return transferFrom.call(eth, ...args);
        };

        break;

      default:
        break;
    }

    return result;
  }
};

function init(eth) {
  return new Proxy(eth, accountHandler);
}
/**
 * Create a HD wallet instance and import private keys for web3 wallet
 */


class HDwallet {
  constructor(_web3In, _wallet) {
    _defineProperty(this, "seed", null);

    _defineProperty(this, "web3Context", void 0);

    _defineProperty(this, "wallet", void 0);

    this.web3Context = _web3In;
    this.wallet = _wallet;
  }
  /**
   * Create new seed and import accounts to wallet
   * Under the hood this function delegates to bip39 module see more in (https://github.com/bitcoinjs/bip39)
   * @public
   * @memberof HDwallet
   * @param {number} [start=0] - Start index
   * @param {number} [total=1] - Total number of addresses to load
   * @param strength?: number
   * @param rng?: (size: number) => Buffer
   * @param wordlist?: string[]
   * @returns {string} seed of words
   */


  create() {
    if (!this.seed) {
      this.seed = this.generateSeed.apply(this, Array.prototype.slice.call(arguments, 2));
    }

    var start = arguments[0] || 0;
    var total = arguments[1] || 1;
    this.importFromSeed(this.seed, start, total);
    return this;
  }
  /**
   * Load seed for hd wallet and save seed
   * Import from seed to wallet using bip39 algorithm with ethereum compatible derived path
   * @public
   * @memberof HDwallet
   * @param {string} seed  - Mnemonic seed
   * @param {number} [start=0] - Start index
   * @param {number} [total=1] - Total number of addresses to load
   */


  load() {
    var seed = arguments[0];
    if (!seed) throw new Error("Seed is missing");
    if (!this.validateSeed(seed)) throw new Error("Invalid seed as argument");
    var start = arguments[1] || 0;
    var total = arguments[2] || 1;
    this.seed = seed;
    return this.importFromSeed(seed, start, total);
    ;
  }
  /**
   * Generate seed for import in HD wallet
   * @public
   * @memberof HDwallet
   * @param strength?: number
   * @param rng?: (size: number) => Buffer
   * @param wordlist?: string[]
   * @returns {string} seed of words
   */


  generateSeed() {
    return _bip.generateMnemonic.apply(this, arguments);
  }
  /**
   * Validate seed
   * @public
   * @memberof HDwallet
   * @param {string} seed 
   * @returns {boolean} true if is valid seed, otherwise false.
   */


  validateSeed(seed) {
    return (0, _bip.validateMnemonic)(seed);
  }
  /**
   * Returns the default word list
   * @public
   * @memberof HDwallet
   * @returns {string} default word list
   */


  getDefaultSeedWordlist() {
    return (0, _bip.getDefaultWordlist)();
  }
  /**
   * Set default word list
   * @public
   * @memberof HDwallet
   * @returns {string} default word list
   */


  setDefaultSeedWordlist(wordListName) {
    return (0, _bip.setDefaultWordlist)(wordListName);
  }
  /**
   * Return available words
   * @public
   * @memberof HDwallet
   * @returns { string[] }
   */


  getAvailableSeedWordList() {
    return Object.keys(_bip.wordlists);
  }
  /**
   * importFromSeed - Imports directly for accounts and not save the seed
   * Import from seed to wallet using bip39 algorithm with ethereum compatible derived path
   * @public
   * @memberof HDwallet
   * @param {string} seed  - Mnemonic seed
   * @param {number} [start=0] - Start index
   * @param {number} [total=1] - Total number of addresses to load
   */


  importFromSeed(seed) {
    var start = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
    var total = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 1;

    if (!(0, _bip.validateMnemonic)(seed)) {
      throw new Error('Invalid seed');
    }

    var seedBuffer = (0, _bip.mnemonicToSeedSync)(seed);
    var hdWallet = (0, _hdkey.fromMasterSeed)(seedBuffer);
    var privateKeys = [];

    for (var i = start; i < start + total; i++) {
      var privateKeyBuffer = hdWallet.derive("m/44'/60'/0'/0/" + i)._privateKey;

      var pkHex = '0x' + Buffer.from(privateKeyBuffer).toString('hex');
      privateKeys.push(pkHex);
      this.web3Context.eth.accounts.wallet.add(pkHex);
    }

    return privateKeys;
  }

}

var acc = function acc(web3In) {
  var {
    wallet
  } = web3In.eth.accounts;
  var hdwallet = new HDwallet(web3In, wallet);
  wallet.accounts = init(web3In.eth);
  wallet.hdwallet = hdwallet;
  wallet.importFromSeed = hdwallet.importFromSeed.bind(hdwallet);
  return wallet;
};

var _default = acc;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJnZXRBY2NvdW50cyIsIndhbGxldCIsIm4iLCJsZW5ndGgiLCJpbmRleEFjY291bnRzIiwicmFuZ2UiLCJtYXAiLCJpbmRleCIsInRvU3RyaW5nIiwidmFsdWVzIiwicGlja0J5IiwiX3ZhbHVlIiwia2V5IiwiaW5jbHVkZXMiLCJhY2NvdW50cyIsImFkZHJlc3MiLCJnZXRCYWxhbmNlcyIsImV0aCIsImFkZHJlc3NlcyIsIlByb21pc2UiLCJyZXNvbHZlIiwicmVqZWN0IiwiYWxsIiwiZ2V0QmFsYW5jZSIsInRoZW4iLCJiYWxhbmNlcyIsInppcE9iamVjdCIsImJhbGFuY2UiLCJjYXRjaCIsImVycm9ycyIsIkVycm9yIiwicFRyYW5zZmVyIiwidG8iLCJ2YWx1ZSIsIm9wdGlvbnMiLCJ0cmFuc2FjdGlvbk9iamVjdCIsImdhcyIsInNlbmRUcmFuc2FjdGlvbiIsInRyYW5zZmVyIiwibmV3T3B0aW9ucyIsImZyb20iLCJ0cmFuc2ZlckZyb20iLCJhY2NvdW50SGFuZGxlciIsImdldCIsInJlc3VsdCIsImFyZ3MiLCJjYWxsIiwiaW5pdCIsIlByb3h5IiwiSER3YWxsZXQiLCJjb25zdHJ1Y3RvciIsIl93ZWIzSW4iLCJfd2FsbGV0Iiwid2ViM0NvbnRleHQiLCJjcmVhdGUiLCJzZWVkIiwiZ2VuZXJhdGVTZWVkIiwiYXBwbHkiLCJBcnJheSIsInByb3RvdHlwZSIsInNsaWNlIiwiYXJndW1lbnRzIiwic3RhcnQiLCJ0b3RhbCIsImltcG9ydEZyb21TZWVkIiwibG9hZCIsInZhbGlkYXRlU2VlZCIsImdlbmVyYXRlTW5lbW9uaWMiLCJ2YWxpZGF0ZU1uZW1vbmljIiwiZ2V0RGVmYXVsdFNlZWRXb3JkbGlzdCIsImdldERlZmF1bHRXb3JkbGlzdCIsInNldERlZmF1bHRTZWVkV29yZGxpc3QiLCJ3b3JkTGlzdE5hbWUiLCJzZXREZWZhdWx0V29yZGxpc3QiLCJnZXRBdmFpbGFibGVTZWVkV29yZExpc3QiLCJPYmplY3QiLCJrZXlzIiwid29yZGxpc3RzIiwic2VlZEJ1ZmZlciIsIm1uZW1vbmljVG9TZWVkU3luYyIsImhkV2FsbGV0IiwiZnJvbU1hc3RlclNlZWQiLCJwcml2YXRlS2V5cyIsImkiLCJwcml2YXRlS2V5QnVmZmVyIiwiZGVyaXZlIiwiX3ByaXZhdGVLZXkiLCJwa0hleCIsIkJ1ZmZlciIsInB1c2giLCJhZGQiLCJhY2MiLCJ3ZWIzSW4iLCJoZHdhbGxldCIsImJpbmQiXSwic291cmNlcyI6WyIuLi8uLi9zcmMvYWNjb3VudHMvaW5kZXguanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBQdWJsaWNNaW50IFdhbGxldCBhY2NvdW50cy5cbiAqIC0gR2V0IGFsbCBiYWxhbmNlKHMpIG9mIHdhbGxldFxuICogLSBUcmFuc2ZlciB0b2tlbnNcbiAqIEBuYW1lc3BhY2UgQWNjb3VudHNcbiAqL1xuXG5pbXBvcnQgcGlja0J5IGZyb20gJ2xvZGFzaC9waWNrQnknO1xuaW1wb3J0IHJhbmdlIGZyb20gJ2xvZGFzaC9yYW5nZSc7XG5pbXBvcnQgdmFsdWVzIGZyb20gJ2xvZGFzaC92YWx1ZXMnO1xuaW1wb3J0IHppcE9iamVjdCBmcm9tICdsb2Rhc2gvemlwT2JqZWN0JztcbmltcG9ydCB7XG5cdG1uZW1vbmljVG9TZWVkU3luYyxcblx0Z2VuZXJhdGVNbmVtb25pYyxcblx0dmFsaWRhdGVNbmVtb25pYyxcblx0c2V0RGVmYXVsdFdvcmRsaXN0LFxuXHRnZXREZWZhdWx0V29yZGxpc3QsXG5cdHdvcmRsaXN0c1xufSBmcm9tICdiaXAzOSc7XG5pbXBvcnQge1xuXHRmcm9tTWFzdGVyU2VlZFxufSBmcm9tICdoZGtleSc7XG5cbi8qKlxuICogR2V0IEFkZHJlc3NlcyBmcm9tIHdhbGxldFxuICogQG1lbWJlcm9mIEFjY291bnRzXG4gKi9cbmNvbnN0IGdldEFjY291bnRzID0gd2FsbGV0ID0+IHtcblx0Y29uc3QgbiA9IHdhbGxldC5sZW5ndGg7XG5cdGlmIChuID09PSAwKSByZXR1cm4gbnVsbDtcblxuXHRjb25zdCBpbmRleEFjY291bnRzID0gcmFuZ2UobikubWFwKGluZGV4ID0+IGluZGV4LnRvU3RyaW5nKCkpO1xuXG5cdHJldHVybiB2YWx1ZXMoXG5cdFx0cGlja0J5KHdhbGxldCwgKF92YWx1ZSwga2V5KSA9PiB7XG5cdFx0XHRyZXR1cm4gaW5kZXhBY2NvdW50cy5pbmNsdWRlcyhrZXkpO1xuXHRcdH0pXG5cdCkubWFwKGFjY291bnRzID0+IGFjY291bnRzLmFkZHJlc3MpO1xufTtcblxuLyoqXG4gKiBHZXQgYWxsIGJhbGFuY2VzIGluIHdhbGxldFxuICogQG1lbWJlcm9mIHdhbGxldC5hY2NvdW50c1xuICogQHJldHVybiBQcm9taXNlXG4gKiBAdGhyb3dzIHtlcnJvcn0gSW4gY2FzZSBvZiBmYWlsIGNvbm5lY3Rpb24gb3IgZW1wdHkgd2FsbGV0XG4gKiBAbWVtYmVyb2YgQWNjb3VudHNcbiAqL1xuY29uc3QgZ2V0QmFsYW5jZXMgPSBhc3luYyAoZXRoLCBhZGRyZXNzZXMpID0+IHtcblx0cmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcblx0XHRpZiAoYWRkcmVzc2VzICE9PSBudWxsICYmIGFkZHJlc3Nlcy5sZW5ndGggIT09IDApIHtcblx0XHRcdFByb21pc2UuYWxsKGFkZHJlc3Nlcy5tYXAoYWRkcmVzcyA9PiBldGguZ2V0QmFsYW5jZShhZGRyZXNzKSkpXG5cdFx0XHRcdC50aGVuKGJhbGFuY2VzID0+IHtcblx0XHRcdFx0XHRyZXNvbHZlKHppcE9iamVjdChhZGRyZXNzZXMsIGJhbGFuY2VzKSwgKGFkZHJlc3MsIGJhbGFuY2UpID0+IHtcblx0XHRcdFx0XHRcdHJldHVybiB7XG5cdFx0XHRcdFx0XHRcdGFkZHJlc3MsXG5cdFx0XHRcdFx0XHRcdGJhbGFuY2Vcblx0XHRcdFx0XHRcdH07XG5cdFx0XHRcdFx0fSk7XG5cdFx0XHRcdH0pXG5cdFx0XHRcdC5jYXRjaChlcnJvcnMgPT4ge1xuXHRcdFx0XHRcdHJlamVjdChlcnJvcnMpO1xuXHRcdFx0XHR9KTtcblx0XHR9IGVsc2Uge1xuXHRcdFx0cmVqZWN0KG5ldyBFcnJvcignV2FsbGV0IGlzIGVtcHR5JykpO1xuXHRcdH1cblx0fSk7XG59O1xuXG4vKipcbiAqIFRyYW5zZmVyIHRva2VucyBmcm9tIGFjY291bnRzIGluIHdhbGxldCB0byBvdGhlciBhY2NvdW50XG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHtzdHJpbmd9IHRvIFJlY2lwaWVudCBhZGRyZXNzXG4gKiBAcGFyYW0ge3N0cmluZ30gdmFsdWUgQW1vdW50IG9mIHRva2VucyB0byB0cmFuc2ZlclxuICogQHBhcmFtIHsqfSBvcHRpb25zIE92ZXJ3cml0ZSBkZWZhdWx0cyBvcHRpb25zIFtUcmFuc2FjdGlvbk9iamVjdF0oaHR0cHM6Ly93ZWIzanMucmVhZHRoZWRvY3MuaW8vZW4vdjEuMi40L3dlYjMtZXRoLmh0bWwjc2lnbnRyYW5zYWN0aW9uKVxuICogQG1lbWJlcm9mIEFjY291bnRzXG4gKi9cbmNvbnN0IHBUcmFuc2ZlciA9IChldGgsIHRvLCB2YWx1ZSwgb3B0aW9ucykgPT4ge1xuXHQvLyBUT0RPOiBjaGFuZ2UgZGVmYXVsdCBnYXNcblx0Y29uc3QgdHJhbnNhY3Rpb25PYmplY3QgPSB7XG5cdFx0dG8sXG5cdFx0dmFsdWUsXG5cdFx0Z2FzOiAnMjUwMDAnLFxuXHRcdC4uLm9wdGlvbnNcblx0fTtcblxuXHRyZXR1cm4gZXRoLnNlbmRUcmFuc2FjdGlvbih0cmFuc2FjdGlvbk9iamVjdCk7XG59O1xuXG4vKipcbiAqIFRyYW5zZmVyIHRva2VucyBmcm9tIGFjY291bnRzIGluIHdhbGxldCB0byBvdGhlciBhY2NvdW50XG4gKiBCeSBkZWZhdWx0IHNlbmQgZnJvbSBmaXJzdCBhY2NvdW50IGluIHdhbGxldFxuICogQHBhcmFtIHtzdHJpbmd9IHRvIFJlY2lwaWVudCBhZGRyZXNzXG4gKiBAcGFyYW0ge3N0cmluZ30gdmFsdWUgQW1vdW50IG9mIHRva2VucyB0byB0cmFuc2ZlclxuICogQHBhcmFtIHsqfSBvcHRpb25zIE92ZXJ3cml0ZSBkZWZhdWx0cyBvcHRpb25zIFtUcmFuc2FjdGlvbk9iamVjdF0oaHR0cHM6Ly93ZWIzanMucmVhZHRoZWRvY3MuaW8vZW4vdjEuMi40L3dlYjMtZXRoLmh0bWwjc2lnbnRyYW5zYWN0aW9uKVxuICogQG1lbWJlcm9mIEFjY291bnRzXG4gKi9cbmZ1bmN0aW9uIHRyYW5zZmVyKHRvLCB2YWx1ZSwgb3B0aW9ucyA9IHt9KSB7XG5cdGNvbnN0IG5ld09wdGlvbnMgPSB7XG5cdFx0ZnJvbTogMCxcblx0XHQuLi5vcHRpb25zXG5cdH07XG5cdHJldHVybiBwVHJhbnNmZXIodGhpcywgdG8sIHZhbHVlLCBuZXdPcHRpb25zKTtcbn1cblxuLyoqXG4gKiBUcmFuc2ZlciB0b2tlbnMgZnJvbSBhY2NvdW50cyBpbiB3YWxsZXQgdG8gb3RoZXIgYWNjb3VudFxuICogQHBhcmFtIHtudW1iZXIgfCBzdHJpbmd9IGZyb20gd2FsbGV0IGFkZHJlc3MgKGluZGV4IG9mIGFjY291bnQgb3IgYWRkcmVzcylcbiAqIEBwYXJhbSB7c3RyaW5nfSB0byBSZWNpcGllbnQgYWRkcmVzc1xuICogQHBhcmFtIHtzdHJpbmd9IHZhbHVlIEFtb3VudCBvZiB0b2tlbnMgdG8gdHJhbnNmZXJcbiAqIEBwYXJhbSB7Kn0gb3B0aW9ucyBPdmVyd3JpdGUgZGVmYXVsdHMgb3B0aW9ucyBbVHJhbnNhY3Rpb25PYmplY3RdKGh0dHBzOi8vd2ViM2pzLnJlYWR0aGVkb2NzLmlvL2VuL3YxLjIuNC93ZWIzLWV0aC5odG1sI3NpZ250cmFuc2FjdGlvbilcbiAqIEBtZW1iZXJvZiBBY2NvdW50c1xuICovXG5mdW5jdGlvbiB0cmFuc2ZlckZyb20oZnJvbSwgdG8sIHZhbHVlLCBvcHRpb25zID0ge30pIHtcblx0Y29uc3QgbmV3T3B0aW9ucyA9IHtcblx0XHRmcm9tLFxuXHRcdC4uLm9wdGlvbnNcblx0fTtcblx0cmV0dXJuIHBUcmFuc2Zlcih0aGlzLCB0bywgdmFsdWUsIG5ld09wdGlvbnMpO1xufVxuXG5jb25zdCBhY2NvdW50SGFuZGxlciA9IHtcblx0Z2V0OiAoZXRoLCBrZXkpID0+IHtcblx0XHRjb25zdCB7XG5cdFx0XHR3YWxsZXRcblx0XHR9ID0gZXRoLmFjY291bnRzO1xuXHRcdGxldCByZXN1bHQgPSB3YWxsZXQ7XG5cdFx0c3dpdGNoIChrZXkpIHtcblx0XHRcdGNhc2UgJ2dldEFjY291bnRzJzpcblx0XHRcdFx0cmVzdWx0ID0gZ2V0QWNjb3VudHMod2FsbGV0KTtcblx0XHRcdFx0YnJlYWs7XG5cdFx0XHRjYXNlICdnZXRCYWxhbmNlcyc6XG5cdFx0XHRcdHJlc3VsdCA9IGdldEJhbGFuY2VzKGV0aCwgZ2V0QWNjb3VudHMod2FsbGV0KSk7XG5cdFx0XHRcdGJyZWFrO1xuXHRcdFx0Y2FzZSAnZ2V0QmFsYW5jZSc6XG5cdFx0XHRcdC8qKlxuXHRcdFx0XHQgKiBAZnVuY3Rpb24gZ2V0QmFsYW5jZVxuXHRcdFx0XHQgKiBAbWVtYmVyb2YgQWNjb3VudHNcblx0XHRcdFx0ICogQHBhcmFtIHsgU3RyaW5nIH0gLSBBY2NvdW50IGFkZHJlc3Ncblx0XHRcdFx0ICogQHJldHVybiB7IFByb21pc2UgfSAtIFRoZSBjdXJyZW50IGJhbGFuY2UgZm9yIHRoZSBnaXZlbiBhZGRyZXNzIGluIHdlaS5cblx0XHRcdFx0ICovXG5cdFx0XHRcdHJlc3VsdCA9IGFkZHJlc3MgPT4ge1xuXHRcdFx0XHRcdHJldHVybiBldGguZ2V0QmFsYW5jZShhZGRyZXNzKTtcblx0XHRcdFx0fTtcblx0XHRcdFx0YnJlYWs7XG5cdFx0XHRjYXNlICd0cmFuc2Zlcic6XG5cdFx0XHRcdHJlc3VsdCA9ICguLi5hcmdzKSA9PiB7XG5cdFx0XHRcdFx0cmV0dXJuIHRyYW5zZmVyLmNhbGwoZXRoLCAuLi5hcmdzKTtcblx0XHRcdFx0fTtcblx0XHRcdFx0YnJlYWs7XG5cdFx0XHRjYXNlICd0cmFuc2ZlckZyb20nOlxuXHRcdFx0XHRyZXN1bHQgPSAoLi4uYXJncykgPT4ge1xuXHRcdFx0XHRcdHJldHVybiB0cmFuc2ZlckZyb20uY2FsbChldGgsIC4uLmFyZ3MpO1xuXHRcdFx0XHR9O1xuXHRcdFx0XHRicmVhaztcblx0XHRcdGRlZmF1bHQ6XG5cdFx0XHRcdGJyZWFrO1xuXHRcdH1cblx0XHRyZXR1cm4gcmVzdWx0O1xuXHR9XG59O1xuXG5mdW5jdGlvbiBpbml0KGV0aCkge1xuXHRyZXR1cm4gbmV3IFByb3h5KGV0aCwgYWNjb3VudEhhbmRsZXIpO1xufVxuXG5cblxuLyoqXG4gKiBDcmVhdGUgYSBIRCB3YWxsZXQgaW5zdGFuY2UgYW5kIGltcG9ydCBwcml2YXRlIGtleXMgZm9yIHdlYjMgd2FsbGV0XG4gKi9cbmNsYXNzIEhEd2FsbGV0IHtcblx0c2VlZCA9IG51bGw7XG5cblx0d2ViM0NvbnRleHQ7XG5cdHdhbGxldDtcblx0Y29uc3RydWN0b3IoX3dlYjNJbiwgX3dhbGxldCkge1xuXHRcdHRoaXMud2ViM0NvbnRleHQgPSBfd2ViM0luO1xuXHRcdHRoaXMud2FsbGV0ID0gX3dhbGxldDtcblx0fVxuXG5cdC8qKlxuXHQgKiBDcmVhdGUgbmV3IHNlZWQgYW5kIGltcG9ydCBhY2NvdW50cyB0byB3YWxsZXRcblx0ICogVW5kZXIgdGhlIGhvb2QgdGhpcyBmdW5jdGlvbiBkZWxlZ2F0ZXMgdG8gYmlwMzkgbW9kdWxlIHNlZSBtb3JlIGluIChodHRwczovL2dpdGh1Yi5jb20vYml0Y29pbmpzL2JpcDM5KVxuXHQgKiBAcHVibGljXG5cdCAqIEBtZW1iZXJvZiBIRHdhbGxldFxuXHQgKiBAcGFyYW0ge251bWJlcn0gW3N0YXJ0PTBdIC0gU3RhcnQgaW5kZXhcblx0ICogQHBhcmFtIHtudW1iZXJ9IFt0b3RhbD0xXSAtIFRvdGFsIG51bWJlciBvZiBhZGRyZXNzZXMgdG8gbG9hZFxuXHQgKiBAcGFyYW0gc3RyZW5ndGg/OiBudW1iZXJcblx0ICogQHBhcmFtIHJuZz86IChzaXplOiBudW1iZXIpID0+IEJ1ZmZlclxuXHQgKiBAcGFyYW0gd29yZGxpc3Q/OiBzdHJpbmdbXVxuXHQgKiBAcmV0dXJucyB7c3RyaW5nfSBzZWVkIG9mIHdvcmRzXG5cdCAqL1xuXHRjcmVhdGUoKSB7XG5cdFx0aWYgKCF0aGlzLnNlZWQpIHtcblx0XHRcdHRoaXMuc2VlZCA9IHRoaXMuZ2VuZXJhdGVTZWVkLmFwcGx5KHRoaXMsXG5cdFx0XHRcdEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGFyZ3VtZW50cywgMilcblx0XHRcdCk7XG5cdFx0fVxuXHRcdGNvbnN0IHN0YXJ0ID0gYXJndW1lbnRzWzBdIHx8IDA7XG5cdFx0Y29uc3QgdG90YWwgPSBhcmd1bWVudHNbMV0gfHwgMTtcblx0XHR0aGlzLmltcG9ydEZyb21TZWVkKHRoaXMuc2VlZCwgc3RhcnQsIHRvdGFsKTtcblx0XHRyZXR1cm4gdGhpc1xuXHR9XG5cblx0LyoqXG5cdCAqIExvYWQgc2VlZCBmb3IgaGQgd2FsbGV0IGFuZCBzYXZlIHNlZWRcblx0ICogSW1wb3J0IGZyb20gc2VlZCB0byB3YWxsZXQgdXNpbmcgYmlwMzkgYWxnb3JpdGhtIHdpdGggZXRoZXJldW0gY29tcGF0aWJsZSBkZXJpdmVkIHBhdGhcblx0ICogQHB1YmxpY1xuXHQgKiBAbWVtYmVyb2YgSER3YWxsZXRcblx0ICogQHBhcmFtIHtzdHJpbmd9IHNlZWQgIC0gTW5lbW9uaWMgc2VlZFxuXHQgKiBAcGFyYW0ge251bWJlcn0gW3N0YXJ0PTBdIC0gU3RhcnQgaW5kZXhcblx0ICogQHBhcmFtIHtudW1iZXJ9IFt0b3RhbD0xXSAtIFRvdGFsIG51bWJlciBvZiBhZGRyZXNzZXMgdG8gbG9hZFxuXHQgKi9cblx0bG9hZCgpIHtcblx0XHRjb25zdCBzZWVkID0gYXJndW1lbnRzWzBdO1xuXHRcdGlmICghc2VlZCkgdGhyb3cgbmV3IEVycm9yKFwiU2VlZCBpcyBtaXNzaW5nXCIpO1xuXHRcdGlmICghdGhpcy52YWxpZGF0ZVNlZWQoc2VlZCkpIHRocm93IG5ldyBFcnJvcihcIkludmFsaWQgc2VlZCBhcyBhcmd1bWVudFwiKVxuXHRcdFxuXHRcdGNvbnN0IHN0YXJ0ID0gYXJndW1lbnRzWzFdIHx8IDA7XG5cdFx0Y29uc3QgdG90YWwgPSBhcmd1bWVudHNbMl0gfHwgMTtcblxuXHRcdHRoaXMuc2VlZCA9IHNlZWQ7XG5cblx0XHRyZXR1cm4gdGhpcy5pbXBvcnRGcm9tU2VlZChzZWVkLCBzdGFydCwgdG90YWwpOztcblx0fVxuXG5cdC8qKlxuXHQgKiBHZW5lcmF0ZSBzZWVkIGZvciBpbXBvcnQgaW4gSEQgd2FsbGV0XG5cdCAqIEBwdWJsaWNcblx0ICogQG1lbWJlcm9mIEhEd2FsbGV0XG5cdCAqIEBwYXJhbSBzdHJlbmd0aD86IG51bWJlclxuXHQgKiBAcGFyYW0gcm5nPzogKHNpemU6IG51bWJlcikgPT4gQnVmZmVyXG5cdCAqIEBwYXJhbSB3b3JkbGlzdD86IHN0cmluZ1tdXG5cdCAqIEByZXR1cm5zIHtzdHJpbmd9IHNlZWQgb2Ygd29yZHNcblx0ICovXG5cdGdlbmVyYXRlU2VlZCgpIHtcblx0XHRyZXR1cm4gZ2VuZXJhdGVNbmVtb25pYy5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xuXHR9XG5cblx0LyoqXG5cdCAqIFZhbGlkYXRlIHNlZWRcblx0ICogQHB1YmxpY1xuXHQgKiBAbWVtYmVyb2YgSER3YWxsZXRcblx0ICogQHBhcmFtIHtzdHJpbmd9IHNlZWQgXG5cdCAqIEByZXR1cm5zIHtib29sZWFufSB0cnVlIGlmIGlzIHZhbGlkIHNlZWQsIG90aGVyd2lzZSBmYWxzZS5cblx0ICovXG5cdHZhbGlkYXRlU2VlZChzZWVkKSB7XG5cdFx0cmV0dXJuIHZhbGlkYXRlTW5lbW9uaWMoc2VlZCk7XG5cdH1cblxuXHQvKipcblx0ICogUmV0dXJucyB0aGUgZGVmYXVsdCB3b3JkIGxpc3Rcblx0ICogQHB1YmxpY1xuXHQgKiBAbWVtYmVyb2YgSER3YWxsZXRcblx0ICogQHJldHVybnMge3N0cmluZ30gZGVmYXVsdCB3b3JkIGxpc3Rcblx0ICovXG5cdGdldERlZmF1bHRTZWVkV29yZGxpc3QoKSB7XG5cdFx0cmV0dXJuIGdldERlZmF1bHRXb3JkbGlzdCgpXG5cdH1cblxuXHQvKipcblx0ICogU2V0IGRlZmF1bHQgd29yZCBsaXN0XG5cdCAqIEBwdWJsaWNcblx0ICogQG1lbWJlcm9mIEhEd2FsbGV0XG5cdCAqIEByZXR1cm5zIHtzdHJpbmd9IGRlZmF1bHQgd29yZCBsaXN0XG5cdCAqL1xuXHRzZXREZWZhdWx0U2VlZFdvcmRsaXN0KHdvcmRMaXN0TmFtZSkge1xuXHRcdHJldHVybiBzZXREZWZhdWx0V29yZGxpc3Qod29yZExpc3ROYW1lKVxuXHR9XG5cblx0LyoqXG5cdCAqIFJldHVybiBhdmFpbGFibGUgd29yZHNcblx0ICogQHB1YmxpY1xuXHQgKiBAbWVtYmVyb2YgSER3YWxsZXRcblx0ICogQHJldHVybnMgeyBzdHJpbmdbXSB9XG5cdCAqL1xuXHRnZXRBdmFpbGFibGVTZWVkV29yZExpc3QoKSB7XG5cdFx0cmV0dXJuIE9iamVjdC5rZXlzKHdvcmRsaXN0cyk7XG5cdH1cblxuXHQvKipcblx0ICogaW1wb3J0RnJvbVNlZWQgLSBJbXBvcnRzIGRpcmVjdGx5IGZvciBhY2NvdW50cyBhbmQgbm90IHNhdmUgdGhlIHNlZWRcblx0ICogSW1wb3J0IGZyb20gc2VlZCB0byB3YWxsZXQgdXNpbmcgYmlwMzkgYWxnb3JpdGhtIHdpdGggZXRoZXJldW0gY29tcGF0aWJsZSBkZXJpdmVkIHBhdGhcblx0ICogQHB1YmxpY1xuXHQgKiBAbWVtYmVyb2YgSER3YWxsZXRcblx0ICogQHBhcmFtIHtzdHJpbmd9IHNlZWQgIC0gTW5lbW9uaWMgc2VlZFxuXHQgKiBAcGFyYW0ge251bWJlcn0gW3N0YXJ0PTBdIC0gU3RhcnQgaW5kZXhcblx0ICogQHBhcmFtIHtudW1iZXJ9IFt0b3RhbD0xXSAtIFRvdGFsIG51bWJlciBvZiBhZGRyZXNzZXMgdG8gbG9hZFxuXHQgKi9cblx0aW1wb3J0RnJvbVNlZWQoc2VlZCwgc3RhcnQgPSAwLCB0b3RhbCA9IDEpIHtcblx0XHRpZiAoIXZhbGlkYXRlTW5lbW9uaWMoc2VlZCkpIHtcblx0XHRcdHRocm93IG5ldyBFcnJvcignSW52YWxpZCBzZWVkJyk7XG5cdFx0fVxuXHRcdGxldCBzZWVkQnVmZmVyID0gbW5lbW9uaWNUb1NlZWRTeW5jKHNlZWQpO1xuXHRcdGxldCBoZFdhbGxldCA9IGZyb21NYXN0ZXJTZWVkKHNlZWRCdWZmZXIpO1xuXHRcdGxldCBwcml2YXRlS2V5cyA9IFtdO1xuXG5cdFx0Zm9yIChsZXQgaSA9IHN0YXJ0OyBpIDwgc3RhcnQgKyB0b3RhbDsgaSsrKSB7XG5cdFx0XHRjb25zdCBwcml2YXRlS2V5QnVmZmVyID0gaGRXYWxsZXQuZGVyaXZlKFwibS80NCcvNjAnLzAnLzAvXCIgKyBpKS5fcHJpdmF0ZUtleTtcblx0XHRcdGNvbnN0IHBrSGV4ID0gJzB4JyArIEJ1ZmZlci5mcm9tKHByaXZhdGVLZXlCdWZmZXIpLnRvU3RyaW5nKCdoZXgnKTtcblx0XHRcdHByaXZhdGVLZXlzLnB1c2gocGtIZXgpO1xuXHRcdFx0dGhpcy53ZWIzQ29udGV4dC5ldGguYWNjb3VudHMud2FsbGV0LmFkZChwa0hleCk7XG5cdFx0fVxuXHRcdHJldHVybiBwcml2YXRlS2V5cztcblx0fVxufVxuXG5cbmNvbnN0IGFjYyA9IGZ1bmN0aW9uICh3ZWIzSW4pIHtcblx0Y29uc3Qge1xuXHRcdHdhbGxldFxuXHR9ID0gd2ViM0luLmV0aC5hY2NvdW50cztcblx0Y29uc3QgaGR3YWxsZXQgPSBuZXcgSER3YWxsZXQod2ViM0luLCB3YWxsZXQpO1xuXHR3YWxsZXQuYWNjb3VudHMgPSBpbml0KHdlYjNJbi5ldGgpO1xuXHR3YWxsZXQuaGR3YWxsZXQgPSBoZHdhbGxldFxuXHR3YWxsZXQuaW1wb3J0RnJvbVNlZWQgPSBoZHdhbGxldC5pbXBvcnRGcm9tU2VlZC5iaW5kKGhkd2FsbGV0KTtcblx0cmV0dXJuIHdhbGxldDtcbn07XG5cbmV4cG9ydCBkZWZhdWx0IGFjYztcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQU9BOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQVFBOzs7Ozs7Ozs7Ozs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBTUEsV0FBVyxHQUFHQyxNQUFNLElBQUk7RUFDN0IsSUFBTUMsQ0FBQyxHQUFHRCxNQUFNLENBQUNFLE1BQWpCO0VBQ0EsSUFBSUQsQ0FBQyxLQUFLLENBQVYsRUFBYSxPQUFPLElBQVA7RUFFYixJQUFNRSxhQUFhLEdBQUcsSUFBQUMsY0FBQSxFQUFNSCxDQUFOLEVBQVNJLEdBQVQsQ0FBYUMsS0FBSyxJQUFJQSxLQUFLLENBQUNDLFFBQU4sRUFBdEIsQ0FBdEI7RUFFQSxPQUFPLElBQUFDLGVBQUEsRUFDTixJQUFBQyxlQUFBLEVBQU9ULE1BQVAsRUFBZSxDQUFDVSxNQUFELEVBQVNDLEdBQVQsS0FBaUI7SUFDL0IsT0FBT1IsYUFBYSxDQUFDUyxRQUFkLENBQXVCRCxHQUF2QixDQUFQO0VBQ0EsQ0FGRCxDQURNLEVBSUxOLEdBSkssQ0FJRFEsUUFBUSxJQUFJQSxRQUFRLENBQUNDLE9BSnBCLENBQVA7QUFLQSxDQVhEO0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBLElBQU1DLFdBQVc7RUFBQSw2QkFBRyxXQUFPQyxHQUFQLEVBQVlDLFNBQVosRUFBMEI7SUFDN0MsT0FBTyxJQUFJQyxPQUFKLENBQVksQ0FBQ0MsT0FBRCxFQUFVQyxNQUFWLEtBQXFCO01BQ3ZDLElBQUlILFNBQVMsS0FBSyxJQUFkLElBQXNCQSxTQUFTLENBQUNmLE1BQVYsS0FBcUIsQ0FBL0MsRUFBa0Q7UUFDakRnQixPQUFPLENBQUNHLEdBQVIsQ0FBWUosU0FBUyxDQUFDWixHQUFWLENBQWNTLE9BQU8sSUFBSUUsR0FBRyxDQUFDTSxVQUFKLENBQWVSLE9BQWYsQ0FBekIsQ0FBWixFQUNFUyxJQURGLENBQ09DLFFBQVEsSUFBSTtVQUNqQkwsT0FBTyxDQUFDLElBQUFNLGtCQUFBLEVBQVVSLFNBQVYsRUFBcUJPLFFBQXJCLENBQUQsRUFBaUMsQ0FBQ1YsT0FBRCxFQUFVWSxPQUFWLEtBQXNCO1lBQzdELE9BQU87Y0FDTlosT0FETTtjQUVOWTtZQUZNLENBQVA7VUFJQSxDQUxNLENBQVA7UUFNQSxDQVJGLEVBU0VDLEtBVEYsQ0FTUUMsTUFBTSxJQUFJO1VBQ2hCUixNQUFNLENBQUNRLE1BQUQsQ0FBTjtRQUNBLENBWEY7TUFZQSxDQWJELE1BYU87UUFDTlIsTUFBTSxDQUFDLElBQUlTLEtBQUosQ0FBVSxpQkFBVixDQUFELENBQU47TUFDQTtJQUNELENBakJNLENBQVA7RUFrQkEsQ0FuQmdCOztFQUFBLGdCQUFYZCxXQUFXO0lBQUE7RUFBQTtBQUFBLEdBQWpCO0FBcUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBLElBQU1lLFNBQVMsR0FBRyxDQUFDZCxHQUFELEVBQU1lLEVBQU4sRUFBVUMsS0FBVixFQUFpQkMsT0FBakIsS0FBNkI7RUFDOUM7RUFDQSxJQUFNQyxpQkFBaUI7SUFDdEJILEVBRHNCO0lBRXRCQyxLQUZzQjtJQUd0QkcsR0FBRyxFQUFFO0VBSGlCLEdBSW5CRixPQUptQixDQUF2Qjs7RUFPQSxPQUFPakIsR0FBRyxDQUFDb0IsZUFBSixDQUFvQkYsaUJBQXBCLENBQVA7QUFDQSxDQVZEO0FBWUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EsU0FBU0csUUFBVCxDQUFrQk4sRUFBbEIsRUFBc0JDLEtBQXRCLEVBQTJDO0VBQUEsSUFBZEMsT0FBYyx1RUFBSixFQUFJOztFQUMxQyxJQUFNSyxVQUFVO0lBQ2ZDLElBQUksRUFBRTtFQURTLEdBRVpOLE9BRlksQ0FBaEI7O0VBSUEsT0FBT0gsU0FBUyxDQUFDLElBQUQsRUFBT0MsRUFBUCxFQUFXQyxLQUFYLEVBQWtCTSxVQUFsQixDQUFoQjtBQUNBO0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EsU0FBU0UsWUFBVCxDQUFzQkQsSUFBdEIsRUFBNEJSLEVBQTVCLEVBQWdDQyxLQUFoQyxFQUFxRDtFQUFBLElBQWRDLE9BQWMsdUVBQUosRUFBSTs7RUFDcEQsSUFBTUssVUFBVTtJQUNmQztFQURlLEdBRVpOLE9BRlksQ0FBaEI7O0VBSUEsT0FBT0gsU0FBUyxDQUFDLElBQUQsRUFBT0MsRUFBUCxFQUFXQyxLQUFYLEVBQWtCTSxVQUFsQixDQUFoQjtBQUNBOztBQUVELElBQU1HLGNBQWMsR0FBRztFQUN0QkMsR0FBRyxFQUFFLENBQUMxQixHQUFELEVBQU1MLEdBQU4sS0FBYztJQUNsQixJQUFNO01BQ0xYO0lBREssSUFFRmdCLEdBQUcsQ0FBQ0gsUUFGUjtJQUdBLElBQUk4QixNQUFNLEdBQUczQyxNQUFiOztJQUNBLFFBQVFXLEdBQVI7TUFDQyxLQUFLLGFBQUw7UUFDQ2dDLE1BQU0sR0FBRzVDLFdBQVcsQ0FBQ0MsTUFBRCxDQUFwQjtRQUNBOztNQUNELEtBQUssYUFBTDtRQUNDMkMsTUFBTSxHQUFHNUIsV0FBVyxDQUFDQyxHQUFELEVBQU1qQixXQUFXLENBQUNDLE1BQUQsQ0FBakIsQ0FBcEI7UUFDQTs7TUFDRCxLQUFLLFlBQUw7UUFDQztBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7UUFDSTJDLE1BQU0sR0FBRzdCLE9BQU8sSUFBSTtVQUNuQixPQUFPRSxHQUFHLENBQUNNLFVBQUosQ0FBZVIsT0FBZixDQUFQO1FBQ0EsQ0FGRDs7UUFHQTs7TUFDRCxLQUFLLFVBQUw7UUFDQzZCLE1BQU0sR0FBRyxrQkFBYTtVQUFBLGtDQUFUQyxJQUFTO1lBQVRBLElBQVM7VUFBQTs7VUFDckIsT0FBT1AsUUFBUSxDQUFDUSxJQUFULENBQWM3QixHQUFkLEVBQW1CLEdBQUc0QixJQUF0QixDQUFQO1FBQ0EsQ0FGRDs7UUFHQTs7TUFDRCxLQUFLLGNBQUw7UUFDQ0QsTUFBTSxHQUFHLGtCQUFhO1VBQUEsbUNBQVRDLElBQVM7WUFBVEEsSUFBUztVQUFBOztVQUNyQixPQUFPSixZQUFZLENBQUNLLElBQWIsQ0FBa0I3QixHQUFsQixFQUF1QixHQUFHNEIsSUFBMUIsQ0FBUDtRQUNBLENBRkQ7O1FBR0E7O01BQ0Q7UUFDQztJQTdCRjs7SUErQkEsT0FBT0QsTUFBUDtFQUNBO0FBdENxQixDQUF2Qjs7QUF5Q0EsU0FBU0csSUFBVCxDQUFjOUIsR0FBZCxFQUFtQjtFQUNsQixPQUFPLElBQUkrQixLQUFKLENBQVUvQixHQUFWLEVBQWV5QixjQUFmLENBQVA7QUFDQTtBQUlEO0FBQ0E7QUFDQTs7O0FBQ0EsTUFBTU8sUUFBTixDQUFlO0VBS2RDLFdBQVcsQ0FBQ0MsT0FBRCxFQUFVQyxPQUFWLEVBQW1CO0lBQUEsOEJBSnZCLElBSXVCOztJQUFBOztJQUFBOztJQUM3QixLQUFLQyxXQUFMLEdBQW1CRixPQUFuQjtJQUNBLEtBQUtsRCxNQUFMLEdBQWNtRCxPQUFkO0VBQ0E7RUFFRDtBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztFQUNDRSxNQUFNLEdBQUc7SUFDUixJQUFJLENBQUMsS0FBS0MsSUFBVixFQUFnQjtNQUNmLEtBQUtBLElBQUwsR0FBWSxLQUFLQyxZQUFMLENBQWtCQyxLQUFsQixDQUF3QixJQUF4QixFQUNYQyxLQUFLLENBQUNDLFNBQU4sQ0FBZ0JDLEtBQWhCLENBQXNCZCxJQUF0QixDQUEyQmUsU0FBM0IsRUFBc0MsQ0FBdEMsQ0FEVyxDQUFaO0lBR0E7O0lBQ0QsSUFBTUMsS0FBSyxHQUFHRCxTQUFTLENBQUMsQ0FBRCxDQUFULElBQWdCLENBQTlCO0lBQ0EsSUFBTUUsS0FBSyxHQUFHRixTQUFTLENBQUMsQ0FBRCxDQUFULElBQWdCLENBQTlCO0lBQ0EsS0FBS0csY0FBTCxDQUFvQixLQUFLVCxJQUF6QixFQUErQk8sS0FBL0IsRUFBc0NDLEtBQXRDO0lBQ0EsT0FBTyxJQUFQO0VBQ0E7RUFFRDtBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztFQUNDRSxJQUFJLEdBQUc7SUFDTixJQUFNVixJQUFJLEdBQUdNLFNBQVMsQ0FBQyxDQUFELENBQXRCO0lBQ0EsSUFBSSxDQUFDTixJQUFMLEVBQVcsTUFBTSxJQUFJekIsS0FBSixDQUFVLGlCQUFWLENBQU47SUFDWCxJQUFJLENBQUMsS0FBS29DLFlBQUwsQ0FBa0JYLElBQWxCLENBQUwsRUFBOEIsTUFBTSxJQUFJekIsS0FBSixDQUFVLDBCQUFWLENBQU47SUFFOUIsSUFBTWdDLEtBQUssR0FBR0QsU0FBUyxDQUFDLENBQUQsQ0FBVCxJQUFnQixDQUE5QjtJQUNBLElBQU1FLEtBQUssR0FBR0YsU0FBUyxDQUFDLENBQUQsQ0FBVCxJQUFnQixDQUE5QjtJQUVBLEtBQUtOLElBQUwsR0FBWUEsSUFBWjtJQUVBLE9BQU8sS0FBS1MsY0FBTCxDQUFvQlQsSUFBcEIsRUFBMEJPLEtBQTFCLEVBQWlDQyxLQUFqQyxDQUFQO0lBQStDO0VBQy9DO0VBRUQ7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7RUFDQ1AsWUFBWSxHQUFHO0lBQ2QsT0FBT1cscUJBQUEsQ0FBaUJWLEtBQWpCLENBQXVCLElBQXZCLEVBQTZCSSxTQUE3QixDQUFQO0VBQ0E7RUFFRDtBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0VBQ0NLLFlBQVksQ0FBQ1gsSUFBRCxFQUFPO0lBQ2xCLE9BQU8sSUFBQWEscUJBQUEsRUFBaUJiLElBQWpCLENBQVA7RUFDQTtFQUVEO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0VBQ0NjLHNCQUFzQixHQUFHO0lBQ3hCLE9BQU8sSUFBQUMsdUJBQUEsR0FBUDtFQUNBO0VBRUQ7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7RUFDQ0Msc0JBQXNCLENBQUNDLFlBQUQsRUFBZTtJQUNwQyxPQUFPLElBQUFDLHVCQUFBLEVBQW1CRCxZQUFuQixDQUFQO0VBQ0E7RUFFRDtBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztFQUNDRSx3QkFBd0IsR0FBRztJQUMxQixPQUFPQyxNQUFNLENBQUNDLElBQVAsQ0FBWUMsY0FBWixDQUFQO0VBQ0E7RUFFRDtBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztFQUNDYixjQUFjLENBQUNULElBQUQsRUFBNkI7SUFBQSxJQUF0Qk8sS0FBc0IsdUVBQWQsQ0FBYztJQUFBLElBQVhDLEtBQVcsdUVBQUgsQ0FBRzs7SUFDMUMsSUFBSSxDQUFDLElBQUFLLHFCQUFBLEVBQWlCYixJQUFqQixDQUFMLEVBQTZCO01BQzVCLE1BQU0sSUFBSXpCLEtBQUosQ0FBVSxjQUFWLENBQU47SUFDQTs7SUFDRCxJQUFJZ0QsVUFBVSxHQUFHLElBQUFDLHVCQUFBLEVBQW1CeEIsSUFBbkIsQ0FBakI7SUFDQSxJQUFJeUIsUUFBUSxHQUFHLElBQUFDLHFCQUFBLEVBQWVILFVBQWYsQ0FBZjtJQUNBLElBQUlJLFdBQVcsR0FBRyxFQUFsQjs7SUFFQSxLQUFLLElBQUlDLENBQUMsR0FBR3JCLEtBQWIsRUFBb0JxQixDQUFDLEdBQUdyQixLQUFLLEdBQUdDLEtBQWhDLEVBQXVDb0IsQ0FBQyxFQUF4QyxFQUE0QztNQUMzQyxJQUFNQyxnQkFBZ0IsR0FBR0osUUFBUSxDQUFDSyxNQUFULENBQWdCLG9CQUFvQkYsQ0FBcEMsRUFBdUNHLFdBQWhFOztNQUNBLElBQU1DLEtBQUssR0FBRyxPQUFPQyxNQUFNLENBQUNoRCxJQUFQLENBQVk0QyxnQkFBWixFQUE4QjVFLFFBQTlCLENBQXVDLEtBQXZDLENBQXJCO01BQ0EwRSxXQUFXLENBQUNPLElBQVosQ0FBaUJGLEtBQWpCO01BQ0EsS0FBS2xDLFdBQUwsQ0FBaUJwQyxHQUFqQixDQUFxQkgsUUFBckIsQ0FBOEJiLE1BQTlCLENBQXFDeUYsR0FBckMsQ0FBeUNILEtBQXpDO0lBQ0E7O0lBQ0QsT0FBT0wsV0FBUDtFQUNBOztBQXRJYTs7QUEwSWYsSUFBTVMsR0FBRyxHQUFHLFNBQU5BLEdBQU0sQ0FBVUMsTUFBVixFQUFrQjtFQUM3QixJQUFNO0lBQ0wzRjtFQURLLElBRUYyRixNQUFNLENBQUMzRSxHQUFQLENBQVdILFFBRmY7RUFHQSxJQUFNK0UsUUFBUSxHQUFHLElBQUk1QyxRQUFKLENBQWEyQyxNQUFiLEVBQXFCM0YsTUFBckIsQ0FBakI7RUFDQUEsTUFBTSxDQUFDYSxRQUFQLEdBQWtCaUMsSUFBSSxDQUFDNkMsTUFBTSxDQUFDM0UsR0FBUixDQUF0QjtFQUNBaEIsTUFBTSxDQUFDNEYsUUFBUCxHQUFrQkEsUUFBbEI7RUFDQTVGLE1BQU0sQ0FBQytELGNBQVAsR0FBd0I2QixRQUFRLENBQUM3QixjQUFULENBQXdCOEIsSUFBeEIsQ0FBNkJELFFBQTdCLENBQXhCO0VBQ0EsT0FBTzVGLE1BQVA7QUFDQSxDQVREOztlQVdlMEYsRyJ9