"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es.symbol.description.js");

require("core-js/modules/es.symbol.async-iterator.js");

require("core-js/modules/es.symbol.match.js");

require("core-js/modules/es.symbol.replace.js");

require("core-js/modules/es.symbol.search.js");

require("core-js/modules/es.symbol.split.js");

require("core-js/modules/es.array.flat.js");

require("core-js/modules/es.array.flat-map.js");

require("core-js/modules/es.array.includes.js");

require("core-js/modules/es.array.iterator.js");

require("core-js/modules/es.array.reduce.js");

require("core-js/modules/es.array.reduce-right.js");

require("core-js/modules/es.array.reverse.js");

require("core-js/modules/es.array.sort.js");

require("core-js/modules/es.array.unscopables.flat.js");

require("core-js/modules/es.array.unscopables.flat-map.js");

require("core-js/modules/es.array-buffer.constructor.js");

require("core-js/modules/es.array-buffer.slice.js");

require("core-js/modules/es.math.hypot.js");

require("core-js/modules/es.number.parse-float.js");

require("core-js/modules/es.number.parse-int.js");

require("core-js/modules/es.number.to-fixed.js");

require("core-js/modules/es.object.assign.js");

require("core-js/modules/es.object.define-getter.js");

require("core-js/modules/es.object.define-setter.js");

require("core-js/modules/es.object.from-entries.js");

require("core-js/modules/es.object.lookup-getter.js");

require("core-js/modules/es.object.lookup-setter.js");

require("core-js/modules/es.parse-float.js");

require("core-js/modules/es.parse-int.js");

require("core-js/modules/es.promise.js");

require("core-js/modules/es.promise.finally.js");

require("core-js/modules/es.reflect.set.js");

require("core-js/modules/es.regexp.constructor.js");

require("core-js/modules/es.regexp.exec.js");

require("core-js/modules/es.regexp.flags.js");

require("core-js/modules/es.regexp.to-string.js");

require("core-js/modules/es.string.ends-with.js");

require("core-js/modules/es.string.includes.js");

require("core-js/modules/es.string.match.js");

require("core-js/modules/es.string.pad-end.js");

require("core-js/modules/es.string.pad-start.js");

require("core-js/modules/es.string.replace.js");

require("core-js/modules/es.string.search.js");

require("core-js/modules/es.string.split.js");

require("core-js/modules/es.string.starts-with.js");

require("core-js/modules/es.string.trim.js");

require("core-js/modules/es.string.trim-end.js");

require("core-js/modules/es.string.trim-start.js");

require("core-js/modules/es.typed-array.float32-array.js");

require("core-js/modules/es.typed-array.float64-array.js");

require("core-js/modules/es.typed-array.int8-array.js");

require("core-js/modules/es.typed-array.int16-array.js");

require("core-js/modules/es.typed-array.int32-array.js");

require("core-js/modules/es.typed-array.uint8-array.js");

require("core-js/modules/es.typed-array.uint8-clamped-array.js");

require("core-js/modules/es.typed-array.uint16-array.js");

require("core-js/modules/es.typed-array.uint32-array.js");

require("core-js/modules/es.typed-array.fill.js");

require("core-js/modules/es.typed-array.from.js");

require("core-js/modules/es.typed-array.of.js");

require("core-js/modules/es.typed-array.set.js");

require("core-js/modules/es.typed-array.sort.js");

require("core-js/modules/es.typed-array.to-locale-string.js");

require("core-js/modules/esnext.aggregate-error.js");

require("core-js/modules/esnext.array.last-index.js");

require("core-js/modules/esnext.array.last-item.js");

require("core-js/modules/esnext.composite-key.js");

require("core-js/modules/esnext.composite-symbol.js");

require("core-js/modules/esnext.global-this.js");

require("core-js/modules/esnext.map.delete-all.js");

require("core-js/modules/esnext.map.every.js");

require("core-js/modules/esnext.map.filter.js");

require("core-js/modules/esnext.map.find.js");

require("core-js/modules/esnext.map.find-key.js");

require("core-js/modules/esnext.map.from.js");

require("core-js/modules/esnext.map.group-by.js");

require("core-js/modules/esnext.map.includes.js");

require("core-js/modules/esnext.map.key-by.js");

require("core-js/modules/esnext.map.key-of.js");

require("core-js/modules/esnext.map.map-keys.js");

require("core-js/modules/esnext.map.map-values.js");

require("core-js/modules/esnext.map.merge.js");

require("core-js/modules/esnext.map.of.js");

require("core-js/modules/esnext.map.reduce.js");

require("core-js/modules/esnext.map.some.js");

require("core-js/modules/esnext.map.update.js");

require("core-js/modules/esnext.math.clamp.js");

require("core-js/modules/esnext.math.deg-per-rad.js");

require("core-js/modules/esnext.math.degrees.js");

require("core-js/modules/esnext.math.fscale.js");

require("core-js/modules/esnext.math.iaddh.js");

require("core-js/modules/esnext.math.imulh.js");

require("core-js/modules/esnext.math.isubh.js");

require("core-js/modules/esnext.math.rad-per-deg.js");

require("core-js/modules/esnext.math.radians.js");

require("core-js/modules/esnext.math.scale.js");

require("core-js/modules/esnext.math.seeded-prng.js");

require("core-js/modules/esnext.math.signbit.js");

require("core-js/modules/esnext.math.umulh.js");

require("core-js/modules/esnext.number.from-string.js");

require("core-js/modules/esnext.observable.js");

require("core-js/modules/esnext.promise.all-settled.js");

require("core-js/modules/esnext.promise.any.js");

require("core-js/modules/esnext.promise.try.js");

require("core-js/modules/esnext.reflect.define-metadata.js");

require("core-js/modules/esnext.reflect.delete-metadata.js");

require("core-js/modules/esnext.reflect.get-metadata.js");

require("core-js/modules/esnext.reflect.get-metadata-keys.js");

require("core-js/modules/esnext.reflect.get-own-metadata.js");

require("core-js/modules/esnext.reflect.get-own-metadata-keys.js");

require("core-js/modules/esnext.reflect.has-metadata.js");

require("core-js/modules/esnext.reflect.has-own-metadata.js");

require("core-js/modules/esnext.reflect.metadata.js");

require("core-js/modules/esnext.set.add-all.js");

require("core-js/modules/esnext.set.delete-all.js");

require("core-js/modules/esnext.set.difference.js");

require("core-js/modules/esnext.set.every.js");

require("core-js/modules/esnext.set.filter.js");

require("core-js/modules/esnext.set.find.js");

require("core-js/modules/esnext.set.from.js");

require("core-js/modules/esnext.set.intersection.js");

require("core-js/modules/esnext.set.is-disjoint-from.js");

require("core-js/modules/esnext.set.is-subset-of.js");

require("core-js/modules/esnext.set.is-superset-of.js");

require("core-js/modules/esnext.set.join.js");

require("core-js/modules/esnext.set.map.js");

require("core-js/modules/esnext.set.of.js");

require("core-js/modules/esnext.set.reduce.js");

require("core-js/modules/esnext.set.some.js");

require("core-js/modules/esnext.set.symmetric-difference.js");

require("core-js/modules/esnext.set.union.js");

require("core-js/modules/esnext.string.at.js");

require("core-js/modules/esnext.string.code-points.js");

require("core-js/modules/esnext.string.match-all.js");

require("core-js/modules/esnext.string.replace-all.js");

require("core-js/modules/esnext.symbol.dispose.js");

require("core-js/modules/esnext.symbol.observable.js");

require("core-js/modules/esnext.symbol.pattern-match.js");

require("core-js/modules/esnext.weak-map.delete-all.js");

require("core-js/modules/esnext.weak-map.from.js");

require("core-js/modules/esnext.weak-map.of.js");

require("core-js/modules/esnext.weak-set.add-all.js");

require("core-js/modules/esnext.weak-set.delete-all.js");

require("core-js/modules/esnext.weak-set.from.js");

require("core-js/modules/esnext.weak-set.of.js");

require("core-js/modules/web.dom-collections.iterator.js");

require("core-js/modules/web.immediate.js");

require("core-js/modules/web.queue-microtask.js");

require("core-js/modules/web.url.js");

require("core-js/modules/web.url.to-json.js");

require("core-js/modules/web.url-search-params.js");

var _web = _interopRequireDefault(require("web3"));

var _accounts = _interopRequireDefault(require("./accounts"));

var _contracts = _interopRequireDefault(require("./contracts"));

var _providers = require("./providers");

var _package = require("../package.json");

var _utils = _interopRequireDefault(require("./utils"));

var _query = _interopRequireDefault(require("./query"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * Provider exception
 * @param {string} message Error message
 */
function InvalidProviderException(message) {
  this.message = message;
  this.name = 'InvalidProviderException';
}

class PublicMint {
  /**
   * PublicMint - javascript devKit
   * Adds new namespace inside web3 `web3.pm`
   * This module depends of web3 package readme more [here](https://web3js.readthedocs.io/en/v1.2.4/index.html)
   *
   * @constructor
   * @param { Number | String | Provider } [ provider = 1 | provider = 'mainNet' | provider = new Provider ] provider
   * @param { String } [ connectionType = 'ws' ] connectionType ('ws' || 'http' ) default setting is ws (events).
   * @param { Object } [ customWeb3Options = { defaultBlock: 'latest', transactionConfirmationBlocks: 1 } ] - Web3 options example: [web3 docs](https://web3js.readthedocs.io/en/v1.2.4/web3-eth.html#transactionconfirmationblocks)
   * @param { Object } [ customContractsOptions = {  } ] - Contract options example: [web3 docs](https://web3js.readthedocs.io/en/v1.2.4/web3-eth-contract.html#web3-eth-contract)
   *
   * @return { Object } Will return an object with the classes of all major sub module from web3, including PublicMint modules
   *
   * [QuickStart tutorial]{@tutorial QuickStart}
   * @example const web3 = new PublicMint();
   */
  constructor() {
    var provider = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 2020;
    var connectionType = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'ws';
    var customWeb3Options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
    var customContractsOptions = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};

    var web3opts = _objectSpread({
      defaultBlock: 'latest',
      transactionConfirmationBlocks: 1
    }, customWeb3Options);

    this.provider = provider instanceof Object ? provider : (0, _providers.resolveProvider)(provider);

    if (this.provider === undefined) {
      throw new InvalidProviderException("Provider identifier ".concat(provider, " not found"));
    }

    this.loadedWeb3Provider = (0, _providers.loadProviderWeb3)(connectionType, this.provider);
    this.web3 = new _web.default(this.loadedWeb3Provider, null, web3opts);
    var contracts = (0, _contracts.default)(this.web3, this.provider.chainId, customContractsOptions);
    /**
     *  @namespace pm
     *  @property { Object } pm.contracts - Contracts instances
     *  @property { Array<Object> } pm.providers - List of providers
     *  @property { Provider } pm.provider Provider in use
     *  @property { Accounts } pm.wallet - Module for manage accounts and related methods (sign, transfer, ...)
     *  @property { Utils } pm.utils - [Utils module ](Utils)
     *  @property { String }  pm.version - Module Version
     *  @property { Query } pm.query - Module with helper functions for query blockchain.
     */

    this.web3 = Object.defineProperty(this.web3, 'pm', {
      value: {
        /**
         *  Contracts instances
         * @memberof pm
         * @type { Object }
         * @namespace pm.contracts
         * @property { Object } contracts.USD - ERC20  contract instance for USD token
         * @property { Object } contracts.admin.USD - Multi Signature contract instance for USD
         */
        contracts,

        /**
         *  Contracts instances
         * @memberof pm
         * @type { Object }
         * @namespace pm.providers
         * @property { Function } providers.getProviderList  - Get list of providers
         */
        providers: _providers.getProviderList,

        /**
         *  Contracts instances
         * @memberof pm
         * @type { Provider }
         * @namespace pm.provider
         * @property { Function } provider - Instance provider
         */
        provider: this.provider,

        /**
         *  Module of wallet actions [more info](https://web3js.readthedocs.io/en/v1.2.0/web3-eth-accounts.html#wallet)
         * @memberof pm
         * @type { Object }
         * @namespace pm.wallet
         * @property { Function } wallet.add - Adds an account using a private key or account object to the wallet.
         * @property { Function } wallet.remove - Removes an account from the wallet.
         * @property { Function } wallet.load - Loads a wallet from local storage and decrypts it.
         * @property { Function } wallet.clear - Securely empties the wallet and removes all its accounts.
         * @property { Function } wallet.encrypt - Encrypts all wallet accounts to an array of encrypted keystore v3 objects.
         * @property { Function } wallet.decrypt - Decrypts keystore v3 objects.
         * @property { Function } wallet.save - Stores the wallet encrypted and as string in local storage.
         * @property { Function } wallet.load - Loads a wallet from local storage and decrypts it.
         * @property { Object } wallet.accounts - Actions for added accounts.
         * @property { Function } wallet.accounts.getBalance - Get balance of account  `accounts.getBalance('0x0')`.
         * @property { Function } wallet.accounts.getBalances - Get balance of account `accounts.getBalances`.
         * @property { Function } wallet.accounts.transfer - Transfer balance using first wallet account.
         * @property { Function } wallet.accounts.transferFrom - TransferFrom same as transfer but specifying index or address .
         */
        wallet: (0, _accounts.default)(this.web3),

        /**
         *  Module of utils
         * @memberof pm
         * @type { Object }
         * @namespace utils
         */
        utils: (0, _utils.default)(this.web3.utils),

        /**
         *  Version of module
         * @memberof pm
         * @type { String }
         */
        version: _package.version,

        /**
         *  Version of module
         * @memberof pm
         * @type { Query }
         */
        query: (0, _query.default)(this.web3)
      }
    }); // Delete modules not compatible with client
    // TODO: needs revision later

    delete this.web3.eth.personal;
    delete this.web3.eth.ens;
    delete this.web3.bzz;
    delete this.web3.shh;
    return this.web3;
  }
  /**
   * Provider constructor
   */


  static provider() {
    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return new _providers.Provider(...args);
  }
  /**
   * Get providers list
   */


  static providers() {
    return (0, _providers.getProviderList)();
  }
  /**
   * Modules versions
   */


  static versions() {
    return {
      Web3: _web.default.version,
      PublicMint: _package.version
    };
  }

} // Expose utils


PublicMint.utils = _objectSpread(_objectSpread({}, _web.default.utils), (0, _utils.default)(_web.default.utils));
var _default = PublicMint;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJJbnZhbGlkUHJvdmlkZXJFeGNlcHRpb24iLCJtZXNzYWdlIiwibmFtZSIsIlB1YmxpY01pbnQiLCJjb25zdHJ1Y3RvciIsInByb3ZpZGVyIiwiY29ubmVjdGlvblR5cGUiLCJjdXN0b21XZWIzT3B0aW9ucyIsImN1c3RvbUNvbnRyYWN0c09wdGlvbnMiLCJ3ZWIzb3B0cyIsImRlZmF1bHRCbG9jayIsInRyYW5zYWN0aW9uQ29uZmlybWF0aW9uQmxvY2tzIiwiT2JqZWN0IiwicmVzb2x2ZVByb3ZpZGVyIiwidW5kZWZpbmVkIiwibG9hZGVkV2ViM1Byb3ZpZGVyIiwibG9hZFByb3ZpZGVyV2ViMyIsIndlYjMiLCJXZWIzIiwiY29udHJhY3RzIiwiY3JlYXRlQ29udHJhY3RzSW50ZXJmYWNlIiwiY2hhaW5JZCIsImRlZmluZVByb3BlcnR5IiwidmFsdWUiLCJwcm92aWRlcnMiLCJnZXRQcm92aWRlckxpc3QiLCJ3YWxsZXQiLCJhY2NvdW50cyIsInV0aWxzIiwidmVyc2lvbiIsInF1ZXJ5IiwiZXRoIiwicGVyc29uYWwiLCJlbnMiLCJienoiLCJzaGgiLCJhcmdzIiwiUHJvdmlkZXIiLCJ2ZXJzaW9ucyJdLCJzb3VyY2VzIjpbIi4uL3NyYy9pbmRleC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgJ2NvcmUtanMnXG5cbmltcG9ydCBXZWIzIGZyb20gJ3dlYjMnXG5cbmltcG9ydCBhY2NvdW50cyBmcm9tICcuL2FjY291bnRzJ1xuaW1wb3J0IGNyZWF0ZUNvbnRyYWN0c0ludGVyZmFjZSBmcm9tICcuL2NvbnRyYWN0cydcblxuaW1wb3J0IHtcbiAgIHJlc29sdmVQcm92aWRlcixcbiAgIGdldFByb3ZpZGVyTGlzdCxcbiAgIGxvYWRQcm92aWRlcldlYjMsXG4gICBQcm92aWRlclxufSBmcm9tICcuL3Byb3ZpZGVycydcblxuaW1wb3J0IHtcbiAgIHZlcnNpb25cbn0gZnJvbSAnLi4vcGFja2FnZS5qc29uJ1xuXG5pbXBvcnQgdXRpbHMgZnJvbSAnLi91dGlscydcbmltcG9ydCBxdWVyeSBmcm9tICcuL3F1ZXJ5J1xuXG4vKipcbiAqIFByb3ZpZGVyIGV4Y2VwdGlvblxuICogQHBhcmFtIHtzdHJpbmd9IG1lc3NhZ2UgRXJyb3IgbWVzc2FnZVxuICovXG5mdW5jdGlvbiBJbnZhbGlkUHJvdmlkZXJFeGNlcHRpb24obWVzc2FnZSkge1xuICAgdGhpcy5tZXNzYWdlID0gbWVzc2FnZVxuICAgdGhpcy5uYW1lID0gJ0ludmFsaWRQcm92aWRlckV4Y2VwdGlvbidcbn1cblxuY2xhc3MgUHVibGljTWludCB7XG4gICAvKipcbiAgICAqIFB1YmxpY01pbnQgLSBqYXZhc2NyaXB0IGRldktpdFxuICAgICogQWRkcyBuZXcgbmFtZXNwYWNlIGluc2lkZSB3ZWIzIGB3ZWIzLnBtYFxuICAgICogVGhpcyBtb2R1bGUgZGVwZW5kcyBvZiB3ZWIzIHBhY2thZ2UgcmVhZG1lIG1vcmUgW2hlcmVdKGh0dHBzOi8vd2ViM2pzLnJlYWR0aGVkb2NzLmlvL2VuL3YxLjIuNC9pbmRleC5odG1sKVxuICAgICpcbiAgICAqIEBjb25zdHJ1Y3RvclxuICAgICogQHBhcmFtIHsgTnVtYmVyIHwgU3RyaW5nIHwgUHJvdmlkZXIgfSBbIHByb3ZpZGVyID0gMSB8IHByb3ZpZGVyID0gJ21haW5OZXQnIHwgcHJvdmlkZXIgPSBuZXcgUHJvdmlkZXIgXSBwcm92aWRlclxuICAgICogQHBhcmFtIHsgU3RyaW5nIH0gWyBjb25uZWN0aW9uVHlwZSA9ICd3cycgXSBjb25uZWN0aW9uVHlwZSAoJ3dzJyB8fCAnaHR0cCcgKSBkZWZhdWx0IHNldHRpbmcgaXMgd3MgKGV2ZW50cykuXG4gICAgKiBAcGFyYW0geyBPYmplY3QgfSBbIGN1c3RvbVdlYjNPcHRpb25zID0geyBkZWZhdWx0QmxvY2s6ICdsYXRlc3QnLCB0cmFuc2FjdGlvbkNvbmZpcm1hdGlvbkJsb2NrczogMSB9IF0gLSBXZWIzIG9wdGlvbnMgZXhhbXBsZTogW3dlYjMgZG9jc10oaHR0cHM6Ly93ZWIzanMucmVhZHRoZWRvY3MuaW8vZW4vdjEuMi40L3dlYjMtZXRoLmh0bWwjdHJhbnNhY3Rpb25jb25maXJtYXRpb25ibG9ja3MpXG4gICAgKiBAcGFyYW0geyBPYmplY3QgfSBbIGN1c3RvbUNvbnRyYWN0c09wdGlvbnMgPSB7ICB9IF0gLSBDb250cmFjdCBvcHRpb25zIGV4YW1wbGU6IFt3ZWIzIGRvY3NdKGh0dHBzOi8vd2ViM2pzLnJlYWR0aGVkb2NzLmlvL2VuL3YxLjIuNC93ZWIzLWV0aC1jb250cmFjdC5odG1sI3dlYjMtZXRoLWNvbnRyYWN0KVxuICAgICpcbiAgICAqIEByZXR1cm4geyBPYmplY3QgfSBXaWxsIHJldHVybiBhbiBvYmplY3Qgd2l0aCB0aGUgY2xhc3NlcyBvZiBhbGwgbWFqb3Igc3ViIG1vZHVsZSBmcm9tIHdlYjMsIGluY2x1ZGluZyBQdWJsaWNNaW50IG1vZHVsZXNcbiAgICAqXG4gICAgKiBbUXVpY2tTdGFydCB0dXRvcmlhbF17QHR1dG9yaWFsIFF1aWNrU3RhcnR9XG4gICAgKiBAZXhhbXBsZSBjb25zdCB3ZWIzID0gbmV3IFB1YmxpY01pbnQoKTtcbiAgICAqL1xuICAgY29uc3RydWN0b3IocHJvdmlkZXIgPSAyMDIwLCBjb25uZWN0aW9uVHlwZSA9ICd3cycsIGN1c3RvbVdlYjNPcHRpb25zID0ge30sIGN1c3RvbUNvbnRyYWN0c09wdGlvbnMgPSB7fSkge1xuICAgICAgY29uc3Qgd2ViM29wdHMgPSB7XG4gICAgICAgICBkZWZhdWx0QmxvY2s6ICdsYXRlc3QnLFxuICAgICAgICAgdHJhbnNhY3Rpb25Db25maXJtYXRpb25CbG9ja3M6IDEsXG4gICAgICAgICAuLi5jdXN0b21XZWIzT3B0aW9uc1xuICAgICAgfVxuXG4gICAgICB0aGlzLnByb3ZpZGVyID0gcHJvdmlkZXIgaW5zdGFuY2VvZiBPYmplY3QgPyBwcm92aWRlciA6IHJlc29sdmVQcm92aWRlcihwcm92aWRlcilcblxuICAgICAgaWYgKHRoaXMucHJvdmlkZXIgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgdGhyb3cgbmV3IEludmFsaWRQcm92aWRlckV4Y2VwdGlvbihgUHJvdmlkZXIgaWRlbnRpZmllciAke3Byb3ZpZGVyfSBub3QgZm91bmRgKVxuICAgICAgfVxuXG4gICAgICB0aGlzLmxvYWRlZFdlYjNQcm92aWRlciA9IGxvYWRQcm92aWRlcldlYjMoY29ubmVjdGlvblR5cGUsIHRoaXMucHJvdmlkZXIpXG5cbiAgICAgIHRoaXMud2ViMyA9IG5ldyBXZWIzKHRoaXMubG9hZGVkV2ViM1Byb3ZpZGVyLCBudWxsLCB3ZWIzb3B0cylcblxuICAgICAgY29uc3QgY29udHJhY3RzID0gY3JlYXRlQ29udHJhY3RzSW50ZXJmYWNlKHRoaXMud2ViMywgdGhpcy5wcm92aWRlci5jaGFpbklkLCBjdXN0b21Db250cmFjdHNPcHRpb25zKVxuXG4gICAgICAvKipcbiAgICAgICAqICBAbmFtZXNwYWNlIHBtXG4gICAgICAgKiAgQHByb3BlcnR5IHsgT2JqZWN0IH0gcG0uY29udHJhY3RzIC0gQ29udHJhY3RzIGluc3RhbmNlc1xuICAgICAgICogIEBwcm9wZXJ0eSB7IEFycmF5PE9iamVjdD4gfSBwbS5wcm92aWRlcnMgLSBMaXN0IG9mIHByb3ZpZGVyc1xuICAgICAgICogIEBwcm9wZXJ0eSB7IFByb3ZpZGVyIH0gcG0ucHJvdmlkZXIgUHJvdmlkZXIgaW4gdXNlXG4gICAgICAgKiAgQHByb3BlcnR5IHsgQWNjb3VudHMgfSBwbS53YWxsZXQgLSBNb2R1bGUgZm9yIG1hbmFnZSBhY2NvdW50cyBhbmQgcmVsYXRlZCBtZXRob2RzIChzaWduLCB0cmFuc2ZlciwgLi4uKVxuICAgICAgICogIEBwcm9wZXJ0eSB7IFV0aWxzIH0gcG0udXRpbHMgLSBbVXRpbHMgbW9kdWxlIF0oVXRpbHMpXG4gICAgICAgKiAgQHByb3BlcnR5IHsgU3RyaW5nIH0gIHBtLnZlcnNpb24gLSBNb2R1bGUgVmVyc2lvblxuICAgICAgICogIEBwcm9wZXJ0eSB7IFF1ZXJ5IH0gcG0ucXVlcnkgLSBNb2R1bGUgd2l0aCBoZWxwZXIgZnVuY3Rpb25zIGZvciBxdWVyeSBibG9ja2NoYWluLlxuICAgICAgICovXG4gICAgICB0aGlzLndlYjMgPSBPYmplY3QuZGVmaW5lUHJvcGVydHkodGhpcy53ZWIzLCAncG0nLCB7XG4gICAgICAgICB2YWx1ZToge1xuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiAgQ29udHJhY3RzIGluc3RhbmNlc1xuICAgICAgICAgICAgICogQG1lbWJlcm9mIHBtXG4gICAgICAgICAgICAgKiBAdHlwZSB7IE9iamVjdCB9XG4gICAgICAgICAgICAgKiBAbmFtZXNwYWNlIHBtLmNvbnRyYWN0c1xuICAgICAgICAgICAgICogQHByb3BlcnR5IHsgT2JqZWN0IH0gY29udHJhY3RzLlVTRCAtIEVSQzIwICBjb250cmFjdCBpbnN0YW5jZSBmb3IgVVNEIHRva2VuXG4gICAgICAgICAgICAgKiBAcHJvcGVydHkgeyBPYmplY3QgfSBjb250cmFjdHMuYWRtaW4uVVNEIC0gTXVsdGkgU2lnbmF0dXJlIGNvbnRyYWN0IGluc3RhbmNlIGZvciBVU0RcbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgY29udHJhY3RzLFxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiAgQ29udHJhY3RzIGluc3RhbmNlc1xuICAgICAgICAgICAgICogQG1lbWJlcm9mIHBtXG4gICAgICAgICAgICAgKiBAdHlwZSB7IE9iamVjdCB9XG4gICAgICAgICAgICAgKiBAbmFtZXNwYWNlIHBtLnByb3ZpZGVyc1xuICAgICAgICAgICAgICogQHByb3BlcnR5IHsgRnVuY3Rpb24gfSBwcm92aWRlcnMuZ2V0UHJvdmlkZXJMaXN0ICAtIEdldCBsaXN0IG9mIHByb3ZpZGVyc1xuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBwcm92aWRlcnM6IGdldFByb3ZpZGVyTGlzdCxcbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogIENvbnRyYWN0cyBpbnN0YW5jZXNcbiAgICAgICAgICAgICAqIEBtZW1iZXJvZiBwbVxuICAgICAgICAgICAgICogQHR5cGUgeyBQcm92aWRlciB9XG4gICAgICAgICAgICAgKiBAbmFtZXNwYWNlIHBtLnByb3ZpZGVyXG4gICAgICAgICAgICAgKiBAcHJvcGVydHkgeyBGdW5jdGlvbiB9IHByb3ZpZGVyIC0gSW5zdGFuY2UgcHJvdmlkZXJcbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgcHJvdmlkZXI6IHRoaXMucHJvdmlkZXIsXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqICBNb2R1bGUgb2Ygd2FsbGV0IGFjdGlvbnMgW21vcmUgaW5mb10oaHR0cHM6Ly93ZWIzanMucmVhZHRoZWRvY3MuaW8vZW4vdjEuMi4wL3dlYjMtZXRoLWFjY291bnRzLmh0bWwjd2FsbGV0KVxuICAgICAgICAgICAgICogQG1lbWJlcm9mIHBtXG4gICAgICAgICAgICAgKiBAdHlwZSB7IE9iamVjdCB9XG4gICAgICAgICAgICAgKiBAbmFtZXNwYWNlIHBtLndhbGxldFxuICAgICAgICAgICAgICogQHByb3BlcnR5IHsgRnVuY3Rpb24gfSB3YWxsZXQuYWRkIC0gQWRkcyBhbiBhY2NvdW50IHVzaW5nIGEgcHJpdmF0ZSBrZXkgb3IgYWNjb3VudCBvYmplY3QgdG8gdGhlIHdhbGxldC5cbiAgICAgICAgICAgICAqIEBwcm9wZXJ0eSB7IEZ1bmN0aW9uIH0gd2FsbGV0LnJlbW92ZSAtIFJlbW92ZXMgYW4gYWNjb3VudCBmcm9tIHRoZSB3YWxsZXQuXG4gICAgICAgICAgICAgKiBAcHJvcGVydHkgeyBGdW5jdGlvbiB9IHdhbGxldC5sb2FkIC0gTG9hZHMgYSB3YWxsZXQgZnJvbSBsb2NhbCBzdG9yYWdlIGFuZCBkZWNyeXB0cyBpdC5cbiAgICAgICAgICAgICAqIEBwcm9wZXJ0eSB7IEZ1bmN0aW9uIH0gd2FsbGV0LmNsZWFyIC0gU2VjdXJlbHkgZW1wdGllcyB0aGUgd2FsbGV0IGFuZCByZW1vdmVzIGFsbCBpdHMgYWNjb3VudHMuXG4gICAgICAgICAgICAgKiBAcHJvcGVydHkgeyBGdW5jdGlvbiB9IHdhbGxldC5lbmNyeXB0IC0gRW5jcnlwdHMgYWxsIHdhbGxldCBhY2NvdW50cyB0byBhbiBhcnJheSBvZiBlbmNyeXB0ZWQga2V5c3RvcmUgdjMgb2JqZWN0cy5cbiAgICAgICAgICAgICAqIEBwcm9wZXJ0eSB7IEZ1bmN0aW9uIH0gd2FsbGV0LmRlY3J5cHQgLSBEZWNyeXB0cyBrZXlzdG9yZSB2MyBvYmplY3RzLlxuICAgICAgICAgICAgICogQHByb3BlcnR5IHsgRnVuY3Rpb24gfSB3YWxsZXQuc2F2ZSAtIFN0b3JlcyB0aGUgd2FsbGV0IGVuY3J5cHRlZCBhbmQgYXMgc3RyaW5nIGluIGxvY2FsIHN0b3JhZ2UuXG4gICAgICAgICAgICAgKiBAcHJvcGVydHkgeyBGdW5jdGlvbiB9IHdhbGxldC5sb2FkIC0gTG9hZHMgYSB3YWxsZXQgZnJvbSBsb2NhbCBzdG9yYWdlIGFuZCBkZWNyeXB0cyBpdC5cbiAgICAgICAgICAgICAqIEBwcm9wZXJ0eSB7IE9iamVjdCB9IHdhbGxldC5hY2NvdW50cyAtIEFjdGlvbnMgZm9yIGFkZGVkIGFjY291bnRzLlxuICAgICAgICAgICAgICogQHByb3BlcnR5IHsgRnVuY3Rpb24gfSB3YWxsZXQuYWNjb3VudHMuZ2V0QmFsYW5jZSAtIEdldCBiYWxhbmNlIG9mIGFjY291bnQgIGBhY2NvdW50cy5nZXRCYWxhbmNlKCcweDAnKWAuXG4gICAgICAgICAgICAgKiBAcHJvcGVydHkgeyBGdW5jdGlvbiB9IHdhbGxldC5hY2NvdW50cy5nZXRCYWxhbmNlcyAtIEdldCBiYWxhbmNlIG9mIGFjY291bnQgYGFjY291bnRzLmdldEJhbGFuY2VzYC5cbiAgICAgICAgICAgICAqIEBwcm9wZXJ0eSB7IEZ1bmN0aW9uIH0gd2FsbGV0LmFjY291bnRzLnRyYW5zZmVyIC0gVHJhbnNmZXIgYmFsYW5jZSB1c2luZyBmaXJzdCB3YWxsZXQgYWNjb3VudC5cbiAgICAgICAgICAgICAqIEBwcm9wZXJ0eSB7IEZ1bmN0aW9uIH0gd2FsbGV0LmFjY291bnRzLnRyYW5zZmVyRnJvbSAtIFRyYW5zZmVyRnJvbSBzYW1lIGFzIHRyYW5zZmVyIGJ1dCBzcGVjaWZ5aW5nIGluZGV4IG9yIGFkZHJlc3MgLlxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICB3YWxsZXQ6IGFjY291bnRzKHRoaXMud2ViMyksXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqICBNb2R1bGUgb2YgdXRpbHNcbiAgICAgICAgICAgICAqIEBtZW1iZXJvZiBwbVxuICAgICAgICAgICAgICogQHR5cGUgeyBPYmplY3QgfVxuICAgICAgICAgICAgICogQG5hbWVzcGFjZSB1dGlsc1xuICAgICAgICAgICAgICovXG4gICAgICAgICAgICB1dGlsczogdXRpbHModGhpcy53ZWIzLnV0aWxzKSxcbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogIFZlcnNpb24gb2YgbW9kdWxlXG4gICAgICAgICAgICAgKiBAbWVtYmVyb2YgcG1cbiAgICAgICAgICAgICAqIEB0eXBlIHsgU3RyaW5nIH1cbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgdmVyc2lvbixcbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogIFZlcnNpb24gb2YgbW9kdWxlXG4gICAgICAgICAgICAgKiBAbWVtYmVyb2YgcG1cbiAgICAgICAgICAgICAqIEB0eXBlIHsgUXVlcnkgfVxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBxdWVyeTogcXVlcnkodGhpcy53ZWIzKVxuICAgICAgICAgfVxuICAgICAgfSlcblxuICAgICAgLy8gRGVsZXRlIG1vZHVsZXMgbm90IGNvbXBhdGlibGUgd2l0aCBjbGllbnRcbiAgICAgIC8vIFRPRE86IG5lZWRzIHJldmlzaW9uIGxhdGVyXG4gICAgICBkZWxldGUgdGhpcy53ZWIzLmV0aC5wZXJzb25hbFxuICAgICAgZGVsZXRlIHRoaXMud2ViMy5ldGguZW5zXG4gICAgICBkZWxldGUgdGhpcy53ZWIzLmJ6elxuICAgICAgZGVsZXRlIHRoaXMud2ViMy5zaGhcblxuICAgICAgcmV0dXJuIHRoaXMud2ViM1xuICAgfVxuXG4gICAvKipcbiAgICAqIFByb3ZpZGVyIGNvbnN0cnVjdG9yXG4gICAgKi9cbiAgIHN0YXRpYyBwcm92aWRlciguLi5hcmdzKSB7XG4gICAgICByZXR1cm4gbmV3IFByb3ZpZGVyKC4uLmFyZ3MpXG4gICB9XG5cbiAgIC8qKlxuICAgICogR2V0IHByb3ZpZGVycyBsaXN0XG4gICAgKi9cbiAgIHN0YXRpYyBwcm92aWRlcnMoKSB7XG4gICAgICByZXR1cm4gZ2V0UHJvdmlkZXJMaXN0KCk7XG4gICB9XG5cbiAgIC8qKlxuICAgICogTW9kdWxlcyB2ZXJzaW9uc1xuICAgICovXG4gICBzdGF0aWMgdmVyc2lvbnMoKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICAgV2ViMzogV2ViMy52ZXJzaW9uLFxuICAgICAgICAgUHVibGljTWludDogdmVyc2lvblxuICAgICAgfVxuICAgfVxufVxuXG4vLyBFeHBvc2UgdXRpbHNcblB1YmxpY01pbnQudXRpbHMgPSB7XG4gICAuLi5XZWIzLnV0aWxzLFxuICAgLi4udXRpbHMoV2ViMy51dGlscylcbn1cblxuZXhwb3J0IGRlZmF1bHQgUHVibGljTWludFxuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVBOztBQUVBOztBQUNBOztBQUVBOztBQU9BOztBQUlBOztBQUNBOzs7Ozs7Ozs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTQSx3QkFBVCxDQUFrQ0MsT0FBbEMsRUFBMkM7RUFDeEMsS0FBS0EsT0FBTCxHQUFlQSxPQUFmO0VBQ0EsS0FBS0MsSUFBTCxHQUFZLDBCQUFaO0FBQ0Y7O0FBRUQsTUFBTUMsVUFBTixDQUFpQjtFQUNkO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0VBQ0dDLFdBQVcsR0FBOEY7SUFBQSxJQUE3RkMsUUFBNkYsdUVBQWxGLElBQWtGO0lBQUEsSUFBNUVDLGNBQTRFLHVFQUEzRCxJQUEyRDtJQUFBLElBQXJEQyxpQkFBcUQsdUVBQWpDLEVBQWlDO0lBQUEsSUFBN0JDLHNCQUE2Qix1RUFBSixFQUFJOztJQUN0RyxJQUFNQyxRQUFRO01BQ1hDLFlBQVksRUFBRSxRQURIO01BRVhDLDZCQUE2QixFQUFFO0lBRnBCLEdBR1JKLGlCQUhRLENBQWQ7O0lBTUEsS0FBS0YsUUFBTCxHQUFnQkEsUUFBUSxZQUFZTyxNQUFwQixHQUE2QlAsUUFBN0IsR0FBd0MsSUFBQVEsMEJBQUEsRUFBZ0JSLFFBQWhCLENBQXhEOztJQUVBLElBQUksS0FBS0EsUUFBTCxLQUFrQlMsU0FBdEIsRUFBaUM7TUFDOUIsTUFBTSxJQUFJZCx3QkFBSiwrQkFBb0RLLFFBQXBELGdCQUFOO0lBQ0Y7O0lBRUQsS0FBS1Usa0JBQUwsR0FBMEIsSUFBQUMsMkJBQUEsRUFBaUJWLGNBQWpCLEVBQWlDLEtBQUtELFFBQXRDLENBQTFCO0lBRUEsS0FBS1ksSUFBTCxHQUFZLElBQUlDLFlBQUosQ0FBUyxLQUFLSCxrQkFBZCxFQUFrQyxJQUFsQyxFQUF3Q04sUUFBeEMsQ0FBWjtJQUVBLElBQU1VLFNBQVMsR0FBRyxJQUFBQyxrQkFBQSxFQUF5QixLQUFLSCxJQUE5QixFQUFvQyxLQUFLWixRQUFMLENBQWNnQixPQUFsRCxFQUEyRGIsc0JBQTNELENBQWxCO0lBRUE7QUFDTjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0lBQ00sS0FBS1MsSUFBTCxHQUFZTCxNQUFNLENBQUNVLGNBQVAsQ0FBc0IsS0FBS0wsSUFBM0IsRUFBaUMsSUFBakMsRUFBdUM7TUFDaERNLEtBQUssRUFBRTtRQUNKO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7UUFDWUosU0FUSTs7UUFVSjtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtRQUNZSyxTQUFTLEVBQUVDLDBCQWpCUDs7UUFrQko7QUFDWjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7UUFDWXBCLFFBQVEsRUFBRSxLQUFLQSxRQXpCWDs7UUEwQko7QUFDWjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7UUFDWXFCLE1BQU0sRUFBRSxJQUFBQyxpQkFBQSxFQUFTLEtBQUtWLElBQWQsQ0E3Q0o7O1FBOENKO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtRQUNZVyxLQUFLLEVBQUUsSUFBQUEsY0FBQSxFQUFNLEtBQUtYLElBQUwsQ0FBVVcsS0FBaEIsQ0FwREg7O1FBcURKO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7UUFDWUMsT0FBTyxFQUFQQSxnQkExREk7O1FBMkRKO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7UUFDWUMsS0FBSyxFQUFFLElBQUFBLGNBQUEsRUFBTSxLQUFLYixJQUFYO01BaEVIO0lBRHlDLENBQXZDLENBQVosQ0E3QnNHLENBa0d0RztJQUNBOztJQUNBLE9BQU8sS0FBS0EsSUFBTCxDQUFVYyxHQUFWLENBQWNDLFFBQXJCO0lBQ0EsT0FBTyxLQUFLZixJQUFMLENBQVVjLEdBQVYsQ0FBY0UsR0FBckI7SUFDQSxPQUFPLEtBQUtoQixJQUFMLENBQVVpQixHQUFqQjtJQUNBLE9BQU8sS0FBS2pCLElBQUwsQ0FBVWtCLEdBQWpCO0lBRUEsT0FBTyxLQUFLbEIsSUFBWjtFQUNGO0VBRUQ7QUFDSDtBQUNBOzs7RUFDa0IsT0FBUlosUUFBUSxHQUFVO0lBQUEsa0NBQU4rQixJQUFNO01BQU5BLElBQU07SUFBQTs7SUFDdEIsT0FBTyxJQUFJQyxtQkFBSixDQUFhLEdBQUdELElBQWhCLENBQVA7RUFDRjtFQUVEO0FBQ0g7QUFDQTs7O0VBQ21CLE9BQVRaLFNBQVMsR0FBRztJQUNoQixPQUFPLElBQUFDLDBCQUFBLEdBQVA7RUFDRjtFQUVEO0FBQ0g7QUFDQTs7O0VBQ2tCLE9BQVJhLFFBQVEsR0FBRztJQUNmLE9BQU87TUFDSnBCLElBQUksRUFBRUEsWUFBQSxDQUFLVyxPQURQO01BRUoxQixVQUFVLEVBQUUwQjtJQUZSLENBQVA7RUFJRjs7QUFuSmEsQyxDQXNKakI7OztBQUNBMUIsVUFBVSxDQUFDeUIsS0FBWCxtQ0FDTVYsWUFBQSxDQUFLVSxLQURYLEdBRU0sSUFBQUEsY0FBQSxFQUFNVixZQUFBLENBQUtVLEtBQVgsQ0FGTjtlQUtlekIsVSJ9