"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * Provider class
 * @class
 */
class Provider {
  /**
   * @constructor
   * @param { String } chainId Chain identifier 
   * @param { String } name Name of chain 
   * @param { String } http Endpoint for http api 
   * @param { String } ws Endpoint for ws api 
   * @param { String } explorer Base URL for explorer 
   * @param { Object } [ opts =  {explorerRoutes: { transactions: '/tx/', address: '/address/', blocks: '/blocks/' } } ] - Base URL for explorer 
   */
  constructor(chainId, name, http, ws, explorer) {
    var opts = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : {};
    this.chainId = chainId;
    this.name = name;
    this.http = http;
    this.ws = ws;
    this.explorer = explorer;
    this.opts = _objectSpread({
      explorerRoutes: {
        transactions: '/tx/',
        address: '/address/',
        blocks: '/blocks/'
      }
    }, opts);
  }
  /**
   * Parse route to transactions give as response with receipt of calls to blockchain
   * @private
   * @param txRoute Transaction route e.g '/tx/'
   */


  explorerRoutesForTransaction(txRoute) {
    // TODO: parse receipt and provide a link to explorer
    this.explorerRoutes.transactions = txRoute;
  }

}

var _default = Provider;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJQcm92aWRlciIsImNvbnN0cnVjdG9yIiwiY2hhaW5JZCIsIm5hbWUiLCJodHRwIiwid3MiLCJleHBsb3JlciIsIm9wdHMiLCJleHBsb3JlclJvdXRlcyIsInRyYW5zYWN0aW9ucyIsImFkZHJlc3MiLCJibG9ja3MiLCJleHBsb3JlclJvdXRlc0ZvclRyYW5zYWN0aW9uIiwidHhSb3V0ZSJdLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9wcm92aWRlcnMvUHJvdmlkZXIuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBQcm92aWRlciBjbGFzc1xuICogQGNsYXNzXG4gKi9cbmNsYXNzIFByb3ZpZGVyIHtcbiAgICAvKipcbiAgICAgKiBAY29uc3RydWN0b3JcbiAgICAgKiBAcGFyYW0geyBTdHJpbmcgfSBjaGFpbklkIENoYWluIGlkZW50aWZpZXIgXG4gICAgICogQHBhcmFtIHsgU3RyaW5nIH0gbmFtZSBOYW1lIG9mIGNoYWluIFxuICAgICAqIEBwYXJhbSB7IFN0cmluZyB9IGh0dHAgRW5kcG9pbnQgZm9yIGh0dHAgYXBpIFxuICAgICAqIEBwYXJhbSB7IFN0cmluZyB9IHdzIEVuZHBvaW50IGZvciB3cyBhcGkgXG4gICAgICogQHBhcmFtIHsgU3RyaW5nIH0gZXhwbG9yZXIgQmFzZSBVUkwgZm9yIGV4cGxvcmVyIFxuICAgICAqIEBwYXJhbSB7IE9iamVjdCB9IFsgb3B0cyA9ICB7ZXhwbG9yZXJSb3V0ZXM6IHsgdHJhbnNhY3Rpb25zOiAnL3R4LycsIGFkZHJlc3M6ICcvYWRkcmVzcy8nLCBibG9ja3M6ICcvYmxvY2tzLycgfSB9IF0gLSBCYXNlIFVSTCBmb3IgZXhwbG9yZXIgXG4gICAgICovXG4gICAgY29uc3RydWN0b3IoY2hhaW5JZCwgbmFtZSwgaHR0cCwgd3MsIGV4cGxvcmVyLCBvcHRzID0ge30pIHtcbiAgICAgICAgdGhpcy5jaGFpbklkID0gY2hhaW5JZDtcbiAgICAgICAgdGhpcy5uYW1lID0gbmFtZTtcbiAgICAgICAgdGhpcy5odHRwID0gaHR0cDtcbiAgICAgICAgdGhpcy53cyA9IHdzO1xuICAgICAgICB0aGlzLmV4cGxvcmVyID0gZXhwbG9yZXI7XG5cblxuICAgICAgICB0aGlzLm9wdHMgPSB7XG4gICAgICAgICAgICBleHBsb3JlclJvdXRlczoge1xuICAgICAgICAgICAgICAgIHRyYW5zYWN0aW9uczogJy90eC8nLFxuICAgICAgICAgICAgICAgIGFkZHJlc3M6ICcvYWRkcmVzcy8nLFxuICAgICAgICAgICAgICAgIGJsb2NrczogJy9ibG9ja3MvJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIC4uLm9wdHNcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFBhcnNlIHJvdXRlIHRvIHRyYW5zYWN0aW9ucyBnaXZlIGFzIHJlc3BvbnNlIHdpdGggcmVjZWlwdCBvZiBjYWxscyB0byBibG9ja2NoYWluXG4gICAgICogQHByaXZhdGVcbiAgICAgKiBAcGFyYW0gdHhSb3V0ZSBUcmFuc2FjdGlvbiByb3V0ZSBlLmcgJy90eC8nXG4gICAgICovXG4gICAgZXhwbG9yZXJSb3V0ZXNGb3JUcmFuc2FjdGlvbih0eFJvdXRlKSB7XG4gICAgICAgIC8vIFRPRE86IHBhcnNlIHJlY2VpcHQgYW5kIHByb3ZpZGUgYSBsaW5rIHRvIGV4cGxvcmVyXG4gICAgICAgIHRoaXMuZXhwbG9yZXJSb3V0ZXMudHJhbnNhY3Rpb25zID0gdHhSb3V0ZTtcbiAgICB9XG5cbn1cblxuZXhwb3J0IGRlZmF1bHQgUHJvdmlkZXI7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE1BQU1BLFFBQU4sQ0FBZTtFQUNYO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtFQUNJQyxXQUFXLENBQUNDLE9BQUQsRUFBVUMsSUFBVixFQUFnQkMsSUFBaEIsRUFBc0JDLEVBQXRCLEVBQTBCQyxRQUExQixFQUErQztJQUFBLElBQVhDLElBQVcsdUVBQUosRUFBSTtJQUN0RCxLQUFLTCxPQUFMLEdBQWVBLE9BQWY7SUFDQSxLQUFLQyxJQUFMLEdBQVlBLElBQVo7SUFDQSxLQUFLQyxJQUFMLEdBQVlBLElBQVo7SUFDQSxLQUFLQyxFQUFMLEdBQVVBLEVBQVY7SUFDQSxLQUFLQyxRQUFMLEdBQWdCQSxRQUFoQjtJQUdBLEtBQUtDLElBQUw7TUFDSUMsY0FBYyxFQUFFO1FBQ1pDLFlBQVksRUFBRSxNQURGO1FBRVpDLE9BQU8sRUFBRSxXQUZHO1FBR1pDLE1BQU0sRUFBRTtNQUhJO0lBRHBCLEdBTU9KLElBTlA7RUFRSDtFQUVEO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7OztFQUNJSyw0QkFBNEIsQ0FBQ0MsT0FBRCxFQUFVO0lBQ2xDO0lBQ0EsS0FBS0wsY0FBTCxDQUFvQkMsWUFBcEIsR0FBbUNJLE9BQW5DO0VBQ0g7O0FBcENVOztlQXdDQWIsUSJ9