"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = {
  chainId: 5777,
  name: 'ganache',
  http: 'http://127.0.0.1:7545/',
  ws: 'ws://127.0.0.1:7545/',
  explorer: 'http://127.0.0.1/'
};
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJjaGFpbklkIiwibmFtZSIsImh0dHAiLCJ3cyIsImV4cGxvcmVyIl0sInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL3Byb3ZpZGVycy9jb25maWcvZ2FuYWNoZS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZGVmYXVsdCB7XG4gICAgY2hhaW5JZDogNTc3NyxcbiAgICBuYW1lOiAnZ2FuYWNoZScsXG4gICAgaHR0cDogJ2h0dHA6Ly8xMjcuMC4wLjE6NzU0NS8nLFxuICAgIHdzOiAnd3M6Ly8xMjcuMC4wLjE6NzU0NS8nLFxuICAgIGV4cGxvcmVyOiAnaHR0cDovLzEyNy4wLjAuMS8nXG59XG4iXSwibWFwcGluZ3MiOiI7Ozs7OztlQUFlO0VBQ1hBLE9BQU8sRUFBRSxJQURFO0VBRVhDLElBQUksRUFBRSxTQUZLO0VBR1hDLElBQUksRUFBRSx3QkFISztFQUlYQyxFQUFFLEVBQUUsc0JBSk87RUFLWEMsUUFBUSxFQUFFO0FBTEMsQyJ9