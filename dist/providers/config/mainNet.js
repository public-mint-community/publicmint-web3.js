"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

/**
 * Providers configuration file
 * @namespace ProviderConfiguration 
 * @property {number}  chainId       - Chain identifier of blockchain
 * @property {string}  name          - Name of blockchain.
 * @property {string}  http          - URL for json rpc with http protocol.
 * @property {string}  ws            - URL for json rpc with websocket protocol (Used for events).
 * @property {number}  explorer      - Explorer url.
 */
var _default = {
  chainId: 2020,
  name: 'mainNet',
  http: 'https://rpc.publicmint.io:8545/',
  ws: 'wss://rpc.publicmint.io:8546/',
  explorer: 'https://explorer.publicmint.io/'
};
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJjaGFpbklkIiwibmFtZSIsImh0dHAiLCJ3cyIsImV4cGxvcmVyIl0sInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL3Byb3ZpZGVycy9jb25maWcvbWFpbk5ldC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIFByb3ZpZGVycyBjb25maWd1cmF0aW9uIGZpbGVcbiAqIEBuYW1lc3BhY2UgUHJvdmlkZXJDb25maWd1cmF0aW9uIFxuICogQHByb3BlcnR5IHtudW1iZXJ9ICBjaGFpbklkICAgICAgIC0gQ2hhaW4gaWRlbnRpZmllciBvZiBibG9ja2NoYWluXG4gKiBAcHJvcGVydHkge3N0cmluZ30gIG5hbWUgICAgICAgICAgLSBOYW1lIG9mIGJsb2NrY2hhaW4uXG4gKiBAcHJvcGVydHkge3N0cmluZ30gIGh0dHAgICAgICAgICAgLSBVUkwgZm9yIGpzb24gcnBjIHdpdGggaHR0cCBwcm90b2NvbC5cbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSAgd3MgICAgICAgICAgICAtIFVSTCBmb3IganNvbiBycGMgd2l0aCB3ZWJzb2NrZXQgcHJvdG9jb2wgKFVzZWQgZm9yIGV2ZW50cykuXG4gKiBAcHJvcGVydHkge251bWJlcn0gIGV4cGxvcmVyICAgICAgLSBFeHBsb3JlciB1cmwuXG4gKi9cbmV4cG9ydCBkZWZhdWx0IHtcbiAgICBjaGFpbklkOiAyMDIwLFxuICAgIG5hbWU6ICdtYWluTmV0JyxcbiAgICBodHRwOiAnaHR0cHM6Ly9ycGMucHVibGljbWludC5pbzo4NTQ1LycsXG4gICAgd3M6ICd3c3M6Ly9ycGMucHVibGljbWludC5pbzo4NTQ2LycsXG4gICAgZXhwbG9yZXI6ICdodHRwczovL2V4cGxvcmVyLnB1YmxpY21pbnQuaW8vJ1xufVxuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO2VBQ2U7RUFDWEEsT0FBTyxFQUFFLElBREU7RUFFWEMsSUFBSSxFQUFFLFNBRks7RUFHWEMsSUFBSSxFQUFFLGlDQUhLO0VBSVhDLEVBQUUsRUFBRSwrQkFKTztFQUtYQyxRQUFRLEVBQUU7QUFMQyxDIn0=