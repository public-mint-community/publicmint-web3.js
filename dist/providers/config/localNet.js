"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = {
  chainId: 9999,
  name: 'localNet',
  http: 'http://localhost:8545/',
  ws: 'ws://localhost:8546/',
  explorer: 'http://localhost/'
};
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJjaGFpbklkIiwibmFtZSIsImh0dHAiLCJ3cyIsImV4cGxvcmVyIl0sInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL3Byb3ZpZGVycy9jb25maWcvbG9jYWxOZXQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGRlZmF1bHQge1xuICAgIGNoYWluSWQ6IDk5OTksXG4gICAgbmFtZTogJ2xvY2FsTmV0JyxcbiAgICBodHRwOiAnaHR0cDovL2xvY2FsaG9zdDo4NTQ1LycsXG4gICAgd3M6ICd3czovL2xvY2FsaG9zdDo4NTQ2LycsXG4gICAgZXhwbG9yZXI6ICdodHRwOi8vbG9jYWxob3N0Lydcbn1cbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7O2VBQWU7RUFDWEEsT0FBTyxFQUFFLElBREU7RUFFWEMsSUFBSSxFQUFFLFVBRks7RUFHWEMsSUFBSSxFQUFFLHdCQUhLO0VBSVhDLEVBQUUsRUFBRSxzQkFKTztFQUtYQyxRQUFRLEVBQUU7QUFMQyxDIn0=