"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = {
  chainId: 2018,
  name: 'devNet',
  http: 'https://rpc.dev.publicmint.io:8545/',
  ws: 'wss://rpc.dev.publicmint.io:8546/',
  explorer: 'https://explorer.dev.publicmint.io/'
};
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJjaGFpbklkIiwibmFtZSIsImh0dHAiLCJ3cyIsImV4cGxvcmVyIl0sInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL3Byb3ZpZGVycy9jb25maWcvZGV2TmV0LmpzIl0sInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBkZWZhdWx0IHtcbiAgICBjaGFpbklkOiAyMDE4LFxuICAgIG5hbWU6ICdkZXZOZXQnLFxuICAgIGh0dHA6ICdodHRwczovL3JwYy5kZXYucHVibGljbWludC5pbzo4NTQ1LycsXG4gICAgd3M6ICd3c3M6Ly9ycGMuZGV2LnB1YmxpY21pbnQuaW86ODU0Ni8nLFxuICAgIGV4cGxvcmVyOiAnaHR0cHM6Ly9leHBsb3Jlci5kZXYucHVibGljbWludC5pby8nXG59XG4iXSwibWFwcGluZ3MiOiI7Ozs7OztlQUFlO0VBQ1hBLE9BQU8sRUFBRSxJQURFO0VBRVhDLElBQUksRUFBRSxRQUZLO0VBR1hDLElBQUksRUFBRSxxQ0FISztFQUlYQyxFQUFFLEVBQUUsbUNBSk87RUFLWEMsUUFBUSxFQUFFO0FBTEMsQyJ9