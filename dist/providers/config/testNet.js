"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = {
  chainId: 2019,
  name: 'testNet',
  http: 'https://rpc.tst.publicmint.io:8545/',
  ws: 'wss://rpc.tst.publicmint.io:8546/',
  explorer: 'https://explorer.tst.publicmint.io/'
};
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJjaGFpbklkIiwibmFtZSIsImh0dHAiLCJ3cyIsImV4cGxvcmVyIl0sInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL3Byb3ZpZGVycy9jb25maWcvdGVzdE5ldC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZGVmYXVsdCB7XG4gICAgY2hhaW5JZDogMjAxOSxcbiAgICBuYW1lOiAndGVzdE5ldCcsXG4gICAgaHR0cDogJ2h0dHBzOi8vcnBjLnRzdC5wdWJsaWNtaW50LmlvOjg1NDUvJyxcbiAgICB3czogJ3dzczovL3JwYy50c3QucHVibGljbWludC5pbzo4NTQ2LycsXG4gICAgZXhwbG9yZXI6ICdodHRwczovL2V4cGxvcmVyLnRzdC5wdWJsaWNtaW50LmlvLydcbn1cbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7O2VBQWU7RUFDWEEsT0FBTyxFQUFFLElBREU7RUFFWEMsSUFBSSxFQUFFLFNBRks7RUFHWEMsSUFBSSxFQUFFLHFDQUhLO0VBSVhDLEVBQUUsRUFBRSxtQ0FKTztFQUtYQyxRQUFRLEVBQUU7QUFMQyxDIn0=