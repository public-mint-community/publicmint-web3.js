"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _mainNet = _interopRequireDefault(require("./mainNet"));

var _testNet = _interopRequireDefault(require("./testNet"));

var _devNet = _interopRequireDefault(require("./devNet"));

var _localNet = _interopRequireDefault(require("./localNet"));

var _ganache = _interopRequireDefault(require("./ganache"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Module exposing all configurations for networks
 * @module ProviderConfigurationFiles
 * @namespace ProviderConfigurationFiles
 * @see ProviderConfiguration
 * @example
 * `
 * Configurations exposed :
 * 
 * |----------|------------------------------------------------------|
 * | Chain Id | Description                                          |
 * |----------|------------------------------------------------------|
 * | mainNet  | Main network                                         |
 * | testNet  | Test network                                         |
 * | devNet   | Client development network (PublicMint internal use) |
 * | localNet | Local network                                        |
 * | ganache  | Local contract development network                   |
 * `
 */
var _default = [_mainNet.default, _testNet.default, _devNet.default, _localNet.default, _ganache.default];
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJtYWluTmV0IiwidGVzdE5ldCIsImRldk5ldCIsImxvY2FsTmV0IiwiZ2FuYWNoZSJdLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9wcm92aWRlcnMvY29uZmlnL2luZGV4LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogTW9kdWxlIGV4cG9zaW5nIGFsbCBjb25maWd1cmF0aW9ucyBmb3IgbmV0d29ya3NcbiAqIEBtb2R1bGUgUHJvdmlkZXJDb25maWd1cmF0aW9uRmlsZXNcbiAqIEBuYW1lc3BhY2UgUHJvdmlkZXJDb25maWd1cmF0aW9uRmlsZXNcbiAqIEBzZWUgUHJvdmlkZXJDb25maWd1cmF0aW9uXG4gKiBAZXhhbXBsZVxuICogYFxuICogQ29uZmlndXJhdGlvbnMgZXhwb3NlZCA6XG4gKiBcbiAqIHwtLS0tLS0tLS0tfC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLXxcbiAqIHwgQ2hhaW4gSWQgfCBEZXNjcmlwdGlvbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHxcbiAqIHwtLS0tLS0tLS0tfC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLXxcbiAqIHwgbWFpbk5ldCAgfCBNYWluIG5ldHdvcmsgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHxcbiAqIHwgdGVzdE5ldCAgfCBUZXN0IG5ldHdvcmsgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHxcbiAqIHwgZGV2TmV0ICAgfCBDbGllbnQgZGV2ZWxvcG1lbnQgbmV0d29yayAoUHVibGljTWludCBpbnRlcm5hbCB1c2UpIHxcbiAqIHwgbG9jYWxOZXQgfCBMb2NhbCBuZXR3b3JrICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHxcbiAqIHwgZ2FuYWNoZSAgfCBMb2NhbCBjb250cmFjdCBkZXZlbG9wbWVudCBuZXR3b3JrICAgICAgICAgICAgICAgICAgIHxcbiAqIGBcbiAqL1xuXG5pbXBvcnQgbWFpbk5ldCBmcm9tICcuL21haW5OZXQnO1xuaW1wb3J0IHRlc3ROZXQgZnJvbSAnLi90ZXN0TmV0JztcbmltcG9ydCBkZXZOZXQgZnJvbSAnLi9kZXZOZXQnO1xuaW1wb3J0IGxvY2FsTmV0IGZyb20gJy4vbG9jYWxOZXQnO1xuaW1wb3J0IGdhbmFjaGUgZnJvbSAnLi9nYW5hY2hlJztcblxuXG5leHBvcnQgZGVmYXVsdCBbXG4gICAgbWFpbk5ldCxcbiAgICB0ZXN0TmV0LFxuICAgIGRldk5ldCxcbiAgICBsb2NhbE5ldCxcbiAgICBnYW5hY2hlXG5dXG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFvQkE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7Ozs7QUF4QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7ZUFTZSxDQUNYQSxnQkFEVyxFQUVYQyxnQkFGVyxFQUdYQyxlQUhXLEVBSVhDLGlCQUpXLEVBS1hDLGdCQUxXLEMifQ==