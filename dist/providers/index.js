"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "Provider", {
  enumerable: true,
  get: function get() {
    return _Provider.default;
  }
});
exports.resolveProviderByNetworkName = exports.resolveProviderByChainId = exports.resolveProvider = exports.loadProviderWeb3 = exports.getProviderList = exports.default = void 0;

var _web = _interopRequireDefault(require("web3"));

var _isNumber = _interopRequireDefault(require("lodash/isNumber"));

var _Provider = _interopRequireDefault(require("./Provider"));

var _config = _interopRequireDefault(require("./config"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Providers module.
 * @module Providers
 * @see ProviderConfigurationFiles
 */

/**
 * Get providers list
 * @return {Provider}
 */
var getProviderList = () => {
  return _config.default;
};
/**
 * Resolves provider by chain id
 * @private
 * @param  {number} chainId Chain identifier (Defaults to chainId 1 representing mainNet)
 * @return {Provider}
 *          Endpoints for public node connection
 */


exports.getProviderList = getProviderList;

var resolveProviderByChainId = function resolveProviderByChainId() {
  var chainId = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
  var id = chainId && typeof chainId.valueOf() === 'number' ? chainId : parseInt(chainId, 10);
  return getProviderList().filter(provider => provider.chainId === id)[0];
};
/**
 * Resolves provider by network name
 * @private
 * @param {string} networkName  ['mainNet', 'testNet', 'localNet', ...]
 * @return {Provider}
 * @example resolveProviderByNetworkName('mainNet')
 */


exports.resolveProviderByChainId = resolveProviderByChainId;

var resolveProviderByNetworkName = networkName => {
  return getProviderList().filter(provider => provider.name === networkName)[0];
};
/**
 * Resolves provider given chain id or name of network 
 * @private
 * @param {(string | number)} network 
 * @example `
 * const provider = resolveProvider(1);
 * const provider = resolveProvider('mainNet');
 * `
 */


exports.resolveProviderByNetworkName = resolveProviderByNetworkName;

var resolveProvider = network => {
  if ((0, _isNumber.default)(network)) {
    return resolveProviderByChainId(network);
  }

  return resolveProviderByNetworkName(network);
};
/**
 * Create web3 provider from Provider object
 * @private
 * @param {*} defaultProvider 
 * @param {*} selectedProvider 
 */


exports.resolveProvider = resolveProvider;

var loadProviderWeb3 = function loadProviderWeb3() {
  var defaultProvider = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'http';
  var selectedProvider = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : getProviderList()[0];
  var providerURL = selectedProvider[defaultProvider];

  if (defaultProvider === 'ws') {
    return new _web.default.providers.WebsocketProvider(providerURL);
  }

  return new _web.default.providers.HttpProvider(providerURL);
};

exports.loadProviderWeb3 = loadProviderWeb3;
var _default = _Provider.default;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJnZXRQcm92aWRlckxpc3QiLCJuZXR3b3JrQ29uZmlnIiwicmVzb2x2ZVByb3ZpZGVyQnlDaGFpbklkIiwiY2hhaW5JZCIsImlkIiwidmFsdWVPZiIsInBhcnNlSW50IiwiZmlsdGVyIiwicHJvdmlkZXIiLCJyZXNvbHZlUHJvdmlkZXJCeU5ldHdvcmtOYW1lIiwibmV0d29ya05hbWUiLCJuYW1lIiwicmVzb2x2ZVByb3ZpZGVyIiwibmV0d29yayIsImlzTnVtYmVyIiwibG9hZFByb3ZpZGVyV2ViMyIsImRlZmF1bHRQcm92aWRlciIsInNlbGVjdGVkUHJvdmlkZXIiLCJwcm92aWRlclVSTCIsIldlYjMiLCJwcm92aWRlcnMiLCJXZWJzb2NrZXRQcm92aWRlciIsIkh0dHBQcm92aWRlciIsIlByb3ZpZGVyIl0sInNvdXJjZXMiOlsiLi4vLi4vc3JjL3Byb3ZpZGVycy9pbmRleC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIFByb3ZpZGVycyBtb2R1bGUuXG4gKiBAbW9kdWxlIFByb3ZpZGVyc1xuICogQHNlZSBQcm92aWRlckNvbmZpZ3VyYXRpb25GaWxlc1xuICovXG5pbXBvcnQgV2ViMyBmcm9tICd3ZWIzJ1xuaW1wb3J0IGlzTnVtYmVyIGZyb20gJ2xvZGFzaC9pc051bWJlcidcbmltcG9ydCBQcm92aWRlciBmcm9tICcuL1Byb3ZpZGVyJ1xuXG5pbXBvcnQgbmV0d29ya0NvbmZpZyBmcm9tICcuL2NvbmZpZydcblxuXG4vKipcbiAqIEdldCBwcm92aWRlcnMgbGlzdFxuICogQHJldHVybiB7UHJvdmlkZXJ9XG4gKi9cbmNvbnN0IGdldFByb3ZpZGVyTGlzdCA9ICgpID0+IHtcbiAgICByZXR1cm4gbmV0d29ya0NvbmZpZztcbn07XG5cbi8qKlxuICogUmVzb2x2ZXMgcHJvdmlkZXIgYnkgY2hhaW4gaWRcbiAqIEBwcml2YXRlXG4gKiBAcGFyYW0gIHtudW1iZXJ9IGNoYWluSWQgQ2hhaW4gaWRlbnRpZmllciAoRGVmYXVsdHMgdG8gY2hhaW5JZCAxIHJlcHJlc2VudGluZyBtYWluTmV0KVxuICogQHJldHVybiB7UHJvdmlkZXJ9XG4gKiAgICAgICAgICBFbmRwb2ludHMgZm9yIHB1YmxpYyBub2RlIGNvbm5lY3Rpb25cbiAqL1xuY29uc3QgcmVzb2x2ZVByb3ZpZGVyQnlDaGFpbklkID0gKGNoYWluSWQgPSAxKSA9PiB7XG4gICAgY29uc3QgaWQgPSBjaGFpbklkICYmIHR5cGVvZiBjaGFpbklkLnZhbHVlT2YoKSA9PT0gJ251bWJlcicgPyBjaGFpbklkIDogcGFyc2VJbnQoY2hhaW5JZCwgMTApO1xuICAgIHJldHVybiBnZXRQcm92aWRlckxpc3QoKS5maWx0ZXIoKHByb3ZpZGVyKSA9PiBwcm92aWRlci5jaGFpbklkID09PSBpZClbMF07XG59XG5cbi8qKlxuICogUmVzb2x2ZXMgcHJvdmlkZXIgYnkgbmV0d29yayBuYW1lXG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHtzdHJpbmd9IG5ldHdvcmtOYW1lICBbJ21haW5OZXQnLCAndGVzdE5ldCcsICdsb2NhbE5ldCcsIC4uLl1cbiAqIEByZXR1cm4ge1Byb3ZpZGVyfVxuICogQGV4YW1wbGUgcmVzb2x2ZVByb3ZpZGVyQnlOZXR3b3JrTmFtZSgnbWFpbk5ldCcpXG4gKi9cbmNvbnN0IHJlc29sdmVQcm92aWRlckJ5TmV0d29ya05hbWUgPSAobmV0d29ya05hbWUpID0+IHtcbiAgICByZXR1cm4gZ2V0UHJvdmlkZXJMaXN0KCkuZmlsdGVyKChwcm92aWRlcikgPT4gcHJvdmlkZXIubmFtZSA9PT0gbmV0d29ya05hbWUpWzBdO1xufVxuXG4vKipcbiAqIFJlc29sdmVzIHByb3ZpZGVyIGdpdmVuIGNoYWluIGlkIG9yIG5hbWUgb2YgbmV0d29yayBcbiAqIEBwcml2YXRlXG4gKiBAcGFyYW0geyhzdHJpbmcgfCBudW1iZXIpfSBuZXR3b3JrIFxuICogQGV4YW1wbGUgYFxuICogY29uc3QgcHJvdmlkZXIgPSByZXNvbHZlUHJvdmlkZXIoMSk7XG4gKiBjb25zdCBwcm92aWRlciA9IHJlc29sdmVQcm92aWRlcignbWFpbk5ldCcpO1xuICogYFxuICovXG5jb25zdCByZXNvbHZlUHJvdmlkZXIgPSAobmV0d29yaykgPT4ge1xuICAgIGlmIChpc051bWJlcihuZXR3b3JrKSkge1xuICAgICAgICByZXR1cm4gcmVzb2x2ZVByb3ZpZGVyQnlDaGFpbklkKG5ldHdvcmspXG4gICAgfVxuXG4gICAgcmV0dXJuIHJlc29sdmVQcm92aWRlckJ5TmV0d29ya05hbWUobmV0d29yaylcbn1cblxuLyoqXG4gKiBDcmVhdGUgd2ViMyBwcm92aWRlciBmcm9tIFByb3ZpZGVyIG9iamVjdFxuICogQHByaXZhdGVcbiAqIEBwYXJhbSB7Kn0gZGVmYXVsdFByb3ZpZGVyIFxuICogQHBhcmFtIHsqfSBzZWxlY3RlZFByb3ZpZGVyIFxuICovXG5jb25zdCBsb2FkUHJvdmlkZXJXZWIzID0gKGRlZmF1bHRQcm92aWRlciA9ICdodHRwJywgc2VsZWN0ZWRQcm92aWRlciA9IGdldFByb3ZpZGVyTGlzdCgpWzBdKSA9PiB7XG4gICAgY29uc3QgcHJvdmlkZXJVUkwgPSBzZWxlY3RlZFByb3ZpZGVyW2RlZmF1bHRQcm92aWRlcl07XG4gICAgaWYgKGRlZmF1bHRQcm92aWRlciA9PT0gJ3dzJykge1xuICAgICAgICByZXR1cm4gbmV3IFdlYjMucHJvdmlkZXJzLldlYnNvY2tldFByb3ZpZGVyKHByb3ZpZGVyVVJMKVxuICAgIH1cbiAgICByZXR1cm4gbmV3IFdlYjMucHJvdmlkZXJzLkh0dHBQcm92aWRlcihwcm92aWRlclVSTClcbn1cblxuZXhwb3J0IHtcbiAgICByZXNvbHZlUHJvdmlkZXJCeUNoYWluSWQsXG4gICAgcmVzb2x2ZVByb3ZpZGVyQnlOZXR3b3JrTmFtZSxcbiAgICByZXNvbHZlUHJvdmlkZXIsXG4gICAgbG9hZFByb3ZpZGVyV2ViMyxcbiAgICBnZXRQcm92aWRlckxpc3QsXG4gICAgUHJvdmlkZXJcbn07XG5cbmV4cG9ydCBkZWZhdWx0IFByb3ZpZGVyO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7O0FBS0E7O0FBQ0E7O0FBQ0E7O0FBRUE7Ozs7QUFUQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBTUEsZUFBZSxHQUFHLE1BQU07RUFDMUIsT0FBT0MsZUFBUDtBQUNILENBRkQ7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFDQSxJQUFNQyx3QkFBd0IsR0FBRyxTQUEzQkEsd0JBQTJCLEdBQWlCO0VBQUEsSUFBaEJDLE9BQWdCLHVFQUFOLENBQU07RUFDOUMsSUFBTUMsRUFBRSxHQUFHRCxPQUFPLElBQUksT0FBT0EsT0FBTyxDQUFDRSxPQUFSLEVBQVAsS0FBNkIsUUFBeEMsR0FBbURGLE9BQW5ELEdBQTZERyxRQUFRLENBQUNILE9BQUQsRUFBVSxFQUFWLENBQWhGO0VBQ0EsT0FBT0gsZUFBZSxHQUFHTyxNQUFsQixDQUEwQkMsUUFBRCxJQUFjQSxRQUFRLENBQUNMLE9BQVQsS0FBcUJDLEVBQTVELEVBQWdFLENBQWhFLENBQVA7QUFDSCxDQUhEO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBQ0EsSUFBTUssNEJBQTRCLEdBQUlDLFdBQUQsSUFBaUI7RUFDbEQsT0FBT1YsZUFBZSxHQUFHTyxNQUFsQixDQUEwQkMsUUFBRCxJQUFjQSxRQUFRLENBQUNHLElBQVQsS0FBa0JELFdBQXpELEVBQXNFLENBQXRFLENBQVA7QUFDSCxDQUZEO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUNBLElBQU1FLGVBQWUsR0FBSUMsT0FBRCxJQUFhO0VBQ2pDLElBQUksSUFBQUMsaUJBQUEsRUFBU0QsT0FBVCxDQUFKLEVBQXVCO0lBQ25CLE9BQU9YLHdCQUF3QixDQUFDVyxPQUFELENBQS9CO0VBQ0g7O0VBRUQsT0FBT0osNEJBQTRCLENBQUNJLE9BQUQsQ0FBbkM7QUFDSCxDQU5EO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUNBLElBQU1FLGdCQUFnQixHQUFHLFNBQW5CQSxnQkFBbUIsR0FBdUU7RUFBQSxJQUF0RUMsZUFBc0UsdUVBQXBELE1BQW9EO0VBQUEsSUFBNUNDLGdCQUE0Qyx1RUFBekJqQixlQUFlLEdBQUcsQ0FBSCxDQUFVO0VBQzVGLElBQU1rQixXQUFXLEdBQUdELGdCQUFnQixDQUFDRCxlQUFELENBQXBDOztFQUNBLElBQUlBLGVBQWUsS0FBSyxJQUF4QixFQUE4QjtJQUMxQixPQUFPLElBQUlHLFlBQUEsQ0FBS0MsU0FBTCxDQUFlQyxpQkFBbkIsQ0FBcUNILFdBQXJDLENBQVA7RUFDSDs7RUFDRCxPQUFPLElBQUlDLFlBQUEsQ0FBS0MsU0FBTCxDQUFlRSxZQUFuQixDQUFnQ0osV0FBaEMsQ0FBUDtBQUNILENBTkQ7OztlQWlCZUssaUIifQ==