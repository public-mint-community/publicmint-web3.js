"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

/**
 * Utils for conversions module.
 * @module Conversions
 * @memberof pm.utils
 */

/**
 * Abstraction for conversion of tokens from USD to token value
 * @private
 * @param {*} toWei web3 
 * @param {*} value 
 */
function _toToken(_ref, value) {
  var {
    toWei
  } = _ref;
  return toWei(value, 'Ether');
}
/**
 * Abstraction for conversion of tokens from token value to USD
 * @private
 * @param {*} fromWei web3 
 * @param {*} value 
 */


function _fromToken(_ref2, value) {
  var {
    fromWei
  } = _ref2;
  return fromWei(value, 'Ether');
}
/**
 * Conversions Module
 * Just a abstraction of web3-utils module .fromWei, .toWei
 * @param { Object } utils module from web3
 * @return { {toToken: Function , fromToken: Function } } Utils functions in object 
 */


function Conversions(utils) {
  return {
    /**
     * Abstraction for conversion of tokens from USD to token value
     * @memberof pm.utils
     * @param { String | Number | BN } value Value to convert
     * @return { String | BN } If a number, or string is given it returns a number string, otherwise a BN.js instance.
     */
    toToken: value => _toToken(utils, value),

    /**
     * Abstraction for conversion of tokens from token value to USD
     * @memberof pm.utils
     * @param { String | Number | BN } value Value to convert
     * @return { String | BN } If a number, or string is given it returns a number string, otherwise a BN.js instance.
     */
    fromToken: value => _fromToken(utils, value)
  };
}

var _default = Conversions;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJ0b1Rva2VuIiwidmFsdWUiLCJ0b1dlaSIsImZyb21Ub2tlbiIsImZyb21XZWkiLCJDb252ZXJzaW9ucyIsInV0aWxzIl0sInNvdXJjZXMiOlsiLi4vLi4vc3JjL3V0aWxzL0NvbnZlcnNpb25zLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogVXRpbHMgZm9yIGNvbnZlcnNpb25zIG1vZHVsZS5cbiAqIEBtb2R1bGUgQ29udmVyc2lvbnNcbiAqIEBtZW1iZXJvZiBwbS51dGlsc1xuICovXG5cblxuLyoqXG4gKiBBYnN0cmFjdGlvbiBmb3IgY29udmVyc2lvbiBvZiB0b2tlbnMgZnJvbSBVU0QgdG8gdG9rZW4gdmFsdWVcbiAqIEBwcml2YXRlXG4gKiBAcGFyYW0geyp9IHRvV2VpIHdlYjMgXG4gKiBAcGFyYW0geyp9IHZhbHVlIFxuICovXG5mdW5jdGlvbiB0b1Rva2VuKHtcbiAgICB0b1dlaVxufSwgdmFsdWUpIHtcbiAgICByZXR1cm4gdG9XZWkodmFsdWUsICdFdGhlcicpXG59XG5cbi8qKlxuICogQWJzdHJhY3Rpb24gZm9yIGNvbnZlcnNpb24gb2YgdG9rZW5zIGZyb20gdG9rZW4gdmFsdWUgdG8gVVNEXG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHsqfSBmcm9tV2VpIHdlYjMgXG4gKiBAcGFyYW0geyp9IHZhbHVlIFxuICovXG5mdW5jdGlvbiBmcm9tVG9rZW4oe1xuICAgIGZyb21XZWlcbn0sIHZhbHVlKSB7XG4gICAgcmV0dXJuIGZyb21XZWkodmFsdWUsICdFdGhlcicpXG59XG5cbi8qKlxuICogQ29udmVyc2lvbnMgTW9kdWxlXG4gKiBKdXN0IGEgYWJzdHJhY3Rpb24gb2Ygd2ViMy11dGlscyBtb2R1bGUgLmZyb21XZWksIC50b1dlaVxuICogQHBhcmFtIHsgT2JqZWN0IH0gdXRpbHMgbW9kdWxlIGZyb20gd2ViM1xuICogQHJldHVybiB7IHt0b1Rva2VuOiBGdW5jdGlvbiAsIGZyb21Ub2tlbjogRnVuY3Rpb24gfSB9IFV0aWxzIGZ1bmN0aW9ucyBpbiBvYmplY3QgXG4gKi9cbmZ1bmN0aW9uIENvbnZlcnNpb25zKHV0aWxzKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgICAgLyoqXG4gICAgICAgICAqIEFic3RyYWN0aW9uIGZvciBjb252ZXJzaW9uIG9mIHRva2VucyBmcm9tIFVTRCB0byB0b2tlbiB2YWx1ZVxuICAgICAgICAgKiBAbWVtYmVyb2YgcG0udXRpbHNcbiAgICAgICAgICogQHBhcmFtIHsgU3RyaW5nIHwgTnVtYmVyIHwgQk4gfSB2YWx1ZSBWYWx1ZSB0byBjb252ZXJ0XG4gICAgICAgICAqIEByZXR1cm4geyBTdHJpbmcgfCBCTiB9IElmIGEgbnVtYmVyLCBvciBzdHJpbmcgaXMgZ2l2ZW4gaXQgcmV0dXJucyBhIG51bWJlciBzdHJpbmcsIG90aGVyd2lzZSBhIEJOLmpzIGluc3RhbmNlLlxuICAgICAgICAgKi9cbiAgICAgICAgdG9Ub2tlbjogKHZhbHVlKSA9PiB0b1Rva2VuKHV0aWxzLCB2YWx1ZSksXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBBYnN0cmFjdGlvbiBmb3IgY29udmVyc2lvbiBvZiB0b2tlbnMgZnJvbSB0b2tlbiB2YWx1ZSB0byBVU0RcbiAgICAgICAgICogQG1lbWJlcm9mIHBtLnV0aWxzXG4gICAgICAgICAqIEBwYXJhbSB7IFN0cmluZyB8IE51bWJlciB8IEJOIH0gdmFsdWUgVmFsdWUgdG8gY29udmVydFxuICAgICAgICAgKiBAcmV0dXJuIHsgU3RyaW5nIHwgQk4gfSBJZiBhIG51bWJlciwgb3Igc3RyaW5nIGlzIGdpdmVuIGl0IHJldHVybnMgYSBudW1iZXIgc3RyaW5nLCBvdGhlcndpc2UgYSBCTi5qcyBpbnN0YW5jZS5cbiAgICAgICAgICovXG4gICAgICAgIGZyb21Ub2tlbjogKHZhbHVlKSA9PiBmcm9tVG9rZW4odXRpbHMsIHZhbHVlKVxuICAgIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgQ29udmVyc2lvbnM7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVNBLFFBQVQsT0FFR0MsS0FGSCxFQUVVO0VBQUEsSUFGTztJQUNiQztFQURhLENBRVA7RUFDTixPQUFPQSxLQUFLLENBQUNELEtBQUQsRUFBUSxPQUFSLENBQVo7QUFDSDtBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EsU0FBU0UsVUFBVCxRQUVHRixLQUZILEVBRVU7RUFBQSxJQUZTO0lBQ2ZHO0VBRGUsQ0FFVDtFQUNOLE9BQU9BLE9BQU8sQ0FBQ0gsS0FBRCxFQUFRLE9BQVIsQ0FBZDtBQUNIO0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSxTQUFTSSxXQUFULENBQXFCQyxLQUFyQixFQUE0QjtFQUN4QixPQUFPO0lBQ0g7QUFDUjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0lBQ1FOLE9BQU8sRUFBR0MsS0FBRCxJQUFXRCxRQUFPLENBQUNNLEtBQUQsRUFBUUwsS0FBUixDQVB4Qjs7SUFRSDtBQUNSO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7SUFDUUUsU0FBUyxFQUFHRixLQUFELElBQVdFLFVBQVMsQ0FBQ0csS0FBRCxFQUFRTCxLQUFSO0VBZDVCLENBQVA7QUFnQkg7O2VBRWNJLFcifQ==