"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = _default;

var _Conversions = _interopRequireDefault(require("./Conversions"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _default(web3In) {
  return _objectSpread(_objectSpread({}, (0, _Conversions.default)(web3In)), {}, {
    /** 
     * sha256
     * To mimic the sha3 behaviour of solidity use soliditySha3
     * @memberof pm.utils
     * @param { String } - A String to hash
     * @return { String } - The result hash
     */
    sha256: key => {
      return web3In.sha3(key);
    },

    /** 
     * isTransactionHash
     * Check if string is a transaction hash 
     * @memberof pm.utils
     * @param { String } - A String to hash
     * @return { Boolean } - Return true if is transactionHash otherwise false 
     */
    isTransactionHash: transactionHash => {
      var pattern = /0x([A-Fa-f0-9]{64})/;
      return pattern.test(transactionHash);
    }
  });
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJ3ZWIzSW4iLCJDb252ZXJzaW9ucyIsInNoYTI1NiIsImtleSIsInNoYTMiLCJpc1RyYW5zYWN0aW9uSGFzaCIsInRyYW5zYWN0aW9uSGFzaCIsInBhdHRlcm4iLCJ0ZXN0Il0sInNvdXJjZXMiOlsiLi4vLi4vc3JjL3V0aWxzL2luZGV4LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogTW9kdWxlIFV0aWxzXG4gKi9cblxuaW1wb3J0IENvbnZlcnNpb25zIGZyb20gJy4vQ29udmVyc2lvbnMnO1xuXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiAod2ViM0luKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgICAgLi4uQ29udmVyc2lvbnMod2ViM0luKSxcbiAgICAgICAgLyoqIFxuICAgICAgICAgKiBzaGEyNTZcbiAgICAgICAgICogVG8gbWltaWMgdGhlIHNoYTMgYmVoYXZpb3VyIG9mIHNvbGlkaXR5IHVzZSBzb2xpZGl0eVNoYTNcbiAgICAgICAgICogQG1lbWJlcm9mIHBtLnV0aWxzXG4gICAgICAgICAqIEBwYXJhbSB7IFN0cmluZyB9IC0gQSBTdHJpbmcgdG8gaGFzaFxuICAgICAgICAgKiBAcmV0dXJuIHsgU3RyaW5nIH0gLSBUaGUgcmVzdWx0IGhhc2hcbiAgICAgICAgICovXG4gICAgICAgIHNoYTI1NjogKGtleSkgPT4ge1xuICAgICAgICAgICAgcmV0dXJuIHdlYjNJbi5zaGEzKGtleSk7XG4gICAgICAgIH0sXG4gICAgICAgIC8qKiBcbiAgICAgICAgICogaXNUcmFuc2FjdGlvbkhhc2hcbiAgICAgICAgICogQ2hlY2sgaWYgc3RyaW5nIGlzIGEgdHJhbnNhY3Rpb24gaGFzaCBcbiAgICAgICAgICogQG1lbWJlcm9mIHBtLnV0aWxzXG4gICAgICAgICAqIEBwYXJhbSB7IFN0cmluZyB9IC0gQSBTdHJpbmcgdG8gaGFzaFxuICAgICAgICAgKiBAcmV0dXJuIHsgQm9vbGVhbiB9IC0gUmV0dXJuIHRydWUgaWYgaXMgdHJhbnNhY3Rpb25IYXNoIG90aGVyd2lzZSBmYWxzZSBcbiAgICAgICAgICovXG4gICAgICAgIGlzVHJhbnNhY3Rpb25IYXNoOiAodHJhbnNhY3Rpb25IYXNoKSA9PiB7XG4gICAgICAgICAgICBjb25zdCBwYXR0ZXJuID0gLzB4KFtBLUZhLWYwLTldezY0fSkvO1xuICAgICAgICAgICAgcmV0dXJuIHBhdHRlcm4udGVzdCh0cmFuc2FjdGlvbkhhc2gpO1xuICAgICAgICB9XG4gICAgfVxufVxuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBSUE7Ozs7Ozs7Ozs7QUFFZSxrQkFBVUEsTUFBVixFQUFrQjtFQUM3Qix1Q0FDTyxJQUFBQyxvQkFBQSxFQUFZRCxNQUFaLENBRFA7SUFFSTtBQUNSO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNRRSxNQUFNLEVBQUdDLEdBQUQsSUFBUztNQUNiLE9BQU9ILE1BQU0sQ0FBQ0ksSUFBUCxDQUFZRCxHQUFaLENBQVA7SUFDSCxDQVhMOztJQVlJO0FBQ1I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0lBQ1FFLGlCQUFpQixFQUFHQyxlQUFELElBQXFCO01BQ3BDLElBQU1DLE9BQU8sR0FBRyxxQkFBaEI7TUFDQSxPQUFPQSxPQUFPLENBQUNDLElBQVIsQ0FBYUYsZUFBYixDQUFQO0lBQ0g7RUF0Qkw7QUF3QkgifQ==