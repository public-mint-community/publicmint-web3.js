"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

/**
 * MultiSignature configuration file
 * @namespace ContractsConfiguration 
 * @property {string}  name          - Contract name (must be unique).
 * @property {string}  deployType    - Type of deploy: 
 *  - `genesis` load one address
 *  - `manual`  load config by chain id.
 * @property {string}  abi    - ABI name
 * @property {string}  abiCommitHash - Commit hash of abi
 * @property {Array.<Object.<chainId, address>>}  deployInfo - Info with address and chainId
 * @property {Array.<Object.<chainId, address>>}  adminsTo - Info with address and chainId
 * 
 * @property {string}  target - MultiSig target
 * @property {Array.string}  proxyMethods - MultiSig target methods to call
 */
var _default = [{
  "name": "USD",
  "abi": "GNOSIS",
  "abiCommitHash": "1046ead915e6bdb9e4f2db7ba99a20d901b4ca06",
  "deployType": "genesis",
  "deployInfo": [{
    "address": "0x0000000000000000000000000000000000001010"
  }],
  "adminsTo": [{
    "target": "token.USD",
    "proxyMethods": ["deposit", "depositFromUSDC", "permitWithdrawalUSDC", "forbidWithdrawalUSDC", "setUSDCmaxWithdrawAllowed", "setUSDCmaxPoolPercentage", "addUsdcChain", "removeUsdcChain"]
  }]
}, {
  "name": "GasManager",
  "abi": "GNOSIS",
  "abiCommitHash": "34aa21f2f25e99ccfba94b2a55707d28857b48bb",
  "deployType": "genesis",
  "deployInfo": [{
    "address": "0x0000000000000000000000000000000000001020"
  }],
  "adminsTo": [{
    "target": "operations.GasManager",
    "proxyMethods": ["addContractWhitelist", "rmContractWhitelisted", "addSenderWhitelist", "rmSenderWhitelisted", "setGasPrice", "addERC", "rmERC", "addFee", "enableFee", "disableFee", "updateFeeAmount", "updatePercentageFee", "addSignatureWithFee", "rmSignatureWithFee", "transferTokens"]
  }]
}];
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2NvbnRyYWN0cy9hZG1pbi9jb25maWcuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBNdWx0aVNpZ25hdHVyZSBjb25maWd1cmF0aW9uIGZpbGVcbiAqIEBuYW1lc3BhY2UgQ29udHJhY3RzQ29uZmlndXJhdGlvbiBcbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSAgbmFtZSAgICAgICAgICAtIENvbnRyYWN0IG5hbWUgKG11c3QgYmUgdW5pcXVlKS5cbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSAgZGVwbG95VHlwZSAgICAtIFR5cGUgb2YgZGVwbG95OiBcbiAqICAtIGBnZW5lc2lzYCBsb2FkIG9uZSBhZGRyZXNzXG4gKiAgLSBgbWFudWFsYCAgbG9hZCBjb25maWcgYnkgY2hhaW4gaWQuXG4gKiBAcHJvcGVydHkge3N0cmluZ30gIGFiaSAgICAtIEFCSSBuYW1lXG4gKiBAcHJvcGVydHkge3N0cmluZ30gIGFiaUNvbW1pdEhhc2ggLSBDb21taXQgaGFzaCBvZiBhYmlcbiAqIEBwcm9wZXJ0eSB7QXJyYXkuPE9iamVjdC48Y2hhaW5JZCwgYWRkcmVzcz4+fSAgZGVwbG95SW5mbyAtIEluZm8gd2l0aCBhZGRyZXNzIGFuZCBjaGFpbklkXG4gKiBAcHJvcGVydHkge0FycmF5LjxPYmplY3QuPGNoYWluSWQsIGFkZHJlc3M+Pn0gIGFkbWluc1RvIC0gSW5mbyB3aXRoIGFkZHJlc3MgYW5kIGNoYWluSWRcbiAqIFxuICogQHByb3BlcnR5IHtzdHJpbmd9ICB0YXJnZXQgLSBNdWx0aVNpZyB0YXJnZXRcbiAqIEBwcm9wZXJ0eSB7QXJyYXkuc3RyaW5nfSAgcHJveHlNZXRob2RzIC0gTXVsdGlTaWcgdGFyZ2V0IG1ldGhvZHMgdG8gY2FsbFxuICovXG5leHBvcnQgZGVmYXVsdCBbe1xuICAgIFwibmFtZVwiOiBcIlVTRFwiLFxuICAgIFwiYWJpXCI6IFwiR05PU0lTXCIsXG4gICAgXCJhYmlDb21taXRIYXNoXCI6IFwiMTA0NmVhZDkxNWU2YmRiOWU0ZjJkYjdiYTk5YTIwZDkwMWI0Y2EwNlwiLFxuICAgIFwiZGVwbG95VHlwZVwiOiBcImdlbmVzaXNcIixcbiAgICBcImRlcGxveUluZm9cIjogW3tcbiAgICAgICAgXCJhZGRyZXNzXCI6IFwiMHgwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAxMDEwXCJcbiAgICB9XSxcbiAgICBcImFkbWluc1RvXCI6IFt7XG4gICAgICAgIFwidGFyZ2V0XCI6IFwidG9rZW4uVVNEXCIsXG4gICAgICAgIFwicHJveHlNZXRob2RzXCI6IFtcImRlcG9zaXRcIiwgXCJkZXBvc2l0RnJvbVVTRENcIiwgXCJwZXJtaXRXaXRoZHJhd2FsVVNEQ1wiLCBcImZvcmJpZFdpdGhkcmF3YWxVU0RDXCIsIFwic2V0VVNEQ21heFdpdGhkcmF3QWxsb3dlZFwiLCBcInNldFVTRENtYXhQb29sUGVyY2VudGFnZVwiLCBcImFkZFVzZGNDaGFpblwiLCBcInJlbW92ZVVzZGNDaGFpblwiXVxuICAgIH1dXG59LCB7XG4gICAgXCJuYW1lXCI6IFwiR2FzTWFuYWdlclwiLFxuICAgIFwiYWJpXCI6IFwiR05PU0lTXCIsXG4gICAgXCJhYmlDb21taXRIYXNoXCI6IFwiMzRhYTIxZjJmMjVlOTljY2ZiYTk0YjJhNTU3MDdkMjg4NTdiNDhiYlwiLFxuICAgIFwiZGVwbG95VHlwZVwiOiBcImdlbmVzaXNcIixcbiAgICBcImRlcGxveUluZm9cIjogW3tcbiAgICAgICAgXCJhZGRyZXNzXCI6IFwiMHgwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAxMDIwXCJcbiAgICB9XSxcbiAgICBcImFkbWluc1RvXCI6IFt7XG4gICAgICAgIFwidGFyZ2V0XCI6IFwib3BlcmF0aW9ucy5HYXNNYW5hZ2VyXCIsXG4gICAgICAgIFwicHJveHlNZXRob2RzXCI6IFtcImFkZENvbnRyYWN0V2hpdGVsaXN0XCIsIFwicm1Db250cmFjdFdoaXRlbGlzdGVkXCIsIFwiYWRkU2VuZGVyV2hpdGVsaXN0XCIsIFwicm1TZW5kZXJXaGl0ZWxpc3RlZFwiLCBcInNldEdhc1ByaWNlXCIsIFwiYWRkRVJDXCIsIFwicm1FUkNcIiwgXCJhZGRGZWVcIiwgXCJlbmFibGVGZWVcIiwgXCJkaXNhYmxlRmVlXCIsIFwidXBkYXRlRmVlQW1vdW50XCIsIFwidXBkYXRlUGVyY2VudGFnZUZlZVwiLCBcImFkZFNpZ25hdHVyZVdpdGhGZWVcIiwgXCJybVNpZ25hdHVyZVdpdGhGZWVcIiwgXCJ0cmFuc2ZlclRva2Vuc1wiXVxuICAgIH1dXG59XVxuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO2VBQ2UsQ0FBQztFQUNaLFFBQVEsS0FESTtFQUVaLE9BQU8sUUFGSztFQUdaLGlCQUFpQiwwQ0FITDtFQUlaLGNBQWMsU0FKRjtFQUtaLGNBQWMsQ0FBQztJQUNYLFdBQVc7RUFEQSxDQUFELENBTEY7RUFRWixZQUFZLENBQUM7SUFDVCxVQUFVLFdBREQ7SUFFVCxnQkFBZ0IsQ0FBQyxTQUFELEVBQVksaUJBQVosRUFBK0Isc0JBQS9CLEVBQXVELHNCQUF2RCxFQUErRSwyQkFBL0UsRUFBNEcsMEJBQTVHLEVBQXdJLGNBQXhJLEVBQXdKLGlCQUF4SjtFQUZQLENBQUQ7QUFSQSxDQUFELEVBWVo7RUFDQyxRQUFRLFlBRFQ7RUFFQyxPQUFPLFFBRlI7RUFHQyxpQkFBaUIsMENBSGxCO0VBSUMsY0FBYyxTQUpmO0VBS0MsY0FBYyxDQUFDO0lBQ1gsV0FBVztFQURBLENBQUQsQ0FMZjtFQVFDLFlBQVksQ0FBQztJQUNULFVBQVUsdUJBREQ7SUFFVCxnQkFBZ0IsQ0FBQyxzQkFBRCxFQUF5Qix1QkFBekIsRUFBa0Qsb0JBQWxELEVBQXdFLHFCQUF4RSxFQUErRixhQUEvRixFQUE4RyxRQUE5RyxFQUF3SCxPQUF4SCxFQUFpSSxRQUFqSSxFQUEySSxXQUEzSSxFQUF3SixZQUF4SixFQUFzSyxpQkFBdEssRUFBeUwscUJBQXpMLEVBQWdOLHFCQUFoTixFQUF1TyxvQkFBdk8sRUFBNlAsZ0JBQTdQO0VBRlAsQ0FBRDtBQVJiLENBWlksQyJ9