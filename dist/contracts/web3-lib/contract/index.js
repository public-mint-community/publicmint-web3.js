"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _readOnlyError(name) { throw new TypeError("\"" + name + "\" is read-only"); }

/* eslint-disable no-case-declarations */

/* eslint-disable no-restricted-globals */

/* eslint-disable prefer-spread */

/* eslint-disable prefer-rest-params */

/* eslint-disable consistent-return */

/* eslint-disable no-const-assign */

/* eslint-disable prefer-destructuring */

/* eslint-disable no-param-reassign */

/* eslint-disable func-names */

/* eslint-disable no-underscore-dangle */
// SOURCE: https://raw.githubusercontent.com/ChainSafe/web3.js/v1.3.1/packages/web3-eth-contract/src/index.js
var _ = require('underscore');

var core = require('web3-core');

var Method = require('web3-core-method');

var utils = require('web3-utils');

var Subscription = require('web3-core-subscriptions').subscription;

var formatters = require('web3-core-helpers').formatters;

var errors = require('web3-core-helpers').errors;

var promiEvent = require('web3-core-promievent');

var abi = require('web3-eth-abi');
/**
 * Should be called to create new contract instance
 *
 * @method Contract
 * @constructor
 * @param {Array} jsonInterface
 * @param {String} address
 * @param {Object} options
 */


var Contract = function Contract(jsonInterface, address, options) {
  var _this = this,
      args = Array.prototype.slice.call(arguments);

  if (!(this instanceof Contract)) {
    throw new Error('Please use the "new" keyword to instantiate a web3.eth.contract() object!');
  } // sets _requestManager


  core.packageInit(this, [this.constructor.currentProvider]);
  this.clearSubscriptions = this._requestManager.clearSubscriptions;

  if (!jsonInterface || !Array.isArray(jsonInterface)) {
    throw new Error('You must provide the json interface of the contract when instantiating a contract object.');
  } // create the options object


  this.options = {};
  var lastArg = args[args.length - 1];

  if (_.isObject(lastArg) && !_.isArray(lastArg)) {
    options = lastArg;
    this.options = _.extend(this.options, this._getOrSetDefaultOptions(options));

    if (_.isObject(address)) {
      address = null;
    }
  } // set address


  Object.defineProperty(this.options, 'address', {
    set(value) {
      if (value) {
        _this._address = utils.toChecksumAddress(formatters.inputAddressFormatter(value));
      }
    },

    get() {
      return _this._address;
    },

    enumerable: true
  }); // add method and event signatures, when the jsonInterface gets set

  Object.defineProperty(this.options, 'jsonInterface', {
    set(value) {
      _this.methods = {};
      _this.events = {};
      _this._jsonInterface = value.map(function (method) {
        var func;
        var funcName; // make constant and payable backwards compatible

        method.constant = method.stateMutability === "view" || method.stateMutability === "pure" || method.constant;
        method.payable = method.stateMutability === "payable" || method.payable;

        if (method.name) {
          funcName = utils._jsonInterfaceMethodToString(method);
        } // function


        if (method.type === 'function') {
          method.signature = abi.encodeFunctionSignature(funcName);
          func = _this._createTxObject.bind({
            method,
            parent: _this
          }); // add method only if not one already exists

          if (!_this.methods[method.name]) {
            _this.methods[method.name] = func;
          } else {
            var cascadeFunc = _this._createTxObject.bind({
              method,
              parent: _this,
              nextMethod: _this.methods[method.name]
            });

            _this.methods[method.name] = cascadeFunc;
          } // definitely add the method based on its signature


          _this.methods[method.signature] = func; // add method by name

          _this.methods[funcName] = func; // event
        } else if (method.type === 'event') {
          method.signature = abi.encodeEventSignature(funcName);

          var event = _this._on.bind(_this, method.signature); // add method only if not already exists


          if (!_this.events[method.name] || _this.events[method.name].name === 'bound ') _this.events[method.name] = event; // definitely add the method based on its signature

          _this.events[method.signature] = event; // add event by name

          _this.events[funcName] = event;
        }

        ;
        return method;
      }); // add allEvents

      _this.events.allEvents = _this._on.bind(_this, 'allevents');
      return _this._jsonInterface;
    },

    get() {
      return _this._jsonInterface;
    },

    enumerable: true
  }); // get default account from the Class

  var defaultAccount = this.constructor.defaultAccount;
  var defaultBlock = this.constructor.defaultBlock || 'latest';
  Object.defineProperty(this, 'handleRevert', {
    get() {
      if (_this.options.handleRevert === false || _this.options.handleRevert === true) {
        return _this.options.handleRevert;
      }

      return this.constructor.handleRevert;
    },

    set(val) {
      _this.options.handleRevert = val;
    },

    enumerable: true
  });
  Object.defineProperty(this, 'defaultCommon', {
    get() {
      return _this.options.common || this.constructor.defaultCommon;
    },

    set(val) {
      _this.options.common = val;
    },

    enumerable: true
  });
  Object.defineProperty(this, 'defaultHardfork', {
    get() {
      return _this.options.hardfork || this.constructor.defaultHardfork;
    },

    set(val) {
      _this.options.hardfork = val;
    },

    enumerable: true
  });
  Object.defineProperty(this, 'defaultChain', {
    get() {
      return _this.options.chain || this.constructor.defaultChain;
    },

    set(val) {
      _this.options.chain = val;
    },

    enumerable: true
  });
  Object.defineProperty(this, 'transactionPollingTimeout', {
    get() {
      if (_this.options.transactionPollingTimeout === 0) {
        return _this.options.transactionPollingTimeout;
      }

      return _this.options.transactionPollingTimeout || this.constructor.transactionPollingTimeout;
    },

    set(val) {
      _this.options.transactionPollingTimeout = val;
    },

    enumerable: true
  });
  Object.defineProperty(this, 'transactionConfirmationBlocks', {
    get() {
      if (_this.options.transactionConfirmationBlocks === 0) {
        return _this.options.transactionConfirmationBlocks;
      }

      return _this.options.transactionConfirmationBlocks || this.constructor.transactionConfirmationBlocks;
    },

    set(val) {
      _this.options.transactionConfirmationBlocks = val;
    },

    enumerable: true
  });
  Object.defineProperty(this, 'transactionBlockTimeout', {
    get() {
      if (_this.options.transactionBlockTimeout === 0) {
        return _this.options.transactionBlockTimeout;
      }

      return _this.options.transactionBlockTimeout || this.constructor.transactionBlockTimeout;
    },

    set(val) {
      _this.options.transactionBlockTimeout = val;
    },

    enumerable: true
  });
  Object.defineProperty(this, 'defaultAccount', {
    get() {
      return defaultAccount;
    },

    set(val) {
      if (val) {
        utils.toChecksumAddress(formatters.inputAddressFormatter(val)), _readOnlyError("defaultAccount");
      }

      return val;
    },

    enumerable: true
  });
  Object.defineProperty(this, 'defaultBlock', {
    get() {
      return defaultBlock;
    },

    set(val) {
      val, _readOnlyError("defaultBlock");
      return val;
    },

    enumerable: true
  }); // properties

  this.methods = {};
  this.events = {};
  this._address = null;
  this._jsonInterface = []; // set getter/setter properties

  this.options.address = address;
  this.options.jsonInterface = jsonInterface;
};

Contract.setProvider = function (provider, accounts) {
  // Contract.currentProvider = provider;
  core.packageInit(this, [provider]);
  this._ethAccounts = accounts;
};
/**
 * Get the callback and modify the array if necessary
 *
 * @method _getCallback
 * @param {Array} args
 * @return {Function} the callback
 */


Contract.prototype._getCallback = function getCallback(args) {
  if (args && _.isFunction(args[args.length - 1])) {
    return args.pop(); // modify the args array!
  }
};
/**
 * Checks that no listener with name "newListener" or "removeListener" is added.
 *
 * @method _checkListener
 * @param {String} type
 * @param {String} event
 * @return {Object} the contract instance
 */


Contract.prototype._checkListener = function (type, event) {
  if (event === type) {
    throw new Error('The event "' + type + '" is a reserved event name, you can\'t use it.');
  }
};
/**
 * Use default values, if options are not available
 *
 * @method _getOrSetDefaultOptions
 * @param {Object} options the options gived by the user
 * @return {Object} the options with gaps filled by defaults
 */


Contract.prototype._getOrSetDefaultOptions = function getOrSetDefaultOptions(options) {
  var gasPrice = options.gasPrice ? String(options.gasPrice) : null;
  var from = options.from ? utils.toChecksumAddress(formatters.inputAddressFormatter(options.from)) : null;
  options.data = options.data || this.options.data;
  options.from = from || this.options.from;
  options.gasPrice = gasPrice || this.options.gasPrice;
  options.gas = options.gas || options.gasLimit || this.options.gas; // TODO replace with only gasLimit?

  delete options.gasLimit;
  return options;
};
/**
 * Should be used to encode indexed params and options to one final object
 *
 * @method _encodeEventABI
 * @param {Object} event
 * @param {Object} options
 * @return {Object} everything combined together and encoded
 */


Contract.prototype._encodeEventABI = function (event, options) {
  options = options || {};
  var filter = options.filter || {};
  var result = {};
  ['fromBlock', 'toBlock'].filter(function (f) {
    return options[f] !== undefined;
  }).forEach(function (f) {
    result[f] = formatters.inputBlockNumberFormatter(options[f]);
  }); // use given topics

  if (_.isArray(options.topics)) {
    result.topics = options.topics; // create topics based on filter
  } else {
    result.topics = []; // add event signature

    if (event && !event.anonymous && event.name !== 'ALLEVENTS') {
      result.topics.push(event.signature);
    } // add event topics (indexed arguments)


    if (event.name !== 'ALLEVENTS') {
      var indexedTopics = event.inputs.filter(function (i) {
        return i.indexed === true;
      }).map(function (i) {
        var value = filter[i.name];

        if (!value) {
          return null;
        } // TODO: https://github.com/ethereum/web3.js/issues/344
        // TODO: deal properly with components


        if (_.isArray(value)) {
          return value.map(function (v) {
            return abi.encodeParameter(i.type, v);
          });
        }

        return abi.encodeParameter(i.type, value);
      });
      result.topics = result.topics.concat(indexedTopics);
    }

    if (!result.topics.length) delete result.topics;
  }

  if (this.options.address) {
    result.address = this.options.address.toLowerCase();
  }

  return result;
};
/**
 * Should be used to decode indexed params and options
 *
 * @method _decodeEventABI
 * @param {Object} data
 * @return {Object} result object with decoded indexed && not indexed params
 */


Contract.prototype._decodeEventABI = function (data) {
  var event = this;
  data.data = data.data || '';
  data.topics = data.topics || [];
  var result = formatters.outputLogFormatter(data); // if allEvents get the right event

  if (event.name === 'ALLEVENTS') {
    event = event.jsonInterface.find(function (intf) {
      return intf.signature === data.topics[0];
    }) || {
      anonymous: true
    };
  } // create empty inputs if none are present (e.g. anonymous events on allEvents)


  event.inputs = event.inputs || []; // Handle case where an event signature shadows the current ABI with non-identical
  // arg indexing. If # of topics doesn't match, event is anon.

  if (!event.anonymous) {
    var indexedInputs = 0;
    event.inputs.forEach(input => input.indexed ? indexedInputs++ : null);

    if (indexedInputs > 0 && data.topics.length !== indexedInputs + 1) {
      event = {
        anonymous: true,
        inputs: []
      };
    }
  }

  var argTopics = event.anonymous ? data.topics : data.topics.slice(1);
  result.returnValues = abi.decodeLog(event.inputs, data.data, argTopics);
  delete result.returnValues.__length__; // add name

  result.event = event.name; // add signature

  result.signature = event.anonymous || !data.topics[0] ? null : data.topics[0]; // move the data and topics to "raw"

  result.raw = {
    data: result.data,
    topics: result.topics
  };
  delete result.data;
  delete result.topics;
  return result;
};
/**
 * Encodes an ABI for a method, including signature or the method.
 * Or when constructor encodes only the constructor parameters.
 *
 * @method _encodeMethodABI
 * @param {Mixed} args the arguments to encode
 * @param {String} the encoded ABI
 */


Contract.prototype._encodeMethodABI = function _encodeMethodABI() {
  var methodSignature = this._method.signature;
  var args = this.arguments || [];
  var signature = false;
  var paramsABI = this._parent.options.jsonInterface.filter(function (json) {
    return methodSignature === 'constructor' && json.type === methodSignature || (json.signature === methodSignature || json.signature === methodSignature.replace('0x', '') || json.name === methodSignature) && json.type === 'function';
  }).map(function (json) {
    var inputLength = _.isArray(json.inputs) ? json.inputs.length : 0;

    if (inputLength !== args.length) {
      throw new Error("The number of arguments is not matching the methods required number. You need to pass ".concat(inputLength, " arguments."));
    }

    if (json.type === 'function') {
      signature = json.signature;
    }

    return _.isArray(json.inputs) ? json.inputs : [];
  }).map(function (inputs) {
    return abi.encodeParameters(inputs, args).replace('0x', '');
  })[0] || ''; // return constructor

  if (methodSignature === 'constructor') {
    if (!this._deployData) throw new Error('The contract has no contract data option set. This is necessary to append the constructor parameters.');
    return this._deployData + paramsABI; // return method
  }

  var returnValue = signature ? signature + paramsABI : paramsABI;

  if (!returnValue) {
    throw new Error("Couldn't find a matching contract method named \"".concat(this._method.name, "\"."));
  } else {
    return returnValue;
  }
};
/**
 * Decode method return values
 *
 * @method _decodeMethodReturn
 * @param {Array} outputs
 * @param {String} returnValues
 * @return {Object} decoded output return values
 */


Contract.prototype._decodeMethodReturn = function (outputs, returnValues) {
  if (!returnValues) {
    return null;
  }

  returnValues = returnValues.length >= 2 ? returnValues.slice(2) : returnValues;
  var result = abi.decodeParameters(outputs, returnValues);

  if (result.__length__ === 1) {
    return result[0];
  }

  delete result.__length__;
  return result;
};
/**
 * Deploys a contract and fire events based on its state: transactionHash, receipt
 *
 * All event listeners will be removed, once the last possible event is fired ("error", or "receipt")
 *
 * @method deploy
 * @param {Object} options
 * @param {Function} callback
 * @return {Object} EventEmitter possible events are "error", "transactionHash" and "receipt"
 */


Contract.prototype.deploy = function (options, callback) {
  options = options || {};
  options.arguments = options.arguments || [];
  options = this._getOrSetDefaultOptions(options); // return error, if no "data" is specified

  if (!options.data) {
    return utils._fireError(new Error('No "data" specified in neither the given options, nor the default options.'), null, null, callback);
  }

  var constructor = _.find(this.options.jsonInterface, function (method) {
    return method.type === 'constructor';
  }) || {};
  constructor.signature = 'constructor';
  return this._createTxObject.apply({
    method: constructor,
    parent: this,
    deployData: options.data,
    _ethAccounts: this.constructor._ethAccounts
  }, options.arguments);
};
/**
 * Gets the event signature and outputFormatters
 *
 * @method _generateEventOptions
 * @param {Object} event
 * @param {Object} options
 * @param {Function} callback
 * @return {Object} the event options object
 */


Contract.prototype._generateEventOptions = function () {
  var args = Array.prototype.slice.call(arguments); // get the callback

  var callback = this._getCallback(args); // get the options


  var options = _.isObject(args[args.length - 1]) ? args.pop() : {};
  var eventName = _.isString(args[0]) ? args[0] : 'allevents';
  var event = eventName.toLowerCase() === 'allevents' ? {
    name: 'ALLEVENTS',
    jsonInterface: this.options.jsonInterface
  } : this.options.jsonInterface.find(function (json) {
    return json.type === 'event' && (json.name === eventName || json.signature === '0x' + eventName.replace('0x', ''));
  });

  if (!event) {
    throw new Error("Event \"".concat(eventName, "\" doesn't exist in this contract."));
  }

  if (!utils.isAddress(this.options.address)) {
    throw new Error('This contract object doesn\'t have address set yet, please set an address first.');
  }

  return {
    params: this._encodeEventABI(event, options),
    event,
    callback
  };
};
/**
 * Adds event listeners and creates a subscription, and remove it once its fired.
 *
 * @method clone
 * @return {Object} the event subscription
 */


Contract.prototype.clone = function () {
  return new this.constructor(this.options.jsonInterface, this.options.address, this.options);
};
/**
 * Adds event listeners and creates a subscription, and remove it once its fired.
 *
 * @method once
 * @param {String} event
 * @param {Object} options
 * @param {Function} callback
 * @return {Object} the event subscription
 */


Contract.prototype.once = function (event, options, callback) {
  var args = Array.prototype.slice.call(arguments); // get the callback

  callback = this._getCallback(args);

  if (!callback) {
    throw new Error('Once requires a callback as the second parameter.');
  } // don't allow fromBlock


  if (options) delete options.fromBlock; // don't return as once shouldn't provide "on"

  this._on(event, options, function (err, res, sub) {
    sub.unsubscribe();

    if (_.isFunction(callback)) {
      callback(err, res, sub);
    }
  });

  return undefined;
};
/**
 * Adds event listeners and creates a subscription.
 *
 * @method _on
 * @param {String} event
 * @param {Object} options
 * @param {Function} callback
 * @return {Object} the event subscription
 */


Contract.prototype._on = function () {
  var subOptions = this._generateEventOptions.apply(this, arguments); // prevent the event "newListener" and "removeListener" from being overwritten


  this._checkListener('newListener', subOptions.event.name, subOptions.callback);

  this._checkListener('removeListener', subOptions.event.name, subOptions.callback); // TODO check if listener already exists? and reuse subscription if options are the same.
  // create new subscription


  var subscription = new Subscription({
    subscription: {
      params: 1,
      inputFormatter: [formatters.inputLogFormatter],
      outputFormatter: this._decodeEventABI.bind(subOptions.event),

      // DUBLICATE, also in web3-eth
      subscriptionHandler(output) {
        if (output.removed) {
          this.emit('changed', output);
        } else {
          this.emit('data', output);
        }

        if (_.isFunction(this.callback)) {
          this.callback(null, output, this);
        }
      }

    },
    type: 'eth',
    requestManager: this._requestManager
  });
  subscription.subscribe('logs', subOptions.params, subOptions.callback || function () {});
  return subscription;
};
/**
 * Get past events from contracts
 *
 * @method getPastEvents
 * @param {String} event
 * @param {Object} options
 * @param {Function} callback
 * @return {Object} the promievent
 */


Contract.prototype.getPastEvents = function () {
  var subOptions = this._generateEventOptions.apply(this, arguments);

  var getPastLogs = new Method({
    name: 'getPastLogs',
    call: 'eth_getLogs',
    params: 1,
    inputFormatter: [formatters.inputLogFormatter],
    outputFormatter: this._decodeEventABI.bind(subOptions.event)
  });
  getPastLogs.setRequestManager(this._requestManager);
  var call = getPastLogs.buildCall();
  null, _readOnlyError("getPastLogs");
  return call(subOptions.params, subOptions.callback);
};
/**
 * returns the an object with call, send, estimate functions
 *
 * @method _createTxObject
 * @returns {Object} an object with functions to call the methods
 */


Contract.prototype._createTxObject = function _createTxObject() {
  var args = Array.prototype.slice.call(arguments);
  var txObject = {};

  if (this.method.type === 'function') {
    txObject.call = this.parent._executeMethod.bind(txObject, 'call');
    txObject.call.request = this.parent._executeMethod.bind(txObject, 'call', true); // to make batch requests
  }

  txObject.send = this.parent._executeMethod.bind(txObject, 'send');
  txObject.send.request = this.parent._executeMethod.bind(txObject, 'send', true); // to make batch requests

  txObject.encodeABI = this.parent._encodeMethodABI.bind(txObject);
  txObject.estimateGas = this.parent._executeMethod.bind(txObject, 'estimate');
  txObject.estimateCosts = this.parent._executeMethod.bind(txObject, 'estimateCosts');

  if (args && this.method.inputs && args.length !== this.method.inputs.length) {
    if (this.nextMethod) {
      return this.nextMethod.apply(null, args);
    }

    throw errors.InvalidNumberOfParams(args.length, this.method.inputs.length, this.method.name);
  }

  txObject.arguments = args || [];
  txObject._method = this.method;
  txObject._parent = this.parent;
  txObject._ethAccounts = this.parent.constructor._ethAccounts || this._ethAccounts;

  if (this.deployData) {
    txObject._deployData = this.deployData;
  }

  return txObject;
};
/**
 * Generates the options for the execute call
 *
 * @method _processExecuteArguments
 * @param {Array} args
 * @param {Promise} defer
 */


Contract.prototype._processExecuteArguments = function _processExecuteArguments(args, defer) {
  var processedArgs = {};
  processedArgs.type = args.shift(); // get the callback

  processedArgs.callback = this._parent._getCallback(args); // get block number to use for call

  if (processedArgs.type === 'call' && args[args.length - 1] !== true && (_.isString(args[args.length - 1]) || isFinite(args[args.length - 1]))) processedArgs.defaultBlock = args.pop(); // get the options

  processedArgs.options = _.isObject(args[args.length - 1]) ? args.pop() : {}; // get the generateRequest argument for batch requests

  processedArgs.generateRequest = args[args.length - 1] === true ? args.pop() : false;
  processedArgs.options = this._parent._getOrSetDefaultOptions(processedArgs.options);
  processedArgs.options.data = this.encodeABI();
  processedArgs.originalArguments = this.arguments; // add contract address

  if (!this._deployData && !utils.isAddress(this._parent.options.address)) throw new Error('This contract object doesn\'t have address set yet, please set an address first.');
  if (!this._deployData) processedArgs.options.to = this._parent.options.address; // return error, if no "data" is specified

  if (!processedArgs.options.data) return utils._fireError(new Error('Couldn\'t find a matching contract method, or the number of parameters is wrong.'), defer.eventEmitter, defer.reject, processedArgs.callback);
  return processedArgs;
};
/**
 * Executes a call, transact or estimateGas on a contract function
 *
 * @method _executeMethod
 * @param {String} type the type this execute function should execute
 * @param {Boolean} makeRequest if true, it simply returns the request parameters, rather than executing it
 */


Contract.prototype._executeMethod = function _executeMethod() {
  var _this = this;

  var defer;

  var args = this._parent._processExecuteArguments.call(this, Array.prototype.slice.call(arguments), defer);

  defer = promiEvent(args.type !== 'send');
  var ethAccounts = _this.constructor._ethAccounts || _this._ethAccounts; // simple return request for batch requests

  if (args.generateRequest) {
    var payload = {
      params: [formatters.inputCallFormatter.call(this._parent, args.options)],
      callback: args.callback
    };

    if (args.type === 'call') {
      payload.params.push(formatters.inputDefaultBlockNumberFormatter.call(this._parent, args.defaultBlock));
      payload.method = 'eth_call';
      payload.format = this._parent._decodeMethodReturn.bind(null, this._method.outputs);
    } else {
      payload.method = 'eth_sendTransaction';
    }

    return payload;
  }

  switch (args.type) {
    case 'estimateCosts':
      /**
        Return Object 
        {
          Optional<message>:(Return on failure) UTF8 decoded message
          Optional<data>: (Return on failure) hex encoded string or null if not exist
          maybeReverts: boolean true if transaction may fail otherwise is false
          Optional<gas>: estimateGas for current method or null if fails
          gasPrice: current blockchain gasPrice
          Optional<minerFee>: (estimateGas * gasPrice <miner profit> ) or null if fails
          gasManagerFee: additionalFee - minerFee 
          additionalFee: additional cost is the result from a contract query - amount value
          totalCost: AdditionalFee is the total amount
        }
       */
      var defaultCosts = {
        gas: 0,
        gasPrice: args.options.gasPrice || 0,
        additionalFee: 0,
        totalCost: 0
      };

      var extraCostsMethods = _this._method.name.toLowerCase().includes("withdraw");

      if (extraCostsMethods) {
        var _estimateGas = new Method({
          name: 'estimateGas',
          call: 'eth_estimateGas',
          params: 1,
          inputFormatter: [formatters.inputCallFormatter],
          outputFormatter: utils.hexToNumber,
          requestManager: _this._parent._requestManager,
          accounts: ethAccounts,
          // is eth.accounts (necessary for wallet signing)
          defaultAccount: _this._parent.defaultAccount,
          defaultBlock: _this._parent.defaultBlock
        }).createFunction();

        var getGasPrice = new Method({
          name: 'getGasPrice',
          call: 'eth_gasPrice',
          params: 0,
          requestManager: _this._parent._requestManager,
          outputFormatter: utils.hexToNumber
        }).createFunction();
        var sig = _this._method.signature; //  _this.methods[sig]()

        var getEstimateCosts = new Method({
          name: 'call',
          call: 'eth_call',
          params: 2,
          inputFormatter: [formatters.inputCallFormatter, formatters.inputDefaultBlockNumberFormatter],

          // add output formatter for decoding
          outputFormatter(result) {
            return _this._parent._decodeMethodReturn([{
              name: "cost",
              type: "uint256"
            }], result);
          },

          requestManager: _this._parent._requestManager,
          accounts: ethAccounts,
          // is eth.accounts (necessary for wallet signing)
          defaultAccount: _this._parent.defaultAccount,
          defaultBlock: _this._parent.defaultBlock,
          handleRevert: _this._parent.handleRevert,
          abiCoder: abi
        }).createFunction();

        var additionalFeeOptions = _objectSpread(_objectSpread({}, args.options), {}, {
          data: this._parent.methods.estimateCosts(sig, args.originalArguments[0]).encodeABI()
        });

        return _estimateGas(args.options).then(function (gas) {
          return Promise.all([getGasPrice(), getEstimateCosts(additionalFeeOptions), Promise.resolve(args.originalArguments[0])]).then(_ref => {
            var [gasPrice, additionalFee, amount] = _ref;
            var BN = utils.BN;

            var _gas = new BN(gas);

            var _gasPrice = new BN(gasPrice);

            var _amount = new BN(amount);

            var _additionalFee = new BN(additionalFee).sub(_amount);

            var _additionalFeeStr = _additionalFee.toString();

            var _minerFee = new BN(_gas).mul(new BN(_gasPrice));

            return Object.assign(defaultCosts, {
              additionalFee: _additionalFeeStr,
              gas: _gas.toString(),
              gasManagerFee: _additionalFee.sub(_minerFee).toString(),
              gasPrice: _gasPrice.toString(),
              maybeReverts: false,
              minerFee: _minerFee.toString(),
              totalCost: _additionalFeeStr
            });
          });
        }).catch(function (estimateGasError) {
          return Promise.all([getGasPrice(), getEstimateCosts(additionalFeeOptions), Promise.resolve(args.originalArguments[0])]).then(_ref2 => {
            var [gasPrice, additionalFee, amount] = _ref2;
            var BN = utils.BN;

            var _additionalFee = new BN(additionalFee).sub(new BN(amount));

            var _additionalFeeStr = _additionalFee.toString();

            return Object.assign(defaultCosts, {
              additionalFee: _additionalFeeStr,
              data: estimateGasError.data,
              gas: null,
              gasPrice: gasPrice.toString(),
              maybeReverts: true,
              message: estimateGasError.message,
              minerFee: null,
              totalCost: _additionalFeeStr
            });
          });
        });
      }

      return defaultCosts;

    case 'estimate':
      var estimateGas = new Method({
        name: 'estimateGas',
        call: 'eth_estimateGas',
        params: 1,
        inputFormatter: [formatters.inputCallFormatter],
        outputFormatter: utils.hexToNumber,
        requestManager: _this._parent._requestManager,
        accounts: ethAccounts,
        // is eth.accounts (necessary for wallet signing)
        defaultAccount: _this._parent.defaultAccount,
        defaultBlock: _this._parent.defaultBlock
      }).createFunction();
      return estimateGas(args.options, args.callback);

    case 'call':
      // TODO check errors: missing "from" should give error on deploy and send, call ?
      var call = new Method({
        name: 'call',
        call: 'eth_call',
        params: 2,
        inputFormatter: [formatters.inputCallFormatter, formatters.inputDefaultBlockNumberFormatter],

        // add output formatter for decoding
        outputFormatter(result) {
          return _this._parent._decodeMethodReturn(_this._method.outputs, result);
        },

        requestManager: _this._parent._requestManager,
        accounts: ethAccounts,
        // is eth.accounts (necessary for wallet signing)
        defaultAccount: _this._parent.defaultAccount,
        defaultBlock: _this._parent.defaultBlock,
        handleRevert: _this._parent.handleRevert,
        abiCoder: abi
      }).createFunction();
      return call(args.options, args.defaultBlock, args.callback);

    case 'send':
      // return error, if no "from" is specified
      if (!utils.isAddress(args.options.from)) {
        return utils._fireError(new Error('No "from" address specified in neither the given options, nor the default options.'), defer.eventEmitter, defer.reject, args.callback);
      }

      if (_.isBoolean(this._method.payable) && !this._method.payable && args.options.value && args.options.value > 0) {
        return utils._fireError(new Error('Can not send value to non-payable contract method or constructor'), defer.eventEmitter, defer.reject, args.callback);
      } // make sure receipt logs are decoded


      var extraFormatters = {
        receiptFormatter(receipt) {
          if (_.isArray(receipt.logs)) {
            // decode logs
            var events = receipt.logs.map(log => {
              return _this._parent._decodeEventABI.call({
                name: 'ALLEVENTS',
                jsonInterface: _this._parent.options.jsonInterface
              }, log);
            }); // make log names keys

            receipt.events = {};
            var count = 0;
            events.forEach(function (ev) {
              if (ev.event) {
                // if > 1 of the same event, don't overwrite any existing events
                if (receipt.events[ev.event]) {
                  if (Array.isArray(receipt.events[ev.event])) {
                    receipt.events[ev.event].push(ev);
                  } else {
                    receipt.events[ev.event] = [receipt.events[ev.event], ev];
                  }
                } else {
                  receipt.events[ev.event] = ev;
                }
              } else {
                receipt.events[count] = ev;
                count++;
              }
            });
            delete receipt.logs;
          }

          return receipt;
        },

        contractDeployFormatter(receipt) {
          var newContract = _this._parent.clone();

          newContract.options.address = receipt.contractAddress;
          return newContract;
        }

      };
      var sendTransaction = new Method({
        name: 'sendTransaction',
        call: 'eth_sendTransaction',
        params: 1,
        inputFormatter: [formatters.inputTransactionFormatter],
        requestManager: _this._parent._requestManager,
        accounts: _this.constructor._ethAccounts || _this._ethAccounts,
        // is eth.accounts (necessary for wallet signing)
        defaultAccount: _this._parent.defaultAccount,
        defaultBlock: _this._parent.defaultBlock,
        transactionBlockTimeout: _this._parent.transactionBlockTimeout,
        transactionConfirmationBlocks: _this._parent.transactionConfirmationBlocks,
        transactionPollingTimeout: _this._parent.transactionPollingTimeout,
        defaultCommon: _this._parent.defaultCommon,
        defaultChain: _this._parent.defaultChain,
        defaultHardfork: _this._parent.defaultHardfork,
        handleRevert: _this._parent.handleRevert,
        extraFormatters,
        abiCoder: abi
      }).createFunction();

      if (!args.options.gas || args.options.gas <= 21000) {
        // auto estimateGas
        return new Promise(function (resolve) {
          // auto estimateGas
          var estimateGas = new Method({
            name: 'estimateGas',
            call: 'eth_estimateGas',
            params: 1,
            inputFormatter: [formatters.inputCallFormatter],
            outputFormatter: utils.hexToNumber,
            requestManager: _this._parent._requestManager,
            accounts: ethAccounts,
            // is eth.accounts (necessary for wallet signing)
            defaultAccount: _this._parent.defaultAccount,
            defaultBlock: _this._parent.defaultBlock
          }).createFunction();
          return resolve(estimateGas(args.options, args.callback));
        }).then(function (gas) {
          var BN = utils.BN; // change args 

          args.options = Object.assign(args.options, {
            // TODO: we give more 10k of gas because estimateGas is not precise, this need revision later
            gas: new BN(gas.toString()).add(new BN("10000")).toString()
          });
          return sendTransaction(args.options, args.callback);
        });
      } else {
        // set user gas passed
        return sendTransaction(args.options, args.callback);
      }

    default: // do nothing

  }
};

var _default = Contract;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJfIiwicmVxdWlyZSIsImNvcmUiLCJNZXRob2QiLCJ1dGlscyIsIlN1YnNjcmlwdGlvbiIsInN1YnNjcmlwdGlvbiIsImZvcm1hdHRlcnMiLCJlcnJvcnMiLCJwcm9taUV2ZW50IiwiYWJpIiwiQ29udHJhY3QiLCJqc29uSW50ZXJmYWNlIiwiYWRkcmVzcyIsIm9wdGlvbnMiLCJfdGhpcyIsImFyZ3MiLCJBcnJheSIsInByb3RvdHlwZSIsInNsaWNlIiwiY2FsbCIsImFyZ3VtZW50cyIsIkVycm9yIiwicGFja2FnZUluaXQiLCJjb25zdHJ1Y3RvciIsImN1cnJlbnRQcm92aWRlciIsImNsZWFyU3Vic2NyaXB0aW9ucyIsIl9yZXF1ZXN0TWFuYWdlciIsImlzQXJyYXkiLCJsYXN0QXJnIiwibGVuZ3RoIiwiaXNPYmplY3QiLCJleHRlbmQiLCJfZ2V0T3JTZXREZWZhdWx0T3B0aW9ucyIsIk9iamVjdCIsImRlZmluZVByb3BlcnR5Iiwic2V0IiwidmFsdWUiLCJfYWRkcmVzcyIsInRvQ2hlY2tzdW1BZGRyZXNzIiwiaW5wdXRBZGRyZXNzRm9ybWF0dGVyIiwiZ2V0IiwiZW51bWVyYWJsZSIsIm1ldGhvZHMiLCJldmVudHMiLCJfanNvbkludGVyZmFjZSIsIm1hcCIsIm1ldGhvZCIsImZ1bmMiLCJmdW5jTmFtZSIsImNvbnN0YW50Iiwic3RhdGVNdXRhYmlsaXR5IiwicGF5YWJsZSIsIm5hbWUiLCJfanNvbkludGVyZmFjZU1ldGhvZFRvU3RyaW5nIiwidHlwZSIsInNpZ25hdHVyZSIsImVuY29kZUZ1bmN0aW9uU2lnbmF0dXJlIiwiX2NyZWF0ZVR4T2JqZWN0IiwiYmluZCIsInBhcmVudCIsImNhc2NhZGVGdW5jIiwibmV4dE1ldGhvZCIsImVuY29kZUV2ZW50U2lnbmF0dXJlIiwiZXZlbnQiLCJfb24iLCJhbGxFdmVudHMiLCJkZWZhdWx0QWNjb3VudCIsImRlZmF1bHRCbG9jayIsImhhbmRsZVJldmVydCIsInZhbCIsImNvbW1vbiIsImRlZmF1bHRDb21tb24iLCJoYXJkZm9yayIsImRlZmF1bHRIYXJkZm9yayIsImNoYWluIiwiZGVmYXVsdENoYWluIiwidHJhbnNhY3Rpb25Qb2xsaW5nVGltZW91dCIsInRyYW5zYWN0aW9uQ29uZmlybWF0aW9uQmxvY2tzIiwidHJhbnNhY3Rpb25CbG9ja1RpbWVvdXQiLCJzZXRQcm92aWRlciIsInByb3ZpZGVyIiwiYWNjb3VudHMiLCJfZXRoQWNjb3VudHMiLCJfZ2V0Q2FsbGJhY2siLCJnZXRDYWxsYmFjayIsImlzRnVuY3Rpb24iLCJwb3AiLCJfY2hlY2tMaXN0ZW5lciIsImdldE9yU2V0RGVmYXVsdE9wdGlvbnMiLCJnYXNQcmljZSIsIlN0cmluZyIsImZyb20iLCJkYXRhIiwiZ2FzIiwiZ2FzTGltaXQiLCJfZW5jb2RlRXZlbnRBQkkiLCJmaWx0ZXIiLCJyZXN1bHQiLCJmIiwidW5kZWZpbmVkIiwiZm9yRWFjaCIsImlucHV0QmxvY2tOdW1iZXJGb3JtYXR0ZXIiLCJ0b3BpY3MiLCJhbm9ueW1vdXMiLCJwdXNoIiwiaW5kZXhlZFRvcGljcyIsImlucHV0cyIsImkiLCJpbmRleGVkIiwidiIsImVuY29kZVBhcmFtZXRlciIsImNvbmNhdCIsInRvTG93ZXJDYXNlIiwiX2RlY29kZUV2ZW50QUJJIiwib3V0cHV0TG9nRm9ybWF0dGVyIiwiZmluZCIsImludGYiLCJpbmRleGVkSW5wdXRzIiwiaW5wdXQiLCJhcmdUb3BpY3MiLCJyZXR1cm5WYWx1ZXMiLCJkZWNvZGVMb2ciLCJfX2xlbmd0aF9fIiwicmF3IiwiX2VuY29kZU1ldGhvZEFCSSIsIm1ldGhvZFNpZ25hdHVyZSIsIl9tZXRob2QiLCJwYXJhbXNBQkkiLCJfcGFyZW50IiwianNvbiIsInJlcGxhY2UiLCJpbnB1dExlbmd0aCIsImVuY29kZVBhcmFtZXRlcnMiLCJfZGVwbG95RGF0YSIsInJldHVyblZhbHVlIiwiX2RlY29kZU1ldGhvZFJldHVybiIsIm91dHB1dHMiLCJkZWNvZGVQYXJhbWV0ZXJzIiwiZGVwbG95IiwiY2FsbGJhY2siLCJfZmlyZUVycm9yIiwiYXBwbHkiLCJkZXBsb3lEYXRhIiwiX2dlbmVyYXRlRXZlbnRPcHRpb25zIiwiZXZlbnROYW1lIiwiaXNTdHJpbmciLCJpc0FkZHJlc3MiLCJwYXJhbXMiLCJjbG9uZSIsIm9uY2UiLCJmcm9tQmxvY2siLCJlcnIiLCJyZXMiLCJzdWIiLCJ1bnN1YnNjcmliZSIsInN1Yk9wdGlvbnMiLCJpbnB1dEZvcm1hdHRlciIsImlucHV0TG9nRm9ybWF0dGVyIiwib3V0cHV0Rm9ybWF0dGVyIiwic3Vic2NyaXB0aW9uSGFuZGxlciIsIm91dHB1dCIsInJlbW92ZWQiLCJlbWl0IiwicmVxdWVzdE1hbmFnZXIiLCJzdWJzY3JpYmUiLCJnZXRQYXN0RXZlbnRzIiwiZ2V0UGFzdExvZ3MiLCJzZXRSZXF1ZXN0TWFuYWdlciIsImJ1aWxkQ2FsbCIsInR4T2JqZWN0IiwiX2V4ZWN1dGVNZXRob2QiLCJyZXF1ZXN0Iiwic2VuZCIsImVuY29kZUFCSSIsImVzdGltYXRlR2FzIiwiZXN0aW1hdGVDb3N0cyIsIkludmFsaWROdW1iZXJPZlBhcmFtcyIsIl9wcm9jZXNzRXhlY3V0ZUFyZ3VtZW50cyIsImRlZmVyIiwicHJvY2Vzc2VkQXJncyIsInNoaWZ0IiwiaXNGaW5pdGUiLCJnZW5lcmF0ZVJlcXVlc3QiLCJvcmlnaW5hbEFyZ3VtZW50cyIsInRvIiwiZXZlbnRFbWl0dGVyIiwicmVqZWN0IiwiZXRoQWNjb3VudHMiLCJwYXlsb2FkIiwiaW5wdXRDYWxsRm9ybWF0dGVyIiwiaW5wdXREZWZhdWx0QmxvY2tOdW1iZXJGb3JtYXR0ZXIiLCJmb3JtYXQiLCJkZWZhdWx0Q29zdHMiLCJhZGRpdGlvbmFsRmVlIiwidG90YWxDb3N0IiwiZXh0cmFDb3N0c01ldGhvZHMiLCJpbmNsdWRlcyIsImhleFRvTnVtYmVyIiwiY3JlYXRlRnVuY3Rpb24iLCJnZXRHYXNQcmljZSIsInNpZyIsImdldEVzdGltYXRlQ29zdHMiLCJhYmlDb2RlciIsImFkZGl0aW9uYWxGZWVPcHRpb25zIiwidGhlbiIsIlByb21pc2UiLCJhbGwiLCJyZXNvbHZlIiwiYW1vdW50IiwiQk4iLCJfZ2FzIiwiX2dhc1ByaWNlIiwiX2Ftb3VudCIsIl9hZGRpdGlvbmFsRmVlIiwiX2FkZGl0aW9uYWxGZWVTdHIiLCJ0b1N0cmluZyIsIl9taW5lckZlZSIsIm11bCIsImFzc2lnbiIsImdhc01hbmFnZXJGZWUiLCJtYXliZVJldmVydHMiLCJtaW5lckZlZSIsImNhdGNoIiwiZXN0aW1hdGVHYXNFcnJvciIsIm1lc3NhZ2UiLCJpc0Jvb2xlYW4iLCJleHRyYUZvcm1hdHRlcnMiLCJyZWNlaXB0Rm9ybWF0dGVyIiwicmVjZWlwdCIsImxvZ3MiLCJsb2ciLCJjb3VudCIsImV2IiwiY29udHJhY3REZXBsb3lGb3JtYXR0ZXIiLCJuZXdDb250cmFjdCIsImNvbnRyYWN0QWRkcmVzcyIsInNlbmRUcmFuc2FjdGlvbiIsImlucHV0VHJhbnNhY3Rpb25Gb3JtYXR0ZXIiLCJhZGQiXSwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9zcmMvY29udHJhY3RzL3dlYjMtbGliL2NvbnRyYWN0L2luZGV4LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qIGVzbGludC1kaXNhYmxlIG5vLWNhc2UtZGVjbGFyYXRpb25zICovXG4vKiBlc2xpbnQtZGlzYWJsZSBuby1yZXN0cmljdGVkLWdsb2JhbHMgKi9cbi8qIGVzbGludC1kaXNhYmxlIHByZWZlci1zcHJlYWQgKi9cbi8qIGVzbGludC1kaXNhYmxlIHByZWZlci1yZXN0LXBhcmFtcyAqL1xuLyogZXNsaW50LWRpc2FibGUgY29uc2lzdGVudC1yZXR1cm4gKi9cbi8qIGVzbGludC1kaXNhYmxlIG5vLWNvbnN0LWFzc2lnbiAqL1xuLyogZXNsaW50LWRpc2FibGUgcHJlZmVyLWRlc3RydWN0dXJpbmcgKi9cbi8qIGVzbGludC1kaXNhYmxlIG5vLXBhcmFtLXJlYXNzaWduICovXG4vKiBlc2xpbnQtZGlzYWJsZSBmdW5jLW5hbWVzICovXG4vKiBlc2xpbnQtZGlzYWJsZSBuby11bmRlcnNjb3JlLWRhbmdsZSAqL1xuLy8gU09VUkNFOiBodHRwczovL3Jhdy5naXRodWJ1c2VyY29udGVudC5jb20vQ2hhaW5TYWZlL3dlYjMuanMvdjEuMy4xL3BhY2thZ2VzL3dlYjMtZXRoLWNvbnRyYWN0L3NyYy9pbmRleC5qc1xuY29uc3QgXyA9IHJlcXVpcmUoJ3VuZGVyc2NvcmUnKTtcbmNvbnN0IGNvcmUgPSByZXF1aXJlKCd3ZWIzLWNvcmUnKTtcbmNvbnN0IE1ldGhvZCA9IHJlcXVpcmUoJ3dlYjMtY29yZS1tZXRob2QnKTtcbmNvbnN0IHV0aWxzID0gcmVxdWlyZSgnd2ViMy11dGlscycpO1xuY29uc3QgU3Vic2NyaXB0aW9uID0gcmVxdWlyZSgnd2ViMy1jb3JlLXN1YnNjcmlwdGlvbnMnKS5zdWJzY3JpcHRpb247XG5jb25zdCBmb3JtYXR0ZXJzID0gcmVxdWlyZSgnd2ViMy1jb3JlLWhlbHBlcnMnKS5mb3JtYXR0ZXJzO1xuY29uc3QgZXJyb3JzID0gcmVxdWlyZSgnd2ViMy1jb3JlLWhlbHBlcnMnKS5lcnJvcnM7XG5jb25zdCBwcm9taUV2ZW50ID0gcmVxdWlyZSgnd2ViMy1jb3JlLXByb21pZXZlbnQnKTtcbmNvbnN0IGFiaSA9IHJlcXVpcmUoJ3dlYjMtZXRoLWFiaScpO1xuXG5cbi8qKlxuICogU2hvdWxkIGJlIGNhbGxlZCB0byBjcmVhdGUgbmV3IGNvbnRyYWN0IGluc3RhbmNlXG4gKlxuICogQG1ldGhvZCBDb250cmFjdFxuICogQGNvbnN0cnVjdG9yXG4gKiBAcGFyYW0ge0FycmF5fSBqc29uSW50ZXJmYWNlXG4gKiBAcGFyYW0ge1N0cmluZ30gYWRkcmVzc1xuICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnNcbiAqL1xuY29uc3QgQ29udHJhY3QgPSBmdW5jdGlvbiBDb250cmFjdChqc29uSW50ZXJmYWNlLCBhZGRyZXNzLCBvcHRpb25zKSB7XG4gICAgY29uc3QgX3RoaXMgPSB0aGlzLFxuICAgICAgICBhcmdzID0gQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoYXJndW1lbnRzKTtcblxuICAgIGlmICghKHRoaXMgaW5zdGFuY2VvZiBDb250cmFjdCkpIHtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdQbGVhc2UgdXNlIHRoZSBcIm5ld1wiIGtleXdvcmQgdG8gaW5zdGFudGlhdGUgYSB3ZWIzLmV0aC5jb250cmFjdCgpIG9iamVjdCEnKTtcbiAgICB9XG5cbiAgICAvLyBzZXRzIF9yZXF1ZXN0TWFuYWdlclxuICAgIGNvcmUucGFja2FnZUluaXQodGhpcywgW3RoaXMuY29uc3RydWN0b3IuY3VycmVudFByb3ZpZGVyXSk7XG5cbiAgICB0aGlzLmNsZWFyU3Vic2NyaXB0aW9ucyA9IHRoaXMuX3JlcXVlc3RNYW5hZ2VyLmNsZWFyU3Vic2NyaXB0aW9ucztcblxuXG5cbiAgICBpZiAoIWpzb25JbnRlcmZhY2UgfHwgIShBcnJheS5pc0FycmF5KGpzb25JbnRlcmZhY2UpKSkge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1lvdSBtdXN0IHByb3ZpZGUgdGhlIGpzb24gaW50ZXJmYWNlIG9mIHRoZSBjb250cmFjdCB3aGVuIGluc3RhbnRpYXRpbmcgYSBjb250cmFjdCBvYmplY3QuJyk7XG4gICAgfVxuXG4gICAgLy8gY3JlYXRlIHRoZSBvcHRpb25zIG9iamVjdFxuICAgIHRoaXMub3B0aW9ucyA9IHt9O1xuXG4gICAgY29uc3QgbGFzdEFyZyA9IGFyZ3NbYXJncy5sZW5ndGggLSAxXTtcbiAgICBpZiAoXy5pc09iamVjdChsYXN0QXJnKSAmJiAhXy5pc0FycmF5KGxhc3RBcmcpKSB7XG4gICAgICAgIG9wdGlvbnMgPSBsYXN0QXJnO1xuXG4gICAgICAgIHRoaXMub3B0aW9ucyA9IF8uZXh0ZW5kKHRoaXMub3B0aW9ucywgdGhpcy5fZ2V0T3JTZXREZWZhdWx0T3B0aW9ucyhvcHRpb25zKSk7XG4gICAgICAgIGlmIChfLmlzT2JqZWN0KGFkZHJlc3MpKSB7XG4gICAgICAgICAgICBhZGRyZXNzID0gbnVsbDtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8vIHNldCBhZGRyZXNzXG4gICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRoaXMub3B0aW9ucywgJ2FkZHJlc3MnLCB7XG4gICAgICAgIHNldCh2YWx1ZSkge1xuICAgICAgICAgICAgaWYgKHZhbHVlKSB7XG4gICAgICAgICAgICAgICAgX3RoaXMuX2FkZHJlc3MgPSB1dGlscy50b0NoZWNrc3VtQWRkcmVzcyhmb3JtYXR0ZXJzLmlucHV0QWRkcmVzc0Zvcm1hdHRlcih2YWx1ZSkpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBnZXQoKSB7XG4gICAgICAgICAgICByZXR1cm4gX3RoaXMuX2FkZHJlc3M7XG4gICAgICAgIH0sXG4gICAgICAgIGVudW1lcmFibGU6IHRydWVcbiAgICB9KTtcblxuICAgIC8vIGFkZCBtZXRob2QgYW5kIGV2ZW50IHNpZ25hdHVyZXMsIHdoZW4gdGhlIGpzb25JbnRlcmZhY2UgZ2V0cyBzZXRcbiAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkodGhpcy5vcHRpb25zLCAnanNvbkludGVyZmFjZScsIHtcbiAgICAgICAgc2V0KHZhbHVlKSB7XG4gICAgICAgICAgICBfdGhpcy5tZXRob2RzID0ge307XG4gICAgICAgICAgICBfdGhpcy5ldmVudHMgPSB7fTtcblxuICAgICAgICAgICAgX3RoaXMuX2pzb25JbnRlcmZhY2UgPSB2YWx1ZS5tYXAoZnVuY3Rpb24gKG1ldGhvZCkge1xuICAgICAgICAgICAgICAgIGxldCBmdW5jO1xuICAgICAgICAgICAgICAgIGxldCBmdW5jTmFtZTtcblxuICAgICAgICAgICAgICAgIC8vIG1ha2UgY29uc3RhbnQgYW5kIHBheWFibGUgYmFja3dhcmRzIGNvbXBhdGlibGVcbiAgICAgICAgICAgICAgICBtZXRob2QuY29uc3RhbnQgPSAobWV0aG9kLnN0YXRlTXV0YWJpbGl0eSA9PT0gXCJ2aWV3XCIgfHwgbWV0aG9kLnN0YXRlTXV0YWJpbGl0eSA9PT0gXCJwdXJlXCIgfHwgbWV0aG9kLmNvbnN0YW50KTtcbiAgICAgICAgICAgICAgICBtZXRob2QucGF5YWJsZSA9IChtZXRob2Quc3RhdGVNdXRhYmlsaXR5ID09PSBcInBheWFibGVcIiB8fCBtZXRob2QucGF5YWJsZSk7XG5cblxuICAgICAgICAgICAgICAgIGlmIChtZXRob2QubmFtZSkge1xuICAgICAgICAgICAgICAgICAgICBmdW5jTmFtZSA9IHV0aWxzLl9qc29uSW50ZXJmYWNlTWV0aG9kVG9TdHJpbmcobWV0aG9kKTtcbiAgICAgICAgICAgICAgICB9XG5cblxuICAgICAgICAgICAgICAgIC8vIGZ1bmN0aW9uXG4gICAgICAgICAgICAgICAgaWYgKG1ldGhvZC50eXBlID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICAgICAgICAgIG1ldGhvZC5zaWduYXR1cmUgPSBhYmkuZW5jb2RlRnVuY3Rpb25TaWduYXR1cmUoZnVuY05hbWUpO1xuICAgICAgICAgICAgICAgICAgICBmdW5jID0gX3RoaXMuX2NyZWF0ZVR4T2JqZWN0LmJpbmQoe1xuICAgICAgICAgICAgICAgICAgICAgICAgbWV0aG9kLFxuICAgICAgICAgICAgICAgICAgICAgICAgcGFyZW50OiBfdGhpc1xuICAgICAgICAgICAgICAgICAgICB9KTtcblxuXG4gICAgICAgICAgICAgICAgICAgIC8vIGFkZCBtZXRob2Qgb25seSBpZiBub3Qgb25lIGFscmVhZHkgZXhpc3RzXG4gICAgICAgICAgICAgICAgICAgIGlmICghX3RoaXMubWV0aG9kc1ttZXRob2QubmFtZV0pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIF90aGlzLm1ldGhvZHNbbWV0aG9kLm5hbWVdID0gZnVuYztcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGNhc2NhZGVGdW5jID0gX3RoaXMuX2NyZWF0ZVR4T2JqZWN0LmJpbmQoe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1ldGhvZCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYXJlbnQ6IF90aGlzLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5leHRNZXRob2Q6IF90aGlzLm1ldGhvZHNbbWV0aG9kLm5hbWVdXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIF90aGlzLm1ldGhvZHNbbWV0aG9kLm5hbWVdID0gY2FzY2FkZUZ1bmM7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAvLyBkZWZpbml0ZWx5IGFkZCB0aGUgbWV0aG9kIGJhc2VkIG9uIGl0cyBzaWduYXR1cmVcbiAgICAgICAgICAgICAgICAgICAgX3RoaXMubWV0aG9kc1ttZXRob2Quc2lnbmF0dXJlXSA9IGZ1bmM7XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gYWRkIG1ldGhvZCBieSBuYW1lXG4gICAgICAgICAgICAgICAgICAgIF90aGlzLm1ldGhvZHNbZnVuY05hbWVdID0gZnVuYztcblxuXG4gICAgICAgICAgICAgICAgICAgIC8vIGV2ZW50XG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChtZXRob2QudHlwZSA9PT0gJ2V2ZW50Jykge1xuICAgICAgICAgICAgICAgICAgICBtZXRob2Quc2lnbmF0dXJlID0gYWJpLmVuY29kZUV2ZW50U2lnbmF0dXJlKGZ1bmNOYW1lKTtcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnQgPSBfdGhpcy5fb24uYmluZChfdGhpcywgbWV0aG9kLnNpZ25hdHVyZSk7XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gYWRkIG1ldGhvZCBvbmx5IGlmIG5vdCBhbHJlYWR5IGV4aXN0c1xuICAgICAgICAgICAgICAgICAgICBpZiAoIV90aGlzLmV2ZW50c1ttZXRob2QubmFtZV0gfHwgX3RoaXMuZXZlbnRzW21ldGhvZC5uYW1lXS5uYW1lID09PSAnYm91bmQgJylcbiAgICAgICAgICAgICAgICAgICAgICAgIF90aGlzLmV2ZW50c1ttZXRob2QubmFtZV0gPSBldmVudDtcblxuICAgICAgICAgICAgICAgICAgICAvLyBkZWZpbml0ZWx5IGFkZCB0aGUgbWV0aG9kIGJhc2VkIG9uIGl0cyBzaWduYXR1cmVcbiAgICAgICAgICAgICAgICAgICAgX3RoaXMuZXZlbnRzW21ldGhvZC5zaWduYXR1cmVdID0gZXZlbnQ7XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gYWRkIGV2ZW50IGJ5IG5hbWVcbiAgICAgICAgICAgICAgICAgICAgX3RoaXMuZXZlbnRzW2Z1bmNOYW1lXSA9IGV2ZW50O1xuICAgICAgICAgICAgICAgIH07XG5cblxuICAgICAgICAgICAgICAgIHJldHVybiBtZXRob2Q7XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgLy8gYWRkIGFsbEV2ZW50c1xuICAgICAgICAgICAgX3RoaXMuZXZlbnRzLmFsbEV2ZW50cyA9IF90aGlzLl9vbi5iaW5kKF90aGlzLCAnYWxsZXZlbnRzJyk7XG5cbiAgICAgICAgICAgIHJldHVybiBfdGhpcy5fanNvbkludGVyZmFjZTtcbiAgICAgICAgfSxcbiAgICAgICAgZ2V0KCkge1xuICAgICAgICAgICAgcmV0dXJuIF90aGlzLl9qc29uSW50ZXJmYWNlO1xuICAgICAgICB9LFxuICAgICAgICBlbnVtZXJhYmxlOiB0cnVlXG4gICAgfSk7XG5cbiAgICAvLyBnZXQgZGVmYXVsdCBhY2NvdW50IGZyb20gdGhlIENsYXNzXG4gICAgY29uc3QgZGVmYXVsdEFjY291bnQgPSB0aGlzLmNvbnN0cnVjdG9yLmRlZmF1bHRBY2NvdW50O1xuICAgIGNvbnN0IGRlZmF1bHRCbG9jayA9IHRoaXMuY29uc3RydWN0b3IuZGVmYXVsdEJsb2NrIHx8ICdsYXRlc3QnO1xuXG4gICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRoaXMsICdoYW5kbGVSZXZlcnQnLCB7XG4gICAgICAgIGdldCgpIHtcbiAgICAgICAgICAgIGlmIChfdGhpcy5vcHRpb25zLmhhbmRsZVJldmVydCA9PT0gZmFsc2UgfHwgX3RoaXMub3B0aW9ucy5oYW5kbGVSZXZlcnQgPT09IHRydWUpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gX3RoaXMub3B0aW9ucy5oYW5kbGVSZXZlcnQ7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHJldHVybiB0aGlzLmNvbnN0cnVjdG9yLmhhbmRsZVJldmVydDtcbiAgICAgICAgfSxcbiAgICAgICAgc2V0KHZhbCkge1xuICAgICAgICAgICAgX3RoaXMub3B0aW9ucy5oYW5kbGVSZXZlcnQgPSB2YWw7XG4gICAgICAgIH0sXG4gICAgICAgIGVudW1lcmFibGU6IHRydWVcbiAgICB9KTtcbiAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkodGhpcywgJ2RlZmF1bHRDb21tb24nLCB7XG4gICAgICAgIGdldCgpIHtcbiAgICAgICAgICAgIHJldHVybiBfdGhpcy5vcHRpb25zLmNvbW1vbiB8fCB0aGlzLmNvbnN0cnVjdG9yLmRlZmF1bHRDb21tb247XG4gICAgICAgIH0sXG4gICAgICAgIHNldCh2YWwpIHtcbiAgICAgICAgICAgIF90aGlzLm9wdGlvbnMuY29tbW9uID0gdmFsO1xuICAgICAgICB9LFxuICAgICAgICBlbnVtZXJhYmxlOiB0cnVlXG4gICAgfSk7XG4gICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRoaXMsICdkZWZhdWx0SGFyZGZvcmsnLCB7XG4gICAgICAgIGdldCgpIHtcbiAgICAgICAgICAgIHJldHVybiBfdGhpcy5vcHRpb25zLmhhcmRmb3JrIHx8IHRoaXMuY29uc3RydWN0b3IuZGVmYXVsdEhhcmRmb3JrO1xuICAgICAgICB9LFxuICAgICAgICBzZXQodmFsKSB7XG4gICAgICAgICAgICBfdGhpcy5vcHRpb25zLmhhcmRmb3JrID0gdmFsO1xuICAgICAgICB9LFxuICAgICAgICBlbnVtZXJhYmxlOiB0cnVlXG4gICAgfSk7XG4gICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRoaXMsICdkZWZhdWx0Q2hhaW4nLCB7XG4gICAgICAgIGdldCgpIHtcbiAgICAgICAgICAgIHJldHVybiBfdGhpcy5vcHRpb25zLmNoYWluIHx8IHRoaXMuY29uc3RydWN0b3IuZGVmYXVsdENoYWluO1xuICAgICAgICB9LFxuICAgICAgICBzZXQodmFsKSB7XG4gICAgICAgICAgICBfdGhpcy5vcHRpb25zLmNoYWluID0gdmFsO1xuICAgICAgICB9LFxuICAgICAgICBlbnVtZXJhYmxlOiB0cnVlXG4gICAgfSk7XG4gICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRoaXMsICd0cmFuc2FjdGlvblBvbGxpbmdUaW1lb3V0Jywge1xuICAgICAgICBnZXQoKSB7XG4gICAgICAgICAgICBpZiAoX3RoaXMub3B0aW9ucy50cmFuc2FjdGlvblBvbGxpbmdUaW1lb3V0ID09PSAwKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIF90aGlzLm9wdGlvbnMudHJhbnNhY3Rpb25Qb2xsaW5nVGltZW91dDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIF90aGlzLm9wdGlvbnMudHJhbnNhY3Rpb25Qb2xsaW5nVGltZW91dCB8fCB0aGlzLmNvbnN0cnVjdG9yLnRyYW5zYWN0aW9uUG9sbGluZ1RpbWVvdXQ7XG4gICAgICAgIH0sXG4gICAgICAgIHNldCh2YWwpIHtcbiAgICAgICAgICAgIF90aGlzLm9wdGlvbnMudHJhbnNhY3Rpb25Qb2xsaW5nVGltZW91dCA9IHZhbDtcbiAgICAgICAgfSxcbiAgICAgICAgZW51bWVyYWJsZTogdHJ1ZVxuICAgIH0pO1xuICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0aGlzLCAndHJhbnNhY3Rpb25Db25maXJtYXRpb25CbG9ja3MnLCB7XG4gICAgICAgIGdldCgpIHtcbiAgICAgICAgICAgIGlmIChfdGhpcy5vcHRpb25zLnRyYW5zYWN0aW9uQ29uZmlybWF0aW9uQmxvY2tzID09PSAwKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIF90aGlzLm9wdGlvbnMudHJhbnNhY3Rpb25Db25maXJtYXRpb25CbG9ja3M7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHJldHVybiBfdGhpcy5vcHRpb25zLnRyYW5zYWN0aW9uQ29uZmlybWF0aW9uQmxvY2tzIHx8IHRoaXMuY29uc3RydWN0b3IudHJhbnNhY3Rpb25Db25maXJtYXRpb25CbG9ja3M7XG4gICAgICAgIH0sXG4gICAgICAgIHNldCh2YWwpIHtcbiAgICAgICAgICAgIF90aGlzLm9wdGlvbnMudHJhbnNhY3Rpb25Db25maXJtYXRpb25CbG9ja3MgPSB2YWw7XG4gICAgICAgIH0sXG4gICAgICAgIGVudW1lcmFibGU6IHRydWVcbiAgICB9KTtcbiAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkodGhpcywgJ3RyYW5zYWN0aW9uQmxvY2tUaW1lb3V0Jywge1xuICAgICAgICBnZXQoKSB7XG4gICAgICAgICAgICBpZiAoX3RoaXMub3B0aW9ucy50cmFuc2FjdGlvbkJsb2NrVGltZW91dCA9PT0gMCkge1xuICAgICAgICAgICAgICAgIHJldHVybiBfdGhpcy5vcHRpb25zLnRyYW5zYWN0aW9uQmxvY2tUaW1lb3V0O1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICByZXR1cm4gX3RoaXMub3B0aW9ucy50cmFuc2FjdGlvbkJsb2NrVGltZW91dCB8fCB0aGlzLmNvbnN0cnVjdG9yLnRyYW5zYWN0aW9uQmxvY2tUaW1lb3V0O1xuICAgICAgICB9LFxuICAgICAgICBzZXQodmFsKSB7XG4gICAgICAgICAgICBfdGhpcy5vcHRpb25zLnRyYW5zYWN0aW9uQmxvY2tUaW1lb3V0ID0gdmFsO1xuICAgICAgICB9LFxuICAgICAgICBlbnVtZXJhYmxlOiB0cnVlXG4gICAgfSk7XG4gICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRoaXMsICdkZWZhdWx0QWNjb3VudCcsIHtcbiAgICAgICAgZ2V0KCkge1xuICAgICAgICAgICAgcmV0dXJuIGRlZmF1bHRBY2NvdW50O1xuICAgICAgICB9LFxuICAgICAgICBzZXQodmFsKSB7XG4gICAgICAgICAgICBpZiAodmFsKSB7XG4gICAgICAgICAgICAgICAgZGVmYXVsdEFjY291bnQgPSB1dGlscy50b0NoZWNrc3VtQWRkcmVzcyhmb3JtYXR0ZXJzLmlucHV0QWRkcmVzc0Zvcm1hdHRlcih2YWwpKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIHZhbDtcbiAgICAgICAgfSxcbiAgICAgICAgZW51bWVyYWJsZTogdHJ1ZVxuICAgIH0pO1xuICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0aGlzLCAnZGVmYXVsdEJsb2NrJywge1xuICAgICAgICBnZXQoKSB7XG4gICAgICAgICAgICByZXR1cm4gZGVmYXVsdEJsb2NrO1xuICAgICAgICB9LFxuICAgICAgICBzZXQodmFsKSB7XG4gICAgICAgICAgICBkZWZhdWx0QmxvY2sgPSB2YWw7XG5cbiAgICAgICAgICAgIHJldHVybiB2YWw7XG4gICAgICAgIH0sXG4gICAgICAgIGVudW1lcmFibGU6IHRydWVcbiAgICB9KTtcblxuICAgIC8vIHByb3BlcnRpZXNcbiAgICB0aGlzLm1ldGhvZHMgPSB7fTtcbiAgICB0aGlzLmV2ZW50cyA9IHt9O1xuXG4gICAgdGhpcy5fYWRkcmVzcyA9IG51bGw7XG4gICAgdGhpcy5fanNvbkludGVyZmFjZSA9IFtdO1xuXG4gICAgLy8gc2V0IGdldHRlci9zZXR0ZXIgcHJvcGVydGllc1xuICAgIHRoaXMub3B0aW9ucy5hZGRyZXNzID0gYWRkcmVzcztcbiAgICB0aGlzLm9wdGlvbnMuanNvbkludGVyZmFjZSA9IGpzb25JbnRlcmZhY2U7XG5cbn07XG5cbkNvbnRyYWN0LnNldFByb3ZpZGVyID0gZnVuY3Rpb24gKHByb3ZpZGVyLCBhY2NvdW50cykge1xuICAgIC8vIENvbnRyYWN0LmN1cnJlbnRQcm92aWRlciA9IHByb3ZpZGVyO1xuICAgIGNvcmUucGFja2FnZUluaXQodGhpcywgW3Byb3ZpZGVyXSk7XG4gICAgdGhpcy5fZXRoQWNjb3VudHMgPSBhY2NvdW50cztcbn07XG5cblxuLyoqXG4gKiBHZXQgdGhlIGNhbGxiYWNrIGFuZCBtb2RpZnkgdGhlIGFycmF5IGlmIG5lY2Vzc2FyeVxuICpcbiAqIEBtZXRob2QgX2dldENhbGxiYWNrXG4gKiBAcGFyYW0ge0FycmF5fSBhcmdzXG4gKiBAcmV0dXJuIHtGdW5jdGlvbn0gdGhlIGNhbGxiYWNrXG4gKi9cbkNvbnRyYWN0LnByb3RvdHlwZS5fZ2V0Q2FsbGJhY2sgPSBmdW5jdGlvbiBnZXRDYWxsYmFjayhhcmdzKSB7XG4gICAgaWYgKGFyZ3MgJiYgXy5pc0Z1bmN0aW9uKGFyZ3NbYXJncy5sZW5ndGggLSAxXSkpIHtcbiAgICAgICAgcmV0dXJuIGFyZ3MucG9wKCk7IC8vIG1vZGlmeSB0aGUgYXJncyBhcnJheSFcbiAgICB9XG59O1xuXG4vKipcbiAqIENoZWNrcyB0aGF0IG5vIGxpc3RlbmVyIHdpdGggbmFtZSBcIm5ld0xpc3RlbmVyXCIgb3IgXCJyZW1vdmVMaXN0ZW5lclwiIGlzIGFkZGVkLlxuICpcbiAqIEBtZXRob2QgX2NoZWNrTGlzdGVuZXJcbiAqIEBwYXJhbSB7U3RyaW5nfSB0eXBlXG4gKiBAcGFyYW0ge1N0cmluZ30gZXZlbnRcbiAqIEByZXR1cm4ge09iamVjdH0gdGhlIGNvbnRyYWN0IGluc3RhbmNlXG4gKi9cbkNvbnRyYWN0LnByb3RvdHlwZS5fY2hlY2tMaXN0ZW5lciA9IGZ1bmN0aW9uICh0eXBlLCBldmVudCkge1xuICAgIGlmIChldmVudCA9PT0gdHlwZSkge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1RoZSBldmVudCBcIicgKyB0eXBlICsgJ1wiIGlzIGEgcmVzZXJ2ZWQgZXZlbnQgbmFtZSwgeW91IGNhblxcJ3QgdXNlIGl0LicpO1xuICAgIH1cbn07XG5cblxuLyoqXG4gKiBVc2UgZGVmYXVsdCB2YWx1ZXMsIGlmIG9wdGlvbnMgYXJlIG5vdCBhdmFpbGFibGVcbiAqXG4gKiBAbWV0aG9kIF9nZXRPclNldERlZmF1bHRPcHRpb25zXG4gKiBAcGFyYW0ge09iamVjdH0gb3B0aW9ucyB0aGUgb3B0aW9ucyBnaXZlZCBieSB0aGUgdXNlclxuICogQHJldHVybiB7T2JqZWN0fSB0aGUgb3B0aW9ucyB3aXRoIGdhcHMgZmlsbGVkIGJ5IGRlZmF1bHRzXG4gKi9cbkNvbnRyYWN0LnByb3RvdHlwZS5fZ2V0T3JTZXREZWZhdWx0T3B0aW9ucyA9IGZ1bmN0aW9uIGdldE9yU2V0RGVmYXVsdE9wdGlvbnMob3B0aW9ucykge1xuICAgIGNvbnN0IGdhc1ByaWNlID0gb3B0aW9ucy5nYXNQcmljZSA/IFN0cmluZyhvcHRpb25zLmdhc1ByaWNlKSA6IG51bGw7XG4gICAgY29uc3QgZnJvbSA9IG9wdGlvbnMuZnJvbSA/IHV0aWxzLnRvQ2hlY2tzdW1BZGRyZXNzKGZvcm1hdHRlcnMuaW5wdXRBZGRyZXNzRm9ybWF0dGVyKG9wdGlvbnMuZnJvbSkpIDogbnVsbDtcblxuICAgIG9wdGlvbnMuZGF0YSA9IG9wdGlvbnMuZGF0YSB8fCB0aGlzLm9wdGlvbnMuZGF0YTtcblxuICAgIG9wdGlvbnMuZnJvbSA9IGZyb20gfHwgdGhpcy5vcHRpb25zLmZyb207XG4gICAgb3B0aW9ucy5nYXNQcmljZSA9IGdhc1ByaWNlIHx8IHRoaXMub3B0aW9ucy5nYXNQcmljZTtcbiAgICBvcHRpb25zLmdhcyA9IG9wdGlvbnMuZ2FzIHx8IG9wdGlvbnMuZ2FzTGltaXQgfHwgdGhpcy5vcHRpb25zLmdhcztcblxuICAgIC8vIFRPRE8gcmVwbGFjZSB3aXRoIG9ubHkgZ2FzTGltaXQ/XG4gICAgZGVsZXRlIG9wdGlvbnMuZ2FzTGltaXQ7XG5cbiAgICByZXR1cm4gb3B0aW9ucztcbn07XG5cblxuLyoqXG4gKiBTaG91bGQgYmUgdXNlZCB0byBlbmNvZGUgaW5kZXhlZCBwYXJhbXMgYW5kIG9wdGlvbnMgdG8gb25lIGZpbmFsIG9iamVjdFxuICpcbiAqIEBtZXRob2QgX2VuY29kZUV2ZW50QUJJXG4gKiBAcGFyYW0ge09iamVjdH0gZXZlbnRcbiAqIEBwYXJhbSB7T2JqZWN0fSBvcHRpb25zXG4gKiBAcmV0dXJuIHtPYmplY3R9IGV2ZXJ5dGhpbmcgY29tYmluZWQgdG9nZXRoZXIgYW5kIGVuY29kZWRcbiAqL1xuQ29udHJhY3QucHJvdG90eXBlLl9lbmNvZGVFdmVudEFCSSA9IGZ1bmN0aW9uIChldmVudCwgb3B0aW9ucykge1xuICAgIG9wdGlvbnMgPSBvcHRpb25zIHx8IHt9O1xuICAgIGNvbnN0IGZpbHRlciA9IG9wdGlvbnMuZmlsdGVyIHx8IHt9O1xuICAgIGNvbnN0IHJlc3VsdCA9IHt9O1xuXG4gICAgWydmcm9tQmxvY2snLCAndG9CbG9jayddLmZpbHRlcihmdW5jdGlvbiAoZikge1xuICAgICAgICByZXR1cm4gb3B0aW9uc1tmXSAhPT0gdW5kZWZpbmVkO1xuICAgIH0pLmZvckVhY2goZnVuY3Rpb24gKGYpIHtcbiAgICAgICAgcmVzdWx0W2ZdID0gZm9ybWF0dGVycy5pbnB1dEJsb2NrTnVtYmVyRm9ybWF0dGVyKG9wdGlvbnNbZl0pO1xuICAgIH0pO1xuXG4gICAgLy8gdXNlIGdpdmVuIHRvcGljc1xuICAgIGlmIChfLmlzQXJyYXkob3B0aW9ucy50b3BpY3MpKSB7XG4gICAgICAgIHJlc3VsdC50b3BpY3MgPSBvcHRpb25zLnRvcGljcztcblxuICAgICAgICAvLyBjcmVhdGUgdG9waWNzIGJhc2VkIG9uIGZpbHRlclxuICAgIH0gZWxzZSB7XG5cbiAgICAgICAgcmVzdWx0LnRvcGljcyA9IFtdO1xuXG4gICAgICAgIC8vIGFkZCBldmVudCBzaWduYXR1cmVcbiAgICAgICAgaWYgKGV2ZW50ICYmICFldmVudC5hbm9ueW1vdXMgJiYgZXZlbnQubmFtZSAhPT0gJ0FMTEVWRU5UUycpIHtcbiAgICAgICAgICAgIHJlc3VsdC50b3BpY3MucHVzaChldmVudC5zaWduYXR1cmUpO1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gYWRkIGV2ZW50IHRvcGljcyAoaW5kZXhlZCBhcmd1bWVudHMpXG4gICAgICAgIGlmIChldmVudC5uYW1lICE9PSAnQUxMRVZFTlRTJykge1xuICAgICAgICAgICAgY29uc3QgaW5kZXhlZFRvcGljcyA9IGV2ZW50LmlucHV0cy5maWx0ZXIoZnVuY3Rpb24gKGkpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gaS5pbmRleGVkID09PSB0cnVlO1xuICAgICAgICAgICAgfSkubWFwKGZ1bmN0aW9uIChpKSB7XG4gICAgICAgICAgICAgICAgY29uc3QgdmFsdWUgPSBmaWx0ZXJbaS5uYW1lXTtcbiAgICAgICAgICAgICAgICBpZiAoIXZhbHVlKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIC8vIFRPRE86IGh0dHBzOi8vZ2l0aHViLmNvbS9ldGhlcmV1bS93ZWIzLmpzL2lzc3Vlcy8zNDRcbiAgICAgICAgICAgICAgICAvLyBUT0RPOiBkZWFsIHByb3Blcmx5IHdpdGggY29tcG9uZW50c1xuXG4gICAgICAgICAgICAgICAgaWYgKF8uaXNBcnJheSh2YWx1ZSkpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHZhbHVlLm1hcChmdW5jdGlvbiAodikge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGFiaS5lbmNvZGVQYXJhbWV0ZXIoaS50eXBlLCB2KTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHJldHVybiBhYmkuZW5jb2RlUGFyYW1ldGVyKGkudHlwZSwgdmFsdWUpO1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIHJlc3VsdC50b3BpY3MgPSByZXN1bHQudG9waWNzLmNvbmNhdChpbmRleGVkVG9waWNzKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICghcmVzdWx0LnRvcGljcy5sZW5ndGgpXG4gICAgICAgICAgICBkZWxldGUgcmVzdWx0LnRvcGljcztcbiAgICB9XG5cbiAgICBpZiAodGhpcy5vcHRpb25zLmFkZHJlc3MpIHtcbiAgICAgICAgcmVzdWx0LmFkZHJlc3MgPSB0aGlzLm9wdGlvbnMuYWRkcmVzcy50b0xvd2VyQ2FzZSgpO1xuICAgIH1cblxuICAgIHJldHVybiByZXN1bHQ7XG59O1xuXG4vKipcbiAqIFNob3VsZCBiZSB1c2VkIHRvIGRlY29kZSBpbmRleGVkIHBhcmFtcyBhbmQgb3B0aW9uc1xuICpcbiAqIEBtZXRob2QgX2RlY29kZUV2ZW50QUJJXG4gKiBAcGFyYW0ge09iamVjdH0gZGF0YVxuICogQHJldHVybiB7T2JqZWN0fSByZXN1bHQgb2JqZWN0IHdpdGggZGVjb2RlZCBpbmRleGVkICYmIG5vdCBpbmRleGVkIHBhcmFtc1xuICovXG5Db250cmFjdC5wcm90b3R5cGUuX2RlY29kZUV2ZW50QUJJID0gZnVuY3Rpb24gKGRhdGEpIHtcbiAgICBsZXQgZXZlbnQgPSB0aGlzO1xuXG4gICAgZGF0YS5kYXRhID0gZGF0YS5kYXRhIHx8ICcnO1xuICAgIGRhdGEudG9waWNzID0gZGF0YS50b3BpY3MgfHwgW107XG4gICAgY29uc3QgcmVzdWx0ID0gZm9ybWF0dGVycy5vdXRwdXRMb2dGb3JtYXR0ZXIoZGF0YSk7XG5cbiAgICAvLyBpZiBhbGxFdmVudHMgZ2V0IHRoZSByaWdodCBldmVudFxuICAgIGlmIChldmVudC5uYW1lID09PSAnQUxMRVZFTlRTJykge1xuICAgICAgICBldmVudCA9IGV2ZW50Lmpzb25JbnRlcmZhY2UuZmluZChmdW5jdGlvbiAoaW50Zikge1xuICAgICAgICAgICAgcmV0dXJuIChpbnRmLnNpZ25hdHVyZSA9PT0gZGF0YS50b3BpY3NbMF0pO1xuICAgICAgICB9KSB8fCB7XG4gICAgICAgICAgICBhbm9ueW1vdXM6IHRydWVcbiAgICAgICAgfTtcbiAgICB9XG5cbiAgICAvLyBjcmVhdGUgZW1wdHkgaW5wdXRzIGlmIG5vbmUgYXJlIHByZXNlbnQgKGUuZy4gYW5vbnltb3VzIGV2ZW50cyBvbiBhbGxFdmVudHMpXG4gICAgZXZlbnQuaW5wdXRzID0gZXZlbnQuaW5wdXRzIHx8IFtdO1xuXG4gICAgLy8gSGFuZGxlIGNhc2Ugd2hlcmUgYW4gZXZlbnQgc2lnbmF0dXJlIHNoYWRvd3MgdGhlIGN1cnJlbnQgQUJJIHdpdGggbm9uLWlkZW50aWNhbFxuICAgIC8vIGFyZyBpbmRleGluZy4gSWYgIyBvZiB0b3BpY3MgZG9lc24ndCBtYXRjaCwgZXZlbnQgaXMgYW5vbi5cbiAgICBpZiAoIWV2ZW50LmFub255bW91cykge1xuICAgICAgICBsZXQgaW5kZXhlZElucHV0cyA9IDA7XG4gICAgICAgIGV2ZW50LmlucHV0cy5mb3JFYWNoKGlucHV0ID0+IGlucHV0LmluZGV4ZWQgPyBpbmRleGVkSW5wdXRzKysgOiBudWxsKTtcblxuICAgICAgICBpZiAoaW5kZXhlZElucHV0cyA+IDAgJiYgKGRhdGEudG9waWNzLmxlbmd0aCAhPT0gaW5kZXhlZElucHV0cyArIDEpKSB7XG4gICAgICAgICAgICBldmVudCA9IHtcbiAgICAgICAgICAgICAgICBhbm9ueW1vdXM6IHRydWUsXG4gICAgICAgICAgICAgICAgaW5wdXRzOiBbXVxuICAgICAgICAgICAgfTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGNvbnN0IGFyZ1RvcGljcyA9IGV2ZW50LmFub255bW91cyA/IGRhdGEudG9waWNzIDogZGF0YS50b3BpY3Muc2xpY2UoMSk7XG5cbiAgICByZXN1bHQucmV0dXJuVmFsdWVzID0gYWJpLmRlY29kZUxvZyhldmVudC5pbnB1dHMsIGRhdGEuZGF0YSwgYXJnVG9waWNzKTtcbiAgICBkZWxldGUgcmVzdWx0LnJldHVyblZhbHVlcy5fX2xlbmd0aF9fO1xuXG4gICAgLy8gYWRkIG5hbWVcbiAgICByZXN1bHQuZXZlbnQgPSBldmVudC5uYW1lO1xuXG4gICAgLy8gYWRkIHNpZ25hdHVyZVxuICAgIHJlc3VsdC5zaWduYXR1cmUgPSAoZXZlbnQuYW5vbnltb3VzIHx8ICFkYXRhLnRvcGljc1swXSkgPyBudWxsIDogZGF0YS50b3BpY3NbMF07XG5cbiAgICAvLyBtb3ZlIHRoZSBkYXRhIGFuZCB0b3BpY3MgdG8gXCJyYXdcIlxuICAgIHJlc3VsdC5yYXcgPSB7XG4gICAgICAgIGRhdGE6IHJlc3VsdC5kYXRhLFxuICAgICAgICB0b3BpY3M6IHJlc3VsdC50b3BpY3NcbiAgICB9O1xuICAgIGRlbGV0ZSByZXN1bHQuZGF0YTtcbiAgICBkZWxldGUgcmVzdWx0LnRvcGljcztcblxuXG4gICAgcmV0dXJuIHJlc3VsdDtcbn07XG5cbi8qKlxuICogRW5jb2RlcyBhbiBBQkkgZm9yIGEgbWV0aG9kLCBpbmNsdWRpbmcgc2lnbmF0dXJlIG9yIHRoZSBtZXRob2QuXG4gKiBPciB3aGVuIGNvbnN0cnVjdG9yIGVuY29kZXMgb25seSB0aGUgY29uc3RydWN0b3IgcGFyYW1ldGVycy5cbiAqXG4gKiBAbWV0aG9kIF9lbmNvZGVNZXRob2RBQklcbiAqIEBwYXJhbSB7TWl4ZWR9IGFyZ3MgdGhlIGFyZ3VtZW50cyB0byBlbmNvZGVcbiAqIEBwYXJhbSB7U3RyaW5nfSB0aGUgZW5jb2RlZCBBQklcbiAqL1xuQ29udHJhY3QucHJvdG90eXBlLl9lbmNvZGVNZXRob2RBQkkgPSBmdW5jdGlvbiBfZW5jb2RlTWV0aG9kQUJJKCkge1xuICAgIGNvbnN0IG1ldGhvZFNpZ25hdHVyZSA9IHRoaXMuX21ldGhvZC5zaWduYXR1cmU7XG4gICAgY29uc3QgYXJncyA9IHRoaXMuYXJndW1lbnRzIHx8IFtdO1xuXG4gICAgbGV0IHNpZ25hdHVyZSA9IGZhbHNlO1xuICAgIGNvbnN0IHBhcmFtc0FCSSA9IHRoaXMuX3BhcmVudC5vcHRpb25zLmpzb25JbnRlcmZhY2UuZmlsdGVyKGZ1bmN0aW9uIChqc29uKSB7XG4gICAgICAgIHJldHVybiAoKG1ldGhvZFNpZ25hdHVyZSA9PT0gJ2NvbnN0cnVjdG9yJyAmJiBqc29uLnR5cGUgPT09IG1ldGhvZFNpZ25hdHVyZSkgfHxcbiAgICAgICAgICAgICgoanNvbi5zaWduYXR1cmUgPT09IG1ldGhvZFNpZ25hdHVyZSB8fCBqc29uLnNpZ25hdHVyZSA9PT0gbWV0aG9kU2lnbmF0dXJlLnJlcGxhY2UoJzB4JywgJycpIHx8IGpzb24ubmFtZSA9PT0gbWV0aG9kU2lnbmF0dXJlKSAmJiBqc29uLnR5cGUgPT09ICdmdW5jdGlvbicpKTtcbiAgICB9KS5tYXAoZnVuY3Rpb24gKGpzb24pIHtcbiAgICAgICAgY29uc3QgaW5wdXRMZW5ndGggPSAoXy5pc0FycmF5KGpzb24uaW5wdXRzKSkgPyBqc29uLmlucHV0cy5sZW5ndGggOiAwO1xuXG4gICAgICAgIGlmIChpbnB1dExlbmd0aCAhPT0gYXJncy5sZW5ndGgpIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihgVGhlIG51bWJlciBvZiBhcmd1bWVudHMgaXMgbm90IG1hdGNoaW5nIHRoZSBtZXRob2RzIHJlcXVpcmVkIG51bWJlci4gWW91IG5lZWQgdG8gcGFzcyAkeyAgaW5wdXRMZW5ndGggIH0gYXJndW1lbnRzLmApO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGpzb24udHlwZSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgc2lnbmF0dXJlID0ganNvbi5zaWduYXR1cmU7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIF8uaXNBcnJheShqc29uLmlucHV0cykgPyBqc29uLmlucHV0cyA6IFtdO1xuICAgIH0pLm1hcChmdW5jdGlvbiAoaW5wdXRzKSB7XG4gICAgICAgIHJldHVybiBhYmkuZW5jb2RlUGFyYW1ldGVycyhpbnB1dHMsIGFyZ3MpLnJlcGxhY2UoJzB4JywgJycpO1xuICAgIH0pWzBdIHx8ICcnO1xuXG4gICAgLy8gcmV0dXJuIGNvbnN0cnVjdG9yXG4gICAgaWYgKG1ldGhvZFNpZ25hdHVyZSA9PT0gJ2NvbnN0cnVjdG9yJykge1xuICAgICAgICBpZiAoIXRoaXMuX2RlcGxveURhdGEpXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1RoZSBjb250cmFjdCBoYXMgbm8gY29udHJhY3QgZGF0YSBvcHRpb24gc2V0LiBUaGlzIGlzIG5lY2Vzc2FyeSB0byBhcHBlbmQgdGhlIGNvbnN0cnVjdG9yIHBhcmFtZXRlcnMuJyk7XG5cbiAgICAgICAgcmV0dXJuIHRoaXMuX2RlcGxveURhdGEgKyBwYXJhbXNBQkk7XG5cbiAgICAgICAgLy8gcmV0dXJuIG1ldGhvZFxuICAgIH1cblxuICAgIGNvbnN0IHJldHVyblZhbHVlID0gKHNpZ25hdHVyZSkgPyBzaWduYXR1cmUgKyBwYXJhbXNBQkkgOiBwYXJhbXNBQkk7XG5cbiAgICBpZiAoIXJldHVyblZhbHVlKSB7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcihgQ291bGRuJ3QgZmluZCBhIG1hdGNoaW5nIGNvbnRyYWN0IG1ldGhvZCBuYW1lZCBcIiR7ICB0aGlzLl9tZXRob2QubmFtZSAgfVwiLmApO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiByZXR1cm5WYWx1ZTtcbiAgICB9XG5cblxufTtcblxuXG4vKipcbiAqIERlY29kZSBtZXRob2QgcmV0dXJuIHZhbHVlc1xuICpcbiAqIEBtZXRob2QgX2RlY29kZU1ldGhvZFJldHVyblxuICogQHBhcmFtIHtBcnJheX0gb3V0cHV0c1xuICogQHBhcmFtIHtTdHJpbmd9IHJldHVyblZhbHVlc1xuICogQHJldHVybiB7T2JqZWN0fSBkZWNvZGVkIG91dHB1dCByZXR1cm4gdmFsdWVzXG4gKi9cbkNvbnRyYWN0LnByb3RvdHlwZS5fZGVjb2RlTWV0aG9kUmV0dXJuID0gZnVuY3Rpb24gKG91dHB1dHMsIHJldHVyblZhbHVlcykge1xuICAgIGlmICghcmV0dXJuVmFsdWVzKSB7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH1cblxuICAgIHJldHVyblZhbHVlcyA9IHJldHVyblZhbHVlcy5sZW5ndGggPj0gMiA/IHJldHVyblZhbHVlcy5zbGljZSgyKSA6IHJldHVyblZhbHVlcztcbiAgICBjb25zdCByZXN1bHQgPSBhYmkuZGVjb2RlUGFyYW1ldGVycyhvdXRwdXRzLCByZXR1cm5WYWx1ZXMpO1xuXG4gICAgaWYgKHJlc3VsdC5fX2xlbmd0aF9fID09PSAxKSB7XG4gICAgICAgIHJldHVybiByZXN1bHRbMF07XG4gICAgfVxuICAgIGRlbGV0ZSByZXN1bHQuX19sZW5ndGhfXztcbiAgICByZXR1cm4gcmVzdWx0O1xuXG59O1xuXG5cbi8qKlxuICogRGVwbG95cyBhIGNvbnRyYWN0IGFuZCBmaXJlIGV2ZW50cyBiYXNlZCBvbiBpdHMgc3RhdGU6IHRyYW5zYWN0aW9uSGFzaCwgcmVjZWlwdFxuICpcbiAqIEFsbCBldmVudCBsaXN0ZW5lcnMgd2lsbCBiZSByZW1vdmVkLCBvbmNlIHRoZSBsYXN0IHBvc3NpYmxlIGV2ZW50IGlzIGZpcmVkIChcImVycm9yXCIsIG9yIFwicmVjZWlwdFwiKVxuICpcbiAqIEBtZXRob2QgZGVwbG95XG4gKiBAcGFyYW0ge09iamVjdH0gb3B0aW9uc1xuICogQHBhcmFtIHtGdW5jdGlvbn0gY2FsbGJhY2tcbiAqIEByZXR1cm4ge09iamVjdH0gRXZlbnRFbWl0dGVyIHBvc3NpYmxlIGV2ZW50cyBhcmUgXCJlcnJvclwiLCBcInRyYW5zYWN0aW9uSGFzaFwiIGFuZCBcInJlY2VpcHRcIlxuICovXG5Db250cmFjdC5wcm90b3R5cGUuZGVwbG95ID0gZnVuY3Rpb24gKG9wdGlvbnMsIGNhbGxiYWNrKSB7XG5cbiAgICBvcHRpb25zID0gb3B0aW9ucyB8fCB7fTtcblxuICAgIG9wdGlvbnMuYXJndW1lbnRzID0gb3B0aW9ucy5hcmd1bWVudHMgfHwgW107XG4gICAgb3B0aW9ucyA9IHRoaXMuX2dldE9yU2V0RGVmYXVsdE9wdGlvbnMob3B0aW9ucyk7XG5cblxuICAgIC8vIHJldHVybiBlcnJvciwgaWYgbm8gXCJkYXRhXCIgaXMgc3BlY2lmaWVkXG4gICAgaWYgKCFvcHRpb25zLmRhdGEpIHtcbiAgICAgICAgcmV0dXJuIHV0aWxzLl9maXJlRXJyb3IobmV3IEVycm9yKCdObyBcImRhdGFcIiBzcGVjaWZpZWQgaW4gbmVpdGhlciB0aGUgZ2l2ZW4gb3B0aW9ucywgbm9yIHRoZSBkZWZhdWx0IG9wdGlvbnMuJyksIG51bGwsIG51bGwsIGNhbGxiYWNrKTtcbiAgICB9XG5cbiAgICBjb25zdCBjb25zdHJ1Y3RvciA9IF8uZmluZCh0aGlzLm9wdGlvbnMuanNvbkludGVyZmFjZSwgZnVuY3Rpb24gKG1ldGhvZCkge1xuICAgICAgICByZXR1cm4gKG1ldGhvZC50eXBlID09PSAnY29uc3RydWN0b3InKTtcbiAgICB9KSB8fCB7fTtcbiAgICBjb25zdHJ1Y3Rvci5zaWduYXR1cmUgPSAnY29uc3RydWN0b3InO1xuXG4gICAgcmV0dXJuIHRoaXMuX2NyZWF0ZVR4T2JqZWN0LmFwcGx5KHtcbiAgICAgICAgbWV0aG9kOiBjb25zdHJ1Y3RvcixcbiAgICAgICAgcGFyZW50OiB0aGlzLFxuICAgICAgICBkZXBsb3lEYXRhOiBvcHRpb25zLmRhdGEsXG4gICAgICAgIF9ldGhBY2NvdW50czogdGhpcy5jb25zdHJ1Y3Rvci5fZXRoQWNjb3VudHNcbiAgICB9LCBvcHRpb25zLmFyZ3VtZW50cyk7XG5cbn07XG5cbi8qKlxuICogR2V0cyB0aGUgZXZlbnQgc2lnbmF0dXJlIGFuZCBvdXRwdXRGb3JtYXR0ZXJzXG4gKlxuICogQG1ldGhvZCBfZ2VuZXJhdGVFdmVudE9wdGlvbnNcbiAqIEBwYXJhbSB7T2JqZWN0fSBldmVudFxuICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnNcbiAqIEBwYXJhbSB7RnVuY3Rpb259IGNhbGxiYWNrXG4gKiBAcmV0dXJuIHtPYmplY3R9IHRoZSBldmVudCBvcHRpb25zIG9iamVjdFxuICovXG5Db250cmFjdC5wcm90b3R5cGUuX2dlbmVyYXRlRXZlbnRPcHRpb25zID0gZnVuY3Rpb24gKCkge1xuICAgIGNvbnN0IGFyZ3MgPSBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChhcmd1bWVudHMpO1xuXG4gICAgLy8gZ2V0IHRoZSBjYWxsYmFja1xuICAgIGNvbnN0IGNhbGxiYWNrID0gdGhpcy5fZ2V0Q2FsbGJhY2soYXJncyk7XG5cbiAgICAvLyBnZXQgdGhlIG9wdGlvbnNcbiAgICBjb25zdCBvcHRpb25zID0gKF8uaXNPYmplY3QoYXJnc1thcmdzLmxlbmd0aCAtIDFdKSkgPyBhcmdzLnBvcCgpIDoge307XG5cbiAgICBjb25zdCBldmVudE5hbWUgPSAoXy5pc1N0cmluZyhhcmdzWzBdKSkgPyBhcmdzWzBdIDogJ2FsbGV2ZW50cyc7XG4gICAgY29uc3QgZXZlbnQgPSAoZXZlbnROYW1lLnRvTG93ZXJDYXNlKCkgPT09ICdhbGxldmVudHMnKSA/IHtcbiAgICAgICAgbmFtZTogJ0FMTEVWRU5UUycsXG4gICAgICAgIGpzb25JbnRlcmZhY2U6IHRoaXMub3B0aW9ucy5qc29uSW50ZXJmYWNlXG4gICAgfSA6IHRoaXMub3B0aW9ucy5qc29uSW50ZXJmYWNlLmZpbmQoZnVuY3Rpb24gKGpzb24pIHtcbiAgICAgICAgcmV0dXJuIChqc29uLnR5cGUgPT09ICdldmVudCcgJiYgKGpzb24ubmFtZSA9PT0gZXZlbnROYW1lIHx8IGpzb24uc2lnbmF0dXJlID09PSAnMHgnICsgZXZlbnROYW1lLnJlcGxhY2UoJzB4JywgJycpKSk7XG4gICAgfSk7XG5cbiAgICBpZiAoIWV2ZW50KSB7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcihgRXZlbnQgXCIkeyAgZXZlbnROYW1lICB9XCIgZG9lc24ndCBleGlzdCBpbiB0aGlzIGNvbnRyYWN0LmApO1xuICAgIH1cblxuICAgIGlmICghdXRpbHMuaXNBZGRyZXNzKHRoaXMub3B0aW9ucy5hZGRyZXNzKSkge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1RoaXMgY29udHJhY3Qgb2JqZWN0IGRvZXNuXFwndCBoYXZlIGFkZHJlc3Mgc2V0IHlldCwgcGxlYXNlIHNldCBhbiBhZGRyZXNzIGZpcnN0LicpO1xuICAgIH1cblxuICAgIHJldHVybiB7XG4gICAgICAgIHBhcmFtczogdGhpcy5fZW5jb2RlRXZlbnRBQkkoZXZlbnQsIG9wdGlvbnMpLFxuICAgICAgICBldmVudCxcbiAgICAgICAgY2FsbGJhY2tcbiAgICB9O1xufTtcblxuLyoqXG4gKiBBZGRzIGV2ZW50IGxpc3RlbmVycyBhbmQgY3JlYXRlcyBhIHN1YnNjcmlwdGlvbiwgYW5kIHJlbW92ZSBpdCBvbmNlIGl0cyBmaXJlZC5cbiAqXG4gKiBAbWV0aG9kIGNsb25lXG4gKiBAcmV0dXJuIHtPYmplY3R9IHRoZSBldmVudCBzdWJzY3JpcHRpb25cbiAqL1xuQ29udHJhY3QucHJvdG90eXBlLmNsb25lID0gZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiBuZXcgdGhpcy5jb25zdHJ1Y3Rvcih0aGlzLm9wdGlvbnMuanNvbkludGVyZmFjZSwgdGhpcy5vcHRpb25zLmFkZHJlc3MsIHRoaXMub3B0aW9ucyk7XG59O1xuXG5cbi8qKlxuICogQWRkcyBldmVudCBsaXN0ZW5lcnMgYW5kIGNyZWF0ZXMgYSBzdWJzY3JpcHRpb24sIGFuZCByZW1vdmUgaXQgb25jZSBpdHMgZmlyZWQuXG4gKlxuICogQG1ldGhvZCBvbmNlXG4gKiBAcGFyYW0ge1N0cmluZ30gZXZlbnRcbiAqIEBwYXJhbSB7T2JqZWN0fSBvcHRpb25zXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBjYWxsYmFja1xuICogQHJldHVybiB7T2JqZWN0fSB0aGUgZXZlbnQgc3Vic2NyaXB0aW9uXG4gKi9cbkNvbnRyYWN0LnByb3RvdHlwZS5vbmNlID0gZnVuY3Rpb24gKGV2ZW50LCBvcHRpb25zLCBjYWxsYmFjaykge1xuICAgIGNvbnN0IGFyZ3MgPSBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChhcmd1bWVudHMpO1xuXG4gICAgLy8gZ2V0IHRoZSBjYWxsYmFja1xuICAgIGNhbGxiYWNrID0gdGhpcy5fZ2V0Q2FsbGJhY2soYXJncyk7XG5cbiAgICBpZiAoIWNhbGxiYWNrKSB7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcignT25jZSByZXF1aXJlcyBhIGNhbGxiYWNrIGFzIHRoZSBzZWNvbmQgcGFyYW1ldGVyLicpO1xuICAgIH1cblxuICAgIC8vIGRvbid0IGFsbG93IGZyb21CbG9ja1xuICAgIGlmIChvcHRpb25zKVxuICAgICAgICBkZWxldGUgb3B0aW9ucy5mcm9tQmxvY2s7XG5cbiAgICAvLyBkb24ndCByZXR1cm4gYXMgb25jZSBzaG91bGRuJ3QgcHJvdmlkZSBcIm9uXCJcbiAgICB0aGlzLl9vbihldmVudCwgb3B0aW9ucywgZnVuY3Rpb24gKGVyciwgcmVzLCBzdWIpIHtcbiAgICAgICAgc3ViLnVuc3Vic2NyaWJlKCk7XG4gICAgICAgIGlmIChfLmlzRnVuY3Rpb24oY2FsbGJhY2spKSB7XG4gICAgICAgICAgICBjYWxsYmFjayhlcnIsIHJlcywgc3ViKTtcbiAgICAgICAgfVxuICAgIH0pO1xuXG4gICAgcmV0dXJuIHVuZGVmaW5lZDtcbn07XG5cbi8qKlxuICogQWRkcyBldmVudCBsaXN0ZW5lcnMgYW5kIGNyZWF0ZXMgYSBzdWJzY3JpcHRpb24uXG4gKlxuICogQG1ldGhvZCBfb25cbiAqIEBwYXJhbSB7U3RyaW5nfSBldmVudFxuICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnNcbiAqIEBwYXJhbSB7RnVuY3Rpb259IGNhbGxiYWNrXG4gKiBAcmV0dXJuIHtPYmplY3R9IHRoZSBldmVudCBzdWJzY3JpcHRpb25cbiAqL1xuQ29udHJhY3QucHJvdG90eXBlLl9vbiA9IGZ1bmN0aW9uICgpIHtcbiAgICBjb25zdCBzdWJPcHRpb25zID0gdGhpcy5fZ2VuZXJhdGVFdmVudE9wdGlvbnMuYXBwbHkodGhpcywgYXJndW1lbnRzKTtcblxuXG4gICAgLy8gcHJldmVudCB0aGUgZXZlbnQgXCJuZXdMaXN0ZW5lclwiIGFuZCBcInJlbW92ZUxpc3RlbmVyXCIgZnJvbSBiZWluZyBvdmVyd3JpdHRlblxuICAgIHRoaXMuX2NoZWNrTGlzdGVuZXIoJ25ld0xpc3RlbmVyJywgc3ViT3B0aW9ucy5ldmVudC5uYW1lLCBzdWJPcHRpb25zLmNhbGxiYWNrKTtcbiAgICB0aGlzLl9jaGVja0xpc3RlbmVyKCdyZW1vdmVMaXN0ZW5lcicsIHN1Yk9wdGlvbnMuZXZlbnQubmFtZSwgc3ViT3B0aW9ucy5jYWxsYmFjayk7XG5cbiAgICAvLyBUT0RPIGNoZWNrIGlmIGxpc3RlbmVyIGFscmVhZHkgZXhpc3RzPyBhbmQgcmV1c2Ugc3Vic2NyaXB0aW9uIGlmIG9wdGlvbnMgYXJlIHRoZSBzYW1lLlxuXG4gICAgLy8gY3JlYXRlIG5ldyBzdWJzY3JpcHRpb25cbiAgICBjb25zdCBzdWJzY3JpcHRpb24gPSBuZXcgU3Vic2NyaXB0aW9uKHtcbiAgICAgICAgc3Vic2NyaXB0aW9uOiB7XG4gICAgICAgICAgICBwYXJhbXM6IDEsXG4gICAgICAgICAgICBpbnB1dEZvcm1hdHRlcjogW2Zvcm1hdHRlcnMuaW5wdXRMb2dGb3JtYXR0ZXJdLFxuICAgICAgICAgICAgb3V0cHV0Rm9ybWF0dGVyOiB0aGlzLl9kZWNvZGVFdmVudEFCSS5iaW5kKHN1Yk9wdGlvbnMuZXZlbnQpLFxuICAgICAgICAgICAgLy8gRFVCTElDQVRFLCBhbHNvIGluIHdlYjMtZXRoXG4gICAgICAgICAgICBzdWJzY3JpcHRpb25IYW5kbGVyKG91dHB1dCkge1xuICAgICAgICAgICAgICAgIGlmIChvdXRwdXQucmVtb3ZlZCkge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmVtaXQoJ2NoYW5nZWQnLCBvdXRwdXQpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZW1pdCgnZGF0YScsIG91dHB1dCk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYgKF8uaXNGdW5jdGlvbih0aGlzLmNhbGxiYWNrKSkge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmNhbGxiYWNrKG51bGwsIG91dHB1dCwgdGhpcyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICB0eXBlOiAnZXRoJyxcbiAgICAgICAgcmVxdWVzdE1hbmFnZXI6IHRoaXMuX3JlcXVlc3RNYW5hZ2VyXG4gICAgfSk7XG4gICAgc3Vic2NyaXB0aW9uLnN1YnNjcmliZSgnbG9ncycsIHN1Yk9wdGlvbnMucGFyYW1zLCBzdWJPcHRpb25zLmNhbGxiYWNrIHx8IGZ1bmN0aW9uICgpIHt9KTtcblxuICAgIHJldHVybiBzdWJzY3JpcHRpb247XG59O1xuXG4vKipcbiAqIEdldCBwYXN0IGV2ZW50cyBmcm9tIGNvbnRyYWN0c1xuICpcbiAqIEBtZXRob2QgZ2V0UGFzdEV2ZW50c1xuICogQHBhcmFtIHtTdHJpbmd9IGV2ZW50XG4gKiBAcGFyYW0ge09iamVjdH0gb3B0aW9uc1xuICogQHBhcmFtIHtGdW5jdGlvbn0gY2FsbGJhY2tcbiAqIEByZXR1cm4ge09iamVjdH0gdGhlIHByb21pZXZlbnRcbiAqL1xuQ29udHJhY3QucHJvdG90eXBlLmdldFBhc3RFdmVudHMgPSBmdW5jdGlvbiAoKSB7XG4gICAgY29uc3Qgc3ViT3B0aW9ucyA9IHRoaXMuX2dlbmVyYXRlRXZlbnRPcHRpb25zLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XG5cbiAgICBjb25zdCBnZXRQYXN0TG9ncyA9IG5ldyBNZXRob2Qoe1xuICAgICAgICBuYW1lOiAnZ2V0UGFzdExvZ3MnLFxuICAgICAgICBjYWxsOiAnZXRoX2dldExvZ3MnLFxuICAgICAgICBwYXJhbXM6IDEsXG4gICAgICAgIGlucHV0Rm9ybWF0dGVyOiBbZm9ybWF0dGVycy5pbnB1dExvZ0Zvcm1hdHRlcl0sXG4gICAgICAgIG91dHB1dEZvcm1hdHRlcjogdGhpcy5fZGVjb2RlRXZlbnRBQkkuYmluZChzdWJPcHRpb25zLmV2ZW50KVxuICAgIH0pO1xuICAgIGdldFBhc3RMb2dzLnNldFJlcXVlc3RNYW5hZ2VyKHRoaXMuX3JlcXVlc3RNYW5hZ2VyKTtcbiAgICBjb25zdCBjYWxsID0gZ2V0UGFzdExvZ3MuYnVpbGRDYWxsKCk7XG5cbiAgICBnZXRQYXN0TG9ncyA9IG51bGw7XG5cbiAgICByZXR1cm4gY2FsbChzdWJPcHRpb25zLnBhcmFtcywgc3ViT3B0aW9ucy5jYWxsYmFjayk7XG59O1xuXG5cbi8qKlxuICogcmV0dXJucyB0aGUgYW4gb2JqZWN0IHdpdGggY2FsbCwgc2VuZCwgZXN0aW1hdGUgZnVuY3Rpb25zXG4gKlxuICogQG1ldGhvZCBfY3JlYXRlVHhPYmplY3RcbiAqIEByZXR1cm5zIHtPYmplY3R9IGFuIG9iamVjdCB3aXRoIGZ1bmN0aW9ucyB0byBjYWxsIHRoZSBtZXRob2RzXG4gKi9cbkNvbnRyYWN0LnByb3RvdHlwZS5fY3JlYXRlVHhPYmplY3QgPSBmdW5jdGlvbiBfY3JlYXRlVHhPYmplY3QoKSB7XG4gICAgY29uc3QgYXJncyA9IEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGFyZ3VtZW50cyk7XG4gICAgY29uc3QgdHhPYmplY3QgPSB7fTtcblxuICAgIGlmICh0aGlzLm1ldGhvZC50eXBlID09PSAnZnVuY3Rpb24nKSB7XG5cbiAgICAgICAgdHhPYmplY3QuY2FsbCA9IHRoaXMucGFyZW50Ll9leGVjdXRlTWV0aG9kLmJpbmQodHhPYmplY3QsICdjYWxsJyk7XG4gICAgICAgIHR4T2JqZWN0LmNhbGwucmVxdWVzdCA9IHRoaXMucGFyZW50Ll9leGVjdXRlTWV0aG9kLmJpbmQodHhPYmplY3QsICdjYWxsJywgdHJ1ZSk7IC8vIHRvIG1ha2UgYmF0Y2ggcmVxdWVzdHNcblxuICAgIH1cblxuICAgIHR4T2JqZWN0LnNlbmQgPSB0aGlzLnBhcmVudC5fZXhlY3V0ZU1ldGhvZC5iaW5kKHR4T2JqZWN0LCAnc2VuZCcpO1xuICAgIHR4T2JqZWN0LnNlbmQucmVxdWVzdCA9IHRoaXMucGFyZW50Ll9leGVjdXRlTWV0aG9kLmJpbmQodHhPYmplY3QsICdzZW5kJywgdHJ1ZSk7IC8vIHRvIG1ha2UgYmF0Y2ggcmVxdWVzdHNcbiAgICB0eE9iamVjdC5lbmNvZGVBQkkgPSB0aGlzLnBhcmVudC5fZW5jb2RlTWV0aG9kQUJJLmJpbmQodHhPYmplY3QpO1xuICAgIHR4T2JqZWN0LmVzdGltYXRlR2FzID0gdGhpcy5wYXJlbnQuX2V4ZWN1dGVNZXRob2QuYmluZCh0eE9iamVjdCwgJ2VzdGltYXRlJyk7XG4gICAgdHhPYmplY3QuZXN0aW1hdGVDb3N0cyA9IHRoaXMucGFyZW50Ll9leGVjdXRlTWV0aG9kLmJpbmQodHhPYmplY3QsICdlc3RpbWF0ZUNvc3RzJyk7XG5cblxuICAgIGlmIChhcmdzICYmIHRoaXMubWV0aG9kLmlucHV0cyAmJiBhcmdzLmxlbmd0aCAhPT0gdGhpcy5tZXRob2QuaW5wdXRzLmxlbmd0aCkge1xuICAgICAgICBpZiAodGhpcy5uZXh0TWV0aG9kKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5uZXh0TWV0aG9kLmFwcGx5KG51bGwsIGFyZ3MpO1xuICAgICAgICB9XG4gICAgICAgIHRocm93IGVycm9ycy5JbnZhbGlkTnVtYmVyT2ZQYXJhbXMoYXJncy5sZW5ndGgsIHRoaXMubWV0aG9kLmlucHV0cy5sZW5ndGgsIHRoaXMubWV0aG9kLm5hbWUpO1xuICAgIH1cblxuICAgIHR4T2JqZWN0LmFyZ3VtZW50cyA9IGFyZ3MgfHwgW107XG4gICAgdHhPYmplY3QuX21ldGhvZCA9IHRoaXMubWV0aG9kO1xuICAgIHR4T2JqZWN0Ll9wYXJlbnQgPSB0aGlzLnBhcmVudDtcbiAgICB0eE9iamVjdC5fZXRoQWNjb3VudHMgPSB0aGlzLnBhcmVudC5jb25zdHJ1Y3Rvci5fZXRoQWNjb3VudHMgfHwgdGhpcy5fZXRoQWNjb3VudHM7XG5cbiAgICBpZiAodGhpcy5kZXBsb3lEYXRhKSB7XG4gICAgICAgIHR4T2JqZWN0Ll9kZXBsb3lEYXRhID0gdGhpcy5kZXBsb3lEYXRhO1xuICAgIH1cblxuICAgIHJldHVybiB0eE9iamVjdDtcbn07XG5cblxuLyoqXG4gKiBHZW5lcmF0ZXMgdGhlIG9wdGlvbnMgZm9yIHRoZSBleGVjdXRlIGNhbGxcbiAqXG4gKiBAbWV0aG9kIF9wcm9jZXNzRXhlY3V0ZUFyZ3VtZW50c1xuICogQHBhcmFtIHtBcnJheX0gYXJnc1xuICogQHBhcmFtIHtQcm9taXNlfSBkZWZlclxuICovXG5Db250cmFjdC5wcm90b3R5cGUuX3Byb2Nlc3NFeGVjdXRlQXJndW1lbnRzID0gZnVuY3Rpb24gX3Byb2Nlc3NFeGVjdXRlQXJndW1lbnRzKGFyZ3MsIGRlZmVyKSB7XG4gICAgY29uc3QgcHJvY2Vzc2VkQXJncyA9IHt9O1xuXG4gICAgcHJvY2Vzc2VkQXJncy50eXBlID0gYXJncy5zaGlmdCgpO1xuXG4gICAgLy8gZ2V0IHRoZSBjYWxsYmFja1xuICAgIHByb2Nlc3NlZEFyZ3MuY2FsbGJhY2sgPSB0aGlzLl9wYXJlbnQuX2dldENhbGxiYWNrKGFyZ3MpO1xuXG4gICAgLy8gZ2V0IGJsb2NrIG51bWJlciB0byB1c2UgZm9yIGNhbGxcbiAgICBpZiAocHJvY2Vzc2VkQXJncy50eXBlID09PSAnY2FsbCcgJiYgYXJnc1thcmdzLmxlbmd0aCAtIDFdICE9PSB0cnVlICYmIChfLmlzU3RyaW5nKGFyZ3NbYXJncy5sZW5ndGggLSAxXSkgfHwgaXNGaW5pdGUoYXJnc1thcmdzLmxlbmd0aCAtIDFdKSkpXG4gICAgICAgIHByb2Nlc3NlZEFyZ3MuZGVmYXVsdEJsb2NrID0gYXJncy5wb3AoKTtcblxuICAgIC8vIGdldCB0aGUgb3B0aW9uc1xuICAgIHByb2Nlc3NlZEFyZ3Mub3B0aW9ucyA9IChfLmlzT2JqZWN0KGFyZ3NbYXJncy5sZW5ndGggLSAxXSkpID8gYXJncy5wb3AoKSA6IHt9O1xuXG4gICAgLy8gZ2V0IHRoZSBnZW5lcmF0ZVJlcXVlc3QgYXJndW1lbnQgZm9yIGJhdGNoIHJlcXVlc3RzXG4gICAgcHJvY2Vzc2VkQXJncy5nZW5lcmF0ZVJlcXVlc3QgPSAoYXJnc1thcmdzLmxlbmd0aCAtIDFdID09PSB0cnVlKSA/IGFyZ3MucG9wKCkgOiBmYWxzZTtcblxuICAgIHByb2Nlc3NlZEFyZ3Mub3B0aW9ucyA9IHRoaXMuX3BhcmVudC5fZ2V0T3JTZXREZWZhdWx0T3B0aW9ucyhwcm9jZXNzZWRBcmdzLm9wdGlvbnMpO1xuICAgIHByb2Nlc3NlZEFyZ3Mub3B0aW9ucy5kYXRhID0gdGhpcy5lbmNvZGVBQkkoKTtcbiAgICBwcm9jZXNzZWRBcmdzLm9yaWdpbmFsQXJndW1lbnRzID0gdGhpcy5hcmd1bWVudHM7XG5cbiAgICAvLyBhZGQgY29udHJhY3QgYWRkcmVzc1xuICAgIGlmICghdGhpcy5fZGVwbG95RGF0YSAmJiAhdXRpbHMuaXNBZGRyZXNzKHRoaXMuX3BhcmVudC5vcHRpb25zLmFkZHJlc3MpKVxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1RoaXMgY29udHJhY3Qgb2JqZWN0IGRvZXNuXFwndCBoYXZlIGFkZHJlc3Mgc2V0IHlldCwgcGxlYXNlIHNldCBhbiBhZGRyZXNzIGZpcnN0LicpO1xuXG4gICAgaWYgKCF0aGlzLl9kZXBsb3lEYXRhKVxuICAgICAgICBwcm9jZXNzZWRBcmdzLm9wdGlvbnMudG8gPSB0aGlzLl9wYXJlbnQub3B0aW9ucy5hZGRyZXNzO1xuXG4gICAgLy8gcmV0dXJuIGVycm9yLCBpZiBubyBcImRhdGFcIiBpcyBzcGVjaWZpZWRcbiAgICBpZiAoIXByb2Nlc3NlZEFyZ3Mub3B0aW9ucy5kYXRhKVxuICAgICAgICByZXR1cm4gdXRpbHMuX2ZpcmVFcnJvcihuZXcgRXJyb3IoJ0NvdWxkblxcJ3QgZmluZCBhIG1hdGNoaW5nIGNvbnRyYWN0IG1ldGhvZCwgb3IgdGhlIG51bWJlciBvZiBwYXJhbWV0ZXJzIGlzIHdyb25nLicpLCBkZWZlci5ldmVudEVtaXR0ZXIsIGRlZmVyLnJlamVjdCwgcHJvY2Vzc2VkQXJncy5jYWxsYmFjayk7XG5cbiAgICByZXR1cm4gcHJvY2Vzc2VkQXJncztcbn07XG5cbi8qKlxuICogRXhlY3V0ZXMgYSBjYWxsLCB0cmFuc2FjdCBvciBlc3RpbWF0ZUdhcyBvbiBhIGNvbnRyYWN0IGZ1bmN0aW9uXG4gKlxuICogQG1ldGhvZCBfZXhlY3V0ZU1ldGhvZFxuICogQHBhcmFtIHtTdHJpbmd9IHR5cGUgdGhlIHR5cGUgdGhpcyBleGVjdXRlIGZ1bmN0aW9uIHNob3VsZCBleGVjdXRlXG4gKiBAcGFyYW0ge0Jvb2xlYW59IG1ha2VSZXF1ZXN0IGlmIHRydWUsIGl0IHNpbXBseSByZXR1cm5zIHRoZSByZXF1ZXN0IHBhcmFtZXRlcnMsIHJhdGhlciB0aGFuIGV4ZWN1dGluZyBpdFxuICovXG5Db250cmFjdC5wcm90b3R5cGUuX2V4ZWN1dGVNZXRob2QgPSBmdW5jdGlvbiBfZXhlY3V0ZU1ldGhvZCgpIHtcbiAgICBjb25zdCBfdGhpcyA9IHRoaXM7XG4gICAgbGV0IGRlZmVyO1xuICAgIGNvbnN0IGFyZ3MgPSB0aGlzLl9wYXJlbnQuX3Byb2Nlc3NFeGVjdXRlQXJndW1lbnRzLmNhbGwodGhpcywgQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoYXJndW1lbnRzKSwgZGVmZXIpXG4gICAgZGVmZXIgPSBwcm9taUV2ZW50KChhcmdzLnR5cGUgIT09ICdzZW5kJykpXG4gICAgY29uc3QgZXRoQWNjb3VudHMgPSBfdGhpcy5jb25zdHJ1Y3Rvci5fZXRoQWNjb3VudHMgfHwgX3RoaXMuX2V0aEFjY291bnRzO1xuXG4gICAgLy8gc2ltcGxlIHJldHVybiByZXF1ZXN0IGZvciBiYXRjaCByZXF1ZXN0c1xuICAgIGlmIChhcmdzLmdlbmVyYXRlUmVxdWVzdCkge1xuXG4gICAgICAgIGNvbnN0IHBheWxvYWQgPSB7XG4gICAgICAgICAgICBwYXJhbXM6IFtmb3JtYXR0ZXJzLmlucHV0Q2FsbEZvcm1hdHRlci5jYWxsKHRoaXMuX3BhcmVudCwgYXJncy5vcHRpb25zKV0sXG4gICAgICAgICAgICBjYWxsYmFjazogYXJncy5jYWxsYmFja1xuICAgICAgICB9O1xuXG4gICAgICAgIGlmIChhcmdzLnR5cGUgPT09ICdjYWxsJykge1xuICAgICAgICAgICAgcGF5bG9hZC5wYXJhbXMucHVzaChmb3JtYXR0ZXJzLmlucHV0RGVmYXVsdEJsb2NrTnVtYmVyRm9ybWF0dGVyLmNhbGwodGhpcy5fcGFyZW50LCBhcmdzLmRlZmF1bHRCbG9jaykpO1xuICAgICAgICAgICAgcGF5bG9hZC5tZXRob2QgPSAnZXRoX2NhbGwnO1xuICAgICAgICAgICAgcGF5bG9hZC5mb3JtYXQgPSB0aGlzLl9wYXJlbnQuX2RlY29kZU1ldGhvZFJldHVybi5iaW5kKG51bGwsIHRoaXMuX21ldGhvZC5vdXRwdXRzKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHBheWxvYWQubWV0aG9kID0gJ2V0aF9zZW5kVHJhbnNhY3Rpb24nO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHBheWxvYWQ7XG5cbiAgICB9XG5cbiAgICBzd2l0Y2ggKGFyZ3MudHlwZSkge1xuICAgICAgICBjYXNlICdlc3RpbWF0ZUNvc3RzJzpcbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICBSZXR1cm4gT2JqZWN0IFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgT3B0aW9uYWw8bWVzc2FnZT46KFJldHVybiBvbiBmYWlsdXJlKSBVVEY4IGRlY29kZWQgbWVzc2FnZVxuICAgICAgICAgICAgICAgIE9wdGlvbmFsPGRhdGE+OiAoUmV0dXJuIG9uIGZhaWx1cmUpIGhleCBlbmNvZGVkIHN0cmluZyBvciBudWxsIGlmIG5vdCBleGlzdFxuICAgICAgICAgICAgICAgIG1heWJlUmV2ZXJ0czogYm9vbGVhbiB0cnVlIGlmIHRyYW5zYWN0aW9uIG1heSBmYWlsIG90aGVyd2lzZSBpcyBmYWxzZVxuICAgICAgICAgICAgICAgIE9wdGlvbmFsPGdhcz46IGVzdGltYXRlR2FzIGZvciBjdXJyZW50IG1ldGhvZCBvciBudWxsIGlmIGZhaWxzXG4gICAgICAgICAgICAgICAgZ2FzUHJpY2U6IGN1cnJlbnQgYmxvY2tjaGFpbiBnYXNQcmljZVxuICAgICAgICAgICAgICAgIE9wdGlvbmFsPG1pbmVyRmVlPjogKGVzdGltYXRlR2FzICogZ2FzUHJpY2UgPG1pbmVyIHByb2ZpdD4gKSBvciBudWxsIGlmIGZhaWxzXG4gICAgICAgICAgICAgICAgZ2FzTWFuYWdlckZlZTogYWRkaXRpb25hbEZlZSAtIG1pbmVyRmVlIFxuICAgICAgICAgICAgICAgIGFkZGl0aW9uYWxGZWU6IGFkZGl0aW9uYWwgY29zdCBpcyB0aGUgcmVzdWx0IGZyb20gYSBjb250cmFjdCBxdWVyeSAtIGFtb3VudCB2YWx1ZVxuICAgICAgICAgICAgICAgIHRvdGFsQ29zdDogQWRkaXRpb25hbEZlZSBpcyB0aGUgdG90YWwgYW1vdW50XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgY29uc3QgZGVmYXVsdENvc3RzID0ge1xuICAgICAgICAgICAgICAgIGdhczogMCxcbiAgICAgICAgICAgICAgICBnYXNQcmljZTogYXJncy5vcHRpb25zLmdhc1ByaWNlIHx8IDAsXG4gICAgICAgICAgICAgICAgYWRkaXRpb25hbEZlZTogMCxcbiAgICAgICAgICAgICAgICB0b3RhbENvc3Q6IDBcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgY29uc3QgZXh0cmFDb3N0c01ldGhvZHMgPSBfdGhpcy5fbWV0aG9kLm5hbWUudG9Mb3dlckNhc2UoKS5pbmNsdWRlcyhcIndpdGhkcmF3XCIpXG4gICAgICAgICAgICBpZiAoZXh0cmFDb3N0c01ldGhvZHMpIHtcblxuICAgICAgICAgICAgICAgIGNvbnN0IGVzdGltYXRlR2FzID0gKG5ldyBNZXRob2Qoe1xuICAgICAgICAgICAgICAgICAgICBuYW1lOiAnZXN0aW1hdGVHYXMnLFxuICAgICAgICAgICAgICAgICAgICBjYWxsOiAnZXRoX2VzdGltYXRlR2FzJyxcbiAgICAgICAgICAgICAgICAgICAgcGFyYW1zOiAxLFxuICAgICAgICAgICAgICAgICAgICBpbnB1dEZvcm1hdHRlcjogW2Zvcm1hdHRlcnMuaW5wdXRDYWxsRm9ybWF0dGVyXSxcbiAgICAgICAgICAgICAgICAgICAgb3V0cHV0Rm9ybWF0dGVyOiB1dGlscy5oZXhUb051bWJlcixcbiAgICAgICAgICAgICAgICAgICAgcmVxdWVzdE1hbmFnZXI6IF90aGlzLl9wYXJlbnQuX3JlcXVlc3RNYW5hZ2VyLFxuICAgICAgICAgICAgICAgICAgICBhY2NvdW50czogZXRoQWNjb3VudHMsIC8vIGlzIGV0aC5hY2NvdW50cyAobmVjZXNzYXJ5IGZvciB3YWxsZXQgc2lnbmluZylcbiAgICAgICAgICAgICAgICAgICAgZGVmYXVsdEFjY291bnQ6IF90aGlzLl9wYXJlbnQuZGVmYXVsdEFjY291bnQsXG4gICAgICAgICAgICAgICAgICAgIGRlZmF1bHRCbG9jazogX3RoaXMuX3BhcmVudC5kZWZhdWx0QmxvY2tcbiAgICAgICAgICAgICAgICB9KSkuY3JlYXRlRnVuY3Rpb24oKTtcblxuICAgICAgICAgICAgICAgIGNvbnN0IGdldEdhc1ByaWNlID0gKG5ldyBNZXRob2Qoe1xuICAgICAgICAgICAgICAgICAgICBuYW1lOiAnZ2V0R2FzUHJpY2UnLFxuICAgICAgICAgICAgICAgICAgICBjYWxsOiAnZXRoX2dhc1ByaWNlJyxcbiAgICAgICAgICAgICAgICAgICAgcGFyYW1zOiAwLFxuICAgICAgICAgICAgICAgICAgICByZXF1ZXN0TWFuYWdlcjogX3RoaXMuX3BhcmVudC5fcmVxdWVzdE1hbmFnZXIsXG4gICAgICAgICAgICAgICAgICAgIG91dHB1dEZvcm1hdHRlcjogdXRpbHMuaGV4VG9OdW1iZXIsXG4gICAgICAgICAgICAgICAgfSkpLmNyZWF0ZUZ1bmN0aW9uKCk7XG5cbiAgICAgICAgICAgICAgICBjb25zdCBzaWcgPSBfdGhpcy5fbWV0aG9kLnNpZ25hdHVyZTtcbiAgICAgICAgICAgICAgICAvLyAgX3RoaXMubWV0aG9kc1tzaWddKClcbiAgICAgICAgICAgICAgICBjb25zdCBnZXRFc3RpbWF0ZUNvc3RzID0gKG5ldyBNZXRob2Qoe1xuICAgICAgICAgICAgICAgICAgICBuYW1lOiAnY2FsbCcsXG4gICAgICAgICAgICAgICAgICAgIGNhbGw6ICdldGhfY2FsbCcsXG4gICAgICAgICAgICAgICAgICAgIHBhcmFtczogMixcbiAgICAgICAgICAgICAgICAgICAgaW5wdXRGb3JtYXR0ZXI6IFtmb3JtYXR0ZXJzLmlucHV0Q2FsbEZvcm1hdHRlciwgZm9ybWF0dGVycy5pbnB1dERlZmF1bHRCbG9ja051bWJlckZvcm1hdHRlcl0sXG4gICAgICAgICAgICAgICAgICAgIC8vIGFkZCBvdXRwdXQgZm9ybWF0dGVyIGZvciBkZWNvZGluZ1xuICAgICAgICAgICAgICAgICAgICBvdXRwdXRGb3JtYXR0ZXIocmVzdWx0KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gX3RoaXMuX3BhcmVudC5fZGVjb2RlTWV0aG9kUmV0dXJuKFt7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogXCJjb3N0XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogXCJ1aW50MjU2XCJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1dLCByZXN1bHQpO1xuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICByZXF1ZXN0TWFuYWdlcjogX3RoaXMuX3BhcmVudC5fcmVxdWVzdE1hbmFnZXIsXG4gICAgICAgICAgICAgICAgICAgIGFjY291bnRzOiBldGhBY2NvdW50cywgLy8gaXMgZXRoLmFjY291bnRzIChuZWNlc3NhcnkgZm9yIHdhbGxldCBzaWduaW5nKVxuICAgICAgICAgICAgICAgICAgICBkZWZhdWx0QWNjb3VudDogX3RoaXMuX3BhcmVudC5kZWZhdWx0QWNjb3VudCxcbiAgICAgICAgICAgICAgICAgICAgZGVmYXVsdEJsb2NrOiBfdGhpcy5fcGFyZW50LmRlZmF1bHRCbG9jayxcbiAgICAgICAgICAgICAgICAgICAgaGFuZGxlUmV2ZXJ0OiBfdGhpcy5fcGFyZW50LmhhbmRsZVJldmVydCxcbiAgICAgICAgICAgICAgICAgICAgYWJpQ29kZXI6IGFiaVxuICAgICAgICAgICAgICAgIH0pKS5jcmVhdGVGdW5jdGlvbigpO1xuXG4gICAgICAgICAgICAgICAgY29uc3QgYWRkaXRpb25hbEZlZU9wdGlvbnMgPSB7XG4gICAgICAgICAgICAgICAgICAgIC4uLmFyZ3Mub3B0aW9ucyxcbiAgICAgICAgICAgICAgICAgICAgZGF0YTogdGhpcy5fcGFyZW50Lm1ldGhvZHMuZXN0aW1hdGVDb3N0cyhzaWcsIGFyZ3Mub3JpZ2luYWxBcmd1bWVudHNbMF0pLmVuY29kZUFCSSgpXG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgcmV0dXJuIGVzdGltYXRlR2FzKGFyZ3Mub3B0aW9ucylcbiAgICAgICAgICAgICAgICAgICAgLnRoZW4oZnVuY3Rpb24gKGdhcykge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFByb21pc2UuYWxsKFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBnZXRHYXNQcmljZSgpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGdldEVzdGltYXRlQ29zdHMoYWRkaXRpb25hbEZlZU9wdGlvbnMpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFByb21pc2UucmVzb2x2ZShhcmdzLm9yaWdpbmFsQXJndW1lbnRzWzBdKVxuICAgICAgICAgICAgICAgICAgICAgICAgXSkudGhlbigoW2dhc1ByaWNlLCBhZGRpdGlvbmFsRmVlLCBhbW91bnRdKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgQk4gPSB1dGlscy5CTjtcbiAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgX2dhcyA9IG5ldyBCTihnYXMpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IF9nYXNQcmljZSA9IG5ldyBCTihnYXNQcmljZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgX2Ftb3VudCA9IG5ldyBCTihhbW91bnQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IF9hZGRpdGlvbmFsRmVlID0gKG5ldyBCTihhZGRpdGlvbmFsRmVlKSkuc3ViKF9hbW91bnQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IF9hZGRpdGlvbmFsRmVlU3RyID0gX2FkZGl0aW9uYWxGZWUudG9TdHJpbmcoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBfbWluZXJGZWUgPSAobmV3IEJOKF9nYXMpKS5tdWwobmV3IEJOKF9nYXNQcmljZSkpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBPYmplY3QuYXNzaWduKGRlZmF1bHRDb3N0cywge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhZGRpdGlvbmFsRmVlOiBfYWRkaXRpb25hbEZlZVN0cixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZ2FzOiBfZ2FzLnRvU3RyaW5nKCksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGdhc01hbmFnZXJGZWU6IF9hZGRpdGlvbmFsRmVlLnN1YihfbWluZXJGZWUpLnRvU3RyaW5nKCksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGdhc1ByaWNlOiBfZ2FzUHJpY2UudG9TdHJpbmcoKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWF5YmVSZXZlcnRzOiBmYWxzZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWluZXJGZWU6IF9taW5lckZlZS50b1N0cmluZygpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0b3RhbENvc3Q6IF9hZGRpdGlvbmFsRmVlU3RyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9KS5jYXRjaChmdW5jdGlvbiAoZXN0aW1hdGVHYXNFcnJvcikge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gUHJvbWlzZS5hbGwoW1xuICAgICAgICAgICAgICAgICAgICAgICAgZ2V0R2FzUHJpY2UoKSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGdldEVzdGltYXRlQ29zdHMoYWRkaXRpb25hbEZlZU9wdGlvbnMpLFxuICAgICAgICAgICAgICAgICAgICAgICAgUHJvbWlzZS5yZXNvbHZlKGFyZ3Mub3JpZ2luYWxBcmd1bWVudHNbMF0pXG4gICAgICAgICAgICAgICAgICAgIF0pLnRoZW4oKFtnYXNQcmljZSwgYWRkaXRpb25hbEZlZSwgYW1vdW50XSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgQk4gPSB1dGlscy5CTjtcbiAgICBcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IF9hZGRpdGlvbmFsRmVlID0gXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKG5ldyBCTihhZGRpdGlvbmFsRmVlKSkuc3ViKG5ldyBCTihhbW91bnQpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IF9hZGRpdGlvbmFsRmVlU3RyID0gX2FkZGl0aW9uYWxGZWUudG9TdHJpbmcoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oZGVmYXVsdENvc3RzLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYWRkaXRpb25hbEZlZTogX2FkZGl0aW9uYWxGZWVTdHIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YTogZXN0aW1hdGVHYXNFcnJvci5kYXRhLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGdhczogbnVsbCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBnYXNQcmljZTogZ2FzUHJpY2UudG9TdHJpbmcoKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXliZVJldmVydHM6IHRydWUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogZXN0aW1hdGVHYXNFcnJvci5tZXNzYWdlLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1pbmVyRmVlOiBudWxsLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRvdGFsQ29zdDogX2FkZGl0aW9uYWxGZWVTdHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gZGVmYXVsdENvc3RzO1xuXG4gICAgICAgIGNhc2UgJ2VzdGltYXRlJzpcbiAgICAgICAgICAgIGNvbnN0IGVzdGltYXRlR2FzID0gKG5ldyBNZXRob2Qoe1xuICAgICAgICAgICAgICAgIG5hbWU6ICdlc3RpbWF0ZUdhcycsXG4gICAgICAgICAgICAgICAgY2FsbDogJ2V0aF9lc3RpbWF0ZUdhcycsXG4gICAgICAgICAgICAgICAgcGFyYW1zOiAxLFxuICAgICAgICAgICAgICAgIGlucHV0Rm9ybWF0dGVyOiBbZm9ybWF0dGVycy5pbnB1dENhbGxGb3JtYXR0ZXJdLFxuICAgICAgICAgICAgICAgIG91dHB1dEZvcm1hdHRlcjogdXRpbHMuaGV4VG9OdW1iZXIsXG4gICAgICAgICAgICAgICAgcmVxdWVzdE1hbmFnZXI6IF90aGlzLl9wYXJlbnQuX3JlcXVlc3RNYW5hZ2VyLFxuICAgICAgICAgICAgICAgIGFjY291bnRzOiBldGhBY2NvdW50cywgLy8gaXMgZXRoLmFjY291bnRzIChuZWNlc3NhcnkgZm9yIHdhbGxldCBzaWduaW5nKVxuICAgICAgICAgICAgICAgIGRlZmF1bHRBY2NvdW50OiBfdGhpcy5fcGFyZW50LmRlZmF1bHRBY2NvdW50LFxuICAgICAgICAgICAgICAgIGRlZmF1bHRCbG9jazogX3RoaXMuX3BhcmVudC5kZWZhdWx0QmxvY2tcbiAgICAgICAgICAgIH0pKS5jcmVhdGVGdW5jdGlvbigpO1xuXG4gICAgICAgICAgICByZXR1cm4gZXN0aW1hdGVHYXMoYXJncy5vcHRpb25zLCBhcmdzLmNhbGxiYWNrKTtcblxuICAgICAgICBjYXNlICdjYWxsJzpcblxuICAgICAgICAgICAgLy8gVE9ETyBjaGVjayBlcnJvcnM6IG1pc3NpbmcgXCJmcm9tXCIgc2hvdWxkIGdpdmUgZXJyb3Igb24gZGVwbG95IGFuZCBzZW5kLCBjYWxsID9cbiAgICAgICAgICAgIGNvbnN0IGNhbGwgPSAobmV3IE1ldGhvZCh7XG4gICAgICAgICAgICAgICAgbmFtZTogJ2NhbGwnLFxuICAgICAgICAgICAgICAgIGNhbGw6ICdldGhfY2FsbCcsXG4gICAgICAgICAgICAgICAgcGFyYW1zOiAyLFxuICAgICAgICAgICAgICAgIGlucHV0Rm9ybWF0dGVyOiBbZm9ybWF0dGVycy5pbnB1dENhbGxGb3JtYXR0ZXIsIGZvcm1hdHRlcnMuaW5wdXREZWZhdWx0QmxvY2tOdW1iZXJGb3JtYXR0ZXJdLFxuICAgICAgICAgICAgICAgIC8vIGFkZCBvdXRwdXQgZm9ybWF0dGVyIGZvciBkZWNvZGluZ1xuICAgICAgICAgICAgICAgIG91dHB1dEZvcm1hdHRlcihyZXN1bHQpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIF90aGlzLl9wYXJlbnQuX2RlY29kZU1ldGhvZFJldHVybihfdGhpcy5fbWV0aG9kLm91dHB1dHMsIHJlc3VsdCk7XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICByZXF1ZXN0TWFuYWdlcjogX3RoaXMuX3BhcmVudC5fcmVxdWVzdE1hbmFnZXIsXG4gICAgICAgICAgICAgICAgYWNjb3VudHM6IGV0aEFjY291bnRzLCAvLyBpcyBldGguYWNjb3VudHMgKG5lY2Vzc2FyeSBmb3Igd2FsbGV0IHNpZ25pbmcpXG4gICAgICAgICAgICAgICAgZGVmYXVsdEFjY291bnQ6IF90aGlzLl9wYXJlbnQuZGVmYXVsdEFjY291bnQsXG4gICAgICAgICAgICAgICAgZGVmYXVsdEJsb2NrOiBfdGhpcy5fcGFyZW50LmRlZmF1bHRCbG9jayxcbiAgICAgICAgICAgICAgICBoYW5kbGVSZXZlcnQ6IF90aGlzLl9wYXJlbnQuaGFuZGxlUmV2ZXJ0LFxuICAgICAgICAgICAgICAgIGFiaUNvZGVyOiBhYmlcbiAgICAgICAgICAgIH0pKS5jcmVhdGVGdW5jdGlvbigpO1xuXG4gICAgICAgICAgICByZXR1cm4gY2FsbChhcmdzLm9wdGlvbnMsIGFyZ3MuZGVmYXVsdEJsb2NrLCBhcmdzLmNhbGxiYWNrKTtcblxuICAgICAgICBjYXNlICdzZW5kJzpcblxuICAgICAgICAgICAgLy8gcmV0dXJuIGVycm9yLCBpZiBubyBcImZyb21cIiBpcyBzcGVjaWZpZWRcbiAgICAgICAgICAgIGlmICghdXRpbHMuaXNBZGRyZXNzKGFyZ3Mub3B0aW9ucy5mcm9tKSkge1xuICAgICAgICAgICAgICAgIHJldHVybiB1dGlscy5fZmlyZUVycm9yKG5ldyBFcnJvcignTm8gXCJmcm9tXCIgYWRkcmVzcyBzcGVjaWZpZWQgaW4gbmVpdGhlciB0aGUgZ2l2ZW4gb3B0aW9ucywgbm9yIHRoZSBkZWZhdWx0IG9wdGlvbnMuJyksIGRlZmVyLmV2ZW50RW1pdHRlciwgZGVmZXIucmVqZWN0LCBhcmdzLmNhbGxiYWNrKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKF8uaXNCb29sZWFuKHRoaXMuX21ldGhvZC5wYXlhYmxlKSAmJiAhdGhpcy5fbWV0aG9kLnBheWFibGUgJiYgYXJncy5vcHRpb25zLnZhbHVlICYmIGFyZ3Mub3B0aW9ucy52YWx1ZSA+IDApIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gdXRpbHMuX2ZpcmVFcnJvcihuZXcgRXJyb3IoJ0NhbiBub3Qgc2VuZCB2YWx1ZSB0byBub24tcGF5YWJsZSBjb250cmFjdCBtZXRob2Qgb3IgY29uc3RydWN0b3InKSwgZGVmZXIuZXZlbnRFbWl0dGVyLCBkZWZlci5yZWplY3QsIGFyZ3MuY2FsbGJhY2spO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAvLyBtYWtlIHN1cmUgcmVjZWlwdCBsb2dzIGFyZSBkZWNvZGVkXG4gICAgICAgICAgICBjb25zdCBleHRyYUZvcm1hdHRlcnMgPSB7XG4gICAgICAgICAgICAgICAgcmVjZWlwdEZvcm1hdHRlcihyZWNlaXB0KSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChfLmlzQXJyYXkocmVjZWlwdC5sb2dzKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gZGVjb2RlIGxvZ3NcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCBldmVudHMgPSByZWNlaXB0LmxvZ3MubWFwKChsb2cpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gX3RoaXMuX3BhcmVudC5fZGVjb2RlRXZlbnRBQkkuY2FsbCh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6ICdBTExFVkVOVFMnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBqc29uSW50ZXJmYWNlOiBfdGhpcy5fcGFyZW50Lm9wdGlvbnMuanNvbkludGVyZmFjZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIGxvZyk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIG1ha2UgbG9nIG5hbWVzIGtleXNcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlY2VpcHQuZXZlbnRzID0ge307XG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQgY291bnQgPSAwO1xuICAgICAgICAgICAgICAgICAgICAgICAgZXZlbnRzLmZvckVhY2goZnVuY3Rpb24gKGV2KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGV2LmV2ZW50KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGlmID4gMSBvZiB0aGUgc2FtZSBldmVudCwgZG9uJ3Qgb3ZlcndyaXRlIGFueSBleGlzdGluZyBldmVudHNcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlY2VpcHQuZXZlbnRzW2V2LmV2ZW50XSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkocmVjZWlwdC5ldmVudHNbZXYuZXZlbnRdKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlY2VpcHQuZXZlbnRzW2V2LmV2ZW50XS5wdXNoKGV2KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVjZWlwdC5ldmVudHNbZXYuZXZlbnRdID0gW3JlY2VpcHQuZXZlbnRzW2V2LmV2ZW50XSwgZXZdO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVjZWlwdC5ldmVudHNbZXYuZXZlbnRdID0gZXY7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZWNlaXB0LmV2ZW50c1tjb3VudF0gPSBldjtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY291bnQrKztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgZGVsZXRlIHJlY2VpcHQubG9ncztcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gcmVjZWlwdDtcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGNvbnRyYWN0RGVwbG95Rm9ybWF0dGVyKHJlY2VpcHQpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgbmV3Q29udHJhY3QgPSBfdGhpcy5fcGFyZW50LmNsb25lKCk7XG4gICAgICAgICAgICAgICAgICAgIG5ld0NvbnRyYWN0Lm9wdGlvbnMuYWRkcmVzcyA9IHJlY2VpcHQuY29udHJhY3RBZGRyZXNzO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gbmV3Q29udHJhY3Q7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgY29uc3Qgc2VuZFRyYW5zYWN0aW9uID0gKG5ldyBNZXRob2Qoe1xuICAgICAgICAgICAgICAgIG5hbWU6ICdzZW5kVHJhbnNhY3Rpb24nLFxuICAgICAgICAgICAgICAgIGNhbGw6ICdldGhfc2VuZFRyYW5zYWN0aW9uJyxcbiAgICAgICAgICAgICAgICBwYXJhbXM6IDEsXG4gICAgICAgICAgICAgICAgaW5wdXRGb3JtYXR0ZXI6IFtmb3JtYXR0ZXJzLmlucHV0VHJhbnNhY3Rpb25Gb3JtYXR0ZXJdLFxuICAgICAgICAgICAgICAgIHJlcXVlc3RNYW5hZ2VyOiBfdGhpcy5fcGFyZW50Ll9yZXF1ZXN0TWFuYWdlcixcbiAgICAgICAgICAgICAgICBhY2NvdW50czogX3RoaXMuY29uc3RydWN0b3IuX2V0aEFjY291bnRzIHx8IF90aGlzLl9ldGhBY2NvdW50cywgLy8gaXMgZXRoLmFjY291bnRzIChuZWNlc3NhcnkgZm9yIHdhbGxldCBzaWduaW5nKVxuICAgICAgICAgICAgICAgIGRlZmF1bHRBY2NvdW50OiBfdGhpcy5fcGFyZW50LmRlZmF1bHRBY2NvdW50LFxuICAgICAgICAgICAgICAgIGRlZmF1bHRCbG9jazogX3RoaXMuX3BhcmVudC5kZWZhdWx0QmxvY2ssXG4gICAgICAgICAgICAgICAgdHJhbnNhY3Rpb25CbG9ja1RpbWVvdXQ6IF90aGlzLl9wYXJlbnQudHJhbnNhY3Rpb25CbG9ja1RpbWVvdXQsXG4gICAgICAgICAgICAgICAgdHJhbnNhY3Rpb25Db25maXJtYXRpb25CbG9ja3M6IF90aGlzLl9wYXJlbnQudHJhbnNhY3Rpb25Db25maXJtYXRpb25CbG9ja3MsXG4gICAgICAgICAgICAgICAgdHJhbnNhY3Rpb25Qb2xsaW5nVGltZW91dDogX3RoaXMuX3BhcmVudC50cmFuc2FjdGlvblBvbGxpbmdUaW1lb3V0LFxuICAgICAgICAgICAgICAgIGRlZmF1bHRDb21tb246IF90aGlzLl9wYXJlbnQuZGVmYXVsdENvbW1vbixcbiAgICAgICAgICAgICAgICBkZWZhdWx0Q2hhaW46IF90aGlzLl9wYXJlbnQuZGVmYXVsdENoYWluLFxuICAgICAgICAgICAgICAgIGRlZmF1bHRIYXJkZm9yazogX3RoaXMuX3BhcmVudC5kZWZhdWx0SGFyZGZvcmssXG4gICAgICAgICAgICAgICAgaGFuZGxlUmV2ZXJ0OiBfdGhpcy5fcGFyZW50LmhhbmRsZVJldmVydCxcbiAgICAgICAgICAgICAgICBleHRyYUZvcm1hdHRlcnMsXG4gICAgICAgICAgICAgICAgYWJpQ29kZXI6IGFiaVxuICAgICAgICAgICAgfSkpLmNyZWF0ZUZ1bmN0aW9uKCk7XG5cblxuICAgICAgICAgICAgaWYgKCFhcmdzLm9wdGlvbnMuZ2FzIHx8IGFyZ3Mub3B0aW9ucy5nYXMgPD0gMjEwMDApIHtcbiAgICAgICAgICAgICAgICAvLyBhdXRvIGVzdGltYXRlR2FzXG4gICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKGZ1bmN0aW9uIChyZXNvbHZlKSB7XG4gICAgICAgICAgICAgICAgICAgIC8vIGF1dG8gZXN0aW1hdGVHYXNcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZXN0aW1hdGVHYXMgPSAobmV3IE1ldGhvZCh7XG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiAnZXN0aW1hdGVHYXMnLFxuICAgICAgICAgICAgICAgICAgICAgICAgY2FsbDogJ2V0aF9lc3RpbWF0ZUdhcycsXG4gICAgICAgICAgICAgICAgICAgICAgICBwYXJhbXM6IDEsXG4gICAgICAgICAgICAgICAgICAgICAgICBpbnB1dEZvcm1hdHRlcjogW2Zvcm1hdHRlcnMuaW5wdXRDYWxsRm9ybWF0dGVyXSxcbiAgICAgICAgICAgICAgICAgICAgICAgIG91dHB1dEZvcm1hdHRlcjogdXRpbHMuaGV4VG9OdW1iZXIsXG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1ZXN0TWFuYWdlcjogX3RoaXMuX3BhcmVudC5fcmVxdWVzdE1hbmFnZXIsXG4gICAgICAgICAgICAgICAgICAgICAgICBhY2NvdW50czogZXRoQWNjb3VudHMsIC8vIGlzIGV0aC5hY2NvdW50cyAobmVjZXNzYXJ5IGZvciB3YWxsZXQgc2lnbmluZylcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlZmF1bHRBY2NvdW50OiBfdGhpcy5fcGFyZW50LmRlZmF1bHRBY2NvdW50LFxuICAgICAgICAgICAgICAgICAgICAgICAgZGVmYXVsdEJsb2NrOiBfdGhpcy5fcGFyZW50LmRlZmF1bHRCbG9ja1xuICAgICAgICAgICAgICAgICAgICB9KSkuY3JlYXRlRnVuY3Rpb24oKTtcblxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gcmVzb2x2ZShcbiAgICAgICAgICAgICAgICAgICAgICAgIGVzdGltYXRlR2FzKGFyZ3Mub3B0aW9ucywgYXJncy5jYWxsYmFjaylcbiAgICAgICAgICAgICAgICAgICAgKTtcblxuICAgICAgICAgICAgICAgIH0pLnRoZW4oZnVuY3Rpb24gKGdhcykge1xuICAgICAgICAgICAgICAgICAgICBjb25zdCBCTiA9IHV0aWxzLkJOO1xuICAgICAgICAgICAgICAgICAgICAvLyBjaGFuZ2UgYXJncyBcbiAgICAgICAgICAgICAgICAgICAgYXJncy5vcHRpb25zID0gT2JqZWN0LmFzc2lnbihhcmdzLm9wdGlvbnMsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIFRPRE86IHdlIGdpdmUgbW9yZSAxMGsgb2YgZ2FzIGJlY2F1c2UgZXN0aW1hdGVHYXMgaXMgbm90IHByZWNpc2UsIHRoaXMgbmVlZCByZXZpc2lvbiBsYXRlclxuICAgICAgICAgICAgICAgICAgICAgICAgZ2FzOiAobmV3IEJOKGdhcy50b1N0cmluZygpKS5hZGQobmV3IEJOKFwiMTAwMDBcIikpKS50b1N0cmluZygpXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBzZW5kVHJhbnNhY3Rpb24oYXJncy5vcHRpb25zLCBhcmdzLmNhbGxiYWNrKTtcbiAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAvLyBzZXQgdXNlciBnYXMgcGFzc2VkXG4gICAgICAgICAgICAgICAgcmV0dXJuIHNlbmRUcmFuc2FjdGlvbihhcmdzLm9wdGlvbnMsIGFyZ3MuY2FsbGJhY2spO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgIC8vIGRvIG5vdGhpbmdcbiAgICB9XG59O1xuXG5leHBvcnQgZGVmYXVsdCBDb250cmFjdDtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7QUFDQTtBQUNBLElBQU1BLENBQUMsR0FBR0MsT0FBTyxDQUFDLFlBQUQsQ0FBakI7O0FBQ0EsSUFBTUMsSUFBSSxHQUFHRCxPQUFPLENBQUMsV0FBRCxDQUFwQjs7QUFDQSxJQUFNRSxNQUFNLEdBQUdGLE9BQU8sQ0FBQyxrQkFBRCxDQUF0Qjs7QUFDQSxJQUFNRyxLQUFLLEdBQUdILE9BQU8sQ0FBQyxZQUFELENBQXJCOztBQUNBLElBQU1JLFlBQVksR0FBR0osT0FBTyxDQUFDLHlCQUFELENBQVAsQ0FBbUNLLFlBQXhEOztBQUNBLElBQU1DLFVBQVUsR0FBR04sT0FBTyxDQUFDLG1CQUFELENBQVAsQ0FBNkJNLFVBQWhEOztBQUNBLElBQU1DLE1BQU0sR0FBR1AsT0FBTyxDQUFDLG1CQUFELENBQVAsQ0FBNkJPLE1BQTVDOztBQUNBLElBQU1DLFVBQVUsR0FBR1IsT0FBTyxDQUFDLHNCQUFELENBQTFCOztBQUNBLElBQU1TLEdBQUcsR0FBR1QsT0FBTyxDQUFDLGNBQUQsQ0FBbkI7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBLElBQU1VLFFBQVEsR0FBRyxTQUFTQSxRQUFULENBQWtCQyxhQUFsQixFQUFpQ0MsT0FBakMsRUFBMENDLE9BQTFDLEVBQW1EO0VBQ2hFLElBQU1DLEtBQUssR0FBRyxJQUFkO0VBQUEsSUFDSUMsSUFBSSxHQUFHQyxLQUFLLENBQUNDLFNBQU4sQ0FBZ0JDLEtBQWhCLENBQXNCQyxJQUF0QixDQUEyQkMsU0FBM0IsQ0FEWDs7RUFHQSxJQUFJLEVBQUUsZ0JBQWdCVixRQUFsQixDQUFKLEVBQWlDO0lBQzdCLE1BQU0sSUFBSVcsS0FBSixDQUFVLDJFQUFWLENBQU47RUFDSCxDQU4rRCxDQVFoRTs7O0VBQ0FwQixJQUFJLENBQUNxQixXQUFMLENBQWlCLElBQWpCLEVBQXVCLENBQUMsS0FBS0MsV0FBTCxDQUFpQkMsZUFBbEIsQ0FBdkI7RUFFQSxLQUFLQyxrQkFBTCxHQUEwQixLQUFLQyxlQUFMLENBQXFCRCxrQkFBL0M7O0VBSUEsSUFBSSxDQUFDZCxhQUFELElBQWtCLENBQUVLLEtBQUssQ0FBQ1csT0FBTixDQUFjaEIsYUFBZCxDQUF4QixFQUF1RDtJQUNuRCxNQUFNLElBQUlVLEtBQUosQ0FBVSwyRkFBVixDQUFOO0VBQ0gsQ0FqQitELENBbUJoRTs7O0VBQ0EsS0FBS1IsT0FBTCxHQUFlLEVBQWY7RUFFQSxJQUFNZSxPQUFPLEdBQUdiLElBQUksQ0FBQ0EsSUFBSSxDQUFDYyxNQUFMLEdBQWMsQ0FBZixDQUFwQjs7RUFDQSxJQUFJOUIsQ0FBQyxDQUFDK0IsUUFBRixDQUFXRixPQUFYLEtBQXVCLENBQUM3QixDQUFDLENBQUM0QixPQUFGLENBQVVDLE9BQVYsQ0FBNUIsRUFBZ0Q7SUFDNUNmLE9BQU8sR0FBR2UsT0FBVjtJQUVBLEtBQUtmLE9BQUwsR0FBZWQsQ0FBQyxDQUFDZ0MsTUFBRixDQUFTLEtBQUtsQixPQUFkLEVBQXVCLEtBQUttQix1QkFBTCxDQUE2Qm5CLE9BQTdCLENBQXZCLENBQWY7O0lBQ0EsSUFBSWQsQ0FBQyxDQUFDK0IsUUFBRixDQUFXbEIsT0FBWCxDQUFKLEVBQXlCO01BQ3JCQSxPQUFPLEdBQUcsSUFBVjtJQUNIO0VBQ0osQ0E5QitELENBZ0NoRTs7O0VBQ0FxQixNQUFNLENBQUNDLGNBQVAsQ0FBc0IsS0FBS3JCLE9BQTNCLEVBQW9DLFNBQXBDLEVBQStDO0lBQzNDc0IsR0FBRyxDQUFDQyxLQUFELEVBQVE7TUFDUCxJQUFJQSxLQUFKLEVBQVc7UUFDUHRCLEtBQUssQ0FBQ3VCLFFBQU4sR0FBaUJsQyxLQUFLLENBQUNtQyxpQkFBTixDQUF3QmhDLFVBQVUsQ0FBQ2lDLHFCQUFYLENBQWlDSCxLQUFqQyxDQUF4QixDQUFqQjtNQUNIO0lBQ0osQ0FMMEM7O0lBTTNDSSxHQUFHLEdBQUc7TUFDRixPQUFPMUIsS0FBSyxDQUFDdUIsUUFBYjtJQUNILENBUjBDOztJQVMzQ0ksVUFBVSxFQUFFO0VBVCtCLENBQS9DLEVBakNnRSxDQTZDaEU7O0VBQ0FSLE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQixLQUFLckIsT0FBM0IsRUFBb0MsZUFBcEMsRUFBcUQ7SUFDakRzQixHQUFHLENBQUNDLEtBQUQsRUFBUTtNQUNQdEIsS0FBSyxDQUFDNEIsT0FBTixHQUFnQixFQUFoQjtNQUNBNUIsS0FBSyxDQUFDNkIsTUFBTixHQUFlLEVBQWY7TUFFQTdCLEtBQUssQ0FBQzhCLGNBQU4sR0FBdUJSLEtBQUssQ0FBQ1MsR0FBTixDQUFVLFVBQVVDLE1BQVYsRUFBa0I7UUFDL0MsSUFBSUMsSUFBSjtRQUNBLElBQUlDLFFBQUosQ0FGK0MsQ0FJL0M7O1FBQ0FGLE1BQU0sQ0FBQ0csUUFBUCxHQUFtQkgsTUFBTSxDQUFDSSxlQUFQLEtBQTJCLE1BQTNCLElBQXFDSixNQUFNLENBQUNJLGVBQVAsS0FBMkIsTUFBaEUsSUFBMEVKLE1BQU0sQ0FBQ0csUUFBcEc7UUFDQUgsTUFBTSxDQUFDSyxPQUFQLEdBQWtCTCxNQUFNLENBQUNJLGVBQVAsS0FBMkIsU0FBM0IsSUFBd0NKLE1BQU0sQ0FBQ0ssT0FBakU7O1FBR0EsSUFBSUwsTUFBTSxDQUFDTSxJQUFYLEVBQWlCO1VBQ2JKLFFBQVEsR0FBRzdDLEtBQUssQ0FBQ2tELDRCQUFOLENBQW1DUCxNQUFuQyxDQUFYO1FBQ0gsQ0FYOEMsQ0FjL0M7OztRQUNBLElBQUlBLE1BQU0sQ0FBQ1EsSUFBUCxLQUFnQixVQUFwQixFQUFnQztVQUM1QlIsTUFBTSxDQUFDUyxTQUFQLEdBQW1COUMsR0FBRyxDQUFDK0MsdUJBQUosQ0FBNEJSLFFBQTVCLENBQW5CO1VBQ0FELElBQUksR0FBR2pDLEtBQUssQ0FBQzJDLGVBQU4sQ0FBc0JDLElBQXRCLENBQTJCO1lBQzlCWixNQUQ4QjtZQUU5QmEsTUFBTSxFQUFFN0M7VUFGc0IsQ0FBM0IsQ0FBUCxDQUY0QixDQVE1Qjs7VUFDQSxJQUFJLENBQUNBLEtBQUssQ0FBQzRCLE9BQU4sQ0FBY0ksTUFBTSxDQUFDTSxJQUFyQixDQUFMLEVBQWlDO1lBQzdCdEMsS0FBSyxDQUFDNEIsT0FBTixDQUFjSSxNQUFNLENBQUNNLElBQXJCLElBQTZCTCxJQUE3QjtVQUNILENBRkQsTUFFTztZQUNILElBQU1hLFdBQVcsR0FBRzlDLEtBQUssQ0FBQzJDLGVBQU4sQ0FBc0JDLElBQXRCLENBQTJCO2NBQzNDWixNQUQyQztjQUUzQ2EsTUFBTSxFQUFFN0MsS0FGbUM7Y0FHM0MrQyxVQUFVLEVBQUUvQyxLQUFLLENBQUM0QixPQUFOLENBQWNJLE1BQU0sQ0FBQ00sSUFBckI7WUFIK0IsQ0FBM0IsQ0FBcEI7O1lBS0F0QyxLQUFLLENBQUM0QixPQUFOLENBQWNJLE1BQU0sQ0FBQ00sSUFBckIsSUFBNkJRLFdBQTdCO1VBQ0gsQ0FsQjJCLENBb0I1Qjs7O1VBQ0E5QyxLQUFLLENBQUM0QixPQUFOLENBQWNJLE1BQU0sQ0FBQ1MsU0FBckIsSUFBa0NSLElBQWxDLENBckI0QixDQXVCNUI7O1VBQ0FqQyxLQUFLLENBQUM0QixPQUFOLENBQWNNLFFBQWQsSUFBMEJELElBQTFCLENBeEI0QixDQTJCNUI7UUFDSCxDQTVCRCxNQTRCTyxJQUFJRCxNQUFNLENBQUNRLElBQVAsS0FBZ0IsT0FBcEIsRUFBNkI7VUFDaENSLE1BQU0sQ0FBQ1MsU0FBUCxHQUFtQjlDLEdBQUcsQ0FBQ3FELG9CQUFKLENBQXlCZCxRQUF6QixDQUFuQjs7VUFDQSxJQUFNZSxLQUFLLEdBQUdqRCxLQUFLLENBQUNrRCxHQUFOLENBQVVOLElBQVYsQ0FBZTVDLEtBQWYsRUFBc0JnQyxNQUFNLENBQUNTLFNBQTdCLENBQWQsQ0FGZ0MsQ0FJaEM7OztVQUNBLElBQUksQ0FBQ3pDLEtBQUssQ0FBQzZCLE1BQU4sQ0FBYUcsTUFBTSxDQUFDTSxJQUFwQixDQUFELElBQThCdEMsS0FBSyxDQUFDNkIsTUFBTixDQUFhRyxNQUFNLENBQUNNLElBQXBCLEVBQTBCQSxJQUExQixLQUFtQyxRQUFyRSxFQUNJdEMsS0FBSyxDQUFDNkIsTUFBTixDQUFhRyxNQUFNLENBQUNNLElBQXBCLElBQTRCVyxLQUE1QixDQU40QixDQVFoQzs7VUFDQWpELEtBQUssQ0FBQzZCLE1BQU4sQ0FBYUcsTUFBTSxDQUFDUyxTQUFwQixJQUFpQ1EsS0FBakMsQ0FUZ0MsQ0FXaEM7O1VBQ0FqRCxLQUFLLENBQUM2QixNQUFOLENBQWFLLFFBQWIsSUFBeUJlLEtBQXpCO1FBQ0g7O1FBQUE7UUFHRCxPQUFPakIsTUFBUDtNQUNILENBNURzQixDQUF2QixDQUpPLENBa0VQOztNQUNBaEMsS0FBSyxDQUFDNkIsTUFBTixDQUFhc0IsU0FBYixHQUF5Qm5ELEtBQUssQ0FBQ2tELEdBQU4sQ0FBVU4sSUFBVixDQUFlNUMsS0FBZixFQUFzQixXQUF0QixDQUF6QjtNQUVBLE9BQU9BLEtBQUssQ0FBQzhCLGNBQWI7SUFDSCxDQXZFZ0Q7O0lBd0VqREosR0FBRyxHQUFHO01BQ0YsT0FBTzFCLEtBQUssQ0FBQzhCLGNBQWI7SUFDSCxDQTFFZ0Q7O0lBMkVqREgsVUFBVSxFQUFFO0VBM0VxQyxDQUFyRCxFQTlDZ0UsQ0E0SGhFOztFQUNBLElBQU15QixjQUFjLEdBQUcsS0FBSzNDLFdBQUwsQ0FBaUIyQyxjQUF4QztFQUNBLElBQU1DLFlBQVksR0FBRyxLQUFLNUMsV0FBTCxDQUFpQjRDLFlBQWpCLElBQWlDLFFBQXREO0VBRUFsQyxNQUFNLENBQUNDLGNBQVAsQ0FBc0IsSUFBdEIsRUFBNEIsY0FBNUIsRUFBNEM7SUFDeENNLEdBQUcsR0FBRztNQUNGLElBQUkxQixLQUFLLENBQUNELE9BQU4sQ0FBY3VELFlBQWQsS0FBK0IsS0FBL0IsSUFBd0N0RCxLQUFLLENBQUNELE9BQU4sQ0FBY3VELFlBQWQsS0FBK0IsSUFBM0UsRUFBaUY7UUFDN0UsT0FBT3RELEtBQUssQ0FBQ0QsT0FBTixDQUFjdUQsWUFBckI7TUFDSDs7TUFFRCxPQUFPLEtBQUs3QyxXQUFMLENBQWlCNkMsWUFBeEI7SUFDSCxDQVB1Qzs7SUFReENqQyxHQUFHLENBQUNrQyxHQUFELEVBQU07TUFDTHZELEtBQUssQ0FBQ0QsT0FBTixDQUFjdUQsWUFBZCxHQUE2QkMsR0FBN0I7SUFDSCxDQVZ1Qzs7SUFXeEM1QixVQUFVLEVBQUU7RUFYNEIsQ0FBNUM7RUFhQVIsTUFBTSxDQUFDQyxjQUFQLENBQXNCLElBQXRCLEVBQTRCLGVBQTVCLEVBQTZDO0lBQ3pDTSxHQUFHLEdBQUc7TUFDRixPQUFPMUIsS0FBSyxDQUFDRCxPQUFOLENBQWN5RCxNQUFkLElBQXdCLEtBQUsvQyxXQUFMLENBQWlCZ0QsYUFBaEQ7SUFDSCxDQUh3Qzs7SUFJekNwQyxHQUFHLENBQUNrQyxHQUFELEVBQU07TUFDTHZELEtBQUssQ0FBQ0QsT0FBTixDQUFjeUQsTUFBZCxHQUF1QkQsR0FBdkI7SUFDSCxDQU53Qzs7SUFPekM1QixVQUFVLEVBQUU7RUFQNkIsQ0FBN0M7RUFTQVIsTUFBTSxDQUFDQyxjQUFQLENBQXNCLElBQXRCLEVBQTRCLGlCQUE1QixFQUErQztJQUMzQ00sR0FBRyxHQUFHO01BQ0YsT0FBTzFCLEtBQUssQ0FBQ0QsT0FBTixDQUFjMkQsUUFBZCxJQUEwQixLQUFLakQsV0FBTCxDQUFpQmtELGVBQWxEO0lBQ0gsQ0FIMEM7O0lBSTNDdEMsR0FBRyxDQUFDa0MsR0FBRCxFQUFNO01BQ0x2RCxLQUFLLENBQUNELE9BQU4sQ0FBYzJELFFBQWQsR0FBeUJILEdBQXpCO0lBQ0gsQ0FOMEM7O0lBTzNDNUIsVUFBVSxFQUFFO0VBUCtCLENBQS9DO0VBU0FSLE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQixJQUF0QixFQUE0QixjQUE1QixFQUE0QztJQUN4Q00sR0FBRyxHQUFHO01BQ0YsT0FBTzFCLEtBQUssQ0FBQ0QsT0FBTixDQUFjNkQsS0FBZCxJQUF1QixLQUFLbkQsV0FBTCxDQUFpQm9ELFlBQS9DO0lBQ0gsQ0FIdUM7O0lBSXhDeEMsR0FBRyxDQUFDa0MsR0FBRCxFQUFNO01BQ0x2RCxLQUFLLENBQUNELE9BQU4sQ0FBYzZELEtBQWQsR0FBc0JMLEdBQXRCO0lBQ0gsQ0FOdUM7O0lBT3hDNUIsVUFBVSxFQUFFO0VBUDRCLENBQTVDO0VBU0FSLE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQixJQUF0QixFQUE0QiwyQkFBNUIsRUFBeUQ7SUFDckRNLEdBQUcsR0FBRztNQUNGLElBQUkxQixLQUFLLENBQUNELE9BQU4sQ0FBYytELHlCQUFkLEtBQTRDLENBQWhELEVBQW1EO1FBQy9DLE9BQU85RCxLQUFLLENBQUNELE9BQU4sQ0FBYytELHlCQUFyQjtNQUNIOztNQUVELE9BQU85RCxLQUFLLENBQUNELE9BQU4sQ0FBYytELHlCQUFkLElBQTJDLEtBQUtyRCxXQUFMLENBQWlCcUQseUJBQW5FO0lBQ0gsQ0FQb0Q7O0lBUXJEekMsR0FBRyxDQUFDa0MsR0FBRCxFQUFNO01BQ0x2RCxLQUFLLENBQUNELE9BQU4sQ0FBYytELHlCQUFkLEdBQTBDUCxHQUExQztJQUNILENBVm9EOztJQVdyRDVCLFVBQVUsRUFBRTtFQVh5QyxDQUF6RDtFQWFBUixNQUFNLENBQUNDLGNBQVAsQ0FBc0IsSUFBdEIsRUFBNEIsK0JBQTVCLEVBQTZEO0lBQ3pETSxHQUFHLEdBQUc7TUFDRixJQUFJMUIsS0FBSyxDQUFDRCxPQUFOLENBQWNnRSw2QkFBZCxLQUFnRCxDQUFwRCxFQUF1RDtRQUNuRCxPQUFPL0QsS0FBSyxDQUFDRCxPQUFOLENBQWNnRSw2QkFBckI7TUFDSDs7TUFFRCxPQUFPL0QsS0FBSyxDQUFDRCxPQUFOLENBQWNnRSw2QkFBZCxJQUErQyxLQUFLdEQsV0FBTCxDQUFpQnNELDZCQUF2RTtJQUNILENBUHdEOztJQVF6RDFDLEdBQUcsQ0FBQ2tDLEdBQUQsRUFBTTtNQUNMdkQsS0FBSyxDQUFDRCxPQUFOLENBQWNnRSw2QkFBZCxHQUE4Q1IsR0FBOUM7SUFDSCxDQVZ3RDs7SUFXekQ1QixVQUFVLEVBQUU7RUFYNkMsQ0FBN0Q7RUFhQVIsTUFBTSxDQUFDQyxjQUFQLENBQXNCLElBQXRCLEVBQTRCLHlCQUE1QixFQUF1RDtJQUNuRE0sR0FBRyxHQUFHO01BQ0YsSUFBSTFCLEtBQUssQ0FBQ0QsT0FBTixDQUFjaUUsdUJBQWQsS0FBMEMsQ0FBOUMsRUFBaUQ7UUFDN0MsT0FBT2hFLEtBQUssQ0FBQ0QsT0FBTixDQUFjaUUsdUJBQXJCO01BQ0g7O01BRUQsT0FBT2hFLEtBQUssQ0FBQ0QsT0FBTixDQUFjaUUsdUJBQWQsSUFBeUMsS0FBS3ZELFdBQUwsQ0FBaUJ1RCx1QkFBakU7SUFDSCxDQVBrRDs7SUFRbkQzQyxHQUFHLENBQUNrQyxHQUFELEVBQU07TUFDTHZELEtBQUssQ0FBQ0QsT0FBTixDQUFjaUUsdUJBQWQsR0FBd0NULEdBQXhDO0lBQ0gsQ0FWa0Q7O0lBV25ENUIsVUFBVSxFQUFFO0VBWHVDLENBQXZEO0VBYUFSLE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQixJQUF0QixFQUE0QixnQkFBNUIsRUFBOEM7SUFDMUNNLEdBQUcsR0FBRztNQUNGLE9BQU8wQixjQUFQO0lBQ0gsQ0FIeUM7O0lBSTFDL0IsR0FBRyxDQUFDa0MsR0FBRCxFQUFNO01BQ0wsSUFBSUEsR0FBSixFQUFTO1FBQ1lsRSxLQUFLLENBQUNtQyxpQkFBTixDQUF3QmhDLFVBQVUsQ0FBQ2lDLHFCQUFYLENBQWlDOEIsR0FBakMsQ0FBeEIsQ0FBakI7TUFDSDs7TUFFRCxPQUFPQSxHQUFQO0lBQ0gsQ0FWeUM7O0lBVzFDNUIsVUFBVSxFQUFFO0VBWDhCLENBQTlDO0VBYUFSLE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQixJQUF0QixFQUE0QixjQUE1QixFQUE0QztJQUN4Q00sR0FBRyxHQUFHO01BQ0YsT0FBTzJCLFlBQVA7SUFDSCxDQUh1Qzs7SUFJeENoQyxHQUFHLENBQUNrQyxHQUFELEVBQU07TUFDVUEsR0FBZjtNQUVBLE9BQU9BLEdBQVA7SUFDSCxDQVJ1Qzs7SUFTeEM1QixVQUFVLEVBQUU7RUFUNEIsQ0FBNUMsRUE1TmdFLENBd09oRTs7RUFDQSxLQUFLQyxPQUFMLEdBQWUsRUFBZjtFQUNBLEtBQUtDLE1BQUwsR0FBYyxFQUFkO0VBRUEsS0FBS04sUUFBTCxHQUFnQixJQUFoQjtFQUNBLEtBQUtPLGNBQUwsR0FBc0IsRUFBdEIsQ0E3T2dFLENBK09oRTs7RUFDQSxLQUFLL0IsT0FBTCxDQUFhRCxPQUFiLEdBQXVCQSxPQUF2QjtFQUNBLEtBQUtDLE9BQUwsQ0FBYUYsYUFBYixHQUE2QkEsYUFBN0I7QUFFSCxDQW5QRDs7QUFxUEFELFFBQVEsQ0FBQ3FFLFdBQVQsR0FBdUIsVUFBVUMsUUFBVixFQUFvQkMsUUFBcEIsRUFBOEI7RUFDakQ7RUFDQWhGLElBQUksQ0FBQ3FCLFdBQUwsQ0FBaUIsSUFBakIsRUFBdUIsQ0FBQzBELFFBQUQsQ0FBdkI7RUFDQSxLQUFLRSxZQUFMLEdBQW9CRCxRQUFwQjtBQUNILENBSkQ7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0F2RSxRQUFRLENBQUNPLFNBQVQsQ0FBbUJrRSxZQUFuQixHQUFrQyxTQUFTQyxXQUFULENBQXFCckUsSUFBckIsRUFBMkI7RUFDekQsSUFBSUEsSUFBSSxJQUFJaEIsQ0FBQyxDQUFDc0YsVUFBRixDQUFhdEUsSUFBSSxDQUFDQSxJQUFJLENBQUNjLE1BQUwsR0FBYyxDQUFmLENBQWpCLENBQVosRUFBaUQ7SUFDN0MsT0FBT2QsSUFBSSxDQUFDdUUsR0FBTCxFQUFQLENBRDZDLENBQzFCO0VBQ3RCO0FBQ0osQ0FKRDtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBNUUsUUFBUSxDQUFDTyxTQUFULENBQW1Cc0UsY0FBbkIsR0FBb0MsVUFBVWpDLElBQVYsRUFBZ0JTLEtBQWhCLEVBQXVCO0VBQ3ZELElBQUlBLEtBQUssS0FBS1QsSUFBZCxFQUFvQjtJQUNoQixNQUFNLElBQUlqQyxLQUFKLENBQVUsZ0JBQWdCaUMsSUFBaEIsR0FBdUIsZ0RBQWpDLENBQU47RUFDSDtBQUNKLENBSkQ7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0E1QyxRQUFRLENBQUNPLFNBQVQsQ0FBbUJlLHVCQUFuQixHQUE2QyxTQUFTd0Qsc0JBQVQsQ0FBZ0MzRSxPQUFoQyxFQUF5QztFQUNsRixJQUFNNEUsUUFBUSxHQUFHNUUsT0FBTyxDQUFDNEUsUUFBUixHQUFtQkMsTUFBTSxDQUFDN0UsT0FBTyxDQUFDNEUsUUFBVCxDQUF6QixHQUE4QyxJQUEvRDtFQUNBLElBQU1FLElBQUksR0FBRzlFLE9BQU8sQ0FBQzhFLElBQVIsR0FBZXhGLEtBQUssQ0FBQ21DLGlCQUFOLENBQXdCaEMsVUFBVSxDQUFDaUMscUJBQVgsQ0FBaUMxQixPQUFPLENBQUM4RSxJQUF6QyxDQUF4QixDQUFmLEdBQXlGLElBQXRHO0VBRUE5RSxPQUFPLENBQUMrRSxJQUFSLEdBQWUvRSxPQUFPLENBQUMrRSxJQUFSLElBQWdCLEtBQUsvRSxPQUFMLENBQWErRSxJQUE1QztFQUVBL0UsT0FBTyxDQUFDOEUsSUFBUixHQUFlQSxJQUFJLElBQUksS0FBSzlFLE9BQUwsQ0FBYThFLElBQXBDO0VBQ0E5RSxPQUFPLENBQUM0RSxRQUFSLEdBQW1CQSxRQUFRLElBQUksS0FBSzVFLE9BQUwsQ0FBYTRFLFFBQTVDO0VBQ0E1RSxPQUFPLENBQUNnRixHQUFSLEdBQWNoRixPQUFPLENBQUNnRixHQUFSLElBQWVoRixPQUFPLENBQUNpRixRQUF2QixJQUFtQyxLQUFLakYsT0FBTCxDQUFhZ0YsR0FBOUQsQ0FSa0YsQ0FVbEY7O0VBQ0EsT0FBT2hGLE9BQU8sQ0FBQ2lGLFFBQWY7RUFFQSxPQUFPakYsT0FBUDtBQUNILENBZEQ7QUFpQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0FILFFBQVEsQ0FBQ08sU0FBVCxDQUFtQjhFLGVBQW5CLEdBQXFDLFVBQVVoQyxLQUFWLEVBQWlCbEQsT0FBakIsRUFBMEI7RUFDM0RBLE9BQU8sR0FBR0EsT0FBTyxJQUFJLEVBQXJCO0VBQ0EsSUFBTW1GLE1BQU0sR0FBR25GLE9BQU8sQ0FBQ21GLE1BQVIsSUFBa0IsRUFBakM7RUFDQSxJQUFNQyxNQUFNLEdBQUcsRUFBZjtFQUVBLENBQUMsV0FBRCxFQUFjLFNBQWQsRUFBeUJELE1BQXpCLENBQWdDLFVBQVVFLENBQVYsRUFBYTtJQUN6QyxPQUFPckYsT0FBTyxDQUFDcUYsQ0FBRCxDQUFQLEtBQWVDLFNBQXRCO0VBQ0gsQ0FGRCxFQUVHQyxPQUZILENBRVcsVUFBVUYsQ0FBVixFQUFhO0lBQ3BCRCxNQUFNLENBQUNDLENBQUQsQ0FBTixHQUFZNUYsVUFBVSxDQUFDK0YseUJBQVgsQ0FBcUN4RixPQUFPLENBQUNxRixDQUFELENBQTVDLENBQVo7RUFDSCxDQUpELEVBTDJELENBVzNEOztFQUNBLElBQUluRyxDQUFDLENBQUM0QixPQUFGLENBQVVkLE9BQU8sQ0FBQ3lGLE1BQWxCLENBQUosRUFBK0I7SUFDM0JMLE1BQU0sQ0FBQ0ssTUFBUCxHQUFnQnpGLE9BQU8sQ0FBQ3lGLE1BQXhCLENBRDJCLENBRzNCO0VBQ0gsQ0FKRCxNQUlPO0lBRUhMLE1BQU0sQ0FBQ0ssTUFBUCxHQUFnQixFQUFoQixDQUZHLENBSUg7O0lBQ0EsSUFBSXZDLEtBQUssSUFBSSxDQUFDQSxLQUFLLENBQUN3QyxTQUFoQixJQUE2QnhDLEtBQUssQ0FBQ1gsSUFBTixLQUFlLFdBQWhELEVBQTZEO01BQ3pENkMsTUFBTSxDQUFDSyxNQUFQLENBQWNFLElBQWQsQ0FBbUJ6QyxLQUFLLENBQUNSLFNBQXpCO0lBQ0gsQ0FQRSxDQVNIOzs7SUFDQSxJQUFJUSxLQUFLLENBQUNYLElBQU4sS0FBZSxXQUFuQixFQUFnQztNQUM1QixJQUFNcUQsYUFBYSxHQUFHMUMsS0FBSyxDQUFDMkMsTUFBTixDQUFhVixNQUFiLENBQW9CLFVBQVVXLENBQVYsRUFBYTtRQUNuRCxPQUFPQSxDQUFDLENBQUNDLE9BQUYsS0FBYyxJQUFyQjtNQUNILENBRnFCLEVBRW5CL0QsR0FGbUIsQ0FFZixVQUFVOEQsQ0FBVixFQUFhO1FBQ2hCLElBQU12RSxLQUFLLEdBQUc0RCxNQUFNLENBQUNXLENBQUMsQ0FBQ3ZELElBQUgsQ0FBcEI7O1FBQ0EsSUFBSSxDQUFDaEIsS0FBTCxFQUFZO1VBQ1IsT0FBTyxJQUFQO1FBQ0gsQ0FKZSxDQU1oQjtRQUNBOzs7UUFFQSxJQUFJckMsQ0FBQyxDQUFDNEIsT0FBRixDQUFVUyxLQUFWLENBQUosRUFBc0I7VUFDbEIsT0FBT0EsS0FBSyxDQUFDUyxHQUFOLENBQVUsVUFBVWdFLENBQVYsRUFBYTtZQUMxQixPQUFPcEcsR0FBRyxDQUFDcUcsZUFBSixDQUFvQkgsQ0FBQyxDQUFDckQsSUFBdEIsRUFBNEJ1RCxDQUE1QixDQUFQO1VBQ0gsQ0FGTSxDQUFQO1FBR0g7O1FBQ0QsT0FBT3BHLEdBQUcsQ0FBQ3FHLGVBQUosQ0FBb0JILENBQUMsQ0FBQ3JELElBQXRCLEVBQTRCbEIsS0FBNUIsQ0FBUDtNQUNILENBakJxQixDQUF0QjtNQW1CQTZELE1BQU0sQ0FBQ0ssTUFBUCxHQUFnQkwsTUFBTSxDQUFDSyxNQUFQLENBQWNTLE1BQWQsQ0FBcUJOLGFBQXJCLENBQWhCO0lBQ0g7O0lBRUQsSUFBSSxDQUFDUixNQUFNLENBQUNLLE1BQVAsQ0FBY3pFLE1BQW5CLEVBQ0ksT0FBT29FLE1BQU0sQ0FBQ0ssTUFBZDtFQUNQOztFQUVELElBQUksS0FBS3pGLE9BQUwsQ0FBYUQsT0FBakIsRUFBMEI7SUFDdEJxRixNQUFNLENBQUNyRixPQUFQLEdBQWlCLEtBQUtDLE9BQUwsQ0FBYUQsT0FBYixDQUFxQm9HLFdBQXJCLEVBQWpCO0VBQ0g7O0VBRUQsT0FBT2YsTUFBUDtBQUNILENBMUREO0FBNERBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQXZGLFFBQVEsQ0FBQ08sU0FBVCxDQUFtQmdHLGVBQW5CLEdBQXFDLFVBQVVyQixJQUFWLEVBQWdCO0VBQ2pELElBQUk3QixLQUFLLEdBQUcsSUFBWjtFQUVBNkIsSUFBSSxDQUFDQSxJQUFMLEdBQVlBLElBQUksQ0FBQ0EsSUFBTCxJQUFhLEVBQXpCO0VBQ0FBLElBQUksQ0FBQ1UsTUFBTCxHQUFjVixJQUFJLENBQUNVLE1BQUwsSUFBZSxFQUE3QjtFQUNBLElBQU1MLE1BQU0sR0FBRzNGLFVBQVUsQ0FBQzRHLGtCQUFYLENBQThCdEIsSUFBOUIsQ0FBZixDQUxpRCxDQU9qRDs7RUFDQSxJQUFJN0IsS0FBSyxDQUFDWCxJQUFOLEtBQWUsV0FBbkIsRUFBZ0M7SUFDNUJXLEtBQUssR0FBR0EsS0FBSyxDQUFDcEQsYUFBTixDQUFvQndHLElBQXBCLENBQXlCLFVBQVVDLElBQVYsRUFBZ0I7TUFDN0MsT0FBUUEsSUFBSSxDQUFDN0QsU0FBTCxLQUFtQnFDLElBQUksQ0FBQ1UsTUFBTCxDQUFZLENBQVosQ0FBM0I7SUFDSCxDQUZPLEtBRUY7TUFDRkMsU0FBUyxFQUFFO0lBRFQsQ0FGTjtFQUtILENBZGdELENBZ0JqRDs7O0VBQ0F4QyxLQUFLLENBQUMyQyxNQUFOLEdBQWUzQyxLQUFLLENBQUMyQyxNQUFOLElBQWdCLEVBQS9CLENBakJpRCxDQW1CakQ7RUFDQTs7RUFDQSxJQUFJLENBQUMzQyxLQUFLLENBQUN3QyxTQUFYLEVBQXNCO0lBQ2xCLElBQUljLGFBQWEsR0FBRyxDQUFwQjtJQUNBdEQsS0FBSyxDQUFDMkMsTUFBTixDQUFhTixPQUFiLENBQXFCa0IsS0FBSyxJQUFJQSxLQUFLLENBQUNWLE9BQU4sR0FBZ0JTLGFBQWEsRUFBN0IsR0FBa0MsSUFBaEU7O0lBRUEsSUFBSUEsYUFBYSxHQUFHLENBQWhCLElBQXNCekIsSUFBSSxDQUFDVSxNQUFMLENBQVl6RSxNQUFaLEtBQXVCd0YsYUFBYSxHQUFHLENBQWpFLEVBQXFFO01BQ2pFdEQsS0FBSyxHQUFHO1FBQ0p3QyxTQUFTLEVBQUUsSUFEUDtRQUVKRyxNQUFNLEVBQUU7TUFGSixDQUFSO0lBSUg7RUFDSjs7RUFFRCxJQUFNYSxTQUFTLEdBQUd4RCxLQUFLLENBQUN3QyxTQUFOLEdBQWtCWCxJQUFJLENBQUNVLE1BQXZCLEdBQWdDVixJQUFJLENBQUNVLE1BQUwsQ0FBWXBGLEtBQVosQ0FBa0IsQ0FBbEIsQ0FBbEQ7RUFFQStFLE1BQU0sQ0FBQ3VCLFlBQVAsR0FBc0IvRyxHQUFHLENBQUNnSCxTQUFKLENBQWMxRCxLQUFLLENBQUMyQyxNQUFwQixFQUE0QmQsSUFBSSxDQUFDQSxJQUFqQyxFQUF1QzJCLFNBQXZDLENBQXRCO0VBQ0EsT0FBT3RCLE1BQU0sQ0FBQ3VCLFlBQVAsQ0FBb0JFLFVBQTNCLENBcENpRCxDQXNDakQ7O0VBQ0F6QixNQUFNLENBQUNsQyxLQUFQLEdBQWVBLEtBQUssQ0FBQ1gsSUFBckIsQ0F2Q2lELENBeUNqRDs7RUFDQTZDLE1BQU0sQ0FBQzFDLFNBQVAsR0FBb0JRLEtBQUssQ0FBQ3dDLFNBQU4sSUFBbUIsQ0FBQ1gsSUFBSSxDQUFDVSxNQUFMLENBQVksQ0FBWixDQUFyQixHQUF1QyxJQUF2QyxHQUE4Q1YsSUFBSSxDQUFDVSxNQUFMLENBQVksQ0FBWixDQUFqRSxDQTFDaUQsQ0E0Q2pEOztFQUNBTCxNQUFNLENBQUMwQixHQUFQLEdBQWE7SUFDVC9CLElBQUksRUFBRUssTUFBTSxDQUFDTCxJQURKO0lBRVRVLE1BQU0sRUFBRUwsTUFBTSxDQUFDSztFQUZOLENBQWI7RUFJQSxPQUFPTCxNQUFNLENBQUNMLElBQWQ7RUFDQSxPQUFPSyxNQUFNLENBQUNLLE1BQWQ7RUFHQSxPQUFPTCxNQUFQO0FBQ0gsQ0F0REQ7QUF3REE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0F2RixRQUFRLENBQUNPLFNBQVQsQ0FBbUIyRyxnQkFBbkIsR0FBc0MsU0FBU0EsZ0JBQVQsR0FBNEI7RUFDOUQsSUFBTUMsZUFBZSxHQUFHLEtBQUtDLE9BQUwsQ0FBYXZFLFNBQXJDO0VBQ0EsSUFBTXhDLElBQUksR0FBRyxLQUFLSyxTQUFMLElBQWtCLEVBQS9CO0VBRUEsSUFBSW1DLFNBQVMsR0FBRyxLQUFoQjtFQUNBLElBQU13RSxTQUFTLEdBQUcsS0FBS0MsT0FBTCxDQUFhbkgsT0FBYixDQUFxQkYsYUFBckIsQ0FBbUNxRixNQUFuQyxDQUEwQyxVQUFVaUMsSUFBVixFQUFnQjtJQUN4RSxPQUFTSixlQUFlLEtBQUssYUFBcEIsSUFBcUNJLElBQUksQ0FBQzNFLElBQUwsS0FBY3VFLGVBQXBELElBQ0gsQ0FBQ0ksSUFBSSxDQUFDMUUsU0FBTCxLQUFtQnNFLGVBQW5CLElBQXNDSSxJQUFJLENBQUMxRSxTQUFMLEtBQW1Cc0UsZUFBZSxDQUFDSyxPQUFoQixDQUF3QixJQUF4QixFQUE4QixFQUE5QixDQUF6RCxJQUE4RkQsSUFBSSxDQUFDN0UsSUFBTCxLQUFjeUUsZUFBN0csS0FBaUlJLElBQUksQ0FBQzNFLElBQUwsS0FBYyxVQURwSjtFQUVILENBSGlCLEVBR2ZULEdBSGUsQ0FHWCxVQUFVb0YsSUFBVixFQUFnQjtJQUNuQixJQUFNRSxXQUFXLEdBQUlwSSxDQUFDLENBQUM0QixPQUFGLENBQVVzRyxJQUFJLENBQUN2QixNQUFmLENBQUQsR0FBMkJ1QixJQUFJLENBQUN2QixNQUFMLENBQVk3RSxNQUF2QyxHQUFnRCxDQUFwRTs7SUFFQSxJQUFJc0csV0FBVyxLQUFLcEgsSUFBSSxDQUFDYyxNQUF6QixFQUFpQztNQUM3QixNQUFNLElBQUlSLEtBQUosaUdBQXFHOEcsV0FBckcsaUJBQU47SUFDSDs7SUFFRCxJQUFJRixJQUFJLENBQUMzRSxJQUFMLEtBQWMsVUFBbEIsRUFBOEI7TUFDMUJDLFNBQVMsR0FBRzBFLElBQUksQ0FBQzFFLFNBQWpCO0lBQ0g7O0lBQ0QsT0FBT3hELENBQUMsQ0FBQzRCLE9BQUYsQ0FBVXNHLElBQUksQ0FBQ3ZCLE1BQWYsSUFBeUJ1QixJQUFJLENBQUN2QixNQUE5QixHQUF1QyxFQUE5QztFQUNILENBZGlCLEVBY2Y3RCxHQWRlLENBY1gsVUFBVTZELE1BQVYsRUFBa0I7SUFDckIsT0FBT2pHLEdBQUcsQ0FBQzJILGdCQUFKLENBQXFCMUIsTUFBckIsRUFBNkIzRixJQUE3QixFQUFtQ21ILE9BQW5DLENBQTJDLElBQTNDLEVBQWlELEVBQWpELENBQVA7RUFDSCxDQWhCaUIsRUFnQmYsQ0FoQmUsS0FnQlQsRUFoQlQsQ0FMOEQsQ0F1QjlEOztFQUNBLElBQUlMLGVBQWUsS0FBSyxhQUF4QixFQUF1QztJQUNuQyxJQUFJLENBQUMsS0FBS1EsV0FBVixFQUNJLE1BQU0sSUFBSWhILEtBQUosQ0FBVSx1R0FBVixDQUFOO0lBRUosT0FBTyxLQUFLZ0gsV0FBTCxHQUFtQk4sU0FBMUIsQ0FKbUMsQ0FNbkM7RUFDSDs7RUFFRCxJQUFNTyxXQUFXLEdBQUkvRSxTQUFELEdBQWNBLFNBQVMsR0FBR3dFLFNBQTFCLEdBQXNDQSxTQUExRDs7RUFFQSxJQUFJLENBQUNPLFdBQUwsRUFBa0I7SUFDZCxNQUFNLElBQUlqSCxLQUFKLDREQUErRCxLQUFLeUcsT0FBTCxDQUFhMUUsSUFBNUUsU0FBTjtFQUNILENBRkQsTUFFTztJQUNILE9BQU9rRixXQUFQO0VBQ0g7QUFHSixDQTFDRDtBQTZDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQTVILFFBQVEsQ0FBQ08sU0FBVCxDQUFtQnNILG1CQUFuQixHQUF5QyxVQUFVQyxPQUFWLEVBQW1CaEIsWUFBbkIsRUFBaUM7RUFDdEUsSUFBSSxDQUFDQSxZQUFMLEVBQW1CO0lBQ2YsT0FBTyxJQUFQO0VBQ0g7O0VBRURBLFlBQVksR0FBR0EsWUFBWSxDQUFDM0YsTUFBYixJQUF1QixDQUF2QixHQUEyQjJGLFlBQVksQ0FBQ3RHLEtBQWIsQ0FBbUIsQ0FBbkIsQ0FBM0IsR0FBbURzRyxZQUFsRTtFQUNBLElBQU12QixNQUFNLEdBQUd4RixHQUFHLENBQUNnSSxnQkFBSixDQUFxQkQsT0FBckIsRUFBOEJoQixZQUE5QixDQUFmOztFQUVBLElBQUl2QixNQUFNLENBQUN5QixVQUFQLEtBQXNCLENBQTFCLEVBQTZCO0lBQ3pCLE9BQU96QixNQUFNLENBQUMsQ0FBRCxDQUFiO0VBQ0g7O0VBQ0QsT0FBT0EsTUFBTSxDQUFDeUIsVUFBZDtFQUNBLE9BQU96QixNQUFQO0FBRUgsQ0FkRDtBQWlCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0F2RixRQUFRLENBQUNPLFNBQVQsQ0FBbUJ5SCxNQUFuQixHQUE0QixVQUFVN0gsT0FBVixFQUFtQjhILFFBQW5CLEVBQTZCO0VBRXJEOUgsT0FBTyxHQUFHQSxPQUFPLElBQUksRUFBckI7RUFFQUEsT0FBTyxDQUFDTyxTQUFSLEdBQW9CUCxPQUFPLENBQUNPLFNBQVIsSUFBcUIsRUFBekM7RUFDQVAsT0FBTyxHQUFHLEtBQUttQix1QkFBTCxDQUE2Qm5CLE9BQTdCLENBQVYsQ0FMcUQsQ0FRckQ7O0VBQ0EsSUFBSSxDQUFDQSxPQUFPLENBQUMrRSxJQUFiLEVBQW1CO0lBQ2YsT0FBT3pGLEtBQUssQ0FBQ3lJLFVBQU4sQ0FBaUIsSUFBSXZILEtBQUosQ0FBVSw0RUFBVixDQUFqQixFQUEwRyxJQUExRyxFQUFnSCxJQUFoSCxFQUFzSHNILFFBQXRILENBQVA7RUFDSDs7RUFFRCxJQUFNcEgsV0FBVyxHQUFHeEIsQ0FBQyxDQUFDb0gsSUFBRixDQUFPLEtBQUt0RyxPQUFMLENBQWFGLGFBQXBCLEVBQW1DLFVBQVVtQyxNQUFWLEVBQWtCO0lBQ3JFLE9BQVFBLE1BQU0sQ0FBQ1EsSUFBUCxLQUFnQixhQUF4QjtFQUNILENBRm1CLEtBRWQsRUFGTjtFQUdBL0IsV0FBVyxDQUFDZ0MsU0FBWixHQUF3QixhQUF4QjtFQUVBLE9BQU8sS0FBS0UsZUFBTCxDQUFxQm9GLEtBQXJCLENBQTJCO0lBQzlCL0YsTUFBTSxFQUFFdkIsV0FEc0I7SUFFOUJvQyxNQUFNLEVBQUUsSUFGc0I7SUFHOUJtRixVQUFVLEVBQUVqSSxPQUFPLENBQUMrRSxJQUhVO0lBSTlCVixZQUFZLEVBQUUsS0FBSzNELFdBQUwsQ0FBaUIyRDtFQUpELENBQTNCLEVBS0pyRSxPQUFPLENBQUNPLFNBTEosQ0FBUDtBQU9ILENBekJEO0FBMkJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0FWLFFBQVEsQ0FBQ08sU0FBVCxDQUFtQjhILHFCQUFuQixHQUEyQyxZQUFZO0VBQ25ELElBQU1oSSxJQUFJLEdBQUdDLEtBQUssQ0FBQ0MsU0FBTixDQUFnQkMsS0FBaEIsQ0FBc0JDLElBQXRCLENBQTJCQyxTQUEzQixDQUFiLENBRG1ELENBR25EOztFQUNBLElBQU11SCxRQUFRLEdBQUcsS0FBS3hELFlBQUwsQ0FBa0JwRSxJQUFsQixDQUFqQixDQUptRCxDQU1uRDs7O0VBQ0EsSUFBTUYsT0FBTyxHQUFJZCxDQUFDLENBQUMrQixRQUFGLENBQVdmLElBQUksQ0FBQ0EsSUFBSSxDQUFDYyxNQUFMLEdBQWMsQ0FBZixDQUFmLENBQUQsR0FBc0NkLElBQUksQ0FBQ3VFLEdBQUwsRUFBdEMsR0FBbUQsRUFBbkU7RUFFQSxJQUFNMEQsU0FBUyxHQUFJakosQ0FBQyxDQUFDa0osUUFBRixDQUFXbEksSUFBSSxDQUFDLENBQUQsQ0FBZixDQUFELEdBQXdCQSxJQUFJLENBQUMsQ0FBRCxDQUE1QixHQUFrQyxXQUFwRDtFQUNBLElBQU1nRCxLQUFLLEdBQUlpRixTQUFTLENBQUNoQyxXQUFWLE9BQTRCLFdBQTdCLEdBQTRDO0lBQ3RENUQsSUFBSSxFQUFFLFdBRGdEO0lBRXREekMsYUFBYSxFQUFFLEtBQUtFLE9BQUwsQ0FBYUY7RUFGMEIsQ0FBNUMsR0FHVixLQUFLRSxPQUFMLENBQWFGLGFBQWIsQ0FBMkJ3RyxJQUEzQixDQUFnQyxVQUFVYyxJQUFWLEVBQWdCO0lBQ2hELE9BQVFBLElBQUksQ0FBQzNFLElBQUwsS0FBYyxPQUFkLEtBQTBCMkUsSUFBSSxDQUFDN0UsSUFBTCxLQUFjNEYsU0FBZCxJQUEyQmYsSUFBSSxDQUFDMUUsU0FBTCxLQUFtQixPQUFPeUYsU0FBUyxDQUFDZCxPQUFWLENBQWtCLElBQWxCLEVBQXdCLEVBQXhCLENBQS9FLENBQVI7RUFDSCxDQUZHLENBSEo7O0VBT0EsSUFBSSxDQUFDbkUsS0FBTCxFQUFZO0lBQ1IsTUFBTSxJQUFJMUMsS0FBSixtQkFBc0IySCxTQUF0Qix3Q0FBTjtFQUNIOztFQUVELElBQUksQ0FBQzdJLEtBQUssQ0FBQytJLFNBQU4sQ0FBZ0IsS0FBS3JJLE9BQUwsQ0FBYUQsT0FBN0IsQ0FBTCxFQUE0QztJQUN4QyxNQUFNLElBQUlTLEtBQUosQ0FBVSxrRkFBVixDQUFOO0VBQ0g7O0VBRUQsT0FBTztJQUNIOEgsTUFBTSxFQUFFLEtBQUtwRCxlQUFMLENBQXFCaEMsS0FBckIsRUFBNEJsRCxPQUE1QixDQURMO0lBRUhrRCxLQUZHO0lBR0g0RTtFQUhHLENBQVA7QUFLSCxDQTlCRDtBQWdDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBakksUUFBUSxDQUFDTyxTQUFULENBQW1CbUksS0FBbkIsR0FBMkIsWUFBWTtFQUNuQyxPQUFPLElBQUksS0FBSzdILFdBQVQsQ0FBcUIsS0FBS1YsT0FBTCxDQUFhRixhQUFsQyxFQUFpRCxLQUFLRSxPQUFMLENBQWFELE9BQTlELEVBQXVFLEtBQUtDLE9BQTVFLENBQVA7QUFDSCxDQUZEO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQUgsUUFBUSxDQUFDTyxTQUFULENBQW1Cb0ksSUFBbkIsR0FBMEIsVUFBVXRGLEtBQVYsRUFBaUJsRCxPQUFqQixFQUEwQjhILFFBQTFCLEVBQW9DO0VBQzFELElBQU01SCxJQUFJLEdBQUdDLEtBQUssQ0FBQ0MsU0FBTixDQUFnQkMsS0FBaEIsQ0FBc0JDLElBQXRCLENBQTJCQyxTQUEzQixDQUFiLENBRDBELENBRzFEOztFQUNBdUgsUUFBUSxHQUFHLEtBQUt4RCxZQUFMLENBQWtCcEUsSUFBbEIsQ0FBWDs7RUFFQSxJQUFJLENBQUM0SCxRQUFMLEVBQWU7SUFDWCxNQUFNLElBQUl0SCxLQUFKLENBQVUsbURBQVYsQ0FBTjtFQUNILENBUnlELENBVTFEOzs7RUFDQSxJQUFJUixPQUFKLEVBQ0ksT0FBT0EsT0FBTyxDQUFDeUksU0FBZixDQVpzRCxDQWMxRDs7RUFDQSxLQUFLdEYsR0FBTCxDQUFTRCxLQUFULEVBQWdCbEQsT0FBaEIsRUFBeUIsVUFBVTBJLEdBQVYsRUFBZUMsR0FBZixFQUFvQkMsR0FBcEIsRUFBeUI7SUFDOUNBLEdBQUcsQ0FBQ0MsV0FBSjs7SUFDQSxJQUFJM0osQ0FBQyxDQUFDc0YsVUFBRixDQUFhc0QsUUFBYixDQUFKLEVBQTRCO01BQ3hCQSxRQUFRLENBQUNZLEdBQUQsRUFBTUMsR0FBTixFQUFXQyxHQUFYLENBQVI7SUFDSDtFQUNKLENBTEQ7O0VBT0EsT0FBT3RELFNBQVA7QUFDSCxDQXZCRDtBQXlCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBekYsUUFBUSxDQUFDTyxTQUFULENBQW1CK0MsR0FBbkIsR0FBeUIsWUFBWTtFQUNqQyxJQUFNMkYsVUFBVSxHQUFHLEtBQUtaLHFCQUFMLENBQTJCRixLQUEzQixDQUFpQyxJQUFqQyxFQUF1Q3pILFNBQXZDLENBQW5CLENBRGlDLENBSWpDOzs7RUFDQSxLQUFLbUUsY0FBTCxDQUFvQixhQUFwQixFQUFtQ29FLFVBQVUsQ0FBQzVGLEtBQVgsQ0FBaUJYLElBQXBELEVBQTBEdUcsVUFBVSxDQUFDaEIsUUFBckU7O0VBQ0EsS0FBS3BELGNBQUwsQ0FBb0IsZ0JBQXBCLEVBQXNDb0UsVUFBVSxDQUFDNUYsS0FBWCxDQUFpQlgsSUFBdkQsRUFBNkR1RyxVQUFVLENBQUNoQixRQUF4RSxFQU5pQyxDQVFqQztFQUVBOzs7RUFDQSxJQUFNdEksWUFBWSxHQUFHLElBQUlELFlBQUosQ0FBaUI7SUFDbENDLFlBQVksRUFBRTtNQUNWOEksTUFBTSxFQUFFLENBREU7TUFFVlMsY0FBYyxFQUFFLENBQUN0SixVQUFVLENBQUN1SixpQkFBWixDQUZOO01BR1ZDLGVBQWUsRUFBRSxLQUFLN0MsZUFBTCxDQUFxQnZELElBQXJCLENBQTBCaUcsVUFBVSxDQUFDNUYsS0FBckMsQ0FIUDs7TUFJVjtNQUNBZ0csbUJBQW1CLENBQUNDLE1BQUQsRUFBUztRQUN4QixJQUFJQSxNQUFNLENBQUNDLE9BQVgsRUFBb0I7VUFDaEIsS0FBS0MsSUFBTCxDQUFVLFNBQVYsRUFBcUJGLE1BQXJCO1FBQ0gsQ0FGRCxNQUVPO1VBQ0gsS0FBS0UsSUFBTCxDQUFVLE1BQVYsRUFBa0JGLE1BQWxCO1FBQ0g7O1FBRUQsSUFBSWpLLENBQUMsQ0FBQ3NGLFVBQUYsQ0FBYSxLQUFLc0QsUUFBbEIsQ0FBSixFQUFpQztVQUM3QixLQUFLQSxRQUFMLENBQWMsSUFBZCxFQUFvQnFCLE1BQXBCLEVBQTRCLElBQTVCO1FBQ0g7TUFDSjs7SUFmUyxDQURvQjtJQWtCbEMxRyxJQUFJLEVBQUUsS0FsQjRCO0lBbUJsQzZHLGNBQWMsRUFBRSxLQUFLekk7RUFuQmEsQ0FBakIsQ0FBckI7RUFxQkFyQixZQUFZLENBQUMrSixTQUFiLENBQXVCLE1BQXZCLEVBQStCVCxVQUFVLENBQUNSLE1BQTFDLEVBQWtEUSxVQUFVLENBQUNoQixRQUFYLElBQXVCLFlBQVksQ0FBRSxDQUF2RjtFQUVBLE9BQU90SSxZQUFQO0FBQ0gsQ0FuQ0Q7QUFxQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQUssUUFBUSxDQUFDTyxTQUFULENBQW1Cb0osYUFBbkIsR0FBbUMsWUFBWTtFQUMzQyxJQUFNVixVQUFVLEdBQUcsS0FBS1oscUJBQUwsQ0FBMkJGLEtBQTNCLENBQWlDLElBQWpDLEVBQXVDekgsU0FBdkMsQ0FBbkI7O0VBRUEsSUFBTWtKLFdBQVcsR0FBRyxJQUFJcEssTUFBSixDQUFXO0lBQzNCa0QsSUFBSSxFQUFFLGFBRHFCO0lBRTNCakMsSUFBSSxFQUFFLGFBRnFCO0lBRzNCZ0ksTUFBTSxFQUFFLENBSG1CO0lBSTNCUyxjQUFjLEVBQUUsQ0FBQ3RKLFVBQVUsQ0FBQ3VKLGlCQUFaLENBSlc7SUFLM0JDLGVBQWUsRUFBRSxLQUFLN0MsZUFBTCxDQUFxQnZELElBQXJCLENBQTBCaUcsVUFBVSxDQUFDNUYsS0FBckM7RUFMVSxDQUFYLENBQXBCO0VBT0F1RyxXQUFXLENBQUNDLGlCQUFaLENBQThCLEtBQUs3SSxlQUFuQztFQUNBLElBQU1QLElBQUksR0FBR21KLFdBQVcsQ0FBQ0UsU0FBWixFQUFiO0VBRWMsSUFBZDtFQUVBLE9BQU9ySixJQUFJLENBQUN3SSxVQUFVLENBQUNSLE1BQVosRUFBb0JRLFVBQVUsQ0FBQ2hCLFFBQS9CLENBQVg7QUFDSCxDQWhCRDtBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBakksUUFBUSxDQUFDTyxTQUFULENBQW1Cd0MsZUFBbkIsR0FBcUMsU0FBU0EsZUFBVCxHQUEyQjtFQUM1RCxJQUFNMUMsSUFBSSxHQUFHQyxLQUFLLENBQUNDLFNBQU4sQ0FBZ0JDLEtBQWhCLENBQXNCQyxJQUF0QixDQUEyQkMsU0FBM0IsQ0FBYjtFQUNBLElBQU1xSixRQUFRLEdBQUcsRUFBakI7O0VBRUEsSUFBSSxLQUFLM0gsTUFBTCxDQUFZUSxJQUFaLEtBQXFCLFVBQXpCLEVBQXFDO0lBRWpDbUgsUUFBUSxDQUFDdEosSUFBVCxHQUFnQixLQUFLd0MsTUFBTCxDQUFZK0csY0FBWixDQUEyQmhILElBQTNCLENBQWdDK0csUUFBaEMsRUFBMEMsTUFBMUMsQ0FBaEI7SUFDQUEsUUFBUSxDQUFDdEosSUFBVCxDQUFjd0osT0FBZCxHQUF3QixLQUFLaEgsTUFBTCxDQUFZK0csY0FBWixDQUEyQmhILElBQTNCLENBQWdDK0csUUFBaEMsRUFBMEMsTUFBMUMsRUFBa0QsSUFBbEQsQ0FBeEIsQ0FIaUMsQ0FHZ0Q7RUFFcEY7O0VBRURBLFFBQVEsQ0FBQ0csSUFBVCxHQUFnQixLQUFLakgsTUFBTCxDQUFZK0csY0FBWixDQUEyQmhILElBQTNCLENBQWdDK0csUUFBaEMsRUFBMEMsTUFBMUMsQ0FBaEI7RUFDQUEsUUFBUSxDQUFDRyxJQUFULENBQWNELE9BQWQsR0FBd0IsS0FBS2hILE1BQUwsQ0FBWStHLGNBQVosQ0FBMkJoSCxJQUEzQixDQUFnQytHLFFBQWhDLEVBQTBDLE1BQTFDLEVBQWtELElBQWxELENBQXhCLENBWjRELENBWXFCOztFQUNqRkEsUUFBUSxDQUFDSSxTQUFULEdBQXFCLEtBQUtsSCxNQUFMLENBQVlpRSxnQkFBWixDQUE2QmxFLElBQTdCLENBQWtDK0csUUFBbEMsQ0FBckI7RUFDQUEsUUFBUSxDQUFDSyxXQUFULEdBQXVCLEtBQUtuSCxNQUFMLENBQVkrRyxjQUFaLENBQTJCaEgsSUFBM0IsQ0FBZ0MrRyxRQUFoQyxFQUEwQyxVQUExQyxDQUF2QjtFQUNBQSxRQUFRLENBQUNNLGFBQVQsR0FBeUIsS0FBS3BILE1BQUwsQ0FBWStHLGNBQVosQ0FBMkJoSCxJQUEzQixDQUFnQytHLFFBQWhDLEVBQTBDLGVBQTFDLENBQXpCOztFQUdBLElBQUkxSixJQUFJLElBQUksS0FBSytCLE1BQUwsQ0FBWTRELE1BQXBCLElBQThCM0YsSUFBSSxDQUFDYyxNQUFMLEtBQWdCLEtBQUtpQixNQUFMLENBQVk0RCxNQUFaLENBQW1CN0UsTUFBckUsRUFBNkU7SUFDekUsSUFBSSxLQUFLZ0MsVUFBVCxFQUFxQjtNQUNqQixPQUFPLEtBQUtBLFVBQUwsQ0FBZ0JnRixLQUFoQixDQUFzQixJQUF0QixFQUE0QjlILElBQTVCLENBQVA7SUFDSDs7SUFDRCxNQUFNUixNQUFNLENBQUN5SyxxQkFBUCxDQUE2QmpLLElBQUksQ0FBQ2MsTUFBbEMsRUFBMEMsS0FBS2lCLE1BQUwsQ0FBWTRELE1BQVosQ0FBbUI3RSxNQUE3RCxFQUFxRSxLQUFLaUIsTUFBTCxDQUFZTSxJQUFqRixDQUFOO0VBQ0g7O0VBRURxSCxRQUFRLENBQUNySixTQUFULEdBQXFCTCxJQUFJLElBQUksRUFBN0I7RUFDQTBKLFFBQVEsQ0FBQzNDLE9BQVQsR0FBbUIsS0FBS2hGLE1BQXhCO0VBQ0EySCxRQUFRLENBQUN6QyxPQUFULEdBQW1CLEtBQUtyRSxNQUF4QjtFQUNBOEcsUUFBUSxDQUFDdkYsWUFBVCxHQUF3QixLQUFLdkIsTUFBTCxDQUFZcEMsV0FBWixDQUF3QjJELFlBQXhCLElBQXdDLEtBQUtBLFlBQXJFOztFQUVBLElBQUksS0FBSzRELFVBQVQsRUFBcUI7SUFDakIyQixRQUFRLENBQUNwQyxXQUFULEdBQXVCLEtBQUtTLFVBQTVCO0VBQ0g7O0VBRUQsT0FBTzJCLFFBQVA7QUFDSCxDQW5DRDtBQXNDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EvSixRQUFRLENBQUNPLFNBQVQsQ0FBbUJnSyx3QkFBbkIsR0FBOEMsU0FBU0Esd0JBQVQsQ0FBa0NsSyxJQUFsQyxFQUF3Q21LLEtBQXhDLEVBQStDO0VBQ3pGLElBQU1DLGFBQWEsR0FBRyxFQUF0QjtFQUVBQSxhQUFhLENBQUM3SCxJQUFkLEdBQXFCdkMsSUFBSSxDQUFDcUssS0FBTCxFQUFyQixDQUh5RixDQUt6Rjs7RUFDQUQsYUFBYSxDQUFDeEMsUUFBZCxHQUF5QixLQUFLWCxPQUFMLENBQWE3QyxZQUFiLENBQTBCcEUsSUFBMUIsQ0FBekIsQ0FOeUYsQ0FRekY7O0VBQ0EsSUFBSW9LLGFBQWEsQ0FBQzdILElBQWQsS0FBdUIsTUFBdkIsSUFBaUN2QyxJQUFJLENBQUNBLElBQUksQ0FBQ2MsTUFBTCxHQUFjLENBQWYsQ0FBSixLQUEwQixJQUEzRCxLQUFvRTlCLENBQUMsQ0FBQ2tKLFFBQUYsQ0FBV2xJLElBQUksQ0FBQ0EsSUFBSSxDQUFDYyxNQUFMLEdBQWMsQ0FBZixDQUFmLEtBQXFDd0osUUFBUSxDQUFDdEssSUFBSSxDQUFDQSxJQUFJLENBQUNjLE1BQUwsR0FBYyxDQUFmLENBQUwsQ0FBakgsQ0FBSixFQUNJc0osYUFBYSxDQUFDaEgsWUFBZCxHQUE2QnBELElBQUksQ0FBQ3VFLEdBQUwsRUFBN0IsQ0FWcUYsQ0FZekY7O0VBQ0E2RixhQUFhLENBQUN0SyxPQUFkLEdBQXlCZCxDQUFDLENBQUMrQixRQUFGLENBQVdmLElBQUksQ0FBQ0EsSUFBSSxDQUFDYyxNQUFMLEdBQWMsQ0FBZixDQUFmLENBQUQsR0FBc0NkLElBQUksQ0FBQ3VFLEdBQUwsRUFBdEMsR0FBbUQsRUFBM0UsQ0FieUYsQ0FlekY7O0VBQ0E2RixhQUFhLENBQUNHLGVBQWQsR0FBaUN2SyxJQUFJLENBQUNBLElBQUksQ0FBQ2MsTUFBTCxHQUFjLENBQWYsQ0FBSixLQUEwQixJQUEzQixHQUFtQ2QsSUFBSSxDQUFDdUUsR0FBTCxFQUFuQyxHQUFnRCxLQUFoRjtFQUVBNkYsYUFBYSxDQUFDdEssT0FBZCxHQUF3QixLQUFLbUgsT0FBTCxDQUFhaEcsdUJBQWIsQ0FBcUNtSixhQUFhLENBQUN0SyxPQUFuRCxDQUF4QjtFQUNBc0ssYUFBYSxDQUFDdEssT0FBZCxDQUFzQitFLElBQXRCLEdBQTZCLEtBQUtpRixTQUFMLEVBQTdCO0VBQ0FNLGFBQWEsQ0FBQ0ksaUJBQWQsR0FBa0MsS0FBS25LLFNBQXZDLENBcEJ5RixDQXNCekY7O0VBQ0EsSUFBSSxDQUFDLEtBQUtpSCxXQUFOLElBQXFCLENBQUNsSSxLQUFLLENBQUMrSSxTQUFOLENBQWdCLEtBQUtsQixPQUFMLENBQWFuSCxPQUFiLENBQXFCRCxPQUFyQyxDQUExQixFQUNJLE1BQU0sSUFBSVMsS0FBSixDQUFVLGtGQUFWLENBQU47RUFFSixJQUFJLENBQUMsS0FBS2dILFdBQVYsRUFDSThDLGFBQWEsQ0FBQ3RLLE9BQWQsQ0FBc0IySyxFQUF0QixHQUEyQixLQUFLeEQsT0FBTCxDQUFhbkgsT0FBYixDQUFxQkQsT0FBaEQsQ0EzQnFGLENBNkJ6Rjs7RUFDQSxJQUFJLENBQUN1SyxhQUFhLENBQUN0SyxPQUFkLENBQXNCK0UsSUFBM0IsRUFDSSxPQUFPekYsS0FBSyxDQUFDeUksVUFBTixDQUFpQixJQUFJdkgsS0FBSixDQUFVLGtGQUFWLENBQWpCLEVBQWdINkosS0FBSyxDQUFDTyxZQUF0SCxFQUFvSVAsS0FBSyxDQUFDUSxNQUExSSxFQUFrSlAsYUFBYSxDQUFDeEMsUUFBaEssQ0FBUDtFQUVKLE9BQU93QyxhQUFQO0FBQ0gsQ0FsQ0Q7QUFvQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBekssUUFBUSxDQUFDTyxTQUFULENBQW1CeUosY0FBbkIsR0FBb0MsU0FBU0EsY0FBVCxHQUEwQjtFQUMxRCxJQUFNNUosS0FBSyxHQUFHLElBQWQ7O0VBQ0EsSUFBSW9LLEtBQUo7O0VBQ0EsSUFBTW5LLElBQUksR0FBRyxLQUFLaUgsT0FBTCxDQUFhaUQsd0JBQWIsQ0FBc0M5SixJQUF0QyxDQUEyQyxJQUEzQyxFQUFpREgsS0FBSyxDQUFDQyxTQUFOLENBQWdCQyxLQUFoQixDQUFzQkMsSUFBdEIsQ0FBMkJDLFNBQTNCLENBQWpELEVBQXdGOEosS0FBeEYsQ0FBYjs7RUFDQUEsS0FBSyxHQUFHMUssVUFBVSxDQUFFTyxJQUFJLENBQUN1QyxJQUFMLEtBQWMsTUFBaEIsQ0FBbEI7RUFDQSxJQUFNcUksV0FBVyxHQUFHN0ssS0FBSyxDQUFDUyxXQUFOLENBQWtCMkQsWUFBbEIsSUFBa0NwRSxLQUFLLENBQUNvRSxZQUE1RCxDQUwwRCxDQU8xRDs7RUFDQSxJQUFJbkUsSUFBSSxDQUFDdUssZUFBVCxFQUEwQjtJQUV0QixJQUFNTSxPQUFPLEdBQUc7TUFDWnpDLE1BQU0sRUFBRSxDQUFDN0ksVUFBVSxDQUFDdUwsa0JBQVgsQ0FBOEIxSyxJQUE5QixDQUFtQyxLQUFLNkcsT0FBeEMsRUFBaURqSCxJQUFJLENBQUNGLE9BQXRELENBQUQsQ0FESTtNQUVaOEgsUUFBUSxFQUFFNUgsSUFBSSxDQUFDNEg7SUFGSCxDQUFoQjs7SUFLQSxJQUFJNUgsSUFBSSxDQUFDdUMsSUFBTCxLQUFjLE1BQWxCLEVBQTBCO01BQ3RCc0ksT0FBTyxDQUFDekMsTUFBUixDQUFlM0MsSUFBZixDQUFvQmxHLFVBQVUsQ0FBQ3dMLGdDQUFYLENBQTRDM0ssSUFBNUMsQ0FBaUQsS0FBSzZHLE9BQXRELEVBQStEakgsSUFBSSxDQUFDb0QsWUFBcEUsQ0FBcEI7TUFDQXlILE9BQU8sQ0FBQzlJLE1BQVIsR0FBaUIsVUFBakI7TUFDQThJLE9BQU8sQ0FBQ0csTUFBUixHQUFpQixLQUFLL0QsT0FBTCxDQUFhTyxtQkFBYixDQUFpQzdFLElBQWpDLENBQXNDLElBQXRDLEVBQTRDLEtBQUtvRSxPQUFMLENBQWFVLE9BQXpELENBQWpCO0lBQ0gsQ0FKRCxNQUlPO01BQ0hvRCxPQUFPLENBQUM5SSxNQUFSLEdBQWlCLHFCQUFqQjtJQUNIOztJQUVELE9BQU84SSxPQUFQO0VBRUg7O0VBRUQsUUFBUTdLLElBQUksQ0FBQ3VDLElBQWI7SUFDSSxLQUFLLGVBQUw7TUFDSTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO01BQ1ksSUFBTTBJLFlBQVksR0FBRztRQUNqQm5HLEdBQUcsRUFBRSxDQURZO1FBRWpCSixRQUFRLEVBQUUxRSxJQUFJLENBQUNGLE9BQUwsQ0FBYTRFLFFBQWIsSUFBeUIsQ0FGbEI7UUFHakJ3RyxhQUFhLEVBQUUsQ0FIRTtRQUlqQkMsU0FBUyxFQUFFO01BSk0sQ0FBckI7O01BT0EsSUFBTUMsaUJBQWlCLEdBQUdyTCxLQUFLLENBQUNnSCxPQUFOLENBQWMxRSxJQUFkLENBQW1CNEQsV0FBbkIsR0FBaUNvRixRQUFqQyxDQUEwQyxVQUExQyxDQUExQjs7TUFDQSxJQUFJRCxpQkFBSixFQUF1QjtRQUVuQixJQUFNckIsWUFBVyxHQUFJLElBQUk1SyxNQUFKLENBQVc7VUFDNUJrRCxJQUFJLEVBQUUsYUFEc0I7VUFFNUJqQyxJQUFJLEVBQUUsaUJBRnNCO1VBRzVCZ0ksTUFBTSxFQUFFLENBSG9CO1VBSTVCUyxjQUFjLEVBQUUsQ0FBQ3RKLFVBQVUsQ0FBQ3VMLGtCQUFaLENBSlk7VUFLNUIvQixlQUFlLEVBQUUzSixLQUFLLENBQUNrTSxXQUxLO1VBTTVCbEMsY0FBYyxFQUFFckosS0FBSyxDQUFDa0gsT0FBTixDQUFjdEcsZUFORjtVQU81QnVELFFBQVEsRUFBRTBHLFdBUGtCO1VBT0w7VUFDdkJ6SCxjQUFjLEVBQUVwRCxLQUFLLENBQUNrSCxPQUFOLENBQWM5RCxjQVJGO1VBUzVCQyxZQUFZLEVBQUVyRCxLQUFLLENBQUNrSCxPQUFOLENBQWM3RDtRQVRBLENBQVgsQ0FBRCxDQVVoQm1JLGNBVmdCLEVBQXBCOztRQVlBLElBQU1DLFdBQVcsR0FBSSxJQUFJck0sTUFBSixDQUFXO1VBQzVCa0QsSUFBSSxFQUFFLGFBRHNCO1VBRTVCakMsSUFBSSxFQUFFLGNBRnNCO1VBRzVCZ0ksTUFBTSxFQUFFLENBSG9CO1VBSTVCZ0IsY0FBYyxFQUFFckosS0FBSyxDQUFDa0gsT0FBTixDQUFjdEcsZUFKRjtVQUs1Qm9JLGVBQWUsRUFBRTNKLEtBQUssQ0FBQ2tNO1FBTEssQ0FBWCxDQUFELENBTWhCQyxjQU5nQixFQUFwQjtRQVFBLElBQU1FLEdBQUcsR0FBRzFMLEtBQUssQ0FBQ2dILE9BQU4sQ0FBY3ZFLFNBQTFCLENBdEJtQixDQXVCbkI7O1FBQ0EsSUFBTWtKLGdCQUFnQixHQUFJLElBQUl2TSxNQUFKLENBQVc7VUFDakNrRCxJQUFJLEVBQUUsTUFEMkI7VUFFakNqQyxJQUFJLEVBQUUsVUFGMkI7VUFHakNnSSxNQUFNLEVBQUUsQ0FIeUI7VUFJakNTLGNBQWMsRUFBRSxDQUFDdEosVUFBVSxDQUFDdUwsa0JBQVosRUFBZ0N2TCxVQUFVLENBQUN3TCxnQ0FBM0MsQ0FKaUI7O1VBS2pDO1VBQ0FoQyxlQUFlLENBQUM3RCxNQUFELEVBQVM7WUFDcEIsT0FBT25GLEtBQUssQ0FBQ2tILE9BQU4sQ0FBY08sbUJBQWQsQ0FBa0MsQ0FBQztjQUN0Q25GLElBQUksRUFBRSxNQURnQztjQUV0Q0UsSUFBSSxFQUFFO1lBRmdDLENBQUQsQ0FBbEMsRUFHSDJDLE1BSEcsQ0FBUDtVQUlILENBWGdDOztVQVlqQ2tFLGNBQWMsRUFBRXJKLEtBQUssQ0FBQ2tILE9BQU4sQ0FBY3RHLGVBWkc7VUFhakN1RCxRQUFRLEVBQUUwRyxXQWJ1QjtVQWFWO1VBQ3ZCekgsY0FBYyxFQUFFcEQsS0FBSyxDQUFDa0gsT0FBTixDQUFjOUQsY0FkRztVQWVqQ0MsWUFBWSxFQUFFckQsS0FBSyxDQUFDa0gsT0FBTixDQUFjN0QsWUFmSztVQWdCakNDLFlBQVksRUFBRXRELEtBQUssQ0FBQ2tILE9BQU4sQ0FBYzVELFlBaEJLO1VBaUJqQ3NJLFFBQVEsRUFBRWpNO1FBakJ1QixDQUFYLENBQUQsQ0FrQnJCNkwsY0FsQnFCLEVBQXpCOztRQW9CQSxJQUFNSyxvQkFBb0IsbUNBQ25CNUwsSUFBSSxDQUFDRixPQURjO1VBRXRCK0UsSUFBSSxFQUFFLEtBQUtvQyxPQUFMLENBQWF0RixPQUFiLENBQXFCcUksYUFBckIsQ0FBbUN5QixHQUFuQyxFQUF3Q3pMLElBQUksQ0FBQ3dLLGlCQUFMLENBQXVCLENBQXZCLENBQXhDLEVBQW1FVixTQUFuRTtRQUZnQixFQUExQjs7UUFLQSxPQUFPQyxZQUFXLENBQUMvSixJQUFJLENBQUNGLE9BQU4sQ0FBWCxDQUNGK0wsSUFERSxDQUNHLFVBQVUvRyxHQUFWLEVBQWU7VUFDakIsT0FBT2dILE9BQU8sQ0FBQ0MsR0FBUixDQUFZLENBQ2ZQLFdBQVcsRUFESSxFQUVmRSxnQkFBZ0IsQ0FBQ0Usb0JBQUQsQ0FGRCxFQUdmRSxPQUFPLENBQUNFLE9BQVIsQ0FBZ0JoTSxJQUFJLENBQUN3SyxpQkFBTCxDQUF1QixDQUF2QixDQUFoQixDQUhlLENBQVosRUFJSnFCLElBSkksQ0FJQyxRQUF1QztZQUFBLElBQXRDLENBQUNuSCxRQUFELEVBQVd3RyxhQUFYLEVBQTBCZSxNQUExQixDQUFzQztZQUMzQyxJQUFNQyxFQUFFLEdBQUc5TSxLQUFLLENBQUM4TSxFQUFqQjs7WUFFQSxJQUFNQyxJQUFJLEdBQUcsSUFBSUQsRUFBSixDQUFPcEgsR0FBUCxDQUFiOztZQUNBLElBQU1zSCxTQUFTLEdBQUcsSUFBSUYsRUFBSixDQUFPeEgsUUFBUCxDQUFsQjs7WUFDQSxJQUFNMkgsT0FBTyxHQUFHLElBQUlILEVBQUosQ0FBT0QsTUFBUCxDQUFoQjs7WUFDQSxJQUFNSyxjQUFjLEdBQUksSUFBSUosRUFBSixDQUFPaEIsYUFBUCxDQUFELENBQXdCeEMsR0FBeEIsQ0FBNEIyRCxPQUE1QixDQUF2Qjs7WUFDQSxJQUFNRSxpQkFBaUIsR0FBR0QsY0FBYyxDQUFDRSxRQUFmLEVBQTFCOztZQUNBLElBQU1DLFNBQVMsR0FBSSxJQUFJUCxFQUFKLENBQU9DLElBQVAsQ0FBRCxDQUFlTyxHQUFmLENBQW1CLElBQUlSLEVBQUosQ0FBT0UsU0FBUCxDQUFuQixDQUFsQjs7WUFFQSxPQUFPbEwsTUFBTSxDQUFDeUwsTUFBUCxDQUFjMUIsWUFBZCxFQUE0QjtjQUMvQkMsYUFBYSxFQUFFcUIsaUJBRGdCO2NBRS9CekgsR0FBRyxFQUFFcUgsSUFBSSxDQUFDSyxRQUFMLEVBRjBCO2NBRy9CSSxhQUFhLEVBQUVOLGNBQWMsQ0FBQzVELEdBQWYsQ0FBbUIrRCxTQUFuQixFQUE4QkQsUUFBOUIsRUFIZ0I7Y0FJL0I5SCxRQUFRLEVBQUUwSCxTQUFTLENBQUNJLFFBQVYsRUFKcUI7Y0FLL0JLLFlBQVksRUFBRSxLQUxpQjtjQU0vQkMsUUFBUSxFQUFFTCxTQUFTLENBQUNELFFBQVYsRUFOcUI7Y0FPL0JyQixTQUFTLEVBQUVvQjtZQVBvQixDQUE1QixDQUFQO1VBU0gsQ0F2Qk0sQ0FBUDtRQXdCUCxDQTFCTSxFQTBCSlEsS0ExQkksQ0EwQkUsVUFBVUMsZ0JBQVYsRUFBNEI7VUFDakMsT0FBT2xCLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLENBQ2ZQLFdBQVcsRUFESSxFQUVmRSxnQkFBZ0IsQ0FBQ0Usb0JBQUQsQ0FGRCxFQUdmRSxPQUFPLENBQUNFLE9BQVIsQ0FBZ0JoTSxJQUFJLENBQUN3SyxpQkFBTCxDQUF1QixDQUF2QixDQUFoQixDQUhlLENBQVosRUFJSnFCLElBSkksQ0FJQyxTQUF1QztZQUFBLElBQXRDLENBQUNuSCxRQUFELEVBQVd3RyxhQUFYLEVBQTBCZSxNQUExQixDQUFzQztZQUMzQyxJQUFNQyxFQUFFLEdBQUc5TSxLQUFLLENBQUM4TSxFQUFqQjs7WUFFQSxJQUFNSSxjQUFjLEdBQ2YsSUFBSUosRUFBSixDQUFPaEIsYUFBUCxDQUFELENBQXdCeEMsR0FBeEIsQ0FBNEIsSUFBSXdELEVBQUosQ0FBT0QsTUFBUCxDQUE1QixDQURKOztZQUVBLElBQU1NLGlCQUFpQixHQUFHRCxjQUFjLENBQUNFLFFBQWYsRUFBMUI7O1lBRUEsT0FBT3RMLE1BQU0sQ0FBQ3lMLE1BQVAsQ0FBYzFCLFlBQWQsRUFBNEI7Y0FDL0JDLGFBQWEsRUFBRXFCLGlCQURnQjtjQUUvQjFILElBQUksRUFBRW1JLGdCQUFnQixDQUFDbkksSUFGUTtjQUcvQkMsR0FBRyxFQUFFLElBSDBCO2NBSS9CSixRQUFRLEVBQUVBLFFBQVEsQ0FBQzhILFFBQVQsRUFKcUI7Y0FLL0JLLFlBQVksRUFBRSxJQUxpQjtjQU0vQkksT0FBTyxFQUFFRCxnQkFBZ0IsQ0FBQ0MsT0FOSztjQU8vQkgsUUFBUSxFQUFFLElBUHFCO2NBUS9CM0IsU0FBUyxFQUFFb0I7WUFSb0IsQ0FBNUIsQ0FBUDtVQVVILENBckJNLENBQVA7UUFzQkgsQ0FqRE0sQ0FBUDtNQW1ESDs7TUFDRCxPQUFPdEIsWUFBUDs7SUFFSixLQUFLLFVBQUw7TUFDSSxJQUFNbEIsV0FBVyxHQUFJLElBQUk1SyxNQUFKLENBQVc7UUFDNUJrRCxJQUFJLEVBQUUsYUFEc0I7UUFFNUJqQyxJQUFJLEVBQUUsaUJBRnNCO1FBRzVCZ0ksTUFBTSxFQUFFLENBSG9CO1FBSTVCUyxjQUFjLEVBQUUsQ0FBQ3RKLFVBQVUsQ0FBQ3VMLGtCQUFaLENBSlk7UUFLNUIvQixlQUFlLEVBQUUzSixLQUFLLENBQUNrTSxXQUxLO1FBTTVCbEMsY0FBYyxFQUFFckosS0FBSyxDQUFDa0gsT0FBTixDQUFjdEcsZUFORjtRQU81QnVELFFBQVEsRUFBRTBHLFdBUGtCO1FBT0w7UUFDdkJ6SCxjQUFjLEVBQUVwRCxLQUFLLENBQUNrSCxPQUFOLENBQWM5RCxjQVJGO1FBUzVCQyxZQUFZLEVBQUVyRCxLQUFLLENBQUNrSCxPQUFOLENBQWM3RDtNQVRBLENBQVgsQ0FBRCxDQVVoQm1JLGNBVmdCLEVBQXBCO01BWUEsT0FBT3hCLFdBQVcsQ0FBQy9KLElBQUksQ0FBQ0YsT0FBTixFQUFlRSxJQUFJLENBQUM0SCxRQUFwQixDQUFsQjs7SUFFSixLQUFLLE1BQUw7TUFFSTtNQUNBLElBQU14SCxJQUFJLEdBQUksSUFBSWpCLE1BQUosQ0FBVztRQUNyQmtELElBQUksRUFBRSxNQURlO1FBRXJCakMsSUFBSSxFQUFFLFVBRmU7UUFHckJnSSxNQUFNLEVBQUUsQ0FIYTtRQUlyQlMsY0FBYyxFQUFFLENBQUN0SixVQUFVLENBQUN1TCxrQkFBWixFQUFnQ3ZMLFVBQVUsQ0FBQ3dMLGdDQUEzQyxDQUpLOztRQUtyQjtRQUNBaEMsZUFBZSxDQUFDN0QsTUFBRCxFQUFTO1VBQ3BCLE9BQU9uRixLQUFLLENBQUNrSCxPQUFOLENBQWNPLG1CQUFkLENBQWtDekgsS0FBSyxDQUFDZ0gsT0FBTixDQUFjVSxPQUFoRCxFQUF5RHZDLE1BQXpELENBQVA7UUFDSCxDQVJvQjs7UUFTckJrRSxjQUFjLEVBQUVySixLQUFLLENBQUNrSCxPQUFOLENBQWN0RyxlQVRUO1FBVXJCdUQsUUFBUSxFQUFFMEcsV0FWVztRQVVFO1FBQ3ZCekgsY0FBYyxFQUFFcEQsS0FBSyxDQUFDa0gsT0FBTixDQUFjOUQsY0FYVDtRQVlyQkMsWUFBWSxFQUFFckQsS0FBSyxDQUFDa0gsT0FBTixDQUFjN0QsWUFaUDtRQWFyQkMsWUFBWSxFQUFFdEQsS0FBSyxDQUFDa0gsT0FBTixDQUFjNUQsWUFiUDtRQWNyQnNJLFFBQVEsRUFBRWpNO01BZFcsQ0FBWCxDQUFELENBZVQ2TCxjQWZTLEVBQWI7TUFpQkEsT0FBT25MLElBQUksQ0FBQ0osSUFBSSxDQUFDRixPQUFOLEVBQWVFLElBQUksQ0FBQ29ELFlBQXBCLEVBQWtDcEQsSUFBSSxDQUFDNEgsUUFBdkMsQ0FBWDs7SUFFSixLQUFLLE1BQUw7TUFFSTtNQUNBLElBQUksQ0FBQ3hJLEtBQUssQ0FBQytJLFNBQU4sQ0FBZ0JuSSxJQUFJLENBQUNGLE9BQUwsQ0FBYThFLElBQTdCLENBQUwsRUFBeUM7UUFDckMsT0FBT3hGLEtBQUssQ0FBQ3lJLFVBQU4sQ0FBaUIsSUFBSXZILEtBQUosQ0FBVSxvRkFBVixDQUFqQixFQUFrSDZKLEtBQUssQ0FBQ08sWUFBeEgsRUFBc0lQLEtBQUssQ0FBQ1EsTUFBNUksRUFBb0ozSyxJQUFJLENBQUM0SCxRQUF6SixDQUFQO01BQ0g7O01BRUQsSUFBSTVJLENBQUMsQ0FBQ2tPLFNBQUYsQ0FBWSxLQUFLbkcsT0FBTCxDQUFhM0UsT0FBekIsS0FBcUMsQ0FBQyxLQUFLMkUsT0FBTCxDQUFhM0UsT0FBbkQsSUFBOERwQyxJQUFJLENBQUNGLE9BQUwsQ0FBYXVCLEtBQTNFLElBQW9GckIsSUFBSSxDQUFDRixPQUFMLENBQWF1QixLQUFiLEdBQXFCLENBQTdHLEVBQWdIO1FBQzVHLE9BQU9qQyxLQUFLLENBQUN5SSxVQUFOLENBQWlCLElBQUl2SCxLQUFKLENBQVUsa0VBQVYsQ0FBakIsRUFBZ0c2SixLQUFLLENBQUNPLFlBQXRHLEVBQW9IUCxLQUFLLENBQUNRLE1BQTFILEVBQWtJM0ssSUFBSSxDQUFDNEgsUUFBdkksQ0FBUDtNQUNILENBVEwsQ0FXSTs7O01BQ0EsSUFBTXVGLGVBQWUsR0FBRztRQUNwQkMsZ0JBQWdCLENBQUNDLE9BQUQsRUFBVTtVQUN0QixJQUFJck8sQ0FBQyxDQUFDNEIsT0FBRixDQUFVeU0sT0FBTyxDQUFDQyxJQUFsQixDQUFKLEVBQTZCO1lBQ3pCO1lBQ0EsSUFBSTFMLE1BQU0sR0FBR3lMLE9BQU8sQ0FBQ0MsSUFBUixDQUFheEwsR0FBYixDQUFrQnlMLEdBQUQsSUFBUztjQUNuQyxPQUFPeE4sS0FBSyxDQUFDa0gsT0FBTixDQUFjZixlQUFkLENBQThCOUYsSUFBOUIsQ0FBbUM7Z0JBQ3RDaUMsSUFBSSxFQUFFLFdBRGdDO2dCQUV0Q3pDLGFBQWEsRUFBRUcsS0FBSyxDQUFDa0gsT0FBTixDQUFjbkgsT0FBZCxDQUFzQkY7Y0FGQyxDQUFuQyxFQUdKMk4sR0FISSxDQUFQO1lBSUgsQ0FMWSxDQUFiLENBRnlCLENBUXpCOztZQUNBRixPQUFPLENBQUN6TCxNQUFSLEdBQWlCLEVBQWpCO1lBQ0EsSUFBSTRMLEtBQUssR0FBRyxDQUFaO1lBQ0E1TCxNQUFNLENBQUN5RCxPQUFQLENBQWUsVUFBVW9JLEVBQVYsRUFBYztjQUN6QixJQUFJQSxFQUFFLENBQUN6SyxLQUFQLEVBQWM7Z0JBQ1Y7Z0JBQ0EsSUFBSXFLLE9BQU8sQ0FBQ3pMLE1BQVIsQ0FBZTZMLEVBQUUsQ0FBQ3pLLEtBQWxCLENBQUosRUFBOEI7a0JBQzFCLElBQUkvQyxLQUFLLENBQUNXLE9BQU4sQ0FBY3lNLE9BQU8sQ0FBQ3pMLE1BQVIsQ0FBZTZMLEVBQUUsQ0FBQ3pLLEtBQWxCLENBQWQsQ0FBSixFQUE2QztvQkFDekNxSyxPQUFPLENBQUN6TCxNQUFSLENBQWU2TCxFQUFFLENBQUN6SyxLQUFsQixFQUF5QnlDLElBQXpCLENBQThCZ0ksRUFBOUI7a0JBQ0gsQ0FGRCxNQUVPO29CQUNISixPQUFPLENBQUN6TCxNQUFSLENBQWU2TCxFQUFFLENBQUN6SyxLQUFsQixJQUEyQixDQUFDcUssT0FBTyxDQUFDekwsTUFBUixDQUFlNkwsRUFBRSxDQUFDekssS0FBbEIsQ0FBRCxFQUEyQnlLLEVBQTNCLENBQTNCO2tCQUNIO2dCQUNKLENBTkQsTUFNTztrQkFDSEosT0FBTyxDQUFDekwsTUFBUixDQUFlNkwsRUFBRSxDQUFDekssS0FBbEIsSUFBMkJ5SyxFQUEzQjtnQkFDSDtjQUNKLENBWEQsTUFXTztnQkFDSEosT0FBTyxDQUFDekwsTUFBUixDQUFlNEwsS0FBZixJQUF3QkMsRUFBeEI7Z0JBQ0FELEtBQUs7Y0FDUjtZQUNKLENBaEJEO1lBa0JBLE9BQU9ILE9BQU8sQ0FBQ0MsSUFBZjtVQUNIOztVQUNELE9BQU9ELE9BQVA7UUFDSCxDQWxDbUI7O1FBbUNwQkssdUJBQXVCLENBQUNMLE9BQUQsRUFBVTtVQUM3QixJQUFNTSxXQUFXLEdBQUc1TixLQUFLLENBQUNrSCxPQUFOLENBQWNvQixLQUFkLEVBQXBCOztVQUNBc0YsV0FBVyxDQUFDN04sT0FBWixDQUFvQkQsT0FBcEIsR0FBOEJ3TixPQUFPLENBQUNPLGVBQXRDO1VBQ0EsT0FBT0QsV0FBUDtRQUNIOztNQXZDbUIsQ0FBeEI7TUEwQ0EsSUFBTUUsZUFBZSxHQUFJLElBQUkxTyxNQUFKLENBQVc7UUFDaENrRCxJQUFJLEVBQUUsaUJBRDBCO1FBRWhDakMsSUFBSSxFQUFFLHFCQUYwQjtRQUdoQ2dJLE1BQU0sRUFBRSxDQUh3QjtRQUloQ1MsY0FBYyxFQUFFLENBQUN0SixVQUFVLENBQUN1Tyx5QkFBWixDQUpnQjtRQUtoQzFFLGNBQWMsRUFBRXJKLEtBQUssQ0FBQ2tILE9BQU4sQ0FBY3RHLGVBTEU7UUFNaEN1RCxRQUFRLEVBQUVuRSxLQUFLLENBQUNTLFdBQU4sQ0FBa0IyRCxZQUFsQixJQUFrQ3BFLEtBQUssQ0FBQ29FLFlBTmxCO1FBTWdDO1FBQ2hFaEIsY0FBYyxFQUFFcEQsS0FBSyxDQUFDa0gsT0FBTixDQUFjOUQsY0FQRTtRQVFoQ0MsWUFBWSxFQUFFckQsS0FBSyxDQUFDa0gsT0FBTixDQUFjN0QsWUFSSTtRQVNoQ1csdUJBQXVCLEVBQUVoRSxLQUFLLENBQUNrSCxPQUFOLENBQWNsRCx1QkFUUDtRQVVoQ0QsNkJBQTZCLEVBQUUvRCxLQUFLLENBQUNrSCxPQUFOLENBQWNuRCw2QkFWYjtRQVdoQ0QseUJBQXlCLEVBQUU5RCxLQUFLLENBQUNrSCxPQUFOLENBQWNwRCx5QkFYVDtRQVloQ0wsYUFBYSxFQUFFekQsS0FBSyxDQUFDa0gsT0FBTixDQUFjekQsYUFaRztRQWFoQ0ksWUFBWSxFQUFFN0QsS0FBSyxDQUFDa0gsT0FBTixDQUFjckQsWUFiSTtRQWNoQ0YsZUFBZSxFQUFFM0QsS0FBSyxDQUFDa0gsT0FBTixDQUFjdkQsZUFkQztRQWVoQ0wsWUFBWSxFQUFFdEQsS0FBSyxDQUFDa0gsT0FBTixDQUFjNUQsWUFmSTtRQWdCaEM4SixlQWhCZ0M7UUFpQmhDeEIsUUFBUSxFQUFFak07TUFqQnNCLENBQVgsQ0FBRCxDQWtCcEI2TCxjQWxCb0IsRUFBeEI7O01BcUJBLElBQUksQ0FBQ3ZMLElBQUksQ0FBQ0YsT0FBTCxDQUFhZ0YsR0FBZCxJQUFxQjlFLElBQUksQ0FBQ0YsT0FBTCxDQUFhZ0YsR0FBYixJQUFvQixLQUE3QyxFQUFvRDtRQUNoRDtRQUNBLE9BQU8sSUFBSWdILE9BQUosQ0FBWSxVQUFVRSxPQUFWLEVBQW1CO1VBQ2xDO1VBQ0EsSUFBTWpDLFdBQVcsR0FBSSxJQUFJNUssTUFBSixDQUFXO1lBQzVCa0QsSUFBSSxFQUFFLGFBRHNCO1lBRTVCakMsSUFBSSxFQUFFLGlCQUZzQjtZQUc1QmdJLE1BQU0sRUFBRSxDQUhvQjtZQUk1QlMsY0FBYyxFQUFFLENBQUN0SixVQUFVLENBQUN1TCxrQkFBWixDQUpZO1lBSzVCL0IsZUFBZSxFQUFFM0osS0FBSyxDQUFDa00sV0FMSztZQU01QmxDLGNBQWMsRUFBRXJKLEtBQUssQ0FBQ2tILE9BQU4sQ0FBY3RHLGVBTkY7WUFPNUJ1RCxRQUFRLEVBQUUwRyxXQVBrQjtZQU9MO1lBQ3ZCekgsY0FBYyxFQUFFcEQsS0FBSyxDQUFDa0gsT0FBTixDQUFjOUQsY0FSRjtZQVM1QkMsWUFBWSxFQUFFckQsS0FBSyxDQUFDa0gsT0FBTixDQUFjN0Q7VUFUQSxDQUFYLENBQUQsQ0FVaEJtSSxjQVZnQixFQUFwQjtVQVlBLE9BQU9TLE9BQU8sQ0FDVmpDLFdBQVcsQ0FBQy9KLElBQUksQ0FBQ0YsT0FBTixFQUFlRSxJQUFJLENBQUM0SCxRQUFwQixDQURELENBQWQ7UUFJSCxDQWxCTSxFQWtCSmlFLElBbEJJLENBa0JDLFVBQVUvRyxHQUFWLEVBQWU7VUFDbkIsSUFBTW9ILEVBQUUsR0FBRzlNLEtBQUssQ0FBQzhNLEVBQWpCLENBRG1CLENBRW5COztVQUNBbE0sSUFBSSxDQUFDRixPQUFMLEdBQWVvQixNQUFNLENBQUN5TCxNQUFQLENBQWMzTSxJQUFJLENBQUNGLE9BQW5CLEVBQTRCO1lBQ3ZDO1lBQ0FnRixHQUFHLEVBQUcsSUFBSW9ILEVBQUosQ0FBT3BILEdBQUcsQ0FBQzBILFFBQUosRUFBUCxFQUF1QnVCLEdBQXZCLENBQTJCLElBQUk3QixFQUFKLENBQU8sT0FBUCxDQUEzQixDQUFELENBQThDTSxRQUE5QztVQUZrQyxDQUE1QixDQUFmO1VBS0EsT0FBT3FCLGVBQWUsQ0FBQzdOLElBQUksQ0FBQ0YsT0FBTixFQUFlRSxJQUFJLENBQUM0SCxRQUFwQixDQUF0QjtRQUNILENBM0JNLENBQVA7TUE2QkgsQ0EvQkQsTUErQk87UUFDSDtRQUNBLE9BQU9pRyxlQUFlLENBQUM3TixJQUFJLENBQUNGLE9BQU4sRUFBZUUsSUFBSSxDQUFDNEgsUUFBcEIsQ0FBdEI7TUFDSDs7SUFFRCxRQW5SUixDQW9SWTs7RUFwUlo7QUFzUkgsQ0FqVEQ7O2VBbVRlakksUSJ9