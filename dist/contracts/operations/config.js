"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

/**
 * ERC20 contracts configuration file 
 * @namespace ContractsConfiguration 
 * @property {string}  name          - Token symbol (must be unique).
 * @property {string}  deployType    - Type of deploy: 
 *  - `genesis` load one address
 *  - `manual`  load config by chain id.
 * @property {string}  abi    - ABI name
 * @property {string}  abiCommitHash - Commit hash of abi
 * @property {Array.<Object.<chainId, address>>}  deployInfo - Info with address and chainId
 */
var _default = [{
  "name": "GasManager",
  "abi": "GasManager",
  "deployType": "genesis",
  "abiCommitHash": "34aa21f2f25e99ccfba94b2a55707d28857b48bb",
  "deployInfo": [{
    "address": "0x0000000000000000000000000000000000004070"
  }]
}];
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2NvbnRyYWN0cy9vcGVyYXRpb25zL2NvbmZpZy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIEVSQzIwIGNvbnRyYWN0cyBjb25maWd1cmF0aW9uIGZpbGUgXG4gKiBAbmFtZXNwYWNlIENvbnRyYWN0c0NvbmZpZ3VyYXRpb24gXG4gKiBAcHJvcGVydHkge3N0cmluZ30gIG5hbWUgICAgICAgICAgLSBUb2tlbiBzeW1ib2wgKG11c3QgYmUgdW5pcXVlKS5cbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSAgZGVwbG95VHlwZSAgICAtIFR5cGUgb2YgZGVwbG95OiBcbiAqICAtIGBnZW5lc2lzYCBsb2FkIG9uZSBhZGRyZXNzXG4gKiAgLSBgbWFudWFsYCAgbG9hZCBjb25maWcgYnkgY2hhaW4gaWQuXG4gKiBAcHJvcGVydHkge3N0cmluZ30gIGFiaSAgICAtIEFCSSBuYW1lXG4gKiBAcHJvcGVydHkge3N0cmluZ30gIGFiaUNvbW1pdEhhc2ggLSBDb21taXQgaGFzaCBvZiBhYmlcbiAqIEBwcm9wZXJ0eSB7QXJyYXkuPE9iamVjdC48Y2hhaW5JZCwgYWRkcmVzcz4+fSAgZGVwbG95SW5mbyAtIEluZm8gd2l0aCBhZGRyZXNzIGFuZCBjaGFpbklkXG4gKi9cbmV4cG9ydCBkZWZhdWx0IFt7XG4gICAgXCJuYW1lXCI6IFwiR2FzTWFuYWdlclwiLFxuICAgIFwiYWJpXCI6IFwiR2FzTWFuYWdlclwiLFxuICAgIFwiZGVwbG95VHlwZVwiOiBcImdlbmVzaXNcIixcbiAgICBcImFiaUNvbW1pdEhhc2hcIjogXCIzNGFhMjFmMmYyNWU5OWNjZmJhOTRiMmE1NTcwN2QyODg1N2I0OGJiXCIsXG4gICAgXCJkZXBsb3lJbmZvXCI6IFt7XG4gICAgICAgIFwiYWRkcmVzc1wiOiBcIjB4MDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwNDA3MFwiXG4gICAgfV1cbn1dXG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO2VBQ2UsQ0FBQztFQUNaLFFBQVEsWUFESTtFQUVaLE9BQU8sWUFGSztFQUdaLGNBQWMsU0FIRjtFQUlaLGlCQUFpQiwwQ0FKTDtFQUtaLGNBQWMsQ0FBQztJQUNYLFdBQVc7RUFEQSxDQUFEO0FBTEYsQ0FBRCxDIn0=