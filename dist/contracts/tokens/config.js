"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

/**
 * ERC20 contracts configuration file 
 * @namespace ContractsConfiguration 
 * @property {string}  name          - Token symbol (must be unique).
 * @property {string}  deployType    - Type of deploy: 
 *  - `genesis` load one address
 *  - `manual`  load config by chain id.
 * @property {string}  abi    - ABI name
 * @property {string}  abiCommitHash - Commit hash of abi
 * @property {Array.<Object.<chainId, address>>}  deployInfo - Info with address and chainId
 */
var _default = [{
  "name": "USD",
  "deployType": "genesis",
  "abi": "ERC20",
  "abiCommitHash": "eabda32f811f6004da259e9da3cff5fac04ddbb1",
  "deployInfo": [{
    "address": "0x0000000000000000000000000000000000002070"
  }]
}];
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2NvbnRyYWN0cy90b2tlbnMvY29uZmlnLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogRVJDMjAgY29udHJhY3RzIGNvbmZpZ3VyYXRpb24gZmlsZSBcbiAqIEBuYW1lc3BhY2UgQ29udHJhY3RzQ29uZmlndXJhdGlvbiBcbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSAgbmFtZSAgICAgICAgICAtIFRva2VuIHN5bWJvbCAobXVzdCBiZSB1bmlxdWUpLlxuICogQHByb3BlcnR5IHtzdHJpbmd9ICBkZXBsb3lUeXBlICAgIC0gVHlwZSBvZiBkZXBsb3k6IFxuICogIC0gYGdlbmVzaXNgIGxvYWQgb25lIGFkZHJlc3NcbiAqICAtIGBtYW51YWxgICBsb2FkIGNvbmZpZyBieSBjaGFpbiBpZC5cbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSAgYWJpICAgIC0gQUJJIG5hbWVcbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSAgYWJpQ29tbWl0SGFzaCAtIENvbW1pdCBoYXNoIG9mIGFiaVxuICogQHByb3BlcnR5IHtBcnJheS48T2JqZWN0LjxjaGFpbklkLCBhZGRyZXNzPj59ICBkZXBsb3lJbmZvIC0gSW5mbyB3aXRoIGFkZHJlc3MgYW5kIGNoYWluSWRcbiAqL1xuZXhwb3J0IGRlZmF1bHQgW3tcbiAgICBcIm5hbWVcIjogXCJVU0RcIixcbiAgICBcImRlcGxveVR5cGVcIjogXCJnZW5lc2lzXCIsXG4gICAgXCJhYmlcIjogXCJFUkMyMFwiLFxuICAgIFwiYWJpQ29tbWl0SGFzaFwiOiBcImVhYmRhMzJmODExZjYwMDRkYTI1OWU5ZGEzY2ZmNWZhYzA0ZGRiYjFcIixcbiAgICBcImRlcGxveUluZm9cIjogW3tcbiAgICAgICAgXCJhZGRyZXNzXCI6IFwiMHgwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAyMDcwXCJcbiAgICB9XVxufV1cbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7ZUFDZSxDQUFDO0VBQ1osUUFBUSxLQURJO0VBRVosY0FBYyxTQUZGO0VBR1osT0FBTyxPQUhLO0VBSVosaUJBQWlCLDBDQUpMO0VBS1osY0FBYyxDQUFDO0lBQ1gsV0FBVztFQURBLENBQUQ7QUFMRixDQUFELEMifQ==