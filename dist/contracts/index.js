"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _get = _interopRequireDefault(require("lodash/get"));

var _isEmpty = _interopRequireDefault(require("lodash/isEmpty"));

var _flatten = _interopRequireDefault(require("lodash/flatten"));

var _reduce = _interopRequireDefault(require("lodash/reduce"));

var _upperFirst = _interopRequireDefault(require("lodash/upperFirst"));

var _find = _interopRequireDefault(require("lodash/find"));

var _config = _interopRequireDefault(require("./tokens/config"));

var _config2 = _interopRequireDefault(require("./admin/config"));

var _config3 = _interopRequireDefault(require("./operations/config"));

var _index = _interopRequireDefault(require("./web3-lib/contract/index"));

var _ERC = _interopRequireDefault(require("./tokens/abi/ERC20.json"));

var _GNOSIS = _interopRequireDefault(require("./admin/abi/GNOSIS.json"));

var _GasManager = _interopRequireDefault(require("./operations/abi/GasManager.json"));

var _GasManagerPseudoEvents = _interopRequireDefault(require("./operations/abi/GasManagerPseudoEvents.json"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * Address exception - address invalid or null
 * @param {string} message Error message
 */
function InvalidAddressException(message) {
  this.message = message;
  this.name = 'InvalidAddressException';
}
/**
 * ABI exception - invalid or undefined
 * @param {string} message Error message
 */


function InvalidAbiException(message) {
  this.message = message;
  this.name = 'InvalidAbiException';
}
/**
 * Load generic contracts scheme with custom abi
 * @access private
 * @param {*} contractConfig
 * @param {*} isAddress
 * @param {*} chainId
 * @param {*} abi
 */


var loadGenericContractScheme = (contractConfig, isAddress, chainId, abiList) => {
  return contractConfig.map(config => {
    var targetAddress;

    if (config.deployType === 'genesis') {
      targetAddress = config.deployInfo[0].address;
    } else {
      targetAddress = config.deployInfo.filter(add => add.chainId === chainId)[0].address;
    }

    if (!targetAddress || !isAddress(targetAddress)) throw new InvalidAddressException('Invalid address in contract configuration file');
    var abi;

    if (Array.isArray(config.abi)) {
      abi = config.abi;
    } else {
      abi = (0, _find.default)(abiList, abi => {
        return abi.name === config.abi;
      });
      abi = abi ? abi.data : null;
    }

    if (!abi) {
      throw new InvalidAbiException('Contract ABI not found or invalid');
    }

    return Object.assign(config, {
      address: targetAddress,
      abi: abi
    });
  });
};
/**
 * Group all contracts configuration to create namespace object
 * @access private
 * @param  {...any} args
 */


var loadConfigs = (isAddress, chainId) => {
  // admin needs to load after all contracts
  var configs = [{
    name: 'operations',
    namespaceConfig: _config3.default
  }, {
    name: 'token',
    namespaceConfig: _config.default
  }, {
    name: 'admin',
    namespaceConfig: _config2.default
  }];
  var contractsABI = [{
    name: "GasManager",
    data: [..._GasManager.default, ..._GasManagerPseudoEvents.default]
  }, {
    name: "ERC20",
    data: _ERC.default
  }, {
    name: "GNOSIS",
    data: _GNOSIS.default
  }];
  return configs.map(cfg => {
    return Object.assign(cfg, {
      contracts: loadGenericContractScheme(cfg.namespaceConfig, isAddress, chainId, contractsABI)
    });
  });
};
/**
 * This will wrap multi sig `submitTransaction` for target contract like USD
 * This will prefix all methods that contract admins transforming the first
 * letter in uppercase and add a prefix with `proxy` word.
 * Using `web3.pm.contracts.admin.proxyDeposit(<depositArgs>)`
 * @access private
 * @throws - All web3 errors and exceptions
 * @param {*} contracts all created contracts before
 * @param {*} adminsTo property inside current contract
 * @param {*} currentContract current contract methods
 * @return {Array.<Object>}
 * @example `returns [{ deposit: [Function: deposit] } ]`
 */


var resolveAdminMethods = (contracts, _ref, currentContract) => {
  var {
    adminsTo
  } = _ref;

  if (adminsTo) {
    var listOfMethodsResolved = (0, _flatten.default)(adminsTo.map(adminContract => {
      var adminMethods = (0, _get.default)(contracts, adminContract.target, 'null');
      return Object.keys(adminMethods).filter(key => adminContract.proxyMethods.includes(key)).map(methodKey => {
        return {
          ["proxy".concat((0, _upperFirst.default)(methodKey))]: function proxy() {
            var target = adminMethods.options.address;
            var encodedData = null;

            try {
              encodedData = adminMethods[methodKey](...arguments).encodeABI();
            } catch (error) {
              return error;
            }

            return currentContract.submitTransaction(target, 0, encodedData);
          }
        };
      });
    }));
    return (0, _reduce.default)(listOfMethodsResolved, (methods, current) => {
      return Object.assign(methods, current);
    });
  }

  return null;
};
/**
 * Should create a interface of contracts in configuration file,
 * passing the methods directly to root (we can use web3 style using prefix of `methods`) of object with name given.
 * @private
 * @param {*} web3in web3 instance
 * @param {*} chainId selected chain id
 * @param {*} opts custom contract options
 */


var createContractsInterface = function createContractsInterface(web3in, chainId) {
  var opts = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
  var {
    isAddress
  } = web3in.utils;
  var namespaceConfigs = loadConfigs(isAddress, chainId);
  var contracts = {}; // loop all namespaces configs

  namespaceConfigs.forEach(namespaceConfig => {
    // load all contracts for parent namespace
    return namespaceConfig.contracts.forEach(co => {
      var ContextContract = _index.default.bind(web3in.eth);

      _index.default.setProvider(web3in.currentProvider, web3in.eth.accounts);

      var instance = new ContextContract(co.abi, co.address, opts);
      var {
        methods
      } = instance; // load proxy admin methods for multiSig wallets

      var adminMethods = resolveAdminMethods(contracts, co, methods);

      if (!(0, _isEmpty.default)(adminMethods)) {
        methods = Object.assign(methods, adminMethods);
      }

      var contractsNamespace = contracts[namespaceConfig.name];
      var contractsNamespaceValue = !contractsNamespace ? {} : contractsNamespace;
      contracts[namespaceConfig.name] = Object.assign(contractsNamespaceValue, {
        [co.name]: _objectSpread(_objectSpread({}, instance), methods)
      });
    });
  });
  return contracts;
};

var _default = createContractsInterface;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJJbnZhbGlkQWRkcmVzc0V4Y2VwdGlvbiIsIm1lc3NhZ2UiLCJuYW1lIiwiSW52YWxpZEFiaUV4Y2VwdGlvbiIsImxvYWRHZW5lcmljQ29udHJhY3RTY2hlbWUiLCJjb250cmFjdENvbmZpZyIsImlzQWRkcmVzcyIsImNoYWluSWQiLCJhYmlMaXN0IiwibWFwIiwiY29uZmlnIiwidGFyZ2V0QWRkcmVzcyIsImRlcGxveVR5cGUiLCJkZXBsb3lJbmZvIiwiYWRkcmVzcyIsImZpbHRlciIsImFkZCIsImFiaSIsIkFycmF5IiwiaXNBcnJheSIsImZpbmQiLCJkYXRhIiwiT2JqZWN0IiwiYXNzaWduIiwibG9hZENvbmZpZ3MiLCJjb25maWdzIiwibmFtZXNwYWNlQ29uZmlnIiwib3BlcmF0aW9uc0NvbmZpZyIsInRva2Vuc0NvbmZpZyIsImFkbWluQ29uZmlnIiwiY29udHJhY3RzQUJJIiwiR2FzTWFuYWdlciIsIkdhc01hbmFnZXJQc2V1ZG9FdmVudHMiLCJFUkMyMCIsIkdOT1NJUyIsImNmZyIsImNvbnRyYWN0cyIsInJlc29sdmVBZG1pbk1ldGhvZHMiLCJjdXJyZW50Q29udHJhY3QiLCJhZG1pbnNUbyIsImxpc3RPZk1ldGhvZHNSZXNvbHZlZCIsImZsYXR0ZW4iLCJhZG1pbkNvbnRyYWN0IiwiYWRtaW5NZXRob2RzIiwiZ2V0IiwidGFyZ2V0Iiwia2V5cyIsImtleSIsInByb3h5TWV0aG9kcyIsImluY2x1ZGVzIiwibWV0aG9kS2V5IiwidXBwZXJGaXJzdCIsIm9wdGlvbnMiLCJlbmNvZGVkRGF0YSIsImVuY29kZUFCSSIsImVycm9yIiwic3VibWl0VHJhbnNhY3Rpb24iLCJyZWR1Y2UiLCJtZXRob2RzIiwiY3VycmVudCIsImNyZWF0ZUNvbnRyYWN0c0ludGVyZmFjZSIsIndlYjNpbiIsIm9wdHMiLCJ1dGlscyIsIm5hbWVzcGFjZUNvbmZpZ3MiLCJmb3JFYWNoIiwiY28iLCJDb250ZXh0Q29udHJhY3QiLCJCYXNlQ29udHJhY3QiLCJiaW5kIiwiZXRoIiwic2V0UHJvdmlkZXIiLCJjdXJyZW50UHJvdmlkZXIiLCJhY2NvdW50cyIsImluc3RhbmNlIiwiaXNFbXB0eSIsImNvbnRyYWN0c05hbWVzcGFjZSIsImNvbnRyYWN0c05hbWVzcGFjZVZhbHVlIl0sInNvdXJjZXMiOlsiLi4vLi4vc3JjL2NvbnRyYWN0cy9pbmRleC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIFB1YmxpY01pbnQgY29udHJhY3RzIGludGVyZmFjZVxuICogVGhpcyBtb2R1bGUgcmVzb2x2ZXMgY29uZmlndXJhdGlvbnMgb2YgY29udHJhY3RzIGFuZCBjcmVhdGUgaW50ZXJmYWNlIGluIGB3ZWIzLnBtLmNvbnRyYWN0c2BcbiAqIEBleGFtcGxlIGBjb25zdCB7IFVTRCB9ID0gd2ViMy5wbS5jb250cmFjdHMudG9rZW5gO1xuICogQG1vZHVsZSBDb250cmFjdHNcbiAqL1xuXG5pbXBvcnQgZ2V0IGZyb20gJ2xvZGFzaC9nZXQnO1xuaW1wb3J0IGlzRW1wdHkgZnJvbSAnbG9kYXNoL2lzRW1wdHknO1xuaW1wb3J0IGZsYXR0ZW4gZnJvbSAnbG9kYXNoL2ZsYXR0ZW4nO1xuaW1wb3J0IHJlZHVjZSBmcm9tICdsb2Rhc2gvcmVkdWNlJztcbmltcG9ydCB1cHBlckZpcnN0IGZyb20gJ2xvZGFzaC91cHBlckZpcnN0JztcbmltcG9ydCBmaW5kIGZyb20gJ2xvZGFzaC9maW5kJztcblxuXG5pbXBvcnQgdG9rZW5zQ29uZmlnIGZyb20gJy4vdG9rZW5zL2NvbmZpZyc7XG5pbXBvcnQgYWRtaW5Db25maWcgZnJvbSAnLi9hZG1pbi9jb25maWcnO1xuaW1wb3J0IG9wZXJhdGlvbnNDb25maWcgZnJvbSAnLi9vcGVyYXRpb25zL2NvbmZpZyc7XG5pbXBvcnQgQmFzZUNvbnRyYWN0IGZyb20gJy4vd2ViMy1saWIvY29udHJhY3QvaW5kZXgnO1xuXG4vLyBDT05UUkFDVFMgQUJJXG5pbXBvcnQgRVJDMjAgZnJvbSAnLi90b2tlbnMvYWJpL0VSQzIwLmpzb24nO1xuaW1wb3J0IEdOT1NJUyBmcm9tICcuL2FkbWluL2FiaS9HTk9TSVMuanNvbic7XG5pbXBvcnQgR2FzTWFuYWdlciBmcm9tICcuL29wZXJhdGlvbnMvYWJpL0dhc01hbmFnZXIuanNvbic7XG5pbXBvcnQgR2FzTWFuYWdlclBzZXVkb0V2ZW50cyBmcm9tICcuL29wZXJhdGlvbnMvYWJpL0dhc01hbmFnZXJQc2V1ZG9FdmVudHMuanNvbic7XG5cbi8qKlxuICogQWRkcmVzcyBleGNlcHRpb24gLSBhZGRyZXNzIGludmFsaWQgb3IgbnVsbFxuICogQHBhcmFtIHtzdHJpbmd9IG1lc3NhZ2UgRXJyb3IgbWVzc2FnZVxuICovXG5mdW5jdGlvbiBJbnZhbGlkQWRkcmVzc0V4Y2VwdGlvbihtZXNzYWdlKSB7XG4gIHRoaXMubWVzc2FnZSA9IG1lc3NhZ2U7XG4gIHRoaXMubmFtZSA9ICdJbnZhbGlkQWRkcmVzc0V4Y2VwdGlvbic7XG59XG5cbi8qKlxuICogQUJJIGV4Y2VwdGlvbiAtIGludmFsaWQgb3IgdW5kZWZpbmVkXG4gKiBAcGFyYW0ge3N0cmluZ30gbWVzc2FnZSBFcnJvciBtZXNzYWdlXG4gKi9cbmZ1bmN0aW9uIEludmFsaWRBYmlFeGNlcHRpb24obWVzc2FnZSkge1xuICB0aGlzLm1lc3NhZ2UgPSBtZXNzYWdlO1xuICB0aGlzLm5hbWUgPSAnSW52YWxpZEFiaUV4Y2VwdGlvbic7XG59XG5cbi8qKlxuICogTG9hZCBnZW5lcmljIGNvbnRyYWN0cyBzY2hlbWUgd2l0aCBjdXN0b20gYWJpXG4gKiBAYWNjZXNzIHByaXZhdGVcbiAqIEBwYXJhbSB7Kn0gY29udHJhY3RDb25maWdcbiAqIEBwYXJhbSB7Kn0gaXNBZGRyZXNzXG4gKiBAcGFyYW0geyp9IGNoYWluSWRcbiAqIEBwYXJhbSB7Kn0gYWJpXG4gKi9cbmNvbnN0IGxvYWRHZW5lcmljQ29udHJhY3RTY2hlbWUgPSAoY29udHJhY3RDb25maWcsIGlzQWRkcmVzcywgY2hhaW5JZCwgYWJpTGlzdCkgPT4ge1xuICByZXR1cm4gY29udHJhY3RDb25maWcubWFwKGNvbmZpZyA9PiB7XG4gICAgbGV0IHRhcmdldEFkZHJlc3M7XG4gICAgaWYgKGNvbmZpZy5kZXBsb3lUeXBlID09PSAnZ2VuZXNpcycpIHtcbiAgICAgIHRhcmdldEFkZHJlc3MgPSBjb25maWcuZGVwbG95SW5mb1swXS5hZGRyZXNzO1xuICAgIH0gZWxzZSB7XG4gICAgICB0YXJnZXRBZGRyZXNzID0gY29uZmlnLmRlcGxveUluZm8uZmlsdGVyKGFkZCA9PiBhZGQuY2hhaW5JZCA9PT0gY2hhaW5JZClbMF0uYWRkcmVzcztcbiAgICB9XG5cbiAgICBpZiAoIXRhcmdldEFkZHJlc3MgfHwgIWlzQWRkcmVzcyh0YXJnZXRBZGRyZXNzKSlcbiAgICAgIHRocm93IG5ldyBJbnZhbGlkQWRkcmVzc0V4Y2VwdGlvbignSW52YWxpZCBhZGRyZXNzIGluIGNvbnRyYWN0IGNvbmZpZ3VyYXRpb24gZmlsZScpO1xuXG4gICAgbGV0IGFiaVxuXG4gICAgaWYgKEFycmF5LmlzQXJyYXkoY29uZmlnLmFiaSkpIHtcbiAgICAgIGFiaSA9IGNvbmZpZy5hYmlcbiAgICB9IGVsc2Uge1xuICAgICAgYWJpID0gZmluZChhYmlMaXN0LCAoYWJpKSA9PiB7XG4gICAgICAgIHJldHVybiBhYmkubmFtZSA9PT0gY29uZmlnLmFiaVxuICAgICAgfSk7XG5cbiAgICAgIGFiaSA9IGFiaSA/IGFiaS5kYXRhIDogbnVsbFxuICAgIH1cblxuICAgIGlmICghYWJpKSB7XG4gICAgICB0aHJvdyBuZXcgSW52YWxpZEFiaUV4Y2VwdGlvbignQ29udHJhY3QgQUJJIG5vdCBmb3VuZCBvciBpbnZhbGlkJyk7XG4gICAgfVxuXG4gICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oY29uZmlnLCB7XG4gICAgICBhZGRyZXNzOiB0YXJnZXRBZGRyZXNzLFxuICAgICAgYWJpOiBhYmlcbiAgICB9KTtcbiAgfSk7XG59O1xuLyoqXG4gKiBHcm91cCBhbGwgY29udHJhY3RzIGNvbmZpZ3VyYXRpb24gdG8gY3JlYXRlIG5hbWVzcGFjZSBvYmplY3RcbiAqIEBhY2Nlc3MgcHJpdmF0ZVxuICogQHBhcmFtICB7Li4uYW55fSBhcmdzXG4gKi9cbmNvbnN0IGxvYWRDb25maWdzID0gKGlzQWRkcmVzcywgY2hhaW5JZCkgPT4ge1xuICAvLyBhZG1pbiBuZWVkcyB0byBsb2FkIGFmdGVyIGFsbCBjb250cmFjdHNcbiAgY29uc3QgY29uZmlncyA9IFt7XG4gICAgICBuYW1lOiAnb3BlcmF0aW9ucycsXG4gICAgICBuYW1lc3BhY2VDb25maWc6IG9wZXJhdGlvbnNDb25maWdcbiAgICB9LFxuICAgIHtcbiAgICAgIG5hbWU6ICd0b2tlbicsXG4gICAgICBuYW1lc3BhY2VDb25maWc6IHRva2Vuc0NvbmZpZ1xuICAgIH0sIHtcbiAgICAgIG5hbWU6ICdhZG1pbicsXG4gICAgICBuYW1lc3BhY2VDb25maWc6IGFkbWluQ29uZmlnXG4gICAgfSxcbiAgXTtcblxuICBjb25zdCBjb250cmFjdHNBQkkgPSBbe1xuICAgIG5hbWU6IFwiR2FzTWFuYWdlclwiLFxuICAgIGRhdGE6IFsuLi5HYXNNYW5hZ2VyLCAuLi5HYXNNYW5hZ2VyUHNldWRvRXZlbnRzXVxuICB9LCB7XG4gICAgbmFtZTogXCJFUkMyMFwiLFxuICAgIGRhdGE6IEVSQzIwLFxuICB9LCB7XG4gICAgbmFtZTogXCJHTk9TSVNcIixcbiAgICBkYXRhOiBHTk9TSVNcbiAgfV07XG5cbiAgcmV0dXJuIGNvbmZpZ3MubWFwKChjZmcpID0+IHtcbiAgICByZXR1cm4gT2JqZWN0LmFzc2lnbihjZmcsIHtcbiAgICAgIGNvbnRyYWN0czogbG9hZEdlbmVyaWNDb250cmFjdFNjaGVtZShjZmcubmFtZXNwYWNlQ29uZmlnLCBpc0FkZHJlc3MsIGNoYWluSWQsIGNvbnRyYWN0c0FCSSlcbiAgICB9KTtcbiAgfSk7XG59XG5cbi8qKlxuICogVGhpcyB3aWxsIHdyYXAgbXVsdGkgc2lnIGBzdWJtaXRUcmFuc2FjdGlvbmAgZm9yIHRhcmdldCBjb250cmFjdCBsaWtlIFVTRFxuICogVGhpcyB3aWxsIHByZWZpeCBhbGwgbWV0aG9kcyB0aGF0IGNvbnRyYWN0IGFkbWlucyB0cmFuc2Zvcm1pbmcgdGhlIGZpcnN0XG4gKiBsZXR0ZXIgaW4gdXBwZXJjYXNlIGFuZCBhZGQgYSBwcmVmaXggd2l0aCBgcHJveHlgIHdvcmQuXG4gKiBVc2luZyBgd2ViMy5wbS5jb250cmFjdHMuYWRtaW4ucHJveHlEZXBvc2l0KDxkZXBvc2l0QXJncz4pYFxuICogQGFjY2VzcyBwcml2YXRlXG4gKiBAdGhyb3dzIC0gQWxsIHdlYjMgZXJyb3JzIGFuZCBleGNlcHRpb25zXG4gKiBAcGFyYW0geyp9IGNvbnRyYWN0cyBhbGwgY3JlYXRlZCBjb250cmFjdHMgYmVmb3JlXG4gKiBAcGFyYW0geyp9IGFkbWluc1RvIHByb3BlcnR5IGluc2lkZSBjdXJyZW50IGNvbnRyYWN0XG4gKiBAcGFyYW0geyp9IGN1cnJlbnRDb250cmFjdCBjdXJyZW50IGNvbnRyYWN0IG1ldGhvZHNcbiAqIEByZXR1cm4ge0FycmF5LjxPYmplY3Q+fVxuICogQGV4YW1wbGUgYHJldHVybnMgW3sgZGVwb3NpdDogW0Z1bmN0aW9uOiBkZXBvc2l0XSB9IF1gXG4gKi9cbmNvbnN0IHJlc29sdmVBZG1pbk1ldGhvZHMgPSAoY29udHJhY3RzLCB7XG4gIGFkbWluc1RvXG59LCBjdXJyZW50Q29udHJhY3QpID0+IHtcbiAgaWYgKGFkbWluc1RvKSB7XG4gICAgY29uc3QgbGlzdE9mTWV0aG9kc1Jlc29sdmVkID0gZmxhdHRlbihcbiAgICAgIGFkbWluc1RvLm1hcChhZG1pbkNvbnRyYWN0ID0+IHtcbiAgICAgICAgY29uc3QgYWRtaW5NZXRob2RzID0gZ2V0KGNvbnRyYWN0cywgYWRtaW5Db250cmFjdC50YXJnZXQsICdudWxsJyk7XG4gICAgICAgIHJldHVybiBPYmplY3Qua2V5cyhhZG1pbk1ldGhvZHMpXG4gICAgICAgICAgLmZpbHRlcihrZXkgPT4gYWRtaW5Db250cmFjdC5wcm94eU1ldGhvZHMuaW5jbHVkZXMoa2V5KSlcbiAgICAgICAgICAubWFwKG1ldGhvZEtleSA9PiB7XG4gICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICBbYHByb3h5JHt1cHBlckZpcnN0KG1ldGhvZEtleSl9YF06ICguLi5hcmdzKSA9PiB7XG4gICAgICAgICAgICAgICAgY29uc3QgdGFyZ2V0ID0gYWRtaW5NZXRob2RzLm9wdGlvbnMuYWRkcmVzcztcbiAgICAgICAgICAgICAgICBsZXQgZW5jb2RlZERhdGEgPSBudWxsO1xuICAgICAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgICBlbmNvZGVkRGF0YSA9IGFkbWluTWV0aG9kc1ttZXRob2RLZXldKC4uLmFyZ3MpLmVuY29kZUFCSSgpO1xuICAgICAgICAgICAgICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICAgICAgICAgICAgICByZXR1cm4gZXJyb3I7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHJldHVybiBjdXJyZW50Q29udHJhY3Quc3VibWl0VHJhbnNhY3Rpb24odGFyZ2V0LCAwLCBlbmNvZGVkRGF0YSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH07XG4gICAgICAgICAgfSk7XG4gICAgICB9KVxuICAgICk7XG5cbiAgICByZXR1cm4gcmVkdWNlKGxpc3RPZk1ldGhvZHNSZXNvbHZlZCwgKG1ldGhvZHMsIGN1cnJlbnQpID0+IHtcbiAgICAgIHJldHVybiBPYmplY3QuYXNzaWduKG1ldGhvZHMsIGN1cnJlbnQpO1xuICAgIH0pO1xuICB9XG4gIHJldHVybiBudWxsO1xufTtcblxuXG4vKipcbiAqIFNob3VsZCBjcmVhdGUgYSBpbnRlcmZhY2Ugb2YgY29udHJhY3RzIGluIGNvbmZpZ3VyYXRpb24gZmlsZSxcbiAqIHBhc3NpbmcgdGhlIG1ldGhvZHMgZGlyZWN0bHkgdG8gcm9vdCAod2UgY2FuIHVzZSB3ZWIzIHN0eWxlIHVzaW5nIHByZWZpeCBvZiBgbWV0aG9kc2ApIG9mIG9iamVjdCB3aXRoIG5hbWUgZ2l2ZW4uXG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHsqfSB3ZWIzaW4gd2ViMyBpbnN0YW5jZVxuICogQHBhcmFtIHsqfSBjaGFpbklkIHNlbGVjdGVkIGNoYWluIGlkXG4gKiBAcGFyYW0geyp9IG9wdHMgY3VzdG9tIGNvbnRyYWN0IG9wdGlvbnNcbiAqL1xuY29uc3QgY3JlYXRlQ29udHJhY3RzSW50ZXJmYWNlID0gKHdlYjNpbiwgY2hhaW5JZCwgb3B0cyA9IHt9KSA9PiB7XG4gIGNvbnN0IHtcbiAgICBpc0FkZHJlc3NcbiAgfSA9IHdlYjNpbi51dGlscztcblxuICBjb25zdCBuYW1lc3BhY2VDb25maWdzID0gbG9hZENvbmZpZ3MoaXNBZGRyZXNzLCBjaGFpbklkKTtcbiAgY29uc3QgY29udHJhY3RzID0ge307XG4gIC8vIGxvb3AgYWxsIG5hbWVzcGFjZXMgY29uZmlnc1xuICBuYW1lc3BhY2VDb25maWdzLmZvckVhY2gobmFtZXNwYWNlQ29uZmlnID0+IHtcbiAgICAvLyBsb2FkIGFsbCBjb250cmFjdHMgZm9yIHBhcmVudCBuYW1lc3BhY2VcbiAgICByZXR1cm4gbmFtZXNwYWNlQ29uZmlnLmNvbnRyYWN0cy5mb3JFYWNoKGNvID0+IHtcblxuICAgICAgY29uc3QgQ29udGV4dENvbnRyYWN0ID0gQmFzZUNvbnRyYWN0LmJpbmQod2ViM2luLmV0aClcbiAgICAgIEJhc2VDb250cmFjdC5zZXRQcm92aWRlcih3ZWIzaW4uY3VycmVudFByb3ZpZGVyLCB3ZWIzaW4uZXRoLmFjY291bnRzKTtcblxuICAgICAgY29uc3QgaW5zdGFuY2UgPSBuZXcgQ29udGV4dENvbnRyYWN0KGNvLmFiaSwgY28uYWRkcmVzcywgb3B0cyk7XG4gICAgICBsZXQge1xuICAgICAgICBtZXRob2RzXG4gICAgICB9ID0gaW5zdGFuY2U7XG5cbiAgICAgIC8vIGxvYWQgcHJveHkgYWRtaW4gbWV0aG9kcyBmb3IgbXVsdGlTaWcgd2FsbGV0c1xuICAgICAgY29uc3QgYWRtaW5NZXRob2RzID0gcmVzb2x2ZUFkbWluTWV0aG9kcyhjb250cmFjdHMsIGNvLCBtZXRob2RzKTtcblxuICAgICAgaWYgKCFpc0VtcHR5KGFkbWluTWV0aG9kcykpIHtcbiAgICAgICAgbWV0aG9kcyA9IE9iamVjdC5hc3NpZ24obWV0aG9kcywgYWRtaW5NZXRob2RzKTtcbiAgICAgIH1cblxuICAgICAgY29uc3QgY29udHJhY3RzTmFtZXNwYWNlID0gY29udHJhY3RzW25hbWVzcGFjZUNvbmZpZy5uYW1lXTtcbiAgICAgIGNvbnN0IGNvbnRyYWN0c05hbWVzcGFjZVZhbHVlID0gIWNvbnRyYWN0c05hbWVzcGFjZSA/IHt9IDogY29udHJhY3RzTmFtZXNwYWNlO1xuXG4gICAgICBjb250cmFjdHNbbmFtZXNwYWNlQ29uZmlnLm5hbWVdID0gT2JqZWN0LmFzc2lnbihjb250cmFjdHNOYW1lc3BhY2VWYWx1ZSwge1xuICAgICAgICBbY28ubmFtZV06IHtcbiAgICAgICAgICAuLi5pbnN0YW5jZSxcbiAgICAgICAgICAuLi5tZXRob2RzXG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH0pO1xuICB9KTtcbiAgcmV0dXJuIGNvbnRyYWN0cztcbn07XG5cbmV4cG9ydCBkZWZhdWx0IGNyZWF0ZUNvbnRyYWN0c0ludGVyZmFjZTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQU9BOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUdBOztBQUNBOztBQUNBOztBQUNBOztBQUdBOztBQUNBOztBQUNBOztBQUNBOzs7Ozs7Ozs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTQSx1QkFBVCxDQUFpQ0MsT0FBakMsRUFBMEM7RUFDeEMsS0FBS0EsT0FBTCxHQUFlQSxPQUFmO0VBQ0EsS0FBS0MsSUFBTCxHQUFZLHlCQUFaO0FBQ0Q7QUFFRDtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EsU0FBU0MsbUJBQVQsQ0FBNkJGLE9BQTdCLEVBQXNDO0VBQ3BDLEtBQUtBLE9BQUwsR0FBZUEsT0FBZjtFQUNBLEtBQUtDLElBQUwsR0FBWSxxQkFBWjtBQUNEO0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EsSUFBTUUseUJBQXlCLEdBQUcsQ0FBQ0MsY0FBRCxFQUFpQkMsU0FBakIsRUFBNEJDLE9BQTVCLEVBQXFDQyxPQUFyQyxLQUFpRDtFQUNqRixPQUFPSCxjQUFjLENBQUNJLEdBQWYsQ0FBbUJDLE1BQU0sSUFBSTtJQUNsQyxJQUFJQyxhQUFKOztJQUNBLElBQUlELE1BQU0sQ0FBQ0UsVUFBUCxLQUFzQixTQUExQixFQUFxQztNQUNuQ0QsYUFBYSxHQUFHRCxNQUFNLENBQUNHLFVBQVAsQ0FBa0IsQ0FBbEIsRUFBcUJDLE9BQXJDO0lBQ0QsQ0FGRCxNQUVPO01BQ0xILGFBQWEsR0FBR0QsTUFBTSxDQUFDRyxVQUFQLENBQWtCRSxNQUFsQixDQUF5QkMsR0FBRyxJQUFJQSxHQUFHLENBQUNULE9BQUosS0FBZ0JBLE9BQWhELEVBQXlELENBQXpELEVBQTRETyxPQUE1RTtJQUNEOztJQUVELElBQUksQ0FBQ0gsYUFBRCxJQUFrQixDQUFDTCxTQUFTLENBQUNLLGFBQUQsQ0FBaEMsRUFDRSxNQUFNLElBQUlYLHVCQUFKLENBQTRCLGdEQUE1QixDQUFOO0lBRUYsSUFBSWlCLEdBQUo7O0lBRUEsSUFBSUMsS0FBSyxDQUFDQyxPQUFOLENBQWNULE1BQU0sQ0FBQ08sR0FBckIsQ0FBSixFQUErQjtNQUM3QkEsR0FBRyxHQUFHUCxNQUFNLENBQUNPLEdBQWI7SUFDRCxDQUZELE1BRU87TUFDTEEsR0FBRyxHQUFHLElBQUFHLGFBQUEsRUFBS1osT0FBTCxFQUFlUyxHQUFELElBQVM7UUFDM0IsT0FBT0EsR0FBRyxDQUFDZixJQUFKLEtBQWFRLE1BQU0sQ0FBQ08sR0FBM0I7TUFDRCxDQUZLLENBQU47TUFJQUEsR0FBRyxHQUFHQSxHQUFHLEdBQUdBLEdBQUcsQ0FBQ0ksSUFBUCxHQUFjLElBQXZCO0lBQ0Q7O0lBRUQsSUFBSSxDQUFDSixHQUFMLEVBQVU7TUFDUixNQUFNLElBQUlkLG1CQUFKLENBQXdCLG1DQUF4QixDQUFOO0lBQ0Q7O0lBRUQsT0FBT21CLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjYixNQUFkLEVBQXNCO01BQzNCSSxPQUFPLEVBQUVILGFBRGtCO01BRTNCTSxHQUFHLEVBQUVBO0lBRnNCLENBQXRCLENBQVA7RUFJRCxDQS9CTSxDQUFQO0FBZ0NELENBakNEO0FBa0NBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBLElBQU1PLFdBQVcsR0FBRyxDQUFDbEIsU0FBRCxFQUFZQyxPQUFaLEtBQXdCO0VBQzFDO0VBQ0EsSUFBTWtCLE9BQU8sR0FBRyxDQUFDO0lBQ2J2QixJQUFJLEVBQUUsWUFETztJQUVid0IsZUFBZSxFQUFFQztFQUZKLENBQUQsRUFJZDtJQUNFekIsSUFBSSxFQUFFLE9BRFI7SUFFRXdCLGVBQWUsRUFBRUU7RUFGbkIsQ0FKYyxFQU9YO0lBQ0QxQixJQUFJLEVBQUUsT0FETDtJQUVEd0IsZUFBZSxFQUFFRztFQUZoQixDQVBXLENBQWhCO0VBYUEsSUFBTUMsWUFBWSxHQUFHLENBQUM7SUFDcEI1QixJQUFJLEVBQUUsWUFEYztJQUVwQm1CLElBQUksRUFBRSxDQUFDLEdBQUdVLG1CQUFKLEVBQWdCLEdBQUdDLCtCQUFuQjtFQUZjLENBQUQsRUFHbEI7SUFDRDlCLElBQUksRUFBRSxPQURMO0lBRURtQixJQUFJLEVBQUVZO0VBRkwsQ0FIa0IsRUFNbEI7SUFDRC9CLElBQUksRUFBRSxRQURMO0lBRURtQixJQUFJLEVBQUVhO0VBRkwsQ0FOa0IsQ0FBckI7RUFXQSxPQUFPVCxPQUFPLENBQUNoQixHQUFSLENBQWEwQixHQUFELElBQVM7SUFDMUIsT0FBT2IsTUFBTSxDQUFDQyxNQUFQLENBQWNZLEdBQWQsRUFBbUI7TUFDeEJDLFNBQVMsRUFBRWhDLHlCQUF5QixDQUFDK0IsR0FBRyxDQUFDVCxlQUFMLEVBQXNCcEIsU0FBdEIsRUFBaUNDLE9BQWpDLEVBQTBDdUIsWUFBMUM7SUFEWixDQUFuQixDQUFQO0VBR0QsQ0FKTSxDQUFQO0FBS0QsQ0EvQkQ7QUFpQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBLElBQU1PLG1CQUFtQixHQUFHLENBQUNELFNBQUQsUUFFekJFLGVBRnlCLEtBRUw7RUFBQSxJQUZpQjtJQUN0Q0M7RUFEc0MsQ0FFakI7O0VBQ3JCLElBQUlBLFFBQUosRUFBYztJQUNaLElBQU1DLHFCQUFxQixHQUFHLElBQUFDLGdCQUFBLEVBQzVCRixRQUFRLENBQUM5QixHQUFULENBQWFpQyxhQUFhLElBQUk7TUFDNUIsSUFBTUMsWUFBWSxHQUFHLElBQUFDLFlBQUEsRUFBSVIsU0FBSixFQUFlTSxhQUFhLENBQUNHLE1BQTdCLEVBQXFDLE1BQXJDLENBQXJCO01BQ0EsT0FBT3ZCLE1BQU0sQ0FBQ3dCLElBQVAsQ0FBWUgsWUFBWixFQUNKNUIsTUFESSxDQUNHZ0MsR0FBRyxJQUFJTCxhQUFhLENBQUNNLFlBQWQsQ0FBMkJDLFFBQTNCLENBQW9DRixHQUFwQyxDQURWLEVBRUp0QyxHQUZJLENBRUF5QyxTQUFTLElBQUk7UUFDaEIsT0FBTztVQUNMLGdCQUFTLElBQUFDLG1CQUFBLEVBQVdELFNBQVgsQ0FBVCxJQUFtQyxpQkFBYTtZQUM5QyxJQUFNTCxNQUFNLEdBQUdGLFlBQVksQ0FBQ1MsT0FBYixDQUFxQnRDLE9BQXBDO1lBQ0EsSUFBSXVDLFdBQVcsR0FBRyxJQUFsQjs7WUFDQSxJQUFJO2NBQ0ZBLFdBQVcsR0FBR1YsWUFBWSxDQUFDTyxTQUFELENBQVosQ0FBd0IsWUFBeEIsRUFBaUNJLFNBQWpDLEVBQWQ7WUFDRCxDQUZELENBRUUsT0FBT0MsS0FBUCxFQUFjO2NBQ2QsT0FBT0EsS0FBUDtZQUNEOztZQUNELE9BQU9qQixlQUFlLENBQUNrQixpQkFBaEIsQ0FBa0NYLE1BQWxDLEVBQTBDLENBQTFDLEVBQTZDUSxXQUE3QyxDQUFQO1VBQ0Q7UUFWSSxDQUFQO01BWUQsQ0FmSSxDQUFQO0lBZ0JELENBbEJELENBRDRCLENBQTlCO0lBc0JBLE9BQU8sSUFBQUksZUFBQSxFQUFPakIscUJBQVAsRUFBOEIsQ0FBQ2tCLE9BQUQsRUFBVUMsT0FBVixLQUFzQjtNQUN6RCxPQUFPckMsTUFBTSxDQUFDQyxNQUFQLENBQWNtQyxPQUFkLEVBQXVCQyxPQUF2QixDQUFQO0lBQ0QsQ0FGTSxDQUFQO0VBR0Q7O0VBQ0QsT0FBTyxJQUFQO0FBQ0QsQ0EvQkQ7QUFrQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EsSUFBTUMsd0JBQXdCLEdBQUcsU0FBM0JBLHdCQUEyQixDQUFDQyxNQUFELEVBQVN0RCxPQUFULEVBQWdDO0VBQUEsSUFBZHVELElBQWMsdUVBQVAsRUFBTztFQUMvRCxJQUFNO0lBQ0p4RDtFQURJLElBRUZ1RCxNQUFNLENBQUNFLEtBRlg7RUFJQSxJQUFNQyxnQkFBZ0IsR0FBR3hDLFdBQVcsQ0FBQ2xCLFNBQUQsRUFBWUMsT0FBWixDQUFwQztFQUNBLElBQU02QixTQUFTLEdBQUcsRUFBbEIsQ0FOK0QsQ0FPL0Q7O0VBQ0E0QixnQkFBZ0IsQ0FBQ0MsT0FBakIsQ0FBeUJ2QyxlQUFlLElBQUk7SUFDMUM7SUFDQSxPQUFPQSxlQUFlLENBQUNVLFNBQWhCLENBQTBCNkIsT0FBMUIsQ0FBa0NDLEVBQUUsSUFBSTtNQUU3QyxJQUFNQyxlQUFlLEdBQUdDLGNBQUEsQ0FBYUMsSUFBYixDQUFrQlIsTUFBTSxDQUFDUyxHQUF6QixDQUF4Qjs7TUFDQUYsY0FBQSxDQUFhRyxXQUFiLENBQXlCVixNQUFNLENBQUNXLGVBQWhDLEVBQWlEWCxNQUFNLENBQUNTLEdBQVAsQ0FBV0csUUFBNUQ7O01BRUEsSUFBTUMsUUFBUSxHQUFHLElBQUlQLGVBQUosQ0FBb0JELEVBQUUsQ0FBQ2pELEdBQXZCLEVBQTRCaUQsRUFBRSxDQUFDcEQsT0FBL0IsRUFBd0NnRCxJQUF4QyxDQUFqQjtNQUNBLElBQUk7UUFDRko7TUFERSxJQUVBZ0IsUUFGSixDQU42QyxDQVU3Qzs7TUFDQSxJQUFNL0IsWUFBWSxHQUFHTixtQkFBbUIsQ0FBQ0QsU0FBRCxFQUFZOEIsRUFBWixFQUFnQlIsT0FBaEIsQ0FBeEM7O01BRUEsSUFBSSxDQUFDLElBQUFpQixnQkFBQSxFQUFRaEMsWUFBUixDQUFMLEVBQTRCO1FBQzFCZSxPQUFPLEdBQUdwQyxNQUFNLENBQUNDLE1BQVAsQ0FBY21DLE9BQWQsRUFBdUJmLFlBQXZCLENBQVY7TUFDRDs7TUFFRCxJQUFNaUMsa0JBQWtCLEdBQUd4QyxTQUFTLENBQUNWLGVBQWUsQ0FBQ3hCLElBQWpCLENBQXBDO01BQ0EsSUFBTTJFLHVCQUF1QixHQUFHLENBQUNELGtCQUFELEdBQXNCLEVBQXRCLEdBQTJCQSxrQkFBM0Q7TUFFQXhDLFNBQVMsQ0FBQ1YsZUFBZSxDQUFDeEIsSUFBakIsQ0FBVCxHQUFrQ29CLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjc0QsdUJBQWQsRUFBdUM7UUFDdkUsQ0FBQ1gsRUFBRSxDQUFDaEUsSUFBSixtQ0FDS3dFLFFBREwsR0FFS2hCLE9BRkw7TUFEdUUsQ0FBdkMsQ0FBbEM7SUFNRCxDQTFCTSxDQUFQO0VBMkJELENBN0JEO0VBOEJBLE9BQU90QixTQUFQO0FBQ0QsQ0F2Q0Q7O2VBeUNld0Isd0IifQ==