# PublicMint - javascript devKit

Create umbrella class of web3 with pm namespace excluding modules not available for client.

- Adds new namespace inside web3 `web3.pm` [PublicMint]{@link PublicMint}
- This module depends of web3 package readme more [here](https://web3js.readthedocs.io/en/v1.2.4/index.html) 
