## Methods in accounts

This methods will be functional when the private keys is added to [pm wallet]{@tutorial AddWalletKeys}

### Wallet accounts

```js
const getAccountsRes = pm.wallet.accounts.getAccounts;
// [
//     '0x4389Af2E0515dDFe3453B1bD748aDfD5e2598cFd',
//     '0xd2FA48924906e00069E81aaf778c4f586b5CA58B'
// ]
```

### Transfer

```js
const transferReceipt = await web3.pm.wallet.accounts.transfer(destinyAccount, toToken(10))

```

### Transfer from x wallet account

- Transfer from default account from wallet

```js

const transferReceipt = await web3.pm.wallet.accounts.transferFrom(1, destinyAccount, toToken(10))

```

### Get all wallet balances

```js
 const myBalances = await web3.pm.wallet.accounts.getBalances;
 /*
    {
        '0x4389Af2E0515dDFe3453B1bD748aDfD5e2598cFd': '0',
        '0xd2FA48924906e00069E81aaf778c4f586b5CA58B': '0'
    }
 */

```

### Get balance of x wallet

Always will return the small unit of blockchain, if you want convert to USD dollars need to use [conversion]{@tutorial Conversions} utils

```js
 const myBalance = await web3.pm.wallet.accounts.getBalance("address in wallet");
 /*
    1000
 */
```
