## Multi signature contracts

> This is only for PublicMint team use.

### Requirements

```js
const web3 = new PublicMint()
const adminUSD = web3.pm.contracts.admin.USD


const {
    fromToken,
    toToken,
    sha256
} = web3.pm.utils


```

### Proxy Methods

Abstract of encoding abi for multi signature methods, prefix of method is < proxy + method name >.

### Deposit for ERC20

Method for deposit for ERC20

```js
const recipientAccount = '0xe1650dD7C9Aef65264341d2c16f0C9D3348723bb'

const refFromApi = sha256(`{id: ${+new Date()}, desc: "Just a example of deposit ref api for " + ${recipientAccount} }`)

const receiptDeposit = await adminUSD.proxyDeposit(recipientAccount, toToken(1), refFromApi).send({
    from: 'owner multi signature contract'
})

const txId = receiptDeposit.events.Submission.returnValues.transactionId

// for each owner need to confirm to deposit tokens 
await adminUSD.confirmTransaction(txId).send({
    from: 'owner multi signature contract'
})

```

Same as bellow, encoding the abi and submitTransaction to approvement of other contract owners.

```js
const { USD } = web3.pm.contracts.token

const recipientAccount = '0xd2FA48924906e00069E81aaf778c4f586b5CA58B'

const refFromApi = sha256(`{id: ${+new Date()}, desc: "Just a example of deposit ref api for " + ${recipientAccount} }`)

const usdDepositData = USD.deposit(recipientAccount, toToken(1), refFromApi).encodeABI();

const receiptDeposit = await adminUSD.submitTransaction(USD.options.address, 0, usdDepositData).send({
    from: 'owner multi signature contract'
})

const txId = receiptDeposit.events.Submission.returnValues.transactionId

await adminUSD.confirmTransaction(txId).send({
    from: 'owner multi signature contract'
})

```

## USDC

### Deposit for ERC20 USDC

Deposit USDC should emit a event of 'Transfer' from standard ERC20 and
respective event for `Deposit(account, amount, REF in bytes32, USDC);`

```js
await adminUSD.proxyDepositFromUSDC(recipientAccount, toToken(1), refFromApi);
```

### Parameters for USDC

```js
// Set max pool amount that users can withdrawal each time
// Default value is 50000 in 10^18 scale
await adminUSD.proxySetUSDCmaxWithdrawAllowed("uint256 max");

/**
* Set max pool percentage that users can withdrawal each time
* Receives a parameter `perc` a uint256 value representing the percentage in 10^18 scale
* Default value is 50% percentage of the pool <=> 0.5 in 10^18 scale <=> 500000000000000000
*/
await adminUSD.proxySetUSDCmaxPoolPercentage("uint256 perc");

```
