# USD

Using USD contract interface this is based on ERC-20 Token Standard, for transfer tokens
[see this interface]{@tutorial AccountMethods}

> IMPORTANT: Add wallet keys [see more]{@tutorial AddWalletKeys}

### Requirements

```js
const {
    toToken, sha256
} =  web3.pm.utils;

const {
 USD
} = web3.pm.contracts.token

```

### balanceOf

- See balance of account (this not need wallet private keys)

```js
const myBalance = await USD.balanceOf('<address here>').call();
```

> Prefer using [web3.pm.wallet.accounts.getBalance]{@link Accounts} or [web3.eth.getBalance](https://web3js.readthedocs.io/en/v1.2.4/web3-eth.html#getbalance) for performance reasons

### Withdrawals

The interface for withdrawals is equal for parameters in all (excluding withdrawal using USDC coin) types of withdrawals methods, these parameters are:

- 1º Amount for withdrawal (**uint256**), passing the value in the smallest unit of Ethereum **Wei** and is preferred pass as **string** format, *eg: '1000000000000000000' this is equivalent to 1 USD or 1 Ether.*

- 2º Reference from api, (**string**) a reference for tracking by the servers of PublicMint.

> !!! The PublicMint api will give a correct value for parameter `referenceFromApi` to withdrawal tokens, don't generate by your self or your tokens can be will be lost !!!
> Easy and safe way consider to use of withdrawals in official PublicMint web wallet.

#### Withdraw Wire international

- Withdraw for wire International method

```js
// withdraw 1 USD this is eq to '1000000000000000000' tokens
const tokensToWithdraw = toToken('1');

// API from Public Mint will provide a reference for each withdraw for your account
const referenceFromApi = '98cb5a14-ae4a-4456-8428-70a4c6ee68fe';

// Methods are available via web3 object withdrawWireInt or hash USD['0x5af05b16'] or string signature USD['withdrawWireInt(uint256,string)']

// withdrawWireInt(uint256,string) - hash 0x5af05b16
await USD.withdrawWireInt(tokensToWithdraw, referenceFromApi).send({
    from: "<Wallet address here>",
});
```

#### Withdraw Wire US

- Withdraw for Wire US method

```js
// withdrawWireUS(uint256,string)  - hash 0x97e4f20f
await USD.withdrawWireUS(tokensToWithdraw, referenceFromApi).send({
    from: "<Wallet address here>",
});
```

#### Withdraw ACH

- Withdraw for ACH method

```js
// withdrawAchUS(uint256,string)   - hash 0x497715a9
await USD.withdrawAchUS(tokensToWithdraw, referenceFromApi).send({
    from: "<Wallet address here>",
});
```

[See more]{@tutorial Withdrawals} of Withdrawal costs

#### Withdraw USDC

- Withdraw for USDC method require permission, it's possible check status using `USD.checkPermissionsWithdrawalToUSDC(<account address>).call();` returns a boolean `True` is valid to withdrawal and False will be rejected.

- Withdraw for USDC method

Params:

- tokensToWithdraw : amount of tokens to withdraw
- targetAccount : target account in string format. Exceptionally to Stellar, the `addressTag` must also be given after a `:` separator.
- chainId : is the integer chain id, check the following table for supported chain ids:

    |Chain name| Chain Id  |
    |----------|-----------|
    |Ethereum  |          1|
    |Algorand  |          2|
    |Avalanche |          3|
    |Solana    |          4|
    |TRON      |          5|
    |Stellar   |          6|
    |Matic     |          7|

This should emit a event with this parameters for withdraw to USDC.

```js
// Ethereum chain example
const targetAccount = '0xDF<...>69';
await USD.withdrawToUSDC(tokensToWithdraw, targetAccount, 1).send({
    from: "<Wallet address here>"
});

// Stellar chain example
const targetAccount = 'GA<..>4A:10<...>37';
await USD.withdrawToUSDC(tokensToWithdraw, targetAccount, 6).send({
    from: "<Wallet address here>"
});

```
