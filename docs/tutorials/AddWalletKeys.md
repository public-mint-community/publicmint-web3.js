# Wallet

Add one or multiple accounts, this module provide access to automatically sign transactions for all accounts methods and contracts.

- [PM Wallet documentation]{@link pm.wallet}
- [Web3 doc](https://web3js.readthedocs.io/en/v1.3.1/web3-eth-accounts.html#wallet)

# Example

```js

const web3 = new PublicMint()

const {
 wallet
} = web3.pm

// Add first account

// 0x4389Af2E0515dDFe3453B1bD748aDfD5e2598cFd
const walletAccountPrivateKey1 = "0xba6cdfcc795484a9774eb98e756983da822ef3481b37d4ba649864e2d1ab4e5e";
wallet.add(walletAccountPrivateKey1);


// Add Second account

// 0xd2FA48924906e00069E81aaf778c4f586b5CA58B
const walletAccountPrivateKey2 = '0xccae15162e106a9fe8b05e5bf6d758041daf1d8f25edae61f2ba392a774183c8'
wallet.add(walletAccountPrivateKey2);


```

# Related methods

- getBalance
- getBalances
- transfer
- transferFrom
- [HDwallet]{@tutorial HDwallet}
