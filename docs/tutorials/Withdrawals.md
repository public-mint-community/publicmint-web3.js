## Withdrawals

!!! NOTE !!!
This is just an example of value, for get the actual costs you need to invoke each existing method with `estimateCost()`.

### Example for ERC20.USD

```js
 await USD.withdrawWireInt(value, ref).estimateCosts();

// output
/*
 {
    gas: '67676',
    gasPrice: '2000000000000',
    additionalFee: '100000000000000',
    totalCost: '100000000000000',
    gasManagerFee: '-135252000000000000',
    maybeReverts: false,
    minerFee: '135352000000000000'
 }
*/

```

```js
 await USD.withdrawAchUS(value, ref).estimateCosts();

// output
/*
{
  gas: '26043',
  additionalFee: '5000000000000000000',
  ...
}
*/

```

```js
 await USD.withdrawWireUS(value, ref).estimateCosts();

// output
/*
{ gas: '26130',
  additionalFee: '20000000000000000000',
  ...
}
*/

```

[See more of estimateGas]{@tutorial EstimateGas}
