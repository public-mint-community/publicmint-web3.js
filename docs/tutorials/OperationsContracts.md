# Operations contracts

> This is only for PublicMint team use.

### Requirements

```js
const web3 = new PublicMint()

const {
 GasManager
} = web3.pm.contracts.operations

```

### Example

```js

const usdGasPrice = await GasManager.getGasPrice("USD").call();
// 20000000000

```

### Info

For multiSig interface and proxy methods see [Admin module]{@tutorial AdminContracts}
