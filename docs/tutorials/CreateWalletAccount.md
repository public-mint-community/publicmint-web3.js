# Wallet

Create one or multiple accounts, this module provide access to automatically sign transactions for all accounts methods and contracts.

- [PM Wallet documentation]{@link pm.wallet}

Add one or multiple accounts, this module provide access to automatically sign transactions for all accounts methods and contracts.

- [PM Wallet documentation]{@link pm.wallet}
- [Same as Web3 wallet.create doc](https://web3js.readthedocs.io/en/v1.3.1/web3-eth-accounts.html#wallet)

# Example

```js
const web3 = new PublicMint()

const {
 wallet
} = web3.pm
// Create first account

wallet.create(1, '54674321§3456764321§345674321§3453647544±±±§±±±!!!43534534534534');

```

# Related methods

- getBalance
- getBalances
- transfer
- transferFrom
