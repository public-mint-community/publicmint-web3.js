## Estimate Gas and Costs

### Example for ERC20.USD

All smart contracts calls that change state will cost gas, is the case for methods with [.send()](https://web3js.readthedocs.io/en/v1.3.1/web3-eth-contract.html#methods-mymethod-send).

In view methods is used [.call()](https://web3js.readthedocs.io/en/v1.3.1/web3-eth-contract.html#methods-mymethod-call), this methods not create any transaction is just for reading state of smart contract.

For methods .send() we can calculate the cost in tokens of transaction calling myMethod.estimateGas [estimateGas](https://web3js.readthedocs.io/en/v1.3.1/web3-eth-contract.html#methods-mymethod-estimategas) and [eth.gasPrice()](https://web3js.readthedocs.io/en/v1.3.1/web3-eth.html#getgasprice), the total cost in tokens is:

- Transaction cost = gasPrice * estimatedGas.
  
This is just a estimate, the real cost can oscillate values at 10% of final the value.

Example:

```js
const gasPrice = await web3.eth.getGasPrice();
const gas = await contractInstance.myMethod.estimateGas();

console.log("gas price is = " + gasPrice + " wei");
console.log("gas estimation = " + gas + " units");
console.log("gas cost estimation = " + (gas * gasPrice) + " wei");
console.log("gas cost estimation = " + utils.fromWei((gas * gasPrice), 'ether') + " ether");

/*
example: 
    gas price is 20000000000 wei
    gas estimation = 26794 units
    gas cost estimation = 535880000000000 wei
    gas cost estimation = 0.00053588 ether
*/
```

#### Estimate gas vs Estimate costs

- EstimateGas
   Is present for all calls that change state and only give the computation cost "Gas".

- EstimateCosts
   Only used for ERC20 methods of PublicMint, is a custom function not present in web3 lib that give all info about withdrawals cost. (If you use original web3 it's possible to retrieve this value by query `estimateCosts` method in contract of USD).
   AdditionalFee is the (miner fee + withdrawal cost) that depends of withdrawal type by amount.

Example:

```js
/**
     Return Object 
    {
        Optional<message>:(Return on failure) UTF8 decoded message
        Optional<data>: (Return on failure) hex encoded string or null if not exist
        maybeReverts: boolean true if transaction may fail otherwise is false
        Optional<gas>: estimateGas for current method or null if fails
        gasPrice: current blockchain gasPrice
        Optional<minerFee>: (estimateGas * gasPrice <miner profit> ) or null if fails
        gasManagerFee: additionalFee - minerFee 
        additionalFee: additional cost is the result from a contract query - amount value
        totalCost: AdditionalFee is the total amount
    }
*/

await USD.withdrawWireInt(value, ref).estimateCosts();

  // output in case of revert
  /*
    {
      gas: null,
      gasPrice: '2000000000000',
      additionalFee: '50000000000000000000',
      totalCost: '50000000000000000000',
      data: null,
      maybeReverts: true,
      message: 'Returned error: Upfront cost exceeds account balance',
      minerFee: null
    }
  */

  /*  output in case of transaction succeeds
    {
        gas: '65505',
        gasPrice: '2000000000000',
        additionalFee: '50000000000000000000',
        totalCost: '50000000000000000000',
        gasManagerFee: '49868990000000000000',
        maybeReverts: false,
        minerFee: '131010000000000000'
    }
  */

```

[See more]{@tutorial Withdrawals} of Withdrawal costs
