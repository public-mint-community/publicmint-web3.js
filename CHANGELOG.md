### Changelog

All notable changes to this project will be documented in this file. Dates are displayed in UTC.

#### [v3.1.0](https://gitlab.com/public-mint-community/publicmint-web3.js/compare/v3.0.0...v3.1.0)

- Release candidate 3.1.0 [`#61`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/61)
- Release beta/v3.1.0 beta [`#59`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/59)
- fix comment about withdrawals available [`#57`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/57)

#### [v3.0.0](https://gitlab.com/public-mint-community/publicmint-web3.js/compare/v2.2.1-beta...v3.0.0)

> 3 August 2022

- Release candidate v3.0.0 [`#56`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/56)
- Remove deprecated methods of withdrawals with bytes32 [`#55`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/55)
- update examples [`9f2341d`](https://gitlab.com/public-mint-community/publicmint-web3.js/commit/9f2341dd2f0e4f6a9b5d30313377a47c848da60d)
- update doc [`49f7867`](https://gitlab.com/public-mint-community/publicmint-web3.js/commit/49f7867319fc1f755fdb4f69bf1c44e69bf350fe)
- build dist [`6dee711`](https://gitlab.com/public-mint-community/publicmint-web3.js/commit/6dee711da839b254e1752d30b6a00174630f07a2)

#### [v2.2.1-beta](https://gitlab.com/public-mint-community/publicmint-web3.js/compare/v2.2.0...v2.2.1-beta)

> 1 August 2022

- Release candidate/2.2.1 beta [`#54`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/54)
- Move babel dependencies to dev and update [`#53`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/53)
- Update withdraw ref type string with new ABI [`#52`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/52)
- Release 2.2.1-beta [`927eebe`](https://gitlab.com/public-mint-community/publicmint-web3.js/commit/927eebe41ec473c3ff000b0d7c7b01e1ffaa0b39)
- Fix gitlab-ci publish pages [`62b3561`](https://gitlab.com/public-mint-community/publicmint-web3.js/commit/62b3561274964701dbb01e7c5848b910ac1426aa)
- Fix gitlab-ci publish pages [`0c80b8a`](https://gitlab.com/public-mint-community/publicmint-web3.js/commit/0c80b8a7c37203c18ce016549369a7d4de5e835c)

#### [v2.2.0](https://gitlab.com/public-mint-community/publicmint-web3.js/compare/v2.1.1-beta.5...v2.2.0)

> 13 July 2022

- Bump/version 2.2.0 [`#50`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/50)
- remove unused command [`#51`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/51)
- Release 2.2.0 [`cb254f8`](https://gitlab.com/public-mint-community/publicmint-web3.js/commit/cb254f85c5720e6937ea32f196961c10d9ab0737)
- Release 2.2.0 [`bd464f2`](https://gitlab.com/public-mint-community/publicmint-web3.js/commit/bd464f21f73c5f45b14fde71a18eaa0d54b30ca0)
- Fix gitlab-ci validation package [`f56cd22`](https://gitlab.com/public-mint-community/publicmint-web3.js/commit/f56cd22c79566faa7308426dd0c5698efe35eff3)

#### [v2.1.1-beta.5](https://gitlab.com/public-mint-community/publicmint-web3.js/compare/v2.1.1-beta.3...v2.1.1-beta.5)

> 13 July 2022

- Merge beta branch [`#49`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/49)
- Update CI/CD [`#42`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/42)
- Bump/version 2.1.0 [`#33`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/33)
- update deps [`#32`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/32)
- Release 2.1.1-beta.5 [`7a8a9bb`](https://gitlab.com/public-mint-community/publicmint-web3.js/commit/7a8a9bb1019cfddeaf5fa63df3894e0818130aec)
- Update dist [`330f71d`](https://gitlab.com/public-mint-community/publicmint-web3.js/commit/330f71df0d7ae91f77e3b264f845a639d3c842f7)
- Fix gitlab-ci validation package [`0c5efe6`](https://gitlab.com/public-mint-community/publicmint-web3.js/commit/0c5efe65e03bd479c2ea6fda7f51c4a431aafe05)

#### [v2.1.1-beta.3](https://gitlab.com/public-mint-community/publicmint-web3.js/compare/v2.1.0...v2.1.1-beta.3)

> 12 May 2021

- v2.1.0 [`#34`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/34)
- version 2.0.2 [`#27`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/27)
- update doc [`7790ed0`](https://gitlab.com/public-mint-community/publicmint-web3.js/commit/7790ed0744a224ea6cd54905888847ed73964563)
- remove old doc [`ab862d4`](https://gitlab.com/public-mint-community/publicmint-web3.js/commit/ab862d463b4e5b0a6568713e40f081bb703ec5ba)
- remove all dev deps for build [`c787049`](https://gitlab.com/public-mint-community/publicmint-web3.js/commit/c78704983f5af8cb758f4b49cb3ef9244c704d30)

#### [v2.1.0](https://gitlab.com/public-mint-community/publicmint-web3.js/compare/2.0.3...v2.1.0)

> 28 April 2021

- update deps [`#32`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/32)
- update doc [`ce3fcbf`](https://gitlab.com/public-mint-community/publicmint-web3.js/commit/ce3fcbf66fc4563bcded90ed3724cf55bb9a3697)
- remove old doc [`941c8aa`](https://gitlab.com/public-mint-community/publicmint-web3.js/commit/941c8aa22570b557514c4962848d976d770a6189)
- update dist [`70074c5`](https://gitlab.com/public-mint-community/publicmint-web3.js/commit/70074c577cf91a376e8a03909fbab72cac00040b)

#### [2.0.3](https://gitlab.com/public-mint-community/publicmint-web3.js/compare/2.0.2...2.0.3)

> 13 January 2021

- Bump/npm version 2.0.3 [`#30`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/30)
- Fix/doc [`#29`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/29)
- Bump/web3 [`#28`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/28)
- Bump/version 2.0.2 [`#26`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/26)
- fix wrong link to documentation [`#25`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/25)
- Bump/version 2.0.1 [`#23`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/23)
- gas as intrinsic gas 25k [`#22`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/22)

#### [2.0.2](https://gitlab.com/public-mint-community/publicmint-web3.js/compare/2.0.1...2.0.2)

> 16 April 2020

- version 2.0.2 [`#27`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/27)
- V2.0.1 [`#24`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/24)
- Develop [`#18`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/18)

#### [2.0.1](https://gitlab.com/public-mint-community/publicmint-web3.js/compare/2.0.0...2.0.1)

> 20 March 2020

- Bump/version 2.0.1 [`#23`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/23)
- gas as intrinsic gas 25k [`#22`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/22)

### [2.0.0](https://gitlab.com/public-mint-community/publicmint-web3.js/compare/1.0.3...2.0.0)

> 19 March 2020

- Bump/version 2.0.0 [`#20`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/20)
- Update/last abi [`#19`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/19)
- Bump/version 1.0.3 [`#17`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/17)
- Change/localnet ports [`#16`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/16)
- Change/pb 218 estimate cost calculation [`#15`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/15)
- add master-docs to push pages on gitlab [`#14`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/14)
- update changelog with unreleased version [`#13`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/13)
- Fix/doc withdrawal [`#11`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/11)
- give better withdrawal tutorial [`#10`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/10)
- fix old doc [`#8`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/8)
- Bump/npm version 1.0.2 [`#6`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/6)
- add babel config [`#5`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/5)
- remove webpack build from dist folder [`#4`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/4)
- Fix/gitlab ci only on master [`#3`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/3)
- Update README.md [`#2`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/2)
- rebuild doc [`#1`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/1)
- gitlab-ci pages only on master [`b1d4972`](https://gitlab.com/public-mint-community/publicmint-web3.js/commit/b1d4972792a4214883e39a6d9f5a883670535438)

#### [1.0.3](https://gitlab.com/public-mint-community/publicmint-web3.js/compare/1.0.2...1.0.3)

> 12 March 2020

- Develop [`#18`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/18)
- Develop [`#9`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/9)

#### 1.0.2

> 21 February 2020

- Develop [`#7`](https://gitlab.com/public-mint-community/publicmint-web3.js/merge_requests/7)
- Initial commit &lt;code from private repo&gt; [`21104a9`](https://gitlab.com/public-mint-community/publicmint-web3.js/commit/21104a9ba32cd121bfa212b77ae4aca911498fe1)
