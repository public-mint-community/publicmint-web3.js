/* eslint-disable func-names */
const {
  server
} = require('ganache-cli');
const extract = require('extract-zip');
const rimraf = require('rimraf');
const del = require('del');
const isFunction = require('lodash/isFunction');
const pathExists = require('path-exists');
var portUsed = require('port-used');
var promiseRetry = require('promise-retry');

module.exports = network => {
  return new Promise((resolve, reject) => {
    const source = `${__dirname}/ganacheDB.zip`;
    const fullPathDatabase = `${__dirname}/ganacheDB/`;
    let opts = {
      port: '7545',
      mnemonic: 'cook blur blur frown food potato again grain unfold level stage connect',
      total_accounts: 5,
      network_id: 5777,
      // db: fullPathDatabase,
      db_path: fullPathDatabase,
      defaultBalanceEther: '100',
      verbose: true,
      asyncRequestProcessing: true
    };

    if (network !== null) {
      opts = Object.assign(opts, {
        fork: network
      });
    }

    del([fullPathDatabase]).then(function () {
      extract(
        source, {
          dir: __dirname
        },
        function (err) {
          if (err) {
            reject(err);
          }

          promiseRetry(function (retry, number) {
            console.log('\n Attempt start server number', number);
            const folderCheck = pathExists(fullPathDatabase);
            const portCheck = portUsed.check(7545, '127.0.0.1');
            return Promise.all([folderCheck, portCheck])
              .then(([folderC, portC]) => {
                if (!folderC || portC) {
                  retry();
                }
                return [folderC, portC];
              })
              .catch(retry);
          }).then(
            function ([fCheck, pCheck]) {
              if (pCheck) {
                reject(new Error('Port in use by fake blockchain.'));
              }
              if (!fCheck) {
                reject(new Error('Database folder not exist'));
              }

              const ganacheServer = server(opts);

              ganacheServer.listen(opts.port, function (serverError) {
                if (serverError) {
                  reject(serverError);
                }
              });

              ganacheServer.exit = function (jestDone) {
                return new Promise(function (resolveExit, rejectExit) {
                  ganacheServer.close();
                  del(fullPathDatabase)
                    .then(() => {
                      if (isFunction(jestDone)) {
                        jestDone();
                        resolveExit();
                      }
                    })
                    .catch(error => {
                      rejectExit(error);
                    });
                })
              };

              resolve(ganacheServer);
            },
            function (retryError) {
              console.log('Retry start server: ', retryError);
            }
          );
        }
      );
    });

  });
};

/*
https://docs.nethereum.com/en/latest/ethereum-and-clients/ganache-cli/

Available Accounts
==================
(0) 0x4389Af2E0515dDFe3453B1bD748aDfD5e2598cFd (100000 ETH)
(1) 0xd2FA48924906e00069E81aaf778c4f586b5CA58B (100000 ETH)
(2) 0x2A679DAd127493eFF9d4845AaE4CeA4eD21Df5f7 (100000 ETH)
(3) 0xC9aaAE02Bd72220A70B915d80B3852d44C25fd59 (100000 ETH)
(4) 0x7911C48065b2d609A442f76aBeDe04BBd0F60c7A (100000 ETH)
(5) 0x22a7933d1a679EC86Ec29B3402C0436A7B298Af4 (100000 ETH)
(6) 0x66d490e3b00e60CaC4a6970857Bb5416570Fc8b8 (100000 ETH)
(7) 0x969505677bEB350e5B14cdcf35b55A28599B672e (100000 ETH)
(8) 0x8fb21A713D7F5758DfD582342dd36a202932AcFC (100000 ETH)
(9) 0x4F34d18AdDfCc36a4c7272b22AD5F99225B652b1 (100000 ETH)

Private Keys
==================
(0) 0xba6cdfcc795484a9774eb98e756983da822ef3481b37d4ba649864e2d1ab4e5e
(1) 0xccae15162e106a9fe8b05e5bf6d758041daf1d8f25edae61f2ba392a774183c8
(2) 0x6b9ab45c35845c8e9352648f460449d00d08f6fbb74a7222f5d642a1c75609f8
(3) 0xe8bde2809819ee1ade39f139385f90faf2fe1e3387f044edb54fbba7e1b919a7
(4) 0x68f3dd3c589d0044b6c1c1c76e63da999b9b1a4b56367f88e1a9332562427e64
(5) 0xcd5ad013b41e187772e495c14fea454cdba2d8c9e3ac94feca49e85971da94ba
(6) 0xff88effac4678b2771344c3d0288706330dacb922d44c00c8ca87ecee56d3f93
(7) 0x77dc2be5afbdbb6837421a3217e2fb925ad739cc190ae2a6c09be8b0456a6811
(8) 0x7993a4c542ed6747631bfdcca77a1fb8c51d755afa0cce52d0b9274f63e6e107
(9) 0x82a7a281cc3d8563203f69983c6ea8b6a12e9279c67c51ad46241ba59a0b6f14

HD Wallet
==================
Mnemonic:      cook blur blur frown food potato again grain unfold level stage connect
Base HD Path:  m/44'/60'/0'/0/{account_index}

Gas Price
==================
20000000000

Gas Limit
==================
6721975
 */
