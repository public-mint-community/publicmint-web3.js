const fakeBlockchain = require('./fakeBlockchain')

fakeBlockchain().then((blockchain) => {
    console.log("\n  blockchain", blockchain)
}).catch(err => console.log(err));
