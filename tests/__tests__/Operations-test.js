const PublicMint = require('../../src/index.js').default;

describe('Contract namespace', () => {
    let web3;
    beforeAll(() => {
        web3 = new PublicMint('testNet');
        return web3;
    });

    it('Should have contracts property', () => {
        expect(web3).toHaveProperty("pm.contracts.operations");
        expect(web3).toHaveProperty("pm.contracts.operations.GasManager");
    });

});
