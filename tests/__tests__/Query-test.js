const PublicMint = require('../../src/index.js').default;

describe('Testing query utils', () => {
    let web3;
    let utils;
    let query;
    let USD;

    beforeAll(async () => {
        // TODO: make this test via ganache-cli 
        web3 = new PublicMint();
        utils = web3.pm.utils;
        query = web3.pm.query;
        USD = web3.pm.contracts.token.USD;
    });

    it('Should throw InvalidArgumentException for invalid transaction hash', async () => {
        const hash = '0x75399824572df192c684765802a85f408f8b8ae1f9939371efecb0267e915ZZZ';
        try {
            await query.getWithdrawalTypeByContract(USD, hash);
        } catch (error) {
            expect(error).toHaveProperty('name', 'InvalidArgumentException');
            expect(error).toHaveProperty('message', 'Invalid transaction hash');
        }
    })

    it('Should throw InvalidArgumentException for invalid address token', async () => {
        const hash = '0x75399824572df192c684765802a85f408f8b8ae1f9939371efecb0267e915b32';
        const address = '0x0';
        try {
            await query.getWithdrawalTypeByContract(address, hash);
        } catch (error) {
            expect(error).toHaveProperty('name', 'InvalidArgumentException');
            expect(error).toHaveProperty('message', 'Invalid contract address');
        }
    })

    it('Should throw InvalidArgumentException for invalid address token', async () => {
        const hash = '0x41d3a3ad559e9c80ed46a281e0af38c489884ed8e6aa18a98287f956eda082cc';
        try {
            await query.getWithdrawalTypeByContract(USD, hash);
        } catch (error) {
            expect(error).toHaveProperty('name', 'TransactionException');
            expect(error).toHaveProperty('message', 'Transaction not found');
        }
    })

    // it('Should get correct withdrawal Type', async () => {
    //     const hash = '0x75399824572df192c684765802a85f408f8b8ae1f9939371efecb0267e915b32';
    //     expect(await query.getWithdrawalTypeByContract(USD, hash)).toEqual('withdrawWireUS')
    // })
});
