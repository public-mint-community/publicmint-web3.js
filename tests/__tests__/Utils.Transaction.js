const PublicMint = require('../../src/index.js').default;

describe('Testing utils for transactions', () => {
    let web3;
    let utils;
    beforeAll(async () => {
        web3 = new PublicMint();
        // eslint-disable-next-line prefer-destructuring
        utils = web3.pm.utils;
    });

    it('Should get true value when is a transaction hash', () => {
        const {
            isTransactionHash
        } = utils;

        const validHash = '0x75399824572df192c684765802a85f408f8b8ae1f9939371efecb0267e915b32';
        expect(isTransactionHash(validHash)).toBeTruthy();
    })

    it('Should get false value when is not a transaction hash', () => {
        const {
            isTransactionHash
        } = utils;

        const validHash = '0x75399824572df192c684765802a85f408f8b8ae1f9939371efecb0267e915ZZZ';
        expect(isTransactionHash(validHash)).not.toBeTruthy();
    })
});
