const PublicMint = require('../../dist/index.js').default;

const mockProviders = require("../__mocks__/Providers");

const findMockProvider = (providerName) => {
    return mockProviders.filter(({
        name
    }) => name === providerName)[0]
}

describe("Testing providers", () => {

    it('Should require as optional parameter a correct provider', () => {
        const chainIDNotValid = 7777;
        expect(() => {
            // eslint-disable-next-line no-new
            new PublicMint(chainIDNotValid);
        }).toThrow()
    });

    it('Should have main network provider as default', () => {
        const web3 = new PublicMint();
        const {
            name
        } = web3.pm.provider;
        expect(name).toEqual('mainNet')
    });

    it('Should have all networks providers given the name', () => {
        const names = ["mainNet", "testNet", "devNet", "localNet", "ganache"]

        for (let i = 0; i < names.length; i += 1) {
            const networkName = names[i];
            const providerSetting = new PublicMint(networkName).pm.provider;
            expect(providerSetting).toMatchObject(findMockProvider(providerSetting.name))
        }
    });

    it('Should have all networks providers by numeric id', () => {
        const ids = [2020, 2019, 2018, 9999, 5777]

        for (let i = 0; i < ids.length; i += 1) {
            const id = ids[i];
            const providerSetting = new PublicMint(id).pm.provider;
            expect(providerSetting).toMatchObject(findMockProvider(providerSetting.name))
        }
    });

    it('Add custom provider', () => {
        const customProvider = PublicMint.provider(5, "CustomNetwork", "http://127.0.0.1:7545/", "ws://127.0.0.1:7545/", "http://127.0.0.1/");
        const providerSetting = new PublicMint(customProvider).pm.provider;
        expect(providerSetting).toMatchObject(customProvider);
    });

})
