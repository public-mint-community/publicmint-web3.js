const PublicMint = require('../../src/index.js').default;
const mockAccounts = require('../utils/Accounts');

describe('Wallet', () => {
    let web3;
    let pm;
    const mnemonic = 'cook blur blur frown food potato again grain unfold level stage connect';

    beforeEach((done) => {
        try {
            web3 = new PublicMint(2018, 'http');
            pm = web3.pm;
        } catch (error) {
            console.error('Error', error)
        }
        pm.wallet.hdwallet.setDefaultSeedWordlist('english');
        done()
    });

    it('Should have importFromSeed function', () => {
        expect(pm.wallet).toEqual(
            expect.objectContaining({
                hdwallet: expect.any(Object),
                importFromSeed: expect.any(Function),
            })
        );
    });

    it('Should load 2 accounts from seed', () => {
        const privateKeys = pm.wallet.hdwallet.load(mnemonic, 0, 2);
        const expectedPrivKeys = mockAccounts.map(acc => acc.privateKey);
        const expectedPubKeys = mockAccounts.map(acc => acc.address);
        expect(pm.wallet.hdwallet.seed).toEqual(mnemonic);
        expect(privateKeys).toEqual(expectedPrivKeys);

        expect(
            web3.pm.wallet.accounts.getAccounts
        ).toEqual(expectedPubKeys)
    });

    it('Should import 2 accounts from seed directly to wallet without saving the seed', () => {
        const privateKeys = pm.wallet.importFromSeed(mnemonic, 0, 2);
        const expectedPrivKeys = mockAccounts.map(acc => acc.privateKey);
        const expectedPubKeys = mockAccounts.map(acc => acc.address);

        expect(privateKeys).toEqual(expectedPrivKeys);

        expect(
            web3.pm.wallet.accounts.getAccounts
        ).toEqual(expectedPubKeys)
    });

    it('Should create seed and save only once', () => {
        const hd = pm.wallet.hdwallet.create();
        expect(hd.seed).toEqual(pm.wallet.hdwallet.seed);
        
        const seed1 = hd.create().seed;
        expect(hd.seed).toEqual(seed1);
         
        const seed2 = hd.create().seed;
        expect(hd.seed).toEqual(seed2);
    });

    it('Should create seed and save with default of one account', () => {
        pm.wallet.hdwallet.create();
        const accounts = web3.pm.wallet.accounts.getAccounts
        expect(accounts).toHaveLength(1);
    });

    it('Should create 5 accounts from the seed and save', () => {
        pm.wallet.hdwallet.create(0, 5);
        const accounts = web3.pm.wallet.accounts.getAccounts
        expect(accounts).toHaveLength(5);
    });

    it('Should create seed and save', () => {
        const hd = pm.wallet.hdwallet.create();
        expect(hd.seed).toEqual(pm.wallet.hdwallet.seed);
        
        const seed = hd.create().seed;
        expect(hd.seed).toEqual(seed);

        const createdSeed = hd.seed;

        expect(typeof createdSeed == 'string').toBe(true);
        expect(createdSeed.split(' ')).toHaveLength(12);
    });

    it('Should generate seed and import with old method `importFromSeed`', () => {
        const seed = pm.wallet.hdwallet.generateSeed();
        const privateKeys = pm.wallet.importFromSeed(seed, 0, 10);
        const accounts = web3.pm.wallet.accounts.getAccounts;
        expect(privateKeys).toHaveLength(10);
        expect(accounts).toHaveLength(10);
    });

    it('Should validate seed when import with old method `importFromSeed`', () => {
        const mnemonic = 'serio padrone braccio idillio estonia camerata elevare agente calcolo crinale saldatura peccato';
        const falseMnemonic = pm.wallet.hdwallet.validateSeed(mnemonic);
        expect(falseMnemonic).toBeFalsy();

        pm.wallet.hdwallet.setDefaultSeedWordlist('italian')
        const defaultWl = pm.wallet.hdwallet.getDefaultSeedWordlist()
        expect(defaultWl).toBe('italian');
        
        const trueMnemonic = pm.wallet.hdwallet.validateSeed(mnemonic);
        expect(trueMnemonic).toBeTruthy();

        const privateKeys = pm.wallet.importFromSeed(mnemonic, 0, 10);
        const accounts = web3.pm.wallet.accounts.getAccounts;
        expect(privateKeys).toHaveLength(10);
        expect(accounts).toHaveLength(10);

        const mnemonicInvalid = 'serio padrone braccio idillio estonia camerata elevare agente calcolo crinale saldatura INVALID';
        expect(() => pm.wallet.importFromSeed(mnemonicInvalid)).toThrowError(new Error('Invalid seed'))
    });

    it('Should get default word list', () => {
        const defaultWl = pm.wallet.hdwallet.getDefaultSeedWordlist()
        expect(defaultWl).toBe('english');
    });

    it('Should retrieve all available words', () => {
        const availableWl = [
            'czech',
            'chinese_simplified',
            'chinese_traditional',
            'korean',
            'french',
            'italian',
            'spanish',
            'japanese',
            'JA',
            'portuguese',
            'english',
            'EN'
        ];
        expect(pm.wallet.hdwallet.getAvailableSeedWordList()).toMatchObject(availableWl);
    })

    it('Should set default word list', () => {
        let defaultWl = pm.wallet.hdwallet.getDefaultSeedWordlist()
        expect(defaultWl).toBe('english');
        
        pm.wallet.hdwallet.setDefaultSeedWordlist('japanese')
        defaultWl = pm.wallet.hdwallet.getDefaultSeedWordlist()
        expect(defaultWl).toBe('japanese');
        
        const seed = pm.wallet.hdwallet.generateSeed();
        expect(seed).not.toMatch(/[a-z]/)
    });

    it('Should validate seed', () => {
        const trueMnemonic = pm.wallet.hdwallet.validateSeed(mnemonic);
        const falseMnemonic = pm.wallet.hdwallet.validateSeed('inmate knee hurdle island');
        expect(trueMnemonic).toBeTruthy();
        expect(falseMnemonic).toBeFalsy();
    });

    it('Should validate seed setting a wordlist', () => {
        const mnemonic = 'serio padrone braccio idillio estonia camerata elevare agente calcolo crinale saldatura peccato';
        const falseMnemonic = pm.wallet.hdwallet.validateSeed(mnemonic);
        expect(falseMnemonic).toBeFalsy();

        pm.wallet.hdwallet.setDefaultSeedWordlist('italian')
        const defaultWl = pm.wallet.hdwallet.getDefaultSeedWordlist()
        expect(defaultWl).toBe('italian');
        
        const trueMnemonic = pm.wallet.hdwallet.validateSeed(mnemonic);
        expect(trueMnemonic).toBeTruthy();
    });

});
