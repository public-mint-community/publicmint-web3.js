/* eslint-disable func-names */
const PublicMint = require('../../src/index.js').default;
const mockAccounts = require('../utils/Accounts');
const fakeBlockchain = require('../utils/FakeBlockchain');

describe('Wallet', () => {
    let web3;
    let pm;
    let USD;
    let adminUSD;
    let accounts;
    let blockchain;
    let BigNumber;
    const [account1, account2] = mockAccounts;

    beforeAll(done => {
        return fakeBlockchain().then(function (bc) {
            blockchain = bc;
            accounts = Object.keys(blockchain.provider.manager.state.accounts);

            web3 = new PublicMint(blockchain.ganacheProvider.options.network_id, 'http');
            BigNumber = web3.utils.BN;
            // eslint-disable-next-line prefer-destructuring
            pm = web3.pm;

            // change gnosis address and USD token
            pm.contracts.token.USD.options.address = '0x1c316f7B7ABf394c1090B1295bA6B9a371e5914b';
            pm.contracts.admin.USD.options.address = '0xE1778Ba6B526138598457e2ec847885dC14DdA6e';
            // eslint-disable-next-line prefer-destructuring
            USD = pm.contracts.token.USD;
            adminUSD = pm.contracts.admin.USD;

            done();
        });
    }, 30000);

    afterAll((done) => {
        return blockchain.exit(done);
    }, 30000);

    it('Should have wallet', () => {
        expect(pm).toHaveProperty('wallet');
    });

    it('Should have wallet properties', () => {
        expect(pm.wallet).toEqual(
            expect.objectContaining({
                add: expect.any(Function),
                remove: expect.any(Function),
                load: expect.any(Function),
                clear: expect.any(Function)
            })
        );
    });

    it('Should add wallet address', () => {
        const account = pm.wallet.add(account1.privateKey);
        expect(account).toEqual(expect.objectContaining(account1));
    });

    it('Should add second wallet address', () => {
        const account = pm.wallet.add(account2.privateKey);
        expect(account).toEqual(expect.objectContaining(account2));
    });

    it('Should get accounts list', () => {
        const getAccountsRes = pm.wallet.accounts.getAccounts;
        expect(getAccountsRes).toEqual(mockAccounts.map(acc => acc.address));
    });

    it('Should have getAccounts', async () => {
        const myWalletAccounts = web3.pm.wallet.accounts.getAccounts;
        expect(myWalletAccounts).toEqual([
            '0x4389Af2E0515dDFe3453B1bD748aDfD5e2598cFd',
            '0xd2FA48924906e00069E81aaf778c4f586b5CA58B'
        ]);
    });

    it('Should have accounts getBalances', async () => {
        const myBalances = await web3.pm.wallet.accounts.getBalances;
        // balances from state of initial database
        expect(myBalances).not.toEqual({
            '0x4389Af2E0515dDFe3453B1bD748aDfD5e2598cFd': '0',
            '0xd2FA48924906e00069E81aaf778c4f586b5CA58B': '0'
        });
    });

    it('Should transfer from default account from wallet ', async () => {
        const account = accounts[3];
        const accountBalance = await web3.eth.getBalance(account);
        const {
            status,
            from
        } = await web3.pm.wallet.accounts.transfer(account, 100);

        expect(status).toBeTruthy();
        expect(from.toLocaleLowerCase()).toEqual(account1.address.toLocaleLowerCase());

        const accountBalanceAfter = await web3.eth.getBalance(account);

        expect(accountBalanceAfter.toString()).toEqual(
            new BigNumber(accountBalance).add(new BigNumber('100')).toString()
        );
    });

    it('Should transferFrom choosing account from wallet ', async () => {
        const account = accounts[3];
        const accountBalance = await web3.eth.getBalance(account);
        const {
            status,
            from
        } = await web3.pm.wallet.accounts.transferFrom(1, account, 100);

        expect(status).toBeTruthy();
        expect(from.toLocaleLowerCase()).toEqual(account2.address.toLocaleLowerCase());

        const accountBalanceAfter = await web3.eth.getBalance(account);

        expect(accountBalanceAfter.toString()).toEqual(
            new BigNumber(accountBalance).add(new BigNumber('100')).toString()
        );
    });
});
