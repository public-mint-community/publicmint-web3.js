const PublicMint = require('../../src/index.js').default;

describe('USD namespace should have withdrawal methods', () => {
    let web3;
    beforeAll(() => {
        web3 = new PublicMint('testNet');
        return web3;
    });

    it('Should have withdraw methods', () => {
        const {
            USD
        } = web3.pm.contracts.token;

        const withdrawalInterfaces = {
            "withdrawWireUS": expect.any(Function),
            "withdrawWireInt": expect.any(Function),
            "withdrawAchUS": expect.any(Function),
            "withdrawToUSDC": expect.any(Function),
            "withdrawWireInt(uint256,string)": expect.any(Function),
            "0x5af05b16": expect.any(Function),
            "withdrawWireUS(uint256,string)": expect.any(Function),
            "0x97e4f20f": expect.any(Function),
            "withdrawAchUS(uint256,string)": expect.any(Function),
            "0x497715a9": expect.any(Function),
        }

        expect(USD).toMatchObject(withdrawalInterfaces)
        // compatible with web3 docs
        expect(USD.methods).toMatchObject(withdrawalInterfaces)
    });

});
