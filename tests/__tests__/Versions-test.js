const PublicMint = require('../../src/index.js').default;
const {
  version,
  dependencies: {
    web3
  }
} = require('../../package.json');

test('PublicMint class has the correct version of package', () => {
  expect(PublicMint.versions()).toMatchObject({
    Web3: web3,
    PublicMint: version
  });
});
