const PublicMint = require('../../src/index.js').default;

describe('Testing utils', () => {
    let web3;
    let utils;
    beforeAll(async () => {
        web3 = new PublicMint();
        // eslint-disable-next-line prefer-destructuring
        utils = web3.pm.utils;
        // utils = PublicMint.utils; same as above
    });


    it('Should convert to token unit from USD', () => {
        const {
            toToken
        } = utils
        expect(toToken('1')).toEqual('1000000000000000000')
    });


    it('Should convert from token unit to USD', () => {
        const {
            fromToken
        } = utils
        expect(fromToken('1000000000000000000')).toEqual('1')
    });
});
