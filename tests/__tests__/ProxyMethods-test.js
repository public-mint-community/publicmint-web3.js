/* eslint-disable func-names */
const PublicMint = require('../../src/index.js').default;

describe('USD', () => {
    let web3;
    let pm;
    let USD;
    let GasManager;
    let adminUSD;
    let adminGasManager;

    beforeAll(() => {
        web3 = new PublicMint(2018)
        pm = web3.pm;

        adminUSD = web3.pm.contracts.admin.USD
        USD = web3.pm.contracts.token.USD

        adminGasManager = web3.pm.contracts.admin.GasManager
        GasManager = web3.pm.contracts.operations.GasManager
        
        USD = web3.pm.contracts.token.USD
    })

    it('Testing proxy methods for USD', async () => {
        const methodsUSD = Object.keys(USD.methods);
        const proxiesAdminUSD =
            Object.keys(adminUSD.methods)
            .filter((methodName) => {
                return (methodName.startsWith("proxy"))
            });

        const proxies = proxiesAdminUSD.filter((methodName) => {
            let methodNameWithoutProxyKeyword = unCapitalizeFirstLetter(clearProxyName(methodName))
            return methodsUSD.some((name) => name === methodNameWithoutProxyKeyword)
        })
        expect(proxiesAdminUSD.length === proxies.length).toBeTruthy;
    });

    it('Testing proxy methods for GasManager', async () => {
        const methodsGasManager = Object.keys(GasManager.methods);
        const proxiesAdminGasManager =
            Object.keys(adminGasManager.methods)
            .filter((methodName) => {
                return (methodName.startsWith("proxy"))
            });

        const proxies = proxiesAdminGasManager.filter((methodName) => {
            let methodNameWithoutProxyKeyword = unCapitalizeFirstLetter(clearProxyName(methodName))
            return methodsGasManager.some((name) => name === methodNameWithoutProxyKeyword)
        })
        expect(proxiesAdminGasManager.length === proxies.length).toBeTruthy;
    });

    function clearProxyName(methodName) {
        return methodName.replace("proxy", '');
    }

    function unCapitalizeFirstLetter(string) {
        return string.charAt(0).toLowerCase() + string.slice(1);
    }

});
