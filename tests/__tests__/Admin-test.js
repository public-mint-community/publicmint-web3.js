/* eslint-disable func-names */
const PublicMint = require('../../src/index.js').default;
const mockAccounts = require('../utils/Accounts')
const fakeBlockchain = require('../utils/FakeBlockchain')


describe('USD', () => {
    let web3;
    let pm;
    let USD;
    let adminUSD;
    let accounts;
    let blockchain;
    let toToken;
    const [
        account1,
        account2
    ] = mockAccounts;

    beforeAll(done => {
        return fakeBlockchain().then((bc) => {
            blockchain = bc;
            accounts = Object.keys(blockchain.provider.manager.state.accounts);

            web3 = new PublicMint(blockchain.ganacheProvider.options.network_id, 'http');
            // eslint-disable-next-line prefer-destructuring
            pm = web3.pm;
            // eslint-disable-next-line prefer-destructuring
            toToken = pm.utils.toToken

            // change gnosis address and USD token
            pm.contracts.token.USD.options.address = '0x288dCcF9e6A7Cc571409688388652363E02f16AB'
            pm.contracts.admin.USD.options.address = '0xE1778Ba6B526138598457e2ec847885dC14DdA6e'

            // eslint-disable-next-line prefer-destructuring
            USD = pm.contracts.token.USD;
            adminUSD = pm.contracts.admin.USD

            pm.wallet.add(account1.privateKey)
            pm.wallet.add(account2.privateKey)
            done();
        });
    }, 30000);

    afterAll((done) => {
        return blockchain.exit(done)
    }, 30000);

    it('Should have accounts initial balances', async () => {
        const myBalances = await web3.pm.wallet.accounts.getBalances;
        // balances from state of initial database
        expect(myBalances).toEqual({
            '0x4389Af2E0515dDFe3453B1bD748aDfD5e2598cFd': '99999669306880000000000',
            '0xd2FA48924906e00069E81aaf778c4f586b5CA58B': '99999995308340000000000'
        })
    });

    it('Deposit via proxy methods', async () => {
        const oneDollar = toToken("1")
        const recipient = accounts[3];
        const refInBytes32 = web3.utils.sha3('REF API PUBLIC MINT')
        const submitTx1 = await adminUSD.proxyDeposit(recipient, oneDollar, refInBytes32).send({
            from: '0x4389Af2E0515dDFe3453B1bD748aDfD5e2598cFd',
            gas: 700000
        });
        expect(submitTx1.status).toBeTruthy()

        const txId = submitTx1.events.Submission.returnValues.transactionId
        const submitTx2 = await adminUSD.confirmTransaction(txId).send({
            from: '0xd2FA48924906e00069E81aaf778c4f586b5CA58B',
            gas: 700000
        });
        expect(submitTx2.status).toBeTruthy()

    }, 30000);

});
