const PublicMint = require('../../src/index.js').default;

describe('Contract USD', () => {
    let web3;
    beforeAll(() => {
        web3 = new PublicMint('testNet');
        return web3;
    });

    it('Should have token property', () => {
        expect(web3).toHaveProperty("pm.contracts.token");
    });

    it('Should have USD contract instance', () => {
        const {
            USD
        } = web3.pm.contracts.token;
        expect(() => USD).toBeInstanceOf(Object)
        expect(USD.options.address).toEqual('0x0000000000000000000000000000000000002070')
    });

});
